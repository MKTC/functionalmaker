package nl.makertim.fm;

import nl.makertim.fm.safe.SafeTry;

import java.util.Properties;

public class FunctionalMaker {

	private static final Properties properties;
	public static final String version;
	public static final String releaseDate;
	public static boolean useBugsnag;

	static {
		properties = new Properties();
		SafeTry.execute(() ->
			properties.load(FunctionalMaker.class.getClassLoader()
				.getResourceAsStream("functionalmaker.properties"))
		);
		version = properties.getProperty("version");
		releaseDate = properties.getProperty("release");
		useBugsnag = properties.getProperty("bugsnag").equalsIgnoreCase("true");
	}

	public static Object overrideProperty(String key, String value) {
		return properties.setProperty(key, value);
	}

	public static String getProperty(String key) {
		return properties.getProperty(key);
	}
}
