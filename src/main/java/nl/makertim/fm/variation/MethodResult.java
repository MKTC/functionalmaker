package nl.makertim.fm.variation;

public enum MethodResult {

	SUCCEEDS, FAILS;

	public static MethodResult reduce(MethodResult a, MethodResult b) {
		if (a == FAILS || b == FAILS) return FAILS;
		return SUCCEEDS;
	}
}
