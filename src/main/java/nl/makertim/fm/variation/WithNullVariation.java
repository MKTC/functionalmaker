package nl.makertim.fm.variation;

import java.util.Arrays;

public class WithNullVariation<T> implements Variation<T> {

	private final Variation<T> variation;
	private final MethodResult result;

	public WithNullVariation(MethodResult result, Variation<T> variation) {
		this.result = result;
		this.variation = variation;
	}

	@Override
	public Class<T> getVariationType() {
		return variation.getVariationType();
	}

	@Override
	@SuppressWarnings("unchecked")
	public VariationSet<T>[] getVariations() {
		return VariationTester
			.addToNewList(
				Arrays.asList(variation.getVariations()),
				new VariationSet<>("null value", null, result))
			.toArray(VariationSet[]::new);
	}
}
