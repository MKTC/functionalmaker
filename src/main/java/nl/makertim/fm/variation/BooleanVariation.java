package nl.makertim.fm.variation;

import java.util.function.Function;

public class BooleanVariation extends BoolVariation {

	public BooleanVariation() {
		super();
	}

	public BooleanVariation(Function<Boolean, MethodResult> resultFunction) {
		super(resultFunction);
	}

	@Override
	public Class<Boolean> getVariationType() {
		return boolean.class;
	}
}
