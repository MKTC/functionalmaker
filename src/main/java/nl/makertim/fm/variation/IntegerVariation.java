package nl.makertim.fm.variation;

public class IntegerVariation implements Variation<Integer> {

	private final int min;
	private final int mid;
	private final int max;

	public IntegerVariation() {
		this(0, 10);
	}

	public IntegerVariation(int min, int max) {
		this.min = min;
		this.mid = (max - min) / 2 + min;
		this.max = max;
	}

	@Override
	@SuppressWarnings("unchecked")
	public VariationSet<Integer>[] getVariations() {
		return new VariationSet[] {
			new VariationSet<>("integer minimum(" + min + ")", min, MethodResult.SUCCEEDS),
			new VariationSet<>("integer maximum(" + max + ")", max, MethodResult.SUCCEEDS),
			new VariationSet<>("integer median(" + mid + ")", mid, MethodResult.SUCCEEDS),
			new VariationSet<>("integer below minimum(" + (min - 1) + ")", min - 1, MethodResult.FAILS),
			new VariationSet<>("integer above maximum(" + (max + 1) + ")", max + 1, MethodResult.FAILS)
		};
	}

	@Override
	public Class<Integer> getVariationType() {
		return Integer.class;
	}
}
