package nl.makertim.fm.variation;

import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;

public class DateVariation implements Variation<java.util.Date> {

	public static final long A_DAY = (24 * 60 * 60 * 1000);
	public static final long A_WEEK = (A_DAY * 7);

	private final long min;
	private final long mid;
	private final long max;

	public static DateVariation untilNow() {
		return new DateVariation();
	}

	public static DateVariation sinceMonday() {
		return new DateVariation(
			java.sql.Date.valueOf(LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.MONDAY))),
			new java.util.Date()
		);
	}

	public static DateVariation sinceSunday() {
		return new DateVariation(
			java.sql.Date.valueOf(LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.SUNDAY))),
			new java.util.Date()
		);
	}

	public static DateVariation sinceStartMonth() {
		return new DateVariation(
			java.sql.Date.valueOf(LocalDate.now().with(TemporalAdjusters.firstDayOfMonth())),
			new java.util.Date()
		);
	}

	public static DateVariation tillThisYear() {
		return new DateVariation(
			new java.util.Date(),
			Timestamp.valueOf(LocalDate.now()
				.with(TemporalAdjusters.lastDayOfYear()).atTime(LocalTime.MAX))
		);
	}

	public DateVariation() {
		this(new java.util.Date(System.currentTimeMillis() - A_WEEK), new java.util.Date());
	}

	public DateVariation(java.util.Date min, java.util.Date max) {
		this.min = min.getTime();
		this.max = max.getTime();
		this.mid = (this.max - this.min) / 2 + this.min;
	}

	@Override
	@SuppressWarnings("unchecked")
	public VariationSet<java.util.Date>[] getVariations() {
		return new VariationSet[] {
			new VariationSet<>("date minimum(" + new java.util.Date(min) + ")", new java.util.Date(min), MethodResult.SUCCEEDS),
			new VariationSet<>("date maximum(" + new java.util.Date(max) + ")", new java.util.Date(max), MethodResult.SUCCEEDS),
			new VariationSet<>("date median(" + new java.util.Date(mid) + ")", new java.util.Date(mid), MethodResult.SUCCEEDS),
			new VariationSet<>("date below minimum(" + new java.util.Date(min - 1) + ")", new java.util.Date(min - 1), MethodResult.FAILS),
			new VariationSet<>("date day below minimum(" + new java.util.Date(min - A_DAY) + ")", new java.util.Date(min - A_DAY), MethodResult.FAILS),
			new VariationSet<>("date above maximum(" + new java.util.Date(max + 1) + ")", new java.util.Date(max + 1), MethodResult.FAILS),
			new VariationSet<>("date day above maximum(" + new java.util.Date(max + A_DAY) + ")", new java.util.Date(max + A_DAY), MethodResult.FAILS)
		};
	}

	@Override
	public Class<java.util.Date> getVariationType() {
		return java.util.Date.class;
	}
}
