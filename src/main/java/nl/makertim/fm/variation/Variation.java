package nl.makertim.fm.variation;

public interface Variation<T> {

	Class<T> getVariationType();

	VariationSet<T>[] getVariations();



}
