package nl.makertim.fm.variation;

public class DoubleVariation extends DoubleObjectVariation {

	public DoubleVariation() {
		super();
	}

	public DoubleVariation(double min, double max) {
		super(min, max);
	}

	@Override
	public Class<Double> getVariationType() {
		return double.class;
	}
}
