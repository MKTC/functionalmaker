package nl.makertim.fm.variation;

import nl.makertim.fm.core.TriSet;

import java.util.function.Function;

public class VariationSet<T> extends TriSet<String, T, Function<T, MethodResult>> {

	public VariationSet(String s, T t, MethodResult result) {
		this(s, t, x -> result);
	}

	public VariationSet(String s, T t, Function<T, MethodResult> isWithinScopeTest) {
		super(s, t, isWithinScopeTest);
	}

	public String getMessage() {
		return getT1();
	}

	public T getVariable() {
		return getT2();
	}

	public MethodResult getResult() {
		return getT3().apply(getVariable());
	}
}
