package nl.makertim.fm.variation;

public class IntVariation extends IntegerVariation {

	public IntVariation() {
		super();
	}

	public IntVariation(int min, int max) {
		super(min, max);
	}

	@Override
	public Class<Integer> getVariationType() {
		return int.class;
	}
}
