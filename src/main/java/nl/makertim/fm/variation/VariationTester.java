package nl.makertim.fm.variation;

import nl.makertim.fm.core.Property;
import nl.makertim.fm.safe.SafeTry;
import nl.makertim.fm.safe.ThrowingTriConsumer;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VariationTester {
	public static void run(Object obj, String methodName, Variation<?>... variations) throws Exception {
		Class<?>[] variationClasses = Arrays.stream(variations).map(Variation::getVariationType).toArray(Class[]::new);
		Method method = obj.getClass().getDeclaredMethod(methodName, variationClasses);

		startPermutations(variations, (variationNames, variationValues, result) -> {
			Property<Throwable> errorProperty = new Property<>(null);

			SafeTry.execute(() -> {
				method.invoke(obj, variationValues);
			}, errorProperty::set);

			if (errorProperty.isNull() && result == MethodResult.SUCCEEDS
				|| errorProperty.isPresent() && result == MethodResult.FAILS) {
				return;
			}

			throw new AssertionError(
				"Assertion error while with variation: \n\t\t" +
					Arrays.stream(variationNames).reduce(((s1, s2) -> s1 + " & " + s2)).orElse("NO VARIABLES"),
				(errorProperty.isNull() ? null : errorProperty.get().getCause())
			);
		});
	}

	private static void startPermutations(Variation<?>[] variations,
										  ThrowingTriConsumer<String[], Object[], MethodResult, Exception> consumer) throws Exception {
		runPermutations(variations, new ArrayList<>(), 0, consumer);
	}

	// Cartesisch product
	private static void runPermutations(Variation<?>[] variations, List<VariationSet<?>> current, int depth,
										ThrowingTriConsumer<String[], Object[], MethodResult, Exception> consumer) throws Exception {
		if (depth == variations.length) {
			consumer.accept(
				current.stream().map(VariationSet::getMessage).toArray(String[]::new),
				current.stream().map(VariationSet::getVariable).toArray(Object[]::new),
				current.stream().map(VariationSet::getResult).reduce(MethodResult::reduce).orElse(MethodResult.FAILS)
			);
			return;
		}

		VariationSet<?>[] variationSets = variations[depth].getVariations();
		for (VariationSet<?> variationSet : variationSets) {
			runPermutations(variations, addToNewList(current, variationSet), depth + 1, consumer);
		}
	}

	public static <T> List<T> addToNewList(List<T> list, T toAdd) {
		list = new ArrayList<>(list);
		list.add(toAdd);
		return list;
	}
}
