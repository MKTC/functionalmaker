package nl.makertim.fm.variation;

import java.util.function.Function;

public class BoolVariation implements Variation<Boolean> {

	private final Function<Boolean, MethodResult> resultFunction;

	public BoolVariation() {
		this.resultFunction = x -> MethodResult.SUCCEEDS;
	}

	public BoolVariation(Function<Boolean, MethodResult> resultFunction) {
		this.resultFunction = resultFunction;
	}

	@Override
	@SuppressWarnings("unchecked")
	public VariationSet<Boolean>[] getVariations() {
		return new VariationSet[] {
			new VariationSet<>("boolean (true)", true, resultFunction),
			new VariationSet<>("boolean (false)", false, resultFunction)
		};
	}

	@Override
	public Class<Boolean> getVariationType() {
		return Boolean.class;
	}
}
