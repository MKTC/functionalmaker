package nl.makertim.fm.variation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class ObjectVariation<T> implements Variation<T> {

	private final Function<T, MethodResult> resultFunction;

	private final Class<T> objectClass;
	private final T[] objectsToSucceed;
	private final T[] objectsToFail;

	public ObjectVariation(Class<T> objectClass, T... objectsToSucceed) {
		this(objectClass, objectsToSucceed, null, x -> MethodResult.SUCCEEDS);
	}

	public ObjectVariation(Class<T> objectClass, T[] objectsToSucceed, T[] objectsToFail, Function<T, MethodResult> resultFunction) {
		this.objectClass = objectClass;
		this.objectsToSucceed = objectsToSucceed;
		this.objectsToFail = objectsToFail;
		this.resultFunction = resultFunction;
	}

	@Override
	@SuppressWarnings("unchecked")
	public VariationSet<T>[] getVariations() {
		List<VariationSet<T>> variations = new ArrayList<>();

		if (objectsToSucceed != null) {
			for (T succeed : objectsToSucceed) {
				String obj = Arrays.deepToString(new Object[] { succeed });
				obj = "'" + obj.substring(1, obj.length() - 1) + "'";
				variations.add(new VariationSet<>("object within spec: " + obj, succeed, MethodResult.SUCCEEDS));
			}
		}

		if (objectsToFail != null) {
			for (T fails : objectsToFail) {
				String obj = Arrays.deepToString(new Object[] { fails });
				obj = "'" + obj.substring(1, obj.length() - 1) + "'";
				variations.add(new VariationSet<>("object outside spec: " + obj, fails, resultFunction));
			}
		}
		return variations.toArray(VariationSet[]::new);
	}

	@Override
	public Class<T> getVariationType() {
		return objectClass;
	}
}
