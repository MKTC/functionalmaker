package nl.makertim.fm.variation;

import java.util.function.Function;

public class ArrayVariation<T> extends ObjectVariation<T[]> {

	@SafeVarargs
	public ArrayVariation(Class<T[]> objectClass, T[]... objectsToSucceed) {
		super(objectClass, objectsToSucceed);
	}

	public ArrayVariation(Class<T[]> objectClass, T[][] objectsToSucceed, T[][] objectsToFail, Function<T[], MethodResult> resultFunction) {
		super(objectClass, objectsToSucceed, objectsToFail, resultFunction);
	}
}
