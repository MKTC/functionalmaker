package nl.makertim.fm.variation;

public class DoubleObjectVariation implements Variation<Double> {

	private final double min;
	private final double mid;
	private final double max;

	public DoubleObjectVariation() {
		this(0, 10);
	}

	public DoubleObjectVariation(double min, double max) {
		this.min = min;
		this.mid = (max - min) / 2 + min;
		this.max = max;
	}

	@Override
	@SuppressWarnings("unchecked")
	public VariationSet<Double>[] getVariations() {
		return new VariationSet[] {
			new VariationSet<>("double minimum (" + min + ")", min, MethodResult.SUCCEEDS),
			new VariationSet<>("double maximum (" + max + ")", max, MethodResult.SUCCEEDS),
			new VariationSet<>("double median (" + mid + ")", mid, MethodResult.SUCCEEDS),
			new VariationSet<>("double below minimum (" + (min - 1) + ")", min - 1, MethodResult.FAILS),
			new VariationSet<>("double below minimum (" + (min - 0.5) + ")", min - 0.5, MethodResult.FAILS),
			new VariationSet<>("double above maximum (" + (max + 1) + ")", max + 1, MethodResult.FAILS),
			new VariationSet<>("double above maximum (" + (max + 0.5) + ")", max + 0.5, MethodResult.FAILS)
		};
	}

	@Override
	public Class<Double> getVariationType() {
		return Double.class;
	}
}
