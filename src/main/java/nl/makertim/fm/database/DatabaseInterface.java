package nl.makertim.fm.database;

public interface DatabaseInterface extends FunctionalDatabaseInterface, PlainDatabaseInterface, ORMDatabaseInterface {
}
