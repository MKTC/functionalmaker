package nl.makertim.fm.database.oop.constructs;

import nl.makertim.fm.database.oop.query.ResultQuery;

import static nl.makertim.fm.database.oop.constructs.Comparison.IN;

public class WhereInSubQuery extends Where<ResultQuery<?>> {

	public static WhereInSubQuery whereInSubQuery(String table, String column, ResultQuery<?> query) {
		return new WhereInSubQuery(table, column, query);
	}

	WhereInSubQuery(String table, String column, ResultQuery<?> query) {
		super(table, column, IN, query);
	}

	public ResultQuery<?> getQuery() {
		return getVar();
	}
}
