package nl.makertim.fm.database.oop.dialects;

import nl.makertim.fm.database.oop.Dialect;

public class SQLiteDialect extends Dialect {

	public SQLiteDialect() {
		super(SQLiteBuilder.class);
	}
}
