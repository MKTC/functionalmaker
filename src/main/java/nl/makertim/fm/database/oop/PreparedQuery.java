package nl.makertim.fm.database.oop;

import nl.makertim.fm.database.core.SafeResultSet;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public interface PreparedQuery {

	SafeResultSet execute(Connection connection, Map<String, Object> variables);

	String getSQL(String suffix);

	default String getSQL() {
		return getSQL(";");
	}

	default String minifiedSQL() {
		return getSQL().replaceAll("\\s+", " ");
	}

	int getPlaceholderAmount();

	int getPlaceholderDistinctAmount();

	String[] getPlaceholderNames();

	String[] getPlaceholderNamesDistinct();

	PreparedQuery setBuildTime(long nanos);

	double getBuildTimeMs();

	double getLastExecuteMs();

	double getLastPrepareMs();

	boolean hasResults();

	default Map<String, Object> cleanup(Map<String, Object> variables) {
		Map<String, Object> newVariables = new HashMap<>(variables);
		variables.keySet().forEach(variable -> {
			if (variable.startsWith(":")) {
				return;
			}
			newVariables.put(":" + variable, newVariables.remove(variable));
		});
		return newVariables;
	}
}
