package nl.makertim.fm.database.oop.query;

import java.util.HashMap;
import java.util.Map;

public interface SQLFactory {

	Map<String, SQLFactory> sqlFactoryLookup = new HashMap<>();

	static SQLFactory getFactory(String dialect) {
		dialect = dialect.toUpperCase();
		return sqlFactoryLookup.get(dialect);
	}

	static void registerFactory(SQLFactory factory) {
		sqlFactoryLookup.put(factory.getDialectName().toUpperCase(), factory);
	}


	String getDialectName();

}
