package nl.makertim.fm.database.oop.constructs;

import static nl.makertim.fm.database.oop.constructs.ConditionCombiner.OR;

public class Or extends CombineCondition {

	public static Or or(Condition condition, Condition... conditions) {
		return new Or(condition, conditions);
	}

	public Or(Condition condition, Condition... conditions) {
		super(OR, condition, conditions);
	}
}
