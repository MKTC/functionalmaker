package nl.makertim.fm.database.oop.dialects;

import nl.makertim.fm.database.oop.Dialect;

public class MySQLDialect extends Dialect {

	public MySQLDialect() {
		super(MySQLBuilder.class);
	}
}
