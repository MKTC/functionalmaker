package nl.makertim.fm.database.oop.dialects;

import nl.makertim.fm.database.oop.Dialect;
import nl.makertim.fm.database.oop.SQLBuilder;
import nl.makertim.fm.database.oop.constructs.*;
import nl.makertim.fm.database.oop.query.*;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.Year;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class MySQLBuilder extends SQLBuilder {

	public MySQLBuilder(AlterQuery query) {
		super(query);
	}

	public MySQLBuilder(CreateQuery query) {
		super(query);
	}

	public MySQLBuilder(DeleteQuery query) {
		super(query);
	}

	public MySQLBuilder(DescribeQuery query) {
		super(query);
	}

	public MySQLBuilder(DropQuery query) {
		super(query);
	}

	public MySQLBuilder(InsertQuery query) {
		super(query);
	}

	public MySQLBuilder(SelectQuery query) {
		super(query);
	}

	public MySQLBuilder(UpdateQuery query) {
		super(query);
	}

	@Override
	protected MySQLBuilder buildAlterQuery(AlterQuery query) {
		AlterQuery.Internal internal = query.internal();

		// ALTER TABLE
		sqlStringBuilder.append("ALTER TABLE ");
		sqlStringBuilder.append(escapeTable(internal.getTable(), " "));

		String separator = "";
		// RENAME TO
		if (internal.getNewTableName() != null) {
			sqlStringBuilder.append("RENAME TO ");
			sqlStringBuilder.append(escapeTable(internal.getNewTableName(), separator));
			separator = ", ";
		}

		// DROP COLUMN
		var droppedColumns = internal.getDroppedColumns();
		for (String droppedColumn : droppedColumns) {
			sqlStringBuilder.append(separator);
			sqlStringBuilder.append("DROP COLUMN ");
			sqlStringBuilder.append(escapeColumn(droppedColumn));
			separator = ", ";
		}

		// RENAME COLUMN TO
		var renameColumns = internal.getRenames();
		for (Map.Entry<String, String> renameColumn : renameColumns.entrySet()) {
			sqlStringBuilder.append(separator);
			sqlStringBuilder.append("RENAME COLUMN ");
			sqlStringBuilder.append(escapeColumn(renameColumn.getKey()));
			sqlStringBuilder.append(" TO ");
			sqlStringBuilder.append(escapeColumn(renameColumn.getValue()));
			separator = ", ";
		}

		// ADD COLUMN
		var addColumns = internal.getNewColumns();
		if (!addColumns.isEmpty()) {
			sqlStringBuilder.append(separator);
			sqlStringBuilder.append("ADD COLUMN ");
			sqlStringBuilder.append(buildColumnDescriptions(addColumns));
		}

		return this;
	}

	@Override
	protected MySQLBuilder buildCreateQuery(CreateQuery query) {
		CreateQuery.Internal internal = query.internal();

		// CREATE TABLE
		sqlStringBuilder.append("CREATE TABLE ");
		// IF NOT EXISTS
		if (internal.isIgnoreExisting()) {
			sqlStringBuilder.append("IF NOT EXISTS ");
		}
		sqlStringBuilder.append(escapeTable(internal.getTable(), " "));

		// columns
		sqlStringBuilder.append(buildColumnDescriptions(internal.getColumns()));

		return this;
	}

	@Override
	protected MySQLBuilder buildDeleteQuery(DeleteQuery query) {
		DeleteQuery.Internal internal = query.internal();

		// DELETE FROM
		sqlStringBuilder.append("DELETE FROM ");
		sqlStringBuilder.append(escapeTable(internal.getTable()));

		// WHERE
		sqlStringBuilder.append(buildWhere(internal.getConditions()));

		// ORDER BY
		sqlStringBuilder.append(buildOrderBy(internal.getOrderBys()));

		// LIMIT
		sqlStringBuilder.append(buildLimit(internal.getLimit(), internal.getOffset()));

		return this;
	}

	@Override
	protected MySQLBuilder buildDescribeQuery(DescribeQuery query) {
		DescribeQuery.Internal internal = query.internal();

		sqlStringBuilder.append("SHOW CREATE TABLE ");
		sqlStringBuilder.append(escapeTable(internal.getTable()));

		return this;
	}

	@Override
	protected MySQLBuilder buildDropQuery(DropQuery query) {
		DropQuery.Internal internal = query.internal();

		// DROP TABLE
		sqlStringBuilder.append("DROP TABLE ");

		// IF EXISTS
		if (internal.ignoreIfNotExists()) {
			sqlStringBuilder.append("IF EXISTS ");
		}

		// name
		sqlStringBuilder.append(escapeTable(internal.getTable()));

		return this;
	}

	@Override
	protected MySQLBuilder buildInsertQuery(InsertQuery query) {
		InsertQuery.Internal internal = query.internal();

		// INSERT INTO
		sqlStringBuilder.append("INSERT ");
		sqlStringBuilder.append(buildUpdateMode(internal.getMode()));
		sqlStringBuilder.append("INTO ");
		sqlStringBuilder.append(escapeTable(internal.getTable()));

		// columns
		sqlStringBuilder.append(" (");
		stringStreamToString(sqlStringBuilder, ", ",
			Arrays.stream(internal.getColumns())
				.map(this::escapeColumn)
		);
		sqlStringBuilder.append(")");

		// VALUES ( ... )
		sqlStringBuilder.append(" VALUES ");
		int amount = internal.getAmount();
		for (int i = 0; i < amount; i++) {
			final int iteration = i;
			sqlStringBuilder.append("(");
			stringStreamToString(sqlStringBuilder, " , ",
				Arrays.stream(internal.getColumns())
					.map(column -> ":" + column + ((amount > 1) ? (iteration + 1) : ""))
			);
			sqlStringBuilder.append(")");
			if (i + 1 < internal.getAmount()) {
				sqlStringBuilder.append(", ");
			}
		}

		return this;
	}

	@Override
	protected MySQLBuilder buildSelectQuery(SelectQuery query) {
		SelectQuery.Internal internal = query.internal();

		// SELECT
		sqlStringBuilder.append("SELECT ");
		if (internal.isDistinct()) sqlStringBuilder.append("DISTINCT ");
		stringStreamToString(sqlStringBuilder, ", ", internal.getColumns()
			.stream()
			.map(this::buildColumn)
		);
		sqlStringBuilder.append(" ");

		// FROM
		if (!internal.getFrom().isEmpty()) {
			sqlStringBuilder.append('\n');
			sqlStringBuilder.append("FROM ");
			stringStreamToString(sqlStringBuilder, ", ",
				internal.getFrom()
					.stream()
					.map(table -> escapeColumn(table, " ") + escapeTable(table))
			);
		}

		// JOIN
		if (!internal.getJoin().isEmpty()) {
			sqlStringBuilder.append('\n');
			stringStreamToString(sqlStringBuilder, "\n",
				internal.getJoin()
					.stream()
					.map(join ->
						buildJoin(join.getType()) + " "
							+ escapeColumn(join.getTable(), " ") + escapeTable(join.getTable())
							+ (join.getOnLeft() == null || join.getOnRight() == null
							? ""
							: (" ON "
							+ buildColumn(join.getOnLeft())
							+ " = "
							+ buildColumn(join.getOnRight())))
					)
			);
		}

		// WHERE
		sqlStringBuilder.append(buildWhere(internal.getConditions()));

		// GROUP BY
		if (!internal.getGroupBys().isEmpty()) {
			sqlStringBuilder.append("\nGROUP BY ");
			stringStreamToString(sqlStringBuilder, ", ",
				internal.getGroupBys()
					.stream()
					.map(grouping -> buildColumn(grouping.getColumn()))
			);
		}

		// ORDER BY
		sqlStringBuilder.append(buildOrderBy(internal.getOrderBys()));

		// LIMIT
		sqlStringBuilder.append(buildLimit(internal.getLimit(), internal.getOffset()));

		return this;
	}

	@Override
	protected MySQLBuilder buildUpdateQuery(UpdateQuery query) {
		UpdateQuery.Internal internal = query.internal();

		// UPDATE
		sqlStringBuilder.append("UPDATE ");
		sqlStringBuilder.append(buildUpdateMode(internal.getMode()));
		sqlStringBuilder.append(escapeTable(internal.getTable()));

		// SET
		sqlStringBuilder.append("\nSET ");
		Arrays.stream(internal.getColumns())
			.map(column -> escapeColumn(column) + " = :" + column)
			.forEach(sqlStringBuilder::append);

		// WHERE
		sqlStringBuilder.append(buildWhere(internal.getConditions()));

		// ORDER BY
		sqlStringBuilder.append(buildOrderBy(internal.getOrderBys()));

		// LIMIT
		sqlStringBuilder.append(buildLimit(internal.getLimit(), internal.getOffset()));

		return this;
	}

	@Override
	protected String buildStringColumn(FieldColumn column) {
		return escapeTable(column.getTable(), ".") + escapeColumn(column.getColumn());
	}

	@Override
	protected String buildTableColumn(TableColumn column) {
		return escapeTable(column.getTable()) + ".*";
	}

	@Override
	protected String buildFunctionColumn(FunctionColumn column) {
		return buildFunction(column.getFunction())
			+ "("
			+ escapeTable(column.getTable(), ".")
			+ escapeColumn(column.getColumn())
			+ ") "
			+ escapeColumn(column.getAlias());
	}

	@Override
	protected String buildRecursiveFunctionColumn(RecursiveFunctionColumn column) {
		String alias = column.getAlias();
		column.setAlias("");
		return column.getFunction().toString()
			+ "("
			+ buildColumn(column.getColumn())
			+ ") "
			+ escapeColumn(column.setAlias(alias).getAlias());
	}

	@Override
	protected String buildFunction(Function function) {
		if (function == Function.AVERAGE) return "AVG";
		return super.buildFunction(function);
	}

	@Override
	protected String buildCondition(Where<?> where) {
		String left = escapeTable(where.getTable(), ".") + escapeColumn(where.getColumn());
		String middle = buildComparison(where.getComparison());
		String right;
		if (where instanceof WhereNull) {
			middle = "IS";
			right = "NULL";
		} else if (where instanceof WhereNotNull) {
			middle = "IS NOT";
			right = "NULL";
		} else if (where instanceof WhereBetween) {
			var between = ((WhereBetween) where);
			right = between.getVarLow() + " AND " + between.getVarHigh();
		} else if (where instanceof WhereIn) {
			var rightVars = ((WhereIn) where).getVar();
			right = "(" + String.join(", ", rightVars) + ")";
		} else if (where instanceof WhereInSubQuery) {
			right = '(' +
				Dialect.getDialect(getDialect()).buildQuery(((WhereInSubQuery) where).getQuery()).getSQL("").trim()
				+ ')';
		} else {
			var var = where.getVar();
			right = var == null ? "NULL" : var.toString();
		}
		return String.format("%s %s %s", left, middle, right);
	}

	@Override
	protected String buildColumnDescription(ColumnDefinition<?> column) {
		var type = buildColumnType(column.getType());
		var lengthString = (column.getLengthType().length == 0
			? ""
			: "( " + Arrays.stream(column.getLengthType()).mapToObj(Integer::toString).collect(Collectors.joining(", ")) + " )");
		if (column.getLengthType().length == 0 && type.equals("VARCHAR")) {
			lengthString = "( 32 )";
		}
		return escapeColumn(column.getName())
			+ " " + type
			+ lengthString
			+ (column.isAutoIncrement() ? " AUTO_INCREMENT" : "")
			+ (column.isNullAllowed() ? " NULL " : " NOT NULL ")
			+ (column.getDefaultValue() != null ? " DEFAULT " + escapeVariable(column.getDefaultValue().toString()) : "");
	}

	@Override
	protected String buildColumnType(Class<?> clazz) {
		return typeMap.getOrDefault(clazz, "VARCHAR");
	}

	@Override
	protected Map<Class<?>, String> overwriteTypes() {
		return Map.ofEntries(
			Map.entry(String.class, "VARCHAR"),
			Map.entry(Integer.class, "INT"),
			Map.entry(int.class, "INT"),
			Map.entry(Long.class, "BIGINT"),
			Map.entry(long.class, "BIGINT"),
			Map.entry(Double.class, "DOUBLE"),
			Map.entry(double.class, "DOUBLE"),
			Map.entry(Float.class, "FLOAT"),
			Map.entry(float.class, "FLOAT"),
			Map.entry(StringBuilder.class, "TEXT"),
			Map.entry(Character[].class, "BINARY"),
			Map.entry(char[].class, "BINARY"),
			Map.entry(Byte[].class, "BLOB"),
			Map.entry(byte[].class, "BLOB"),
			Map.entry(java.sql.Date.class, "DATETIME"),
			Map.entry(Timestamp.class, "TIMESTAMP"),
			Map.entry(Year.class, "YEAR"),
			Map.entry(Time.class, "TIME"),
			Map.entry(java.util.Date.class, "DATETIME"),
			Map.entry(Enum.class, "ENUM")
		);
	}

	@Override
	public Class<? extends Dialect> getDialect() {
		return MySQLDialect.class;
	}
}
