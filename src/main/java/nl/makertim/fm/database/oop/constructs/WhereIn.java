package nl.makertim.fm.database.oop.constructs;

import static nl.makertim.fm.database.oop.constructs.Comparison.IN;

public class WhereIn extends Where<String[]> {

	public static WhereIn whereIn(String table, String column, String... var) {
		return new WhereIn(table, column, var);
	}

	WhereIn(String table, String column, String... var) {
		super(table, column, IN, validateVars(var));
	}
}
