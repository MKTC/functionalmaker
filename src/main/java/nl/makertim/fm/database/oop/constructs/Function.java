package nl.makertim.fm.database.oop.constructs;

public enum Function {
	AVERAGE,
	COUNT,
	MIN,
	MAX,
	SUM,
	ROUND
}
