package nl.makertim.fm.database.oop.constructs;

import static nl.makertim.fm.database.oop.constructs.Comparison.NOT;

public class WhereNotNull extends Where<Object> {

	public static WhereNotNull whereNotNull(String table, String column) {
		return new WhereNotNull(table, column);
	}

	WhereNotNull(String table, String column) {
		super(table, column, NOT, null);
	}
}
