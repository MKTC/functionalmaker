package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.database.oop.constructs.*;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static nl.makertim.fm.database.oop.constructs.Function.*;

public class SelectQuery
	extends BaseQuery<SelectQuery.Internal>
	implements Query<SelectQuery.Internal>, ResultQuery<SelectQuery.Internal>, Cloneable {

	private Internal internal;
	BidiMap<String, String> currentTableShorts = new TreeBidiMap<>();

	boolean distinct = false;
	List<Column> columns = new LinkedList<>();
	List<String> from = new LinkedList<>();
	List<Join> join = new LinkedList<>();
	List<Condition> conditions = new LinkedList<>();
	List<Grouping> groupBys = new LinkedList<>();
	List<Ordering> orderBys = new LinkedList<>();
	int limit = Integer.MIN_VALUE;
	int offset = Integer.MIN_VALUE;

	public static SelectQuery selectFromWithNoColumns(String tableName) {
		SelectQuery select = new SelectQuery();
		select.newTableShortFor(tableName);
		select.from.add(tableName);
		return select;
	}

	public static SelectQuery selectFrom(String tableName) {
		SelectQuery select = selectFromWithNoColumns(tableName);
		select.columns.add(Column.table(tableName));
		return select;
	}

	public static SelectQuery selectFrom(String tableName, String... columns) {
		SelectQuery select = selectFromWithNoColumns(tableName);
		for (String column : columns) {
			select.columns.add(new FieldColumn(tableName, column));
		}
		return select;
	}

	protected SelectQuery() {
	}

	public SelectQuery(Object constant, Object... constants) {
		for (Object c : ArrayUtils.add(constants, constant)) {
			columns.add(new ConstantColumn(c));
		}
	}

	public SelectQuery addColumnAverage(String table, String column, String alias) {
		return addColumn(new FunctionColumn(table, column, AVERAGE, alias));
	}

	public SelectQuery addColumnCount(String table, String column, String alias) {
		return addColumn(new FunctionColumn(table, column, COUNT, alias));
	}

	public SelectQuery addColumnSum(String table, String column, String alias) {
		return addColumn(new FunctionColumn(table, column, SUM, alias));
	}

	public SelectQuery addColumnMin(String table, String column, String alias) {
		return addColumn(new FunctionColumn(table, column, MIN, alias));
	}

	public SelectQuery addColumnMax(String table, String column, String alias) {
		return addColumn(new FunctionColumn(table, column, MAX, alias));
	}

	public SelectQuery addColumn(Column column) {
		this.columns.add(column);
		return this;
	}

	public SelectQuery setDistinct(boolean distinct) {
		this.distinct = distinct;
		return this;
	}

	public SelectQuery joinLeft(String table, Column onLeft, Column onRight) {
		return join(Join.left(table, onLeft, onRight));
	}

	public SelectQuery joinRight(String table, Column onLeft, Column onRight) {
		return join(Join.right(table, onLeft, onRight));
	}

	public SelectQuery joinFull(String table) {
		return join(Join.full(table));
	}

	public SelectQuery joinInner(String table, Column onLeft, Column onRight) {
		return join(Join.inner(table, onLeft, onRight));
	}

	public SelectQuery join(Join joinPair) {
		join.add(joinPair);
		newTableShortFor(joinPair.getTable());
		return this;
	}

	public SelectQuery addCondition(Condition condition) {
		conditions.add(condition);
		return this;
	}

	public SelectQuery groupBy(String column) {
		groupBys.add(new Grouping(new FieldColumn(column)));
		return this;
	}

	public SelectQuery groupBy(String table, String column) {
		groupBys.add(new Grouping(new FieldColumn(table, column)));
		return this;
	}

	public SelectQuery orderByAscending(String table, String column) {
		return orderBy(Ordering.orderByAscending(new FieldColumn(table, column)));
	}

	public SelectQuery orderByDescending(String table, String column) {
		return orderBy(Ordering.orderByDescending(new FieldColumn(table, column)));
	}

	public SelectQuery orderByAscending(String column) {
		return orderBy(Ordering.orderByAscending(new FieldColumn(column)));
	}

	public SelectQuery orderByDescending(String column) {
		return orderBy(Ordering.orderByDescending(new FieldColumn(column)));
	}

	public SelectQuery orderBy(Ordering ordering) {
		orderBys.add(ordering);
		return this;
	}

	public SelectQuery onlyOne() {
		return setLimit(1);
	}

	public SelectQuery setLimit(int limit) {
		if (limit <= 0) {
			limit = Integer.MIN_VALUE;
		}
		this.limit = limit;
		return this;
	}

	public SelectQuery setOffset(int offset) {
		if (offset <= 0) {
			offset = Integer.MIN_VALUE;
		}
		this.offset = offset;
		return this;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public int getLimit() {
		return limit;
	}

	public int getOffset() {
		return offset;
	}

	@Override
	public Internal internal() {
		if (this.internal == null) {
			this.internal = new Internal();
		}
		return this.internal;
	}

	private String newTableShortFor(String table) {
		String proposalShort = table.substring(0, 1);
		for (int i = 0; currentTableShorts.containsValue(proposalShort); i++) {
			proposalShort = table.substring(0, 1) + i;
		}
		currentTableShorts.put(table, proposalShort);
		return proposalShort;
	}

	@Override
	@SuppressWarnings({ "MethodDoesntCallSuperMethod" })
	public SelectQuery clone() {
		SelectQuery clone = new SelectQuery();
		clone.currentTableShorts = new TreeBidiMap<>(this.currentTableShorts);
		clone.distinct = this.distinct;
		clone.columns = new LinkedList<>(this.columns);
		clone.from = new LinkedList<>(this.from);
		clone.join = new LinkedList<>(this.join);
		clone.conditions = new LinkedList<>(this.conditions);
		clone.groupBys = new LinkedList<>(this.groupBys);
		clone.orderBys = new LinkedList<>(this.orderBys);
		clone.limit = this.limit;
		clone.offset = this.offset;
		return clone;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SelectQuery that = (SelectQuery) o;
		return new EqualsBuilder()
			.append(distinct, that.distinct)
			.append(limit, that.limit)
			.append(offset, that.offset)
			.append(currentTableShorts, that.currentTableShorts)
			.append(columns, that.columns)
			.append(from, that.from)
			.append(join, that.join)
			.append(conditions, that.conditions)
			.append(groupBys, that.groupBys)
			.append(orderBys, that.orderBys)
			.isEquals();
	}

	public class Internal implements TableShortInternal, InternalQuery {

		public Map<String, String> getCurrentTableShorts() {
			return Collections.unmodifiableMap(SelectQuery.this.currentTableShorts);
		}

		@Override
		public String getTable() {
			return SelectQuery.this.currentTableShorts
				.keySet()
				.stream()
				.findFirst()
				.orElse(null);
		}

		@Override
		public Map<String, String> getTableShorts() {
			return getCurrentTableShorts();
		}

		public boolean isDistinct() {
			return SelectQuery.this.distinct;
		}

		public List<Column> getColumns() {
			return Collections.unmodifiableList(SelectQuery.this.columns);
		}

		public List<String> getFrom() {
			return Collections.unmodifiableList(SelectQuery.this.from);
		}

		public List<Join> getJoin() {
			return Collections.unmodifiableList(SelectQuery.this.join);
		}

		public List<Condition> getConditions() {
			return Collections.unmodifiableList(SelectQuery.this.conditions);
		}

		public List<Grouping> getGroupBys() {
			return Collections.unmodifiableList(SelectQuery.this.groupBys);
		}

		public List<Ordering> getOrderBys() {
			return Collections.unmodifiableList(SelectQuery.this.orderBys);
		}

		public int getLimit() {
			return SelectQuery.this.limit;
		}

		public int getOffset() {
			return SelectQuery.this.offset;
		}
	}
}
