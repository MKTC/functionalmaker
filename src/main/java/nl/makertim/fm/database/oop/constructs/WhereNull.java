package nl.makertim.fm.database.oop.constructs;

import static nl.makertim.fm.database.oop.constructs.Comparison.EQUAL;

public class WhereNull extends Where<Object> {

	public static WhereNull whereNull(String table, String column) {
		return new WhereNull(table, column);
	}

	WhereNull(String table, String column) {
		super(table, column, EQUAL, null);
	}
}
