package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.database.oop.Dialect;
import nl.makertim.fm.database.oop.PreparedQuery;
import nl.makertim.fm.database.oop.constructs.Condition;

import static nl.makertim.fm.database.oop.constructs.WhereIn.whereIn;

public interface Query<Internal> {

	void setCache(Class<? extends Dialect> dialectClass, PreparedQuery query);

	boolean hasCache(Class<? extends Dialect> dialectClass);

	PreparedQuery getCache(Class<? extends Dialect> dialectClass);

	boolean removeCache(Class<? extends Dialect> dialectClass);

	default PreparedQuery build(Dialect dialect) {
		if (dialect == null) {
			throw new IllegalArgumentException("JDBC not supported for ORM queries. Make sure you register your Dialect in DialectManager.");
		}
		return dialect.buildQuery(this);
	}

	Internal internal();

	// SELECT ... FROM <table> <t>
	static SelectQuery selectFrom(String table, String... columns) {
		return SelectQuery
			.selectFrom(table, columns);
	}

	// SELECT ... FROM <table> <t> WHERE ...
	static SelectQuery selectFromWhere(String table, String[] columns, Condition where) {
		return SelectQuery
			.selectFrom(table, columns)
			.addCondition(where);
	}

	// SELECT ... FROM <table> <t> WHERE ...
	static SelectQuery selectFromWhereLimit(String table, String[] columns, Condition where, int limit) {
		return SelectQuery
			.selectFrom(table, columns)
			.addCondition(where)
			.setLimit(limit);
	}

	// SELECT * FROM <table> <t>
	static SelectQuery selectFrom(String table) {
		return SelectQuery
			.selectFrom(table);
	}

	// SELECT * FROM <table> <t> WHERE ...
	static SelectQuery selectFromWhere(String table, Condition where) {
		return SelectQuery
			.selectFrom(table)
			.addCondition(where);
	}

	// SELECT * FROM <table> <t> WHERE ... IN (...)
	static SelectQuery selectFromWhere(String table, String column, String... var) {
		return SelectQuery
			.selectFrom(table)
			.addCondition(whereIn(table, column, var));
	}

	// SELECT * FROM <table> <t> WHERE ... LIMIT *
	static SelectQuery selectFromWhereLimit(String table, Condition where, int limit) {
		return SelectQuery
			.selectFrom(table)
			.addCondition(where)
			.setLimit(limit);
	}

	// SELECT * FROM <table> <t> WHERE ... IN (...) LIMIT *
	static SelectQuery selectFromWhereLimit(String table, String column, String[] var, int limit) {
		return SelectQuery
			.selectFrom(table)
			.addCondition(whereIn(table, column, var))
			.setLimit(limit);
	}
}
