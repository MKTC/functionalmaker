package nl.makertim.fm.database.oop.constructs;

import static nl.makertim.fm.database.oop.constructs.ConditionCombiner.AND;

public class And extends CombineCondition {

	public static And and(Condition condition, Condition... conditions) {
		return new And(condition, conditions);
	}

	public And(Condition condition, Condition... conditions) {
		super(AND, condition, conditions);
	}
}
