package nl.makertim.fm.database.oop.query;

public interface InternalQuery {

	String getTable();
}
