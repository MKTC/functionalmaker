package nl.makertim.fm.database.oop.constructs;

import org.apache.commons.lang3.builder.EqualsBuilder;

import static nl.makertim.fm.database.oop.constructs.JoinType.*;

public class Join {

	private final JoinType type;
	private final String table;
	private final Column onLeft;
	private final Column onRight;

	public static Join full(String table) {
		return new Join(INNER, table, null, null);
	}

	public static Join left(String table, Column onLeft, Column onRight) {
		return new Join(LEFT, table, onLeft, onRight);
	}

	public static Join right(String table, Column onLeft, Column onRight) {
		return new Join(RIGHT, table, onLeft, onRight);
	}

	public static Join inner(String table, Column onLeft, Column onRight) {
		return new Join(INNER, table, onLeft, onRight);
	}

	protected Join(JoinType type, String table, Column onLeft, Column onRight) {
		this.type = type;
		this.table = table;
		this.onLeft = onLeft;
		this.onRight = onRight;
	}

	public String getTable() {
		return table;
	}

	public JoinType getType() {
		return type;
	}

	public Column getOnLeft() {
		return onLeft;
	}

	public Column getOnRight() {
		return onRight;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Join join = (Join) o;
		return new EqualsBuilder()
			.append(type, join.type)
			.append(table, join.table)
			.append(onLeft, join.onLeft)
			.append(onRight, join.onRight)
			.isEquals();
	}
}
