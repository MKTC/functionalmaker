package nl.makertim.fm.database.oop.constructs;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;

public abstract class CombineCondition implements Condition {

	private final Condition[] conditions;
	private final ConditionCombiner joiner;

	public CombineCondition(ConditionCombiner joiner, Condition condition, Condition... conditions) {
		this.conditions = ArrayUtils.insert(0, conditions, condition);
		this.joiner = joiner;
	}

	public Condition[] getConditions() {
		return conditions;
	}

	public ConditionCombiner getJoiner() {
		return joiner;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CombineCondition that = (CombineCondition) o;
		return new EqualsBuilder()
			.append(conditions, that.conditions)
			.append(joiner, that.joiner)
			.isEquals();
	}
}
