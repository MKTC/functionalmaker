package nl.makertim.fm.database.oop.query;

import java.util.Map;

public interface TableShortInternal {

	Map<String, String> getTableShorts();

}
