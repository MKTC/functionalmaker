package nl.makertim.fm.database.oop.constructs;

import nl.makertim.fm.core.BiSet;

import static nl.makertim.fm.database.oop.constructs.Comparison.BETWEEN;

public class WhereBetween extends Where<BiSet<String, String>> {

	public static WhereBetween whereBetween(String table, String column, String varLow, String varHigh) {
		return new WhereBetween(table, column, varLow, varHigh);
	}

	public WhereBetween(String table, String column, String varLow, String varHigh) {
		super(table, column, BETWEEN, new BiSet<>(validateVar(varLow), validateVar(varHigh)));
	}

	public String getVarHigh() {
		return getVar().getT2();
	}

	public String getVarLow() {
		return getVar().getT1();
	}
}
