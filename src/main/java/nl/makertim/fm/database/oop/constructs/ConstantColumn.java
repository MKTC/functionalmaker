package nl.makertim.fm.database.oop.constructs;

import org.apache.commons.lang3.builder.EqualsBuilder;

public class ConstantColumn implements Column {

	private final Object constant;

	public ConstantColumn(Object constant) {
		this.constant = constant;
	}

	@Override
	public String getTable() {
		return null;
	}

	public Object getColumn() {
		return constant;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		ConstantColumn that = (ConstantColumn) o;
		return new EqualsBuilder()
			.append(constant, that.constant)
			.isEquals();
	}
}
