package nl.makertim.fm.database.oop.constructs;

public enum Comparison {
	BIGGER_THEN,
	BIGGER_OR_EQUAL_THEN,
	SMALLER_THEN,
	SMALLER_OR_EQUAL_THEN,
	EQUAL,
	NOT,
	LIKE,
	BETWEEN,
	IN,
}
