package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.database.oop.constructs.Mode;
import org.apache.commons.lang3.builder.EqualsBuilder;

import static nl.makertim.fm.database.oop.constructs.Mode.*;

public class InsertQuery
	extends BaseQuery<InsertQuery.Internal>
	implements Query<InsertQuery.Internal>, Cloneable {

	private Internal internal;

	Mode mode = PLAIN;
	String table;
	String[] columns;
	int amount = 1;

	public InsertQuery(String table) {
		this.table = table;
	}

	public static InsertQuery insertInto(String table) {
		return new InsertQuery(table);
	}

	public static InsertQuery insertIntoIgnore(String table) {
		return insertInto(table)
			.setMode(IGNORE);
	}

	public static InsertQuery insertIntoWithColumns(String table, String... columns) {
		InsertQuery insertQuery = insertInto(table);
		insertQuery.columns = columns;
		return insertQuery;
	}

	public static InsertQuery insertIntoIgnoreWithColumns(String table, String... columns) {
		return insertIntoWithColumns(table, columns)
			.setMode(IGNORE);
	}

	public static InsertQuery insertMultipleInto(String table, int amount) {
		return new InsertQuery(table)
			.setAmount(amount);
	}

	public static InsertQuery insertIgnoreMultipleInto(String table, int amount) {
		return insertMultipleInto(table, amount)
			.setMode(IGNORE);
	}

	public static InsertQuery insertAbortMultipleInto(String table, int amount) {
		return insertMultipleInto(table, amount)
			.setMode(ABORT);
	}

	public static InsertQuery insertReplaceMultipleInto(String table, int amount) {
		return insertMultipleInto(table, amount)
			.setMode(REPLACE);
	}

	public static InsertQuery insertRollbackMultipleInto(String table, int amount) {
		return insertMultipleInto(table, amount)
			.setMode(ROLLBACK);
	}

	public static InsertQuery insertMultipleIntoWithColumns(String table, int amount, String... columns) {
		InsertQuery insertQuery = insertMultipleInto(table, amount);
		insertQuery.columns = columns;
		return insertQuery;
	}

	public static InsertQuery insertIgnoreMultipleIntoWithColumns(String table, int amount, String... columns) {
		return insertMultipleIntoWithColumns(table, amount, columns)
			.setMode(IGNORE);
	}

	public static InsertQuery insertAbortMultipleIntoWithColumns(String table, int amount, String... columns) {
		return insertMultipleIntoWithColumns(table, amount, columns)
			.setMode(ABORT);
	}

	public static InsertQuery insertReplaceMultipleIntoWithColumns(String table, int amount, String... columns) {
		return insertMultipleIntoWithColumns(table, amount, columns)
			.setMode(REPLACE);
	}

	public static InsertQuery insertRollbackMultipleIntoWithColumns(String table, int amount, String... columns) {
		return insertMultipleIntoWithColumns(table, amount, columns)
			.setMode(ROLLBACK);
	}

	public InsertQuery setMode(Mode mode) {
		this.mode = mode;
		return this;
	}

	public InsertQuery onlyOne() {
		return setAmount(1);
	}

	public InsertQuery setAmount(int amount) {
		if (amount <= 0) {
			throw new IllegalArgumentException("Insert amount on a insert query is not allowed to go below one");
		}
		this.amount = amount;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		InsertQuery that = (InsertQuery) o;
		return new EqualsBuilder()
			.append(amount, that.amount)
			.append(internal, that.internal)
			.append(mode, that.mode)
			.append(table, that.table)
			.append(columns, that.columns)
			.isEquals();
	}

	@Override
	@SuppressWarnings({ "MethodDoesntCallSuperMethod" })
	public InsertQuery clone() {
		InsertQuery clone = new InsertQuery(this.table);
		clone.amount = amount;
		clone.internal = this.internal;
		clone.mode = mode;
		clone.table = table;
		clone.columns = columns != null ? columns.clone() : null;
		return clone;
	}

	@Override
	public Internal internal() {
		if (this.internal == null) {
			this.internal = new Internal();
		}
		return this.internal;
	}

	public class Internal implements InternalQuery {
		public Mode getMode() {
			return InsertQuery.this.mode;
		}

		@Override
		public String getTable() {
			return InsertQuery.this.table;
		}

		public String[] getColumns() {
			return InsertQuery.this.columns != null ? InsertQuery.this.columns.clone() : null;
		}

		public int getAmount() {
			return InsertQuery.this.amount;
		}
	}
}
