package nl.makertim.fm.database.oop.constructs;

import org.apache.commons.lang3.builder.EqualsBuilder;

public class ColumnDefinition<Type> {

	String name;
	int[] lengthType;
	Class<Type> type;
	Type defaultValue;
	boolean allowNull;
	boolean isAutoIncrement;

	@SuppressWarnings("unchecked")
	public static <T> ColumnDefinition<T> column(String name, T defaultValue, int... lengthType) {
		return new ColumnDefinition<>(name, defaultValue, (Class<T>) defaultValue.getClass(), lengthType);
	}

	public static <T> ColumnDefinition<T> column(String name, T defaultValue, boolean allowNull, int... lengthType) {
		return column(name, defaultValue, lengthType).setAllowNull(allowNull);
	}

	public static <T> ColumnDefinition<T> columnWithNull(String name, T defaultValue, int... lengthType) {
		return column(name, defaultValue, true, lengthType);
	}

	public static <T> ColumnDefinition<T> column(String name, Class<T> defaultValueType, int... lengthType) {
		return new ColumnDefinition<>(name, null, defaultValueType, lengthType);
	}

	public static <T> ColumnDefinition<T> column(String name, Class<T> defaultValueType, boolean allowNull, int... lengthType) {
		return column(name, defaultValueType, lengthType).setAllowNull(allowNull);
	}

	public static <T> ColumnDefinition<T> columnWithNull(String name, Class<T> defaultValueType, int... lengthType) {
		return column(name, defaultValueType, true, lengthType);
	}

	public static <T> ColumnDefinition<T> columnTypeless(String name) {
		return new ColumnDefinition<>(name, null, null, new int[0]);
	}

	public ColumnDefinition(String name, Type defaultValue, Class<Type> type, int... lengthType) {
		this.name = name;
		this.defaultValue = defaultValue;
		this.type = type;
		this.lengthType = lengthType;
	}

	public String getName() {
		return name;
	}

	public Object getDefaultValue() {
		return defaultValue;
	}

	public int[] getLengthType() {
		return lengthType;
	}

	public ColumnDefinition<Type> setAutoIncrementing() {
		return setAutoIncrementing(true);
	}

	public ColumnDefinition<Type> setAutoIncrementing(boolean isAutoIncrement) {
		this.isAutoIncrement = isAutoIncrement;
		return this;
	}

	public boolean isAutoIncrement() {
		return isAutoIncrement;
	}

	public ColumnDefinition<Type> setAllowNull() {
		return setAllowNull(true);
	}

	public ColumnDefinition<Type> setAllowNull(boolean allowNull) {
		this.allowNull = allowNull;
		return this;
	}

	public boolean isNullAllowed() {
		return allowNull;
	}

	public Class<Type> getType() {
		return type;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		ColumnDefinition<?> that = (ColumnDefinition<?>) o;
		return new EqualsBuilder()
			.append(name, that.name)
			.append(lengthType, that.lengthType)
			.append(defaultValue, that.defaultValue)
			.append(type, that.type)
			.isEquals();
	}
}
