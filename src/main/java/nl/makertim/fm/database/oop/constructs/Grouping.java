package nl.makertim.fm.database.oop.constructs;

import org.apache.commons.lang3.builder.EqualsBuilder;

public class Grouping {

	private final Column column;

	public Grouping(Column column) {
		this.column = column;
	}

	public Column getColumn() {
		return column;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Grouping grouping = (Grouping) o;
		return new EqualsBuilder()
			.append(column, grouping.column)
			.isEquals();
	}
}
