package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.database.oop.constructs.Condition;
import nl.makertim.fm.database.oop.constructs.FieldColumn;
import nl.makertim.fm.database.oop.constructs.Mode;
import nl.makertim.fm.database.oop.constructs.Ordering;
import org.apache.commons.lang3.builder.EqualsBuilder;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class UpdateQuery
	extends BaseQuery<UpdateQuery.Internal>
	implements Query<UpdateQuery.Internal>, Cloneable {

	private Internal internal;

	Mode mode = Mode.PLAIN;
	String[] columns;
	String table;
	List<Condition> conditions = new LinkedList<>();
	List<Ordering> orderBys = new LinkedList<>();
	int limit = Integer.MIN_VALUE;
	int offset = Integer.MIN_VALUE;

	public UpdateQuery(String table) {
		this.table = table;
	}

	public static UpdateQuery update(String table) {
		return new UpdateQuery(table);
	}

	public static UpdateQuery updateIgnore(String table) {
		return update(table)
			.setMode(Mode.IGNORE);
	}

	public static UpdateQuery updateSingleIgnore(String table) {
		return update(table)
			.setMode(Mode.IGNORE)
			.onlyOne();
	}

	public static UpdateQuery updateSpecificColumns(String table, String... columns) {
		UpdateQuery updateQuery = new UpdateQuery(table);
		updateQuery.columns = columns;
		return updateQuery;
	}

	public static UpdateQuery updateSingleSpecificColumns(String table, String... columns) {
		return updateSpecificColumns(table, columns)
			.onlyOne();
	}

	public static UpdateQuery updateIgnoreSpecificColumns(String table, String... columns) {
		return updateSpecificColumns(table, columns)
			.setMode(Mode.IGNORE);
	}

	public static UpdateQuery updateIgnoreSingeSpecificColumns(String table, String... columns) {
		return updateSpecificColumns(table, columns)
			.setMode(Mode.IGNORE)
			.onlyOne();
	}

	public UpdateQuery setMode(Mode mode) {
		this.mode = mode;
		return this;
	}

	public UpdateQuery addCondition(Condition condition) {
		conditions.add(condition);
		return this;
	}

	public UpdateQuery orderByAscending(String table, String column) {
		orderBys.add(Ordering.orderByAscending(new FieldColumn(table, column)));
		return this;
	}

	public UpdateQuery orderByDescending(String table, String column) {
		orderBys.add(Ordering.orderByDescending(new FieldColumn(table, column)));
		return this;
	}

	public UpdateQuery onlyOne() {
		return setLimit(1);
	}

	public UpdateQuery setLimit(int limit) {
		if (limit <= 0) {
			limit = Integer.MIN_VALUE;
		}
		this.limit = limit;
		return this;
	}

	public UpdateQuery setOffset(int offset) {
		if (offset <= 0) {
			offset = Integer.MIN_VALUE;
		}
		this.offset = offset;
		return this;
	}

	@Override
	@SuppressWarnings({ "MethodDoesntCallSuperMethod" })
	public UpdateQuery clone() {
		UpdateQuery updateQuery = new UpdateQuery(table);
		updateQuery.mode = this.mode;
		updateQuery.columns = this.columns != null ? this.columns.clone() : null;
		updateQuery.conditions = new LinkedList<>(this.conditions);
		updateQuery.orderBys = new LinkedList<>(this.orderBys);
		updateQuery.limit = this.limit;
		updateQuery.offset = this.offset;
		return updateQuery;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		UpdateQuery that = (UpdateQuery) o;
		return new EqualsBuilder()
			.append(limit, that.limit)
			.append(offset, that.offset)
			.append(internal, that.internal)
			.append(mode, that.mode)
			.append(columns, that.columns)
			.append(table, that.table)
			.append(conditions, that.conditions)
			.append(orderBys, that.orderBys)
			.isEquals();
	}

	@Override
	public Internal internal() {
		if (internal == null) {
			internal = new Internal();
		}
		return internal;
	}

	public class Internal implements InternalQuery {
		public Mode getMode() {
			return UpdateQuery.this.mode;
		}

		public String[] getColumns() {
			return UpdateQuery.this.columns != null ? UpdateQuery.this.columns.clone() : null;
		}

		@Override
		public String getTable() {
			return UpdateQuery.this.table;
		}

		public List<Condition> getConditions() {
			return Collections.unmodifiableList(UpdateQuery.this.conditions);
		}

		public List<Ordering> getOrderBys() {
			return Collections.unmodifiableList(UpdateQuery.this.orderBys);
		}

		public int getLimit() {
			return UpdateQuery.this.limit;
		}

		public int getOffset() {
			return UpdateQuery.this.offset;
		}
	}
}
