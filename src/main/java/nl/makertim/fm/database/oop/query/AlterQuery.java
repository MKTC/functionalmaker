package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.core.BiSet;
import nl.makertim.fm.database.oop.constructs.ColumnDefinition;
import org.apache.commons.lang3.builder.EqualsBuilder;

import java.util.*;

public class AlterQuery
	extends BaseQuery<AlterQuery.Internal>
	implements Cloneable, Query<AlterQuery.Internal> {

	private Internal internal;

	String table;
	String newTableName;
	Map<String, String> renames = new LinkedHashMap<>();
	List<ColumnDefinition<?>> newColumns = new LinkedList<>();
	List<String> droppedColumns = new LinkedList<>();

	private AlterQuery(String table) {
		this.table = table;
	}

	public AlterQuery(String tableName, String newTableName) {
		this(tableName);
		this.newTableName = newTableName;
	}

	@SafeVarargs
	public AlterQuery(String tableName, BiSet<String, String>... renames) {
		this(tableName);
		Arrays.stream(renames).forEach(rename -> this.renames.put(rename.getT1(), rename.getT2()));
	}

	public AlterQuery(String tableName, ColumnDefinition<?>... newColumns) {
		this(tableName);
		this.newColumns.addAll(Arrays.asList(newColumns));
	}

	public AlterQuery(String tableName, String... droppedColumns) {
		this(tableName);
		this.droppedColumns.addAll(Arrays.asList(droppedColumns));
	}

	public static AlterQuery alterName(String oldTableName, String newTableName) {
		return new AlterQuery(oldTableName, newTableName);
	}

	@SafeVarargs
	public static AlterQuery renameColumns(String tableName, BiSet<String, String>... renames) {
		return new AlterQuery(tableName, renames);
	}

	public static AlterQuery addColumn(String tableName, ColumnDefinition<?> newColumn) {
		return addColumns(tableName, newColumn);
	}

	public static AlterQuery addColumns(String tableName, ColumnDefinition<?>... newColumns) {
		return new AlterQuery(tableName, newColumns);
	}

	public static AlterQuery dropColumn(String tableName, String columnsName) {
		return dropColumns(tableName, columnsName);
	}

	public static AlterQuery dropColumns(String tableName, String... columnsNames) {
		return new AlterQuery(tableName, columnsNames);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		AlterQuery that = (AlterQuery) o;
		return new EqualsBuilder()
			.append(table, that.table)
			.append(newTableName, that.newTableName)
			.append(renames, that.renames)
			.append(newColumns, that.newColumns)
			.append(droppedColumns, that.droppedColumns)
			.isEquals();
	}

	@Override
	@SuppressWarnings("MethodDoesntCallSuperMethod")
	public AlterQuery clone() {
		AlterQuery alterQuery = new AlterQuery(table);
		alterQuery.table = this.table;
		alterQuery.newTableName = this.newTableName;
		alterQuery.renames = this.renames;
		alterQuery.newColumns = this.newColumns;
		alterQuery.droppedColumns = this.droppedColumns;
		return alterQuery;
	}

	@Override
	public Internal internal() {
		if (internal == null) {
			internal = new Internal();
		}
		return internal;
	}

	public class Internal implements InternalQuery  {
		@Override
		public String getTable() {
			return AlterQuery.this.table;
		}

		public String getNewTableName() {
			return AlterQuery.this.newTableName;
		}

		public Map<String, String> getRenames() {
			return Collections.unmodifiableMap(AlterQuery.this.renames);
		}

		public List<ColumnDefinition<?>> getNewColumns() {
			return Collections.unmodifiableList(AlterQuery.this.newColumns);
		}

		public List<String> getDroppedColumns() {
			return Collections.unmodifiableList(AlterQuery.this.droppedColumns);
		}
	}
}
