package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.database.oop.constructs.Condition;
import nl.makertim.fm.database.oop.constructs.FieldColumn;
import nl.makertim.fm.database.oop.constructs.Ordering;
import org.apache.commons.lang3.builder.EqualsBuilder;

import java.util.LinkedList;
import java.util.List;

public class DeleteQuery
	extends BaseQuery<DeleteQuery.Internal>
	implements Query<DeleteQuery.Internal>, Cloneable {

	private Internal internal;

	String table;
	List<Condition> conditions = new LinkedList<>();
	List<Ordering> orderBys = new LinkedList<>();
	int limit = Integer.MIN_VALUE;
	int offset = Integer.MIN_VALUE;

	public DeleteQuery(String table) {
		this.table = table;
	}

	public static DeleteQuery truncate(String table) {
		return new DeleteQuery(table);
	}

	public static DeleteQuery deleteWhere(String table, Condition condition) {
		return truncate(table)
			.addCondition(condition);
	}

	public static DeleteQuery deleteFirst(String table) {
		return truncate(table)
			.onlyOne();
	}

	public static DeleteQuery deleteFirstFromOrderAscending(String table, String column) {
		return truncate(table)
			.orderByAscending(table, column)
			.onlyOne();
	}

	public static DeleteQuery deleteFirstFromOrderDescending(String table, String column) {
		return truncate(table)
			.orderByDescending(table, column)
			.onlyOne();
	}

	public static DeleteQuery deleteFirstWhere(String table, Condition condition) {
		return truncate(table)
			.addCondition(condition)
			.onlyOne();
	}

	public DeleteQuery addCondition(Condition condition) {
		conditions.add(condition);
		return this;
	}

	public DeleteQuery orderByAscending(String table, String column) {
		orderBys.add(Ordering.orderByAscending(new FieldColumn(table, column)));
		return this;
	}

	public DeleteQuery orderByDescending(String table, String column) {
		orderBys.add(Ordering.orderByDescending(new FieldColumn(table, column)));
		return this;
	}

	public DeleteQuery onlyOne() {
		return setLimit(1);
	}

	public DeleteQuery setLimit(int limit) {
		if (limit <= 0) {
			limit = Integer.MIN_VALUE;
		}
		this.limit = limit;
		return this;
	}

	public DeleteQuery setOffset(int offset) {
		if (offset <= 0) {
			offset = Integer.MIN_VALUE;
		}
		this.offset = offset;
		return this;
	}

	@Override
	public Internal internal() {
		if (internal == null) {
			internal = new Internal();
		}
		return internal;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		DeleteQuery that = (DeleteQuery) o;
		return new EqualsBuilder()
			.append(limit, that.limit)
			.append(offset, that.offset)
			.append(table, that.table)
			.append(orderBys, that.orderBys)
			.append(conditions, that.conditions)
			.isEquals();
	}

	@Override
	@SuppressWarnings({ "MethodDoesntCallSuperMethod" })
	public DeleteQuery clone() {
		DeleteQuery query = new DeleteQuery(table);
		query.conditions = this.conditions;
		query.orderBys = this.orderBys;
		query.limit = this.limit;
		query.offset = this.offset;
		return query;
	}

	public class Internal implements InternalQuery {
		@Override
		public String getTable() {
			return DeleteQuery.this.table;
		}

		public List<Condition> getConditions() {
			return DeleteQuery.this.conditions;
		}

		public List<Ordering> getOrderBys() {
			return DeleteQuery.this.orderBys;
		}

		public int getLimit() {
			return DeleteQuery.this.limit;
		}

		public int getOffset() {
			return DeleteQuery.this.offset;
		}
	}
}
