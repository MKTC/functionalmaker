package nl.makertim.fm.database.oop.constructs;

import nl.makertim.fm.database.oop.SQLBuilder;

import java.util.function.Function;

public class ConditionCustom implements Condition {

	protected final Function<SQLBuilder, String> parser;

	public ConditionCustom(Function<SQLBuilder, String> parser) {
		this.parser = parser;
	}

	public String parseQuery(SQLBuilder sqlBuilder){
		return parser.apply(sqlBuilder);
	}
}
