package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.database.oop.constructs.ColumnDefinition;
import org.apache.commons.lang3.builder.EqualsBuilder;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class CreateQuery
	extends BaseQuery<CreateQuery.Internal>
	implements Cloneable, Query<CreateQuery.Internal> {

	private Internal internal;

	boolean ignoreExisting;
	String table;
	List<ColumnDefinition<?>> columns = new LinkedList<>();
	List<String> primaryColumns = new LinkedList<>();
	List<String> keyColumns = new LinkedList<>();

	public CreateQuery(String table, ColumnDefinition<?>... columnDefinitions) {
		this.table = table;
		columns.addAll(Arrays.asList(columnDefinitions));
	}

	public static CreateQuery create(String table, ColumnDefinition<?>... columnDefinitions) {
		return new CreateQuery(table, columnDefinitions);
	}

	public CreateQuery setIgnoreExisting(boolean shouldIgnore) {
		this.ignoreExisting = shouldIgnore;
		return this;
	}

	public CreateQuery setIgnoreExisting() {
		return setIgnoreExisting(true);
	}

	public CreateQuery addColumn(ColumnDefinition<?> columnDefinition) {
		columns.add(columnDefinition);
		return this;
	}

	public CreateQuery addPrimaryColumns(String... primaryColumns) {
		Arrays.stream(primaryColumns)
			.forEach(this::checkColumnExists);
		this.primaryColumns.addAll(Arrays.asList(primaryColumns));
		return this;
	}

	public CreateQuery addIndexedColumns(String... keyColumns) {
		Arrays.stream(keyColumns)
			.forEach(this::checkColumnExists);
		this.keyColumns.addAll(Arrays.asList(keyColumns));
		return this;
	}

	private void checkColumnExists(String column) {
		var columnsNames = columns.stream()
			.map(ColumnDefinition::getName);
		if (columnsNames.noneMatch(columns -> columns.equals(column))) {
			throw new IllegalArgumentException("Column " + column + " is not defined yet, can't add it as an index");
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CreateQuery that = (CreateQuery) o;
		return new EqualsBuilder()
			.append(ignoreExisting, that.ignoreExisting)
			.append(table, that.table)
			.append(columns, that.columns)
			.append(primaryColumns, that.primaryColumns)
			.append(keyColumns, that.keyColumns)
			.isEquals();
	}

	@Override
	@SuppressWarnings("MethodDoesntCallSuperMethod")
	public CreateQuery clone() {
		CreateQuery createQuery = new CreateQuery(table);
		createQuery.ignoreExisting = this.ignoreExisting;
		createQuery.columns = this.columns;
		createQuery.primaryColumns = this.primaryColumns;
		createQuery.keyColumns = this.keyColumns;
		return createQuery;
	}

	@Override
	public Internal internal() {
		if (internal == null) {
			internal = new Internal();
		}
		return internal;
	}

	public class Internal implements InternalQuery {
		public boolean isIgnoreExisting() {
			return CreateQuery.this.ignoreExisting;
		}

		@Override
		public String getTable() {
			return CreateQuery.this.table;
		}

		public List<ColumnDefinition<?>> getColumns() {
			return Collections.unmodifiableList(CreateQuery.this.columns);
		}

		public List<String> getPrimaryColumns() {
			return Collections.unmodifiableList(CreateQuery.this.primaryColumns);
		}

		public List<String> getKeyColumns() {
			return Collections.unmodifiableList(CreateQuery.this.keyColumns);
		}
	}
}
