package nl.makertim.fm.database.oop.constructs;

import nl.makertim.fm.core.BiSet;
import nl.makertim.fm.database.oop.query.SelectQuery;
import org.apache.commons.lang3.builder.EqualsBuilder;

import static nl.makertim.fm.database.oop.constructs.Comparison.*;
import static nl.makertim.fm.database.oop.constructs.WhereBetween.whereBetween;
import static nl.makertim.fm.database.oop.constructs.WhereIn.whereIn;
import static nl.makertim.fm.database.oop.constructs.WhereInSubQuery.whereInSubQuery;
import static nl.makertim.fm.database.oop.constructs.WhereNotNull.whereNotNull;
import static nl.makertim.fm.database.oop.constructs.WhereNull.whereNull;

public class Where<Type> implements Condition {

	private final String table;
	private final String column;
	private final Comparison comparison;
	private final Type var;

	public static Where<String> biggerThan(String table, String column, String var) {
		return new Where<>(table, column, BIGGER_THEN, var);
	}

	public static Where<String> biggerThanOrEqual(String table, String column, String var) {
		return new Where<>(table, column, BIGGER_OR_EQUAL_THEN, var);
	}

	public static Where<String> smallerThan(String table, String column, String var) {
		return new Where<>(table, column, SMALLER_THEN, var);
	}

	public static Where<String> smallerThanOrEqual(String table, String column, String var) {
		return new Where<>(table, column, SMALLER_OR_EQUAL_THEN, var);
	}

	public static Condition inSubQuery(String table, String column, SelectQuery query) {
		return whereInSubQuery(table, column, query);
	}

	public static Where<?> isNull(String table, String column) {
		return whereNull(table, column);
	}

	public static Where<?> isNotNull(String table, String column) {
		return whereNotNull(table, column);
	}

	public static Where<String> like(String table, String column, String var) {
		return new Where<>(table, column, LIKE, var);
	}

	public static Where<BiSet<String, String>> between(String table, String column, String varLow, String varHigh) {
		return whereBetween(table, column, varLow, varHigh);
	}

	public static Where<String[]> in(String table, String column, String... vars) {
		return whereIn(table, column, vars);
	}

	public static Where<String> equalTo(String table, String column, String var) {
		return new Where<>(table, column, EQUAL, var);
	}

	public static Where<String> notEqualTo(String table, String column, String var) {
		return new Where<>(table, column, NOT, var);
	}

	protected Where(String table, String column, Comparison comparison, Type var) {
		this.table = table;
		this.column = column;
		this.comparison = comparison;
		this.var = validateVar(var);
	}

	public String getTable() {
		return table;
	}

	public String getColumn() {
		return column;
	}

	public Comparison getComparison() {
		return comparison;
	}

	public Type getVar() {
		return var;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Where<?> where = (Where<?>) o;
		return new EqualsBuilder()
			.append(table, where.table)
			.append(column, where.column)
			.append(comparison, where.comparison)
			.append(var, where.var)
			.isEquals();
	}

	protected static <T> T[] validateVars(T[] vars) {
		for (int i = 0; i < vars.length; i++) {
			vars[i] = validateVar(vars[i]);
		}
		return vars;
	}

	@SuppressWarnings("unchecked")
	protected static <T> T validateVar(T var) {
		if (var instanceof String && !((String) var).startsWith(":")) {
			var = (T) (":" + var);
		}
		return (T) var;
	}
}
