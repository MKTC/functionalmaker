package nl.makertim.fm.database.oop;

import nl.makertim.fm.database.oop.dialects.MySQLDialect;
import nl.makertim.fm.database.oop.dialects.SQLiteDialect;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class DialectManager {

	private static final LinkedList<Dialect> registeredDialects = new LinkedList<>();

	static {
		registerDialect(new MySQLDialect());
		registerDialect(new SQLiteDialect());
	}

	public static void registerDialect(Dialect dialect) {
		registeredDialects.addFirst(dialect);
	}

	public static Optional<Dialect> findDialectOnJDBC(String jdbc) {
		return registeredDialects
			.stream()
			.filter(dialect -> dialect.canHandleThisJDBC(jdbc))
			.findFirst();
	}

	public static List<Dialect> getRegisteredDialects() {
		return Collections.unmodifiableList(registeredDialects);
	}
}
