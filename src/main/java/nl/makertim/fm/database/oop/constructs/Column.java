package nl.makertim.fm.database.oop.constructs;

public interface Column {

	String getTable();

	static VarColumn var(String var) {
		return new VarColumn(var);
	}

	static TableColumn table(String table) {
		return new TableColumn(table);
	}

	static RecursiveFunctionColumn recursiveFunction(Function function, FunctionColumnInterface column) {
		return RecursiveFunctionColumn.overFunction(function, column);
	}

	static FunctionColumn function(String table, String column, Function function, String alias) {
		return new FunctionColumn(table, column, function, alias);
	}

	static FieldColumn field(String table, String column) {
		return new FieldColumn(table, column);
	}

	static ConstantColumn constant(Object constant) {
		return new ConstantColumn(constant);
	}
}
