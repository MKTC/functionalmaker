package nl.makertim.fm.database.oop.constructs;

public enum Mode {

	PLAIN, REPLACE, IGNORE, ABORT, ROLLBACK

}
