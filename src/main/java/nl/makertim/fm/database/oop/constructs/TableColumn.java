package nl.makertim.fm.database.oop.constructs;

import org.apache.commons.lang3.builder.EqualsBuilder;

public class TableColumn implements Column {

	private final String table;

	public TableColumn(String table) {
		this.table = table;
	}

	@Override
	public String getTable() {
		return table;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TableColumn that = (TableColumn) o;
		return new EqualsBuilder()
			.append(table, that.table)
			.isEquals();
	}
}
