package nl.makertim.fm.database.oop.constructs;

public enum ConditionCombiner {
	OR,
	AND,
}
