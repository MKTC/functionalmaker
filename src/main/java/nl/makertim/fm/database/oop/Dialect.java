package nl.makertim.fm.database.oop;

import nl.makertim.fm.database.oop.query.Query;
import nl.makertim.fm.safe.SafeTry;
import org.apache.commons.lang3.builder.EqualsBuilder;

import java.util.Locale;

public abstract class Dialect {

	private final Class<? extends SQLBuilder> builderClass;

	public static <D extends Dialect> D getDialect(Class<D> dClass) {
		return SafeTry.throwAsRuntimeException(() -> dClass.getConstructor().newInstance());
	}

	public Dialect(Class<? extends SQLBuilder> builderClass) {
		this.builderClass = builderClass;
	}

	public boolean canHandleThisJDBC(String jdbc) {
		return jdbc.startsWith("jdbc:" + getName().toLowerCase(Locale.ROOT));
	}

	public String getName() {
		return getClass().getSimpleName().replaceAll("Dialect$", "");
	}

	protected Class<? extends SQLBuilder> getBuilderClass() {
		return builderClass;
	}

	protected SQLBuilder getBuilder(Query<?> query) {
		return SafeTry.throwAsRuntimeException(() -> getBuilderClass()
			.getConstructor(query.getClass())
			.newInstance(query)
		);
	}

	public PreparedQuery buildQuery(Query<?> query) {
		return getBuilder(query).build();
	}

	public PreparedQuery buildExplainQuery(Query<?> query) {
		return getBuilder(query).buildExplain();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Dialect dialect = (Dialect) o;
		return new EqualsBuilder().append(builderClass, dialect.builderClass).isEquals();
	}
}
