package nl.makertim.fm.database.oop.constructs;

import nl.makertim.fm.database.oop.SQLBuilder;
import nl.makertim.fm.database.oop.query.ResultQuery;

import java.util.function.Function;

import static nl.makertim.fm.database.oop.constructs.WhereBetween.whereBetween;
import static nl.makertim.fm.database.oop.constructs.WhereIn.whereIn;
import static nl.makertim.fm.database.oop.constructs.WhereInSubQuery.whereInSubQuery;
import static nl.makertim.fm.database.oop.constructs.WhereNotNull.whereNotNull;
import static nl.makertim.fm.database.oop.constructs.WhereNull.whereNull;

public interface Condition {

	static Where<String> biggerThan(String table, String column, String var) {
		return Where.biggerThan(table, column, var);
	}

	static Where<String> biggerThanOrEqual(String table, String column, String var) {
		return Where.biggerThanOrEqual(table, column, var);
	}

	static Where<String> smallerThan(String table, String column, String var) {
		return Where.smallerThan(table, column, var);
	}

	static Where<String> smallerThanOrEqual(String table, String column, String var) {
		return Where.smallerThanOrEqual(table, column, var);
	}

	static WhereInSubQuery inSubQuery(String table, String column, ResultQuery<?> query) {
		return whereInSubQuery(table, column, query);
	}

	static WhereNull isNull(String table, String column) {
		return whereNull(table, column);
	}

	static WhereNotNull isNotNull(String table, String column) {
		return whereNotNull(table, column);
	}

	static Where<String> like(String table, String column, String var) {
		return Where.like(table, column, var);
	}

	static WhereBetween between(String table, String column, String varLow, String varHigh) {
		return whereBetween(table, column, varLow, varHigh);
	}

	static WhereIn in(String table, String column, String... vars) {
		return whereIn(table, column, vars);
	}

	static Where<String> equalTo(String table, String column, String var) {
		return Where.equalTo(table, column, var);
	}

	static Where<String> notEqualTo(String table, String column, String var) {
		return Where.notEqualTo(table, column, var);
	}

	static And and(Condition condition, Condition... conditions) {
		return new And(condition, conditions);
	}

	static Or or(Condition condition, Condition... conditions) {
		return new Or(condition, conditions);
	}

	static ConditionCustom custom(Function<SQLBuilder, String> parser) {
		return new ConditionCustom(parser);
	}
}
