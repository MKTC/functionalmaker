package nl.makertim.fm.database.oop.constructs;

import org.apache.commons.lang3.builder.EqualsBuilder;

public class FieldColumn implements Column {

	private final String table;
	private final String column;

	public FieldColumn(String column) {
		this(null, column);
	}

	public FieldColumn(String table, String column) {
		this.table = table;
		this.column = column;
	}

	@Override
	public String getTable() {
		return table;
	}

	public String getColumn() {
		return column;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		FieldColumn that = (FieldColumn) o;
		return new EqualsBuilder()
			.append(table, that.table)
			.append(column, that.column)
			.isEquals();
	}
}
