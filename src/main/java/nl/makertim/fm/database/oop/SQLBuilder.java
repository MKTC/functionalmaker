package nl.makertim.fm.database.oop;

import nl.makertim.fm.database.oop.constructs.*;
import nl.makertim.fm.database.oop.query.*;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.Year;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("unused")
public abstract class SQLBuilder {

	protected final Map<Class<? extends Column>, BiFunction<SQLBuilder, Column, String>> columnFunctions = new LinkedHashMap<>(
		Map.ofEntries(
			Map.entry(FieldColumn.class, (builder, column) -> builder.buildStringColumn((FieldColumn) column)),
			Map.entry(TableColumn.class, (builder, column) -> builder.buildTableColumn((TableColumn) column)),
			Map.entry(FunctionColumn.class, (builder, column) -> builder.buildFunctionColumn((FunctionColumn) column)),
			Map.entry(ConstantColumn.class, (builder, column) -> builder.buildConstantColumn((ConstantColumn) column)),
			Map.entry(VarColumn.class, (builder, column) -> builder.buildVarColumn((VarColumn) column)),
			Map.entry(RecursiveFunctionColumn.class, (builder, column) -> builder.buildRecursiveFunctionColumn((RecursiveFunctionColumn) column))
		)
	);
	protected Map<Class<?>, String> typeMap = new LinkedHashMap<>(
		Map.ofEntries(
			Map.entry(String.class, "UNKNOWN"),
			Map.entry(Integer.class, "UNKNOWN"),
			Map.entry(int.class, "UNKNOWN"),
			Map.entry(Long.class, "UNKNOWN"),
			Map.entry(long.class, "UNKNOWN"),
			Map.entry(Double.class, "UNKNOWN"),
			Map.entry(double.class, "UNKNOWN"),
			Map.entry(Float.class, "UNKNOWN"),
			Map.entry(float.class, "UNKNOWN"),
			Map.entry(StringBuilder.class, "UNKNOWN"),
			Map.entry(Character[].class, "UNKNOWN"),
			Map.entry(char[].class, "UNKNOWN"),
			Map.entry(Byte[].class, "UNKNOWN"),
			Map.entry(byte[].class, "UNKNOWN"),
			Map.entry(java.sql.Date.class, "UNKNOWN"),
			Map.entry(Timestamp.class, "UNKNOWN"),
			Map.entry(Year.class, "UNKNOWN"),
			Map.entry(Time.class, "UNKNOWN"),
			Map.entry(java.util.Date.class, "UNKNOWN"),
			Map.entry(Enum.class, "UNKNOWN")
		)
	);

	protected final Query<?> query;
	protected final long startTimeNs;
	protected final StringBuilder sqlStringBuilder = new StringBuilder(1024);
	protected final BidiMap<String, String> tableAlias = new TreeBidiMap<>();

	protected SQLBuilder(Query<?> query) {
		this.query = query;
		this.startTimeNs = System.nanoTime();
		mergeTypes();
		overwriteColumnFunctions();
	}

	public SQLBuilder(AlterQuery query) {
		this((Query<?>) query);
		buildAlterQuery(query);
	}

	public SQLBuilder(CreateQuery query) {
		this((Query<?>) query);
		buildCreateQuery(query);
	}

	public SQLBuilder(DeleteQuery query) {
		this((Query<?>) query);
		buildDeleteQuery(query);
	}

	public SQLBuilder(DescribeQuery query) {
		this((Query<?>) query);
		buildDescribeQuery(query);
	}

	public SQLBuilder(DropQuery query) {
		this((Query<?>) query);
		buildDropQuery(query);
	}

	public SQLBuilder(InsertQuery query) {
		this((Query<?>) query);
		buildInsertQuery(query);
	}

	public SQLBuilder(SelectQuery query) {
		this((Query<?>) query);
		buildSelectQuery(query);
	}

	public SQLBuilder(UpdateQuery query) {
		this((Query<?>) query);
		buildUpdateQuery(query);
	}

	public PreparedQuery build() {
		Class<? extends Dialect> dialectClass = getDialect();
		if (query.hasCache(dialectClass)) {
			return query.getCache(dialectClass);
		}

		PreparedQuery preparedQuery = new BasePreparedQuery(
			sqlStringBuilder.toString(),
			query instanceof ResultQuery
		).setBuildTime(System.nanoTime() - this.startTimeNs);
		query.setCache(getDialect(), preparedQuery);
		return preparedQuery;
	}

	public PreparedQuery buildExplain() {
		sqlStringBuilder.insert(0, "EXPLAIN ");
		return build();
	}

	public abstract Class<? extends Dialect> getDialect();

	protected abstract SQLBuilder buildAlterQuery(AlterQuery query);

	protected abstract SQLBuilder buildCreateQuery(CreateQuery query);

	protected abstract SQLBuilder buildDeleteQuery(DeleteQuery query);

	protected abstract SQLBuilder buildDescribeQuery(DescribeQuery query);

	protected abstract SQLBuilder buildDropQuery(DropQuery query);

	protected abstract SQLBuilder buildInsertQuery(InsertQuery query);

	protected abstract SQLBuilder buildSelectQuery(SelectQuery query);

	protected abstract SQLBuilder buildUpdateQuery(UpdateQuery query);

	protected String buildColumn(Column column) {
		return handleSubFunction(columnFunctions, column);
	}

	protected abstract String buildStringColumn(FieldColumn column);

	protected abstract String buildTableColumn(TableColumn column);

	protected abstract String buildFunctionColumn(FunctionColumn column);

	protected String buildConstantColumn(ConstantColumn column) {
		var constant = column.getColumn();
		if (constant instanceof Number) {
			// interpret numbers as safe
			return constant.toString();
		} else if (constant instanceof Boolean) {
			// parse booleans as number, almost all databases do and otherwise they parse.
			return (Boolean) constant ? "1" : "0";
		}
		return escapeVariable(constant.toString());
	}

	protected String buildVarColumn(VarColumn column) {
		return column.getColumn();
	}

	protected abstract String buildRecursiveFunctionColumn(RecursiveFunctionColumn column);

	protected String buildFunction(Function function) {
		return function.toString().toUpperCase(Locale.ROOT);
	}

	protected String buildCondition(Condition condition) {
		if (condition instanceof ConditionCustom) {
			return ((ConditionCustom) condition).parseQuery(this);
		} else if (condition instanceof CombineCondition) {
			return buildCondition((CombineCondition) condition);
		}
		return buildCondition((Where<?>) condition);
	}

	protected String buildCondition(CombineCondition combineCondition) {
		return
			"(" + String.join("\n" + buildConditionCombiner(combineCondition.getJoiner()) + "\n",
				Arrays.stream(combineCondition.getConditions())
					.map(condition -> "(" + buildCondition(condition) + ")")
					.toArray(String[]::new)
			) + ")";
	}

	protected abstract String buildCondition(Where<?> condition);

	protected String buildConditionCombiner(ConditionCombiner conditionCombiner) {
		return conditionCombiner.toString();
	}

	protected String buildComparison(Comparison comparison) {
		if (comparison == null) {
			return "";
		}
		switch (comparison) {
			case BIGGER_THEN:
				return ">";
			case BIGGER_OR_EQUAL_THEN:
				return ">=";
			case SMALLER_THEN:
				return "<";
			case SMALLER_OR_EQUAL_THEN:
				return "<=";
			case IN:
				return "IN";
			case EQUAL:
			default:
				return "=";
			case NOT:
				return "<>";
			case LIKE:
				return "LIKE";
			case BETWEEN:
				return "BETWEEN";
		}
	}

	protected String buildWhere(Collection<Condition> conditions) {
		if (conditions.isEmpty()) {
			return "";
		}
		return "\nWHERE " + conditions
			.stream()
			.map(this::buildCondition)
			.collect(Collectors.joining("\nAND "));
	}

	protected String buildOrderBy(Collection<Ordering> orderBys) {
		if (orderBys.isEmpty()) {
			return "";
		}
		return "\nORDER BY " + orderBys
			.stream()
			.map(ordering -> buildColumn(ordering.getColumn()) + buildOrderingDirection(ordering.getDirection()))
			.collect(Collectors.joining(", "));
	}

	protected String buildLimit(int limit, int offset) {
		if (limit == Integer.MIN_VALUE) {
			return "";
		}
		return "\nLIMIT " + limit +
			(offset != Integer.MIN_VALUE ? ", " + offset : "");
	}

	protected String buildJoin(JoinType joinType) {
		return joinType.toString() + " JOIN";
	}

	protected String buildOrderingDirection(OrderingDirection orderingDirection) {
		switch (orderingDirection) {
			default:
			case ASCENDING:
				return "ASC";
			case DESCENDING:
				return "DESC";
		}
	}

	protected String buildUpdateMode(Mode mode) {
		if (mode == Mode.PLAIN) return "";
		return "OR " + mode.name() + " ";
	}

	protected String buildColumnDescriptions(List<ColumnDefinition<?>> columns) {
		return "("
			+ columns.stream()
			.map(this::buildColumnDescription)
			.collect(Collectors.joining(", "))
			+ ")";
	}

	protected abstract String buildColumnDescription(ColumnDefinition<?> column);

	protected abstract String buildColumnType(Class<?> clazz);

	private <T, R> R handleSubFunction(Map<Class<? extends T>, BiFunction<SQLBuilder, T, R>> map, T type) {
		var action = map.get(type.getClass());
		if (action == null) {
			throw new RuntimeException("SQLBuilder " + getClass().getSimpleName() + " cannot handle " + type.getClass().getSimpleName());
		}
		return action.apply(this, type);
	}

	protected String escapeTable(String table, String suffix) {
		String escapedTable = escapeTable(table);
		if (escapedTable.isEmpty()) {
			return escapedTable;
		}
		return escapedTable + suffix;
	}

	protected String escapeTable(String table) {
		if (table == null) {
			return "";
		}

		Object internal = query.internal();
		if (internal instanceof TableShortInternal) {
			Map<String, String> tableShorts = ((TableShortInternal) internal).getTableShorts();
			String tableShort = tableShorts.get(table);
			if (tableShort == null || tableShort.equals("")) return "";
			table = tableShort;
		}
		return escapeColumn(table);
	}

	protected String escapeColumn(String column, String suffix) {
		String escapedColumn = escapeColumn(column);
		if (escapedColumn.isEmpty()) {
			return escapedColumn;
		}
		return escapedColumn + suffix;
	}

	protected String escapeColumn(String column) {
		if (column == null || column.equals("")) return "";
		return '`' + column.replaceAll("`", "") + '`';
	}

	protected String escapeVariable(String var) {
		if (var == null) return "NULL";
		if (var.equals("")) return "";
		return '"' + var.replaceAll("\"", "") + '"';
	}

	protected void stringStreamToString(StringBuilder builder, String delimiter, Stream<String> stream) {
		builder.append(stringStreamToString(delimiter, stream));
	}

	protected String stringStreamToString(String delimiter, Stream<String> stream) {
		return stream.collect(Collectors.joining(delimiter));
	}

	protected abstract Map<Class<?>, String> overwriteTypes();

	protected void mergeTypes() {
		typeMap.putAll(overwriteTypes());
	}

	protected void overwriteColumnFunctions() {
	}
}
