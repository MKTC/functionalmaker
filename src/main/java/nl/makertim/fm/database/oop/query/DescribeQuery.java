package nl.makertim.fm.database.oop.query;

import org.apache.commons.lang3.builder.EqualsBuilder;

public class DescribeQuery
	extends BaseQuery<DescribeQuery.Internal>
	implements Query<DescribeQuery.Internal>, ResultQuery<DescribeQuery.Internal>, Cloneable {

	private Internal internal;

	String table;

	public DescribeQuery(String table) {
		this.table = table;
	}

	public static DescribeQuery describe(String tableName) {
		return new DescribeQuery(tableName);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		DescribeQuery that = (DescribeQuery) o;
		return new EqualsBuilder()
			.append(table, that.table)
			.isEquals();
	}

	@Override
	@SuppressWarnings("MethodDoesntCallSuperMethod")
	public DescribeQuery clone() {
		return new DescribeQuery(table);
	}

	@Override
	public Internal internal() {
		if (internal == null) {
			internal = new Internal();
		}
		return internal;
	}

	public class Internal implements InternalQuery {
		@Override
		public String getTable() {
			return DescribeQuery.this.table;
		}
	}
}
