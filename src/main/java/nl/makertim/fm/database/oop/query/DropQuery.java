package nl.makertim.fm.database.oop.query;

import org.apache.commons.lang3.builder.EqualsBuilder;

public class DropQuery
	extends BaseQuery<DropQuery.Internal>
	implements Query<DropQuery.Internal>, Cloneable {

	private Internal internal;

	String table;
	boolean ignoreIfNotExists;

	public DropQuery(String table) {
		this.table = table;
	}

	public static DropQuery drop(String tableName) {
		return new DropQuery(tableName);
	}

	public static DropQuery drop(String tableName, boolean ignoreIfNotExists) {
		return new DropQuery(tableName).ignoreIfNotExists(ignoreIfNotExists);
	}

	public static DropQuery dropIgnore(String tableName) {
		return new DropQuery(tableName).ignoreIfNotExists();
	}

	public DropQuery ignoreIfNotExists(boolean shouldIgnore) {
		ignoreIfNotExists = shouldIgnore;
		return this;
	}

	public DropQuery ignoreIfNotExists() {
		return ignoreIfNotExists(true);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		DropQuery that = (DropQuery) o;
		return new EqualsBuilder()
			.append(table, that.table)
			.append(ignoreIfNotExists, that.ignoreIfNotExists)
			.isEquals();
	}

	@Override
	@SuppressWarnings("MethodDoesntCallSuperMethod")
	public DropQuery clone() {
		DropQuery dropQuery = new DropQuery(table);
		dropQuery.ignoreIfNotExists = ignoreIfNotExists;
		return dropQuery;
	}

	@Override
	public Internal internal() {
		if (internal == null) {
			internal = new Internal();
		}
		return internal;
	}

	public class Internal implements InternalQuery {
		public boolean ignoreIfNotExists() {
			return DropQuery.this.ignoreIfNotExists;
		}

		@Override
		public String getTable() {
			return DropQuery.this.table;
		}
	}
}
