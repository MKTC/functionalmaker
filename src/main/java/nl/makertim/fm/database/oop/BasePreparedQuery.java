package nl.makertim.fm.database.oop;

import nl.makertim.fm.core.Property;
import nl.makertim.fm.database.core.SafeResultSet;
import nl.makertim.fm.database.core.SafeResultSetExecute;
import nl.makertim.fm.safe.SafeTry;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BasePreparedQuery implements PreparedQuery {

	private final boolean isResultingQuery;
	private final String sql;
	private final String realSQL;
	private final String[] placeholders;

	private long buildTimeNs = -1;
	private long lastPrepareNs = -1;
	private long lastExecuteNs = -1;

	protected BasePreparedQuery(BasePreparedQuery base) {
		this.isResultingQuery = base.isResultingQuery;
		this.sql = base.sql;
		this.realSQL = base.realSQL;
		this.placeholders = base.placeholders.clone();
		this.buildTimeNs = base.buildTimeNs;
		this.lastPrepareNs = base.lastPrepareNs;
		this.lastExecuteNs = base.lastExecuteNs;
	}

	public BasePreparedQuery(String sql, boolean isResultingQuery) {
		this.isResultingQuery = isResultingQuery;
		this.sql = sql;

		Pattern p = Pattern.compile("(:[^\\s),]+)");
		Matcher m = p.matcher(sql);
		StringBuilder realSQL = new StringBuilder();
		List<String> orderedVariables = new LinkedList<>();
		while (m.find()) {
			orderedVariables.add(m.group(1));
			m.appendReplacement(realSQL, "?");
		}
		m.appendTail(realSQL);

		this.placeholders = orderedVariables.toArray(String[]::new);
		this.realSQL = realSQL.toString();
	}

	@Override
	public SafeResultSet execute(Connection connection, Map<String, Object> dirtyVariables) {
		Property<Boolean> executeDataProperty = new Property<>(null);
		final Map<String, Object> variables = cleanup(dirtyVariables);
		long startNs = System.nanoTime();

		Arrays.stream(placeholders).forEach(placeholder -> {
			if (!variables.containsKey(placeholder)) {
				throw new RuntimeException("Query \"" + getSQL() + "\"\n is missing param " + placeholder);
			}
		});

		return SafeTry.execute(() -> {
			PreparedStatement statement = connection.prepareStatement(realSQL);
			for (int i = 0; i < placeholders.length; i++) {
				statement.setObject(i + 1, variables.get(placeholders[i]));
			}
			lastPrepareNs = System.nanoTime() - startNs;

			SafeResultSet result;
			if (isResultingQuery) {
				result = new SafeResultSet(statement.executeQuery());
			} else {
				executeDataProperty.set(statement.execute());
				result = new SafeResultSetExecute(true);
			}
			lastExecuteNs = System.nanoTime() - startNs;
			return result;
		}, exception -> {
			throw new RuntimeException(
				"Error while executing SQL: " + sql.replaceAll("\\s+", " ")
					+ (executeDataProperty.isPresent() ? " -- execute has resulted in data: " + executeDataProperty.get() : "") + "\n"
					+ "\t- " + exception.getMessage(),
				exception);
		});
	}

	@Override
	public String getSQL(String suffix) {
		return sql + suffix;
	}

	@Override
	public int getPlaceholderAmount() {
		return placeholders.length;
	}

	@Override
	public int getPlaceholderDistinctAmount() {
		return (int) Arrays.stream(placeholders).distinct().count();
	}

	@Override
	public String[] getPlaceholderNames() {
		return placeholders.clone();
	}

	@Override
	public String[] getPlaceholderNamesDistinct() {
		return Arrays.stream(placeholders)
			.distinct()
			.toArray(String[]::new);
	}

	@Override
	public BasePreparedQuery setBuildTime(long timeNs) {
		if (this.buildTimeNs == -1) {
			this.buildTimeNs = timeNs;
		}
		return this;
	}

	@Override
	public double getBuildTimeMs() {
		return Math.round(this.buildTimeNs / 10000D) / 100D;
	}

	@Override
	public double getLastExecuteMs() {
		return Math.round(lastExecuteNs / 10000D) / 100D;
	}

	@Override
	public double getLastPrepareMs() {
		return Math.round(lastPrepareNs / 10000D) / 100D;
	}

	@Override
	public boolean hasResults() {
		return isResultingQuery;
	}
}
