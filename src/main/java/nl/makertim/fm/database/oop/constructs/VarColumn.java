package nl.makertim.fm.database.oop.constructs;

import org.apache.commons.lang3.builder.EqualsBuilder;

public class VarColumn implements Column {

	private final String var;

	public VarColumn(String var) {
		if (!var.startsWith(":")) {
			var = ':' + var;
		}
		this.var = var;
	}

	@Override
	public String getTable() {
		return null;
	}

	public String getColumn() {
		return var;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		VarColumn that = (VarColumn) o;
		return new EqualsBuilder()
			.append(var, that.var)
			.isEquals();
	}
}
