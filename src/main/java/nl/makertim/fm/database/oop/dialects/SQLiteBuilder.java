package nl.makertim.fm.database.oop.dialects;

import nl.makertim.fm.database.oop.Dialect;
import nl.makertim.fm.database.oop.constructs.ColumnDefinition;
import nl.makertim.fm.database.oop.constructs.Condition;
import nl.makertim.fm.database.oop.query.*;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.Year;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SQLiteBuilder extends MySQLBuilder {

	public SQLiteBuilder(AlterQuery query) {
		super(query);
	}

	public SQLiteBuilder(CreateQuery query) {
		super(query);
	}

	public SQLiteBuilder(DeleteQuery query) {
		super(query);
	}

	public SQLiteBuilder(DescribeQuery query) {
		super(query);
	}

	public SQLiteBuilder(DropQuery query) {
		super(query);
	}

	public SQLiteBuilder(InsertQuery query) {
		super(query);
	}

	public SQLiteBuilder(SelectQuery query) {
		super(query);
	}

	public SQLiteBuilder(UpdateQuery query) {
		super(query);
	}

	@Override
	protected SQLiteBuilder buildDeleteQuery(DeleteQuery query) {
		DeleteQuery.Internal internal = query.internal();

		// DELETE FROM
		sqlStringBuilder.append("DELETE FROM ");
		sqlStringBuilder.append(escapeTable(internal.getTable()));

		// WHERE & sub query for order by/limit/offset
		List<Condition> conditions = internal.getConditions();

		SelectQuery selectQuery = SelectQuery.selectFrom(internal.getTable(), "rowid");
		conditions.forEach(selectQuery::addCondition);
		internal.getOrderBys().forEach(selectQuery::orderBy);
		selectQuery.setLimit(internal.getLimit());
		selectQuery.setOffset(internal.getOffset());
		conditions.add(Condition.inSubQuery(internal.getTable(), "rowid", selectQuery));

		sqlStringBuilder.append(buildWhere(conditions));

		return this;
	}

	@Override
	protected SQLiteBuilder buildDescribeQuery(DescribeQuery query) {
		DescribeQuery.Internal internal = query.internal();

		sqlStringBuilder.append("SELECT `sql` AS `Create Table` FROM `sqlite_master` WHERE `type`='table' AND `name`=");
		sqlStringBuilder.append(escapeVariable(internal.getTable()));

		return this;
	}

	@Override
	protected String buildColumnDescription(ColumnDefinition<?> column) {
		return escapeColumn(column.getName())
			+ " " + buildColumnType(column.getType())
			+ (column.getLengthType().length == 0 ? "" : "( "
			+ Arrays.stream(column.getLengthType()).mapToObj(Integer::toString).collect(Collectors.joining(", "))
			+ " )")
			+ (column.isAutoIncrement() ? " AUTO_INCREMENT" : "")
			+ (column.isNullAllowed() ? " NULL " : " NOT NULL ")
			+ (column.getDefaultValue() != null ? " DEFAULT " + escapeVariable(column.getDefaultValue().toString()) : "");
	}

	@Override
	protected Map<Class<?>, String> overwriteTypes() {
		return Map.ofEntries(
			Map.entry(String.class, "VARCHAR"),
			Map.entry(Integer.class, "INTEGER"),
			Map.entry(int.class, "INTEGER"),
			Map.entry(Long.class, "BIGINT"),
			Map.entry(long.class, "BIGINT"),
			Map.entry(Double.class, "REAL"),
			Map.entry(double.class, "REAL"),
			Map.entry(Float.class, "REAL"),
			Map.entry(float.class, "REAL"),
			Map.entry(StringBuilder.class, "TEXT"),
			Map.entry(Character[].class, "UNKNOWN"),
			Map.entry(char[].class, "UNKNOWN"),
			Map.entry(Byte[].class, "BLOB"),
			Map.entry(byte[].class, "BLOB"),
			Map.entry(java.sql.Date.class, "DATE"),
			Map.entry(Timestamp.class, "DATETIME"),
			Map.entry(Year.class, "INTEGER"),
			Map.entry(Time.class, "TIME"),
			Map.entry(java.util.Date.class, "DATETIME"),
			Map.entry(Enum.class, "ENUM")
		);
	}

	@Override
	public Class<? extends Dialect> getDialect() {
		return SQLiteDialect.class;
	}
}
