package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.database.oop.Dialect;
import nl.makertim.fm.database.oop.PreparedQuery;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseQuery<Internal extends InternalQuery> implements Query<Internal> {

	private transient final Map<Class<? extends Dialect>, PreparedQuery> cache = new HashMap<>();

	@Override
	public void setCache(Class<? extends Dialect> dialectClass, PreparedQuery query) {
		cache.put(dialectClass, query);
	}

	@Override
	public boolean hasCache(Class<? extends Dialect> dialectClass) {
		return cache.containsKey(dialectClass);
	}

	@Override
	public PreparedQuery getCache(Class<? extends Dialect> dialectClass) {
		return cache.get(dialectClass);
	}

	@Override
	public boolean removeCache(Class<? extends Dialect> dialectClass) {
		return cache.remove(dialectClass) != null;
	}
}
