package nl.makertim.fm.database.oop.constructs;

public interface FunctionColumnInterface extends Column {

	String getAlias();

	FunctionColumnInterface setAlias(String alias);
}
