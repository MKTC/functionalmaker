package nl.makertim.fm.database.oop.constructs;

import org.apache.commons.lang3.builder.EqualsBuilder;

public class FunctionColumn implements FunctionColumnInterface {

	private final String table;
	private final String column;
	private final Function function;
	private String alias;

	public FunctionColumn(String table, String column, Function function, String alias) {
		this.table = table;
		this.column = column;
		this.function = function;
		this.alias = alias;
	}

	@Override
	public String getTable() {
		return table;
	}

	public String getColumn() {
		return column;
	}

	public Function getFunction() {
		return function;
	}

	@Override
	public String getAlias() {
		return alias;
	}

	@Override
	public FunctionColumnInterface setAlias(String alias) {
		this.alias = alias;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		FunctionColumn that = (FunctionColumn) o;
		return new EqualsBuilder()
			.append(table, that.table)
			.append(column, that.column)
			.append(function, that.function)
			.append(alias, that.alias)
			.isEquals();
	}
}
