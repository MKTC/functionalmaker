package nl.makertim.fm.database.oop.constructs;

public enum JoinType {
	LEFT, RIGHT, INNER
}
