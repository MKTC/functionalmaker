package nl.makertim.fm.database.oop.constructs;

public enum OrderingDirection {
	ASCENDING, DESCENDING
}
