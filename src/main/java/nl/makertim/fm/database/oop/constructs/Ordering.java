package nl.makertim.fm.database.oop.constructs;

import org.apache.commons.lang3.builder.EqualsBuilder;

import static nl.makertim.fm.database.oop.constructs.OrderingDirection.ASCENDING;
import static nl.makertim.fm.database.oop.constructs.OrderingDirection.DESCENDING;

public class Ordering {

	private final Column column;
	private final OrderingDirection direction;

	public static Ordering orderByAscending(Column column) {
		return new Ordering(column, ASCENDING);
	}

	public static Ordering orderByDescending(Column column) {
		return new Ordering(column, DESCENDING);
	}

	public Ordering(Column column, OrderingDirection direction) {
		this.column = column;
		this.direction = direction;
	}

	public Column getColumn() {
		return column;
	}

	public OrderingDirection getDirection() {
		return direction;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Ordering ordering = (Ordering) o;
		return new EqualsBuilder()
			.append(column, ordering.column)
			.append(direction, ordering.direction)
			.isEquals();
	}
}
