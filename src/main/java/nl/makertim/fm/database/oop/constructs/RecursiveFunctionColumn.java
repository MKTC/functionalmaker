package nl.makertim.fm.database.oop.constructs;

import org.apache.commons.lang3.builder.EqualsBuilder;

public class RecursiveFunctionColumn implements FunctionColumnInterface {

	private final Function function;
	private final FunctionColumnInterface column;

	public static RecursiveFunctionColumn overFunction(Function function, FunctionColumnInterface column) {
		return new RecursiveFunctionColumn(function, column);
	}

	public RecursiveFunctionColumn(Function function, FunctionColumnInterface column) {
		this.function = function;
		this.column = column;
	}

	@Override
	public String getTable() {
		return column.getTable();
	}

	@Override
	public String getAlias() {
		return column.getAlias();
	}

	@Override
	public FunctionColumnInterface setAlias(String alias) {
		return column.setAlias(alias);
	}

	public Function getFunction() {
		return function;
	}

	public FunctionColumnInterface getColumn() {
		return column;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		RecursiveFunctionColumn that = (RecursiveFunctionColumn) o;
		return new EqualsBuilder()
			.append(function, that.function)
			.append(column, that.column)
			.isEquals();
	}
}
