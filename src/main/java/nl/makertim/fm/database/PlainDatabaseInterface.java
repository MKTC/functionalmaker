package nl.makertim.fm.database;

import nl.makertim.fm.database.core.SafeResultSet;
import nl.makertim.fm.safe.ThrowingConsumer;
import nl.makertim.fm.safe.ThrowingFunction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.function.Supplier;

public interface PlainDatabaseInterface {
	/* ------------------------- */
	/*         Plain SQL         */
	/* ------------------------- */
	<T> List<T> resultToList(String query, ThrowingFunction<SafeResultSet, T, SQLException> function);

	<T> List<T> resultToList(Connection connection, String query,
							 ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException;

	<T> List<T> resultToList(String query,
							 ThrowingConsumer<PreparedStatement, SQLException> prepare,
							 ThrowingFunction<SafeResultSet, T, SQLException> function);

	<T> List<T> resultToList(Connection connection, String query,
							 ThrowingConsumer<PreparedStatement, SQLException> prepare,
							 ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException;

	<T> List<T> resultToList(String query,
							 ThrowingConsumer<PreparedStatement, SQLException> prepare,
							 ThrowingFunction<SafeResultSet, T, SQLException> function,
							 Supplier<? extends List<T>> listSupplier);

	<T> List<T> resultToList(Connection connection, String query,
							 ThrowingConsumer<PreparedStatement, SQLException> prepare,
							 ThrowingFunction<SafeResultSet, T, SQLException> function,
							 Supplier<? extends List<T>> listSupplier) throws SQLException;

	<T> T executeSingleResultQuery(String query, ThrowingFunction<SafeResultSet, T, SQLException> function);

	<T> T executeSingleResultQuery(Connection connection, String query,
								   ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException;

	<T> T executeSingleResultQuery(String query,
								   ThrowingConsumer<PreparedStatement, SQLException> prepare,
								   ThrowingFunction<SafeResultSet, T, SQLException> function);

	<T> T executeSingleResultQuery(Connection connection, String query,
								   ThrowingConsumer<PreparedStatement, SQLException> prepare,
								   ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException;

	<T> T executeResultQuery(String query, ThrowingFunction<SafeResultSet, T, SQLException> function);

	<T> T executeResultQuery(Connection connection, String query,
							 ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException;

	<T> T executeResultQuery(String query,
							 ThrowingConsumer<PreparedStatement, SQLException> prepare,
							 ThrowingFunction<SafeResultSet, T, SQLException> function);

	<T> T executeResultQuery(Connection connection, String query,
							 ThrowingConsumer<PreparedStatement, SQLException> prepare,
							 ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException;

	boolean executeRawQuery(String query);

	boolean executeRawQuery(Connection connection, String query) throws SQLException;

	boolean executeRawQuery(String query, ThrowingConsumer<PreparedStatement, SQLException> prepare);

	boolean executeRawQuery(Connection connection, String query,
							ThrowingConsumer<PreparedStatement, SQLException> prepare) throws SQLException;

}
