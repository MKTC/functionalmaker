package nl.makertim.fm.database;

import nl.makertim.fm.database.config.DatabaseConfig;
import nl.makertim.fm.database.core.SafeResultSet;
import nl.makertim.fm.database.oop.Dialect;
import nl.makertim.fm.database.oop.PreparedQuery;
import nl.makertim.fm.database.oop.query.Query;
import nl.makertim.fm.database.oop.query.ResultQuery;
import nl.makertim.fm.raid.ForEachRaid;
import nl.makertim.fm.raid.RaidStrategy;
import nl.makertim.fm.safe.ThrowingConsumer;
import nl.makertim.fm.safe.ThrowingFunction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class RaidDatabase implements DatabaseInterface {

	private final RaidStrategy<DatabaseInterface> raidStrategy;
	private final DatabaseInterface[] databases;

	public RaidDatabase(DatabaseInterface... databases) {
		this(new ForEachRaid<>(), databases);
	}

	public RaidDatabase(RaidStrategy<DatabaseInterface> raidStrategy, DatabaseInterface... databases) {
		this.raidStrategy = raidStrategy;
		assert databases.length > 0;
		this.databases = databases;
	}

	public DatabaseInterface[] accessDatabasesIndividual() {
		return databases;
	}

	@Override
	public DatabaseInterface open() {
		return raidStrategy.forEachReturnFastest(databases, FunctionalDatabaseInterface::open);
	}

	@Override
	public Dialect getDialect() {
		return raidStrategy.forEachReturnResult(databases, FunctionalDatabaseInterface::getDialect);
	}

	@Override
	public void getConnectionExecute(ThrowingConsumer<Connection, SQLException> connectionFunction) {
		raidStrategy.forEach(databases, database -> database.getConnectionExecute(connectionFunction));
	}

	@Override
	public <T> T getConnectionWithResult(ThrowingFunction<Connection, T, SQLException> connectionFunction) {
		return raidStrategy.forEachReturnResult(databases, database -> database.getConnectionWithResult(connectionFunction));
	}

	@Override
	public DatabaseConfig<?, ?> internal() {
		return raidStrategy.forEachReturnResult(databases, FunctionalDatabaseInterface::internal);
	}

	@Override
	public DatabaseInterface setupSettingsManually(Consumer<DatabaseConfig<?, ?>> settings) {
		return raidStrategy.forEachReturnFastest(databases, database -> database.setupSettingsManually(settings));
	}

	@Override
	public boolean close() {
		return raidStrategy.forEachReturnBoolean(databases, FunctionalDatabaseInterface::close);
	}

	@Override
	public <T> List<T> getMultiple(ResultQuery<?> query, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return raidStrategy.forEachReturnResult(databases, database -> database.getMultiple(query, function));
	}

	@Override
	public <T> List<T> getMultiple(PreparedQuery query, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return raidStrategy.forEachReturnResult(databases, database -> database.getMultiple(query, function));
	}

	@Override
	public <T> List<T> getMultiple(ResultQuery<?> query, Map<String, Object> variables, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return raidStrategy.forEachReturnResult(databases, database -> database.getMultiple(query, variables, function));
	}

	@Override
	public <T> List<T> getMultiple(PreparedQuery query, Map<String, Object> variables, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return raidStrategy.forEachReturnResult(databases, database -> database.getMultiple(query, variables, function));
	}

	@Override
	public <T> List<T> getMultiple(Connection connection, ResultQuery<?> query, ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.getMultiple(connection, query, function));
	}

	@Override
	public <T> List<T> getMultiple(Connection connection, PreparedQuery query, ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.getMultiple(connection, query, function));
	}

	@Override
	public <T> List<T> getMultiple(Connection connection, ResultQuery<?> query, Map<String, Object> variables, ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.getMultiple(connection, query, variables, function));
	}

	@Override
	public <T> List<T> getMultiple(Connection connection, PreparedQuery query, Map<String, Object> variables, ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.getMultiple(connection, query, variables, function));
	}

	@Override
	public <T> T getSingle(ResultQuery<?> query, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return raidStrategy.forEachReturnResult(databases, database -> database.getSingle(query, function));
	}

	@Override
	public <T> T getSingle(PreparedQuery query, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return raidStrategy.forEachReturnResult(databases, database -> database.getSingle(query, function));
	}

	@Override
	public <T> T getSingle(ResultQuery<?> query, Map<String, Object> variables, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return raidStrategy.forEachReturnResult(databases, database -> database.getSingle(query, variables, function));
	}

	@Override
	public <T> T getSingle(PreparedQuery query, Map<String, Object> variables, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return raidStrategy.forEachReturnResult(databases, database -> database.getSingle(query, variables, function));
	}

	@Override
	public <T> T getSingle(Connection connection, ResultQuery<?> query, ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.getSingle(connection, query, function));
	}

	@Override
	public <T> T getSingle(Connection connection, PreparedQuery query, ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.getSingle(connection, query, function));
	}

	@Override
	public <T> T getSingle(Connection connection, ResultQuery<?> query, Map<String, Object> variables, ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.getSingle(connection, query, variables, function));
	}

	@Override
	public <T> T getSingle(Connection connection, PreparedQuery query, Map<String, Object> variables, ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.getSingle(connection, query, variables, function));
	}

	@Override
	public boolean execute(Query<?> query) {
		return raidStrategy.forEachReturnBoolean(databases, database -> database.execute(query));
	}

	@Override
	public boolean execute(PreparedQuery query) {
		return raidStrategy.forEachReturnBoolean(databases, database -> database.execute(query));
	}

	@Override
	public boolean execute(Query<?> query, Map<String, Object> variables) {
		return raidStrategy.forEachReturnBoolean(databases, database -> database.execute(query, variables));
	}

	@Override
	public boolean execute(PreparedQuery query, Map<String, Object> variables) {
		return raidStrategy.forEachReturnBoolean(databases, database -> database.execute(query, variables));
	}

	@Override
	public boolean execute(Connection connection, Query<?> query) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.execute(connection, query));
	}

	@Override
	public boolean execute(Connection connection, PreparedQuery query) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.execute(connection, query));
	}

	@Override
	public boolean execute(Connection connection, Query<?> query, Map<String, Object> variables) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.execute(connection, query, variables));
	}

	@Override
	public boolean execute(Connection connection, PreparedQuery query, Map<String, Object> variables) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.execute(connection, query, variables));
	}

	@Override
	public <T> List<T> resultToList(String query, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return raidStrategy.forEachReturnResult(databases, database -> database.resultToList(query, function));
	}

	@Override
	public <T> List<T> resultToList(Connection connection, String query, ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.resultToList(connection, query, function));
	}

	@Override
	public <T> List<T> resultToList(String query, ThrowingConsumer<PreparedStatement, SQLException> prepare, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return raidStrategy.forEachReturnResult(databases, database -> database.resultToList(query, prepare, function));
	}

	@Override
	public <T> List<T> resultToList(Connection connection, String query, ThrowingConsumer<PreparedStatement, SQLException> prepare, ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.resultToList(connection, query, prepare, function));
	}

	@Override
	public <T> List<T> resultToList(String query, ThrowingConsumer<PreparedStatement, SQLException> prepare, ThrowingFunction<SafeResultSet, T, SQLException> function, Supplier<? extends List<T>> listSupplier) {
		return raidStrategy.forEachReturnResult(databases, database -> database.resultToList(query, prepare, function, listSupplier));
	}

	@Override
	public <T> List<T> resultToList(Connection connection, String query, ThrowingConsumer<PreparedStatement, SQLException> prepare, ThrowingFunction<SafeResultSet, T, SQLException> function, Supplier<? extends List<T>> listSupplier) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.resultToList(connection, query, prepare, function, listSupplier));
	}

	@Override
	public <T> T executeSingleResultQuery(String query, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return raidStrategy.forEachReturnResult(databases, database -> database.executeSingleResultQuery(query, function));
	}

	@Override
	public <T> T executeSingleResultQuery(Connection connection, String query, ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.executeSingleResultQuery(connection, query, function));
	}

	@Override
	public <T> T executeSingleResultQuery(String query, ThrowingConsumer<PreparedStatement, SQLException> prepare, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return raidStrategy.forEachReturnResult(databases, database -> database.executeSingleResultQuery(query, prepare, function));
	}

	@Override
	public <T> T executeSingleResultQuery(Connection connection, String query, ThrowingConsumer<PreparedStatement, SQLException> prepare, ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.executeSingleResultQuery(connection, query, prepare, function));
	}

	@Override
	public <T> T executeResultQuery(String query, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return raidStrategy.forEachReturnResult(databases, database -> database.executeResultQuery(query, function));
	}

	@Override
	public <T> T executeResultQuery(Connection connection, String query, ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.executeResultQuery(connection, query, function));
	}

	@Override
	public <T> T executeResultQuery(String query, ThrowingConsumer<PreparedStatement, SQLException> prepare, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return raidStrategy.forEachReturnResult(databases, database -> database.executeResultQuery(query, prepare, function));
	}

	@Override
	public <T> T executeResultQuery(Connection connection, String query, ThrowingConsumer<PreparedStatement, SQLException> prepare, ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.executeResultQuery(connection, query, prepare, function));
	}

	@Override
	public boolean executeRawQuery(String query) {
		return raidStrategy.forEachReturnBoolean(databases, database -> database.executeRawQuery(query));
	}

	@Override
	public boolean executeRawQuery(Connection connection, String query) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.executeRawQuery(connection, query));
	}

	@Override
	public boolean executeRawQuery(String query, ThrowingConsumer<PreparedStatement, SQLException> prepare) {
		return raidStrategy.forEachReturnBoolean(databases, database -> database.executeRawQuery(query, prepare));
	}

	@Override
	public boolean executeRawQuery(Connection connection, String query, ThrowingConsumer<PreparedStatement, SQLException> prepare) throws SQLException {
		return raidStrategy.throwingForEachReturnResult(databases, database -> database.executeRawQuery(connection, query, prepare));
	}
}
