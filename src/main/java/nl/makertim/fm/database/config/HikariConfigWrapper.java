package nl.makertim.fm.database.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

public class HikariConfigWrapper implements DatabaseConfig<HikariConfig, HikariConfigWrapper> {

	protected final HikariConfig config = new HikariConfig();

	@Override
	public HikariConfig getWrapped() {
		return config;
	}

	@Override
	public DataSource toSource() {
		return new HikariDataSource(getWrapped());
	}

	@Override
	public String getDriverClassName() {
		return config.getDriverClassName();
	}

	@Override
	public HikariConfigWrapper setDriverClassName(String driverClassName) {
		config.setDriverClassName(driverClassName);
		return this;
	}

	@Override
	public String getJdbcUrl() {
		return config.getJdbcUrl();
	}

	@Override
	public HikariConfigWrapper setJdbcUrl(String jdbcUrl) {
		config.setJdbcUrl(jdbcUrl);
		return this;
	}

	@Override
	public String getUsername() {
		return config.getUsername();
	}

	@Override
	public HikariConfigWrapper setUsername(String username) {
		config.setUsername(username);
		return this;
	}

	@Override
	public String getPassword() {
		return config.getPassword();
	}

	@Override
	public HikariConfigWrapper setPassword(String password) {
		config.setPassword(password);
		return this;
	}

	@Override
	public boolean isAutoCommit() {
		return config.isAutoCommit();
	}

	@Override
	public HikariConfigWrapper setAutoCommit(boolean isAutoCommit) {
		config.setAutoCommit(isAutoCommit);
		return this;
	}

	@Override
	public int getConnectionTimeout() {
		return (int) config.getConnectionTimeout();
	}

	@Override
	public HikariConfigWrapper setConnectionTimeout(int timeoutMs) {
		config.setConnectionTimeout(timeoutMs);
		return this;
	}

	@Override
	public int getIdleTimeout() {
		return (int) config.getIdleTimeout();
	}

	@Override
	public HikariConfigWrapper setIdleTimeout(int idleTimeoutMs) {
		config.setIdleTimeout(idleTimeoutMs);
		return this;
	}

	@Override
	public int getMaxLifetime() {
		return (int) config.getMaxLifetime();
	}

	@Override
	public HikariConfigWrapper setMaxLifetime(int maxLifetimeMs) {
		config.setMaxLifetime(maxLifetimeMs);
		return this;
	}

	@Override
	public int getMinimumIdle() {
		return config.getMinimumIdle();
	}

	@Override
	public HikariConfigWrapper setMinimumIdle(int minimumIdleMs) {
		config.setMinimumIdle(minimumIdleMs);
		return this;
	}

	@Override
	public int getMaximumPoolSize() {
		return config.getMaximumPoolSize();
	}

	@Override
	public HikariConfigWrapper setMaximumPoolSize(int maximumPoolSize) {
		config.setMaximumPoolSize(maximumPoolSize);
		return this;
	}

	@Override
	public String getPoolName() {
		return config.getPoolName();
	}

	@Override
	public HikariConfigWrapper setPoolName(String poolName) {
		config.setPoolName(poolName);
		return this;
	}

	@Override
	public String getConnectionInitSql() {
		return config.getConnectionInitSql();
	}

	@Override
	public HikariConfigWrapper setConnectionInitSql(String initSql) {
		config.setConnectionInitSql(initSql);
		return this;
	}

	@Override
	public HikariConfigWrapper addDataSourceProperty(String dataSource, Object property) {
		config.addDataSourceProperty(dataSource, property);
		return this;
	}
}
