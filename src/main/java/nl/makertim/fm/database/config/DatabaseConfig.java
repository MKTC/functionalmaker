package nl.makertim.fm.database.config;

import javax.sql.DataSource;

public interface DatabaseConfig<Type, Config extends DatabaseConfig<Type, Config>> {

	Type getWrapped();

	DataSource toSource();

	String getDriverClassName();

	Config setDriverClassName(String driverClassName);

	String getJdbcUrl();

	Config setJdbcUrl(String jdbcUrl);

	String getUsername();

	Config setUsername(String username);

	String getPassword();

	Config setPassword(String password);

	boolean isAutoCommit();

	Config setAutoCommit(boolean isAutoCommit);

	int getConnectionTimeout();

	Config setConnectionTimeout(int timeoutMs);

	int getIdleTimeout();

	Config setIdleTimeout(int idleTimeoutMs);

	int getMaxLifetime();

	Config setMaxLifetime(int maxLifetimeMs);

	int getMinimumIdle();

	Config setMinimumIdle(int minimumIdleMs);

	int getMaximumPoolSize();

	Config setMaximumPoolSize(int maximumPoolSize);

	String getPoolName();

	Config setPoolName(String poolName);

	String getConnectionInitSql();

	Config setConnectionInitSql(String initSql);

	Config addDataSourceProperty(String dataSource, Object property);
}
