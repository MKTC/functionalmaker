package nl.makertim.fm.database;

import nl.makertim.fm.core.Logger;
import nl.makertim.fm.core.Settings;
import nl.makertim.fm.database.config.DatabaseConfig;
import nl.makertim.fm.database.config.HikariConfigWrapper;
import nl.makertim.fm.database.core.SafeResultSet;
import nl.makertim.fm.database.oop.Dialect;
import nl.makertim.fm.database.oop.DialectManager;
import nl.makertim.fm.database.oop.PreparedQuery;
import nl.makertim.fm.database.oop.query.Query;
import nl.makertim.fm.database.oop.query.ResultQuery;
import nl.makertim.fm.database.oop.query.SelectQuery;
import nl.makertim.fm.safe.SafeTry;
import nl.makertim.fm.safe.ThrowingConsumer;
import nl.makertim.fm.safe.ThrowingFunction;

import javax.sql.DataSource;
import java.io.Closeable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Database implements DatabaseInterface {

	public static final ThrowingConsumer<PreparedStatement, SQLException> PREPARE_NOTHING = (preparedStatement) -> {};

	protected final DatabaseConfig<?, ?> databaseConfig;
	protected DataSource dataSource;
	protected Dialect dialect;
	protected final SelectQuery testSQL = new SelectQuery(true);
	protected String fallbackTestSQL = null;

	/* ------------------------- */
	/*     static Constructor    */
	/* ------------------------- */
	public static Database createConnection() {
		return new Database(null, HikariConfigWrapper.class);
	}

	public static Database createConnection(Class<? extends DatabaseConfig<?, ?>> databaseConfigClass) {
		return new Database(null, databaseConfigClass);
	}

	public static Database createConnection(String jdbc) {
		Database instance = new Database(null, HikariConfigWrapper.class);
		instance.databaseConfig.setJdbcUrl(jdbc);
		return instance;
	}

	public static Database createConnection(String jdbc, String username, String password) {
		Database instance = new Database(null, HikariConfigWrapper.class);
		instance.databaseConfig
			.setJdbcUrl(jdbc)
			.setUsername(username)
			.setPassword(password);
		return instance;
	}

	public static Database createConnection(String jdbc, String username, String password,
											Class<? extends DatabaseConfig<?, ?>> databaseConfigClass) {
		Database instance = new Database(null, databaseConfigClass);
		instance.databaseConfig
			.setJdbcUrl(jdbc)
			.setUsername(username)
			.setPassword(password);
		return instance;
	}

	public static Database createConnection(Settings settings) {
		return new Database(settings, HikariConfigWrapper.class);
	}

	public static Database createConnection(Settings settings,
											Class<? extends DatabaseConfig<?, ?>> databaseConfigClass) {
		return new Database(settings, databaseConfigClass);
	}

	/* ------------------------- */
	/*         Plain SQL         */
	/* ------------------------- */
	public static ThrowingConsumer<PreparedStatement, SQLException> fillObject(Object[]... values) {
		return preparedStatement -> {
			if (values == null || values.length == 0) {
				return;
			}
			try {
				int i = 1;
				for (Object[] objs : values) {
					for (Object obj : objs) {
						preparedStatement.setObject(i++, obj);
					}
				}
			} catch (Exception exception) {
				Logger.danger(exception);
			}
		};
	}

	@Override
	public <T> List<T> resultToList(String query, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return getConnectionWithResult(connection -> resultToList(connection, query, function));
	}

	@Override
	public <T> List<T> resultToList(Connection connection, String query,
									ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return resultToList(connection, query, PREPARE_NOTHING, function);
	}

	@Override
	public <T> List<T> resultToList(String query,
									ThrowingConsumer<PreparedStatement, SQLException> prepare,
									ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return getConnectionWithResult(connection -> resultToList(connection, query, prepare, function));
	}

	@Override
	public <T> List<T> resultToList(Connection connection, String query,
									ThrowingConsumer<PreparedStatement, SQLException> prepare,
									ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return resultToList(connection, query, prepare, function, LinkedList::new);
	}

	@Override
	public <T> List<T> resultToList(String query,
									ThrowingConsumer<PreparedStatement, SQLException> prepare,
									ThrowingFunction<SafeResultSet, T, SQLException> function,
									Supplier<? extends List<T>> listSupplier) {
		return getConnectionWithResult(connection -> resultToList(connection, query, prepare, function, listSupplier));
	}

	@Override
	public <T> List<T> resultToList(Connection connection, String query,
									ThrowingConsumer<PreparedStatement, SQLException> prepare,
									ThrowingFunction<SafeResultSet, T, SQLException> function,
									Supplier<? extends List<T>> listSupplier) throws SQLException {
		return executeResultQuery(connection, query, prepare,
			(resultSet) -> internalResultSetToList(resultSet, function, listSupplier));
	}

	@Override
	public <T> T executeSingleResultQuery(String query, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return getConnectionWithResult(connection -> executeSingleResultQuery(connection, query, function));
	}

	@Override
	public <T> T executeSingleResultQuery(Connection connection, String query,
										  ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return executeResultQuery(connection, query, PREPARE_NOTHING,
			(resultSet) -> internalResultSetToObject(resultSet, function));
	}

	@Override
	public <T> T executeSingleResultQuery(String query,
										  ThrowingConsumer<PreparedStatement, SQLException> prepare,
										  ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return getConnectionWithResult(connection -> executeSingleResultQuery(connection, query, prepare, function));
	}

	@Override
	public <T> T executeSingleResultQuery(Connection connection, String query,
										  ThrowingConsumer<PreparedStatement, SQLException> prepare,
										  ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return executeResultQuery(connection, query, prepare,
			(resultSet) -> internalResultSetToObject(resultSet, function));
	}

	@Override
	public <T> T executeResultQuery(String query, ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return getConnectionWithResult(connection -> executeResultQuery(connection, query, function));
	}

	@Override
	public <T> T executeResultQuery(Connection connection, String query,
									ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return executeResultQuery(connection, query, PREPARE_NOTHING, function);
	}

	@Override
	public <T> T executeResultQuery(String query,
									ThrowingConsumer<PreparedStatement, SQLException> prepare,
									ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return getConnectionWithResult(connection -> executeResultQuery(connection, query, prepare, function));
	}

	@Override
	public <T> T executeResultQuery(Connection connection, String query,
									ThrowingConsumer<PreparedStatement, SQLException> prepare,
									ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return internalExecuteQuery(connection, query,
			statement -> {
				prepare.accept(statement);
				return function.apply(new SafeResultSet(statement.executeQuery()));
			});
	}

	@Override
	public boolean executeRawQuery(String query) {
		return getConnectionWithResult(connection -> executeRawQuery(connection, query)) != null;
	}

	@Override
	public boolean executeRawQuery(Connection connection, String query) throws SQLException {
		return executeRawQuery(connection, query, PREPARE_NOTHING);
	}

	@Override
	public boolean executeRawQuery(String query, ThrowingConsumer<PreparedStatement, SQLException> prepare) {
		return getConnectionWithResult(connection -> executeRawQuery(connection, query, prepare));
	}

	@Override
	public boolean executeRawQuery(Connection connection, String query,
								   ThrowingConsumer<PreparedStatement, SQLException> prepare) throws SQLException {
		return internalExecuteQuery(
			connection,
			query,
			preparedStatement -> {
				prepare.accept(preparedStatement);
				preparedStatement.execute();
				return true;
			});
	}

	protected static <T> T internalResultSetToObject(SafeResultSet resultSet,
													 ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		if (resultSet != null && resultSet.next()) {
			return function.apply(resultSet);
		}
		return null;
	}

	protected static <T> List<T> internalResultSetToList(SafeResultSet resultSet,
														 ThrowingFunction<SafeResultSet, T, SQLException> function,
														 Supplier<? extends List<T>> listSupplier) throws SQLException {
		List<T> list = listSupplier.get();
		while (resultSet.next()) {
			list.add(function.apply(resultSet));
		}
		return list;
	}

	protected static <T> T internalExecuteQuery(Connection connection, String query,
												ThrowingFunction<PreparedStatement, T, SQLException> prepare) throws SQLException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(query);
			return prepare.apply(statement);
		} finally {
			close(statement);
		}
	}

	/* ------------------------- */
	/*         Instance          */
	/* ------------------------- */
	protected Database(Settings settings, Class<? extends DatabaseConfig<?, ?>> configClass) {
		databaseConfig = SafeTry.execute(() -> configClass.getConstructor().newInstance());
		setupSettings(settings);
	}

	@Override
	public Database open() {
		dataSource = databaseConfig.toSource();
		dialect = DialectManager.findDialectOnJDBC(databaseConfig.getJdbcUrl()).orElse(null);
		checkDatasource();
		return this;
	}

	@Override
	public Dialect getDialect() {
		return dialect;
	}

	@Override
	public void getConnectionExecute(ThrowingConsumer<Connection, SQLException> connectionFunction) {
		getConnectionWithResult(connectionFunction.toFunction(null));
	}

	@Override
	public <T> T getConnectionWithResult(ThrowingFunction<Connection, T, SQLException> connectionFunction) {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			return connectionFunction.apply(connection);
		} catch (SQLException exception) {
			Logger.danger(exception);
		} finally {
			close(connection);
		}
		return null;
	}

	@Override
	public DatabaseConfig<?, ?> internal() {
		return databaseConfig;
	}

	@Override
	public Database setupSettingsManually(Consumer<DatabaseConfig<?, ?>> settings) {
		settings.accept(databaseConfig);
		return this;
	}

	@Override
	public boolean close() {
		if (dataSource instanceof Closeable) {
			return SafeTry.execute(() -> {
				((Closeable) dataSource).close();
				dataSource = null;
				return true;
			}) != null;
		}
		return false;
	}

	/* ------------------------- */
	/*            ORM            */
	/* ------------------------- */
	@Override
	public <T> List<T> getMultiple(ResultQuery<?> query,
								   ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return getMultiple(query.build(getDialect()), function);
	}

	@Override
	public <T> List<T> getMultiple(PreparedQuery query,
								   ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return getMultiple(query, Collections.emptyMap(), function);
	}

	@Override
	public <T> List<T> getMultiple(ResultQuery<?> query,
								   Map<String, Object> variables,
								   ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return getMultiple(query.build(getDialect()), variables, function);
	}

	@Override
	public <T> List<T> getMultiple(PreparedQuery query,
								   Map<String, Object> variables,
								   ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return getConnectionWithResult(connection -> getMultiple(connection, query, variables, function));
	}

	@Override
	public <T> List<T> getMultiple(Connection connection, ResultQuery<?> query,
								   ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return getMultiple(connection, query.build(getDialect()), function);
	}

	@Override
	public <T> List<T> getMultiple(Connection connection, PreparedQuery query,
								   ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return getMultiple(connection, query, Collections.emptyMap(), function);
	}

	@Override
	public <T> List<T> getMultiple(Connection connection, ResultQuery<?> query,
								   Map<String, Object> variables,
								   ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return getMultiple(connection, query.build(getDialect()), variables, function);
	}

	@Override
	public <T> List<T> getMultiple(Connection connection, PreparedQuery query,
								   Map<String, Object> variables,
								   ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return internalResultSetToList(query.execute(connection, variables), function, LinkedList::new);
	}

	@Override
	public <T> T getSingle(ResultQuery<?> query,
						   ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return getSingle(query.build(getDialect()), function);
	}

	@Override
	public <T> T getSingle(PreparedQuery query,
						   ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return getSingle(query, Collections.emptyMap(), function);
	}

	@Override
	public <T> T getSingle(ResultQuery<?> query,
						   Map<String, Object> variables,
						   ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return getSingle(query.build(getDialect()), variables, function);
	}

	@Override
	public <T> T getSingle(PreparedQuery query,
						   Map<String, Object> variables,
						   ThrowingFunction<SafeResultSet, T, SQLException> function) {
		return getConnectionWithResult(connection -> getSingle(connection, query, variables, function));
	}

	@Override
	public <T> T getSingle(Connection connection, ResultQuery<?> query,
						   ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return getSingle(connection, query.build(getDialect()), function);
	}

	@Override
	public <T> T getSingle(Connection connection, PreparedQuery query,
						   ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return getSingle(connection, query, Collections.emptyMap(), function);
	}

	@Override
	public <T> T getSingle(Connection connection, ResultQuery<?> query,
						   Map<String, Object> variables,
						   ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return getSingle(connection, query.build(getDialect()), variables, function);
	}

	@Override
	public <T> T getSingle(Connection connection, PreparedQuery query,
						   Map<String, Object> variables,
						   ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return internalResultSetToObject(query.execute(connection, variables), function);
	}

	@Override
	public boolean execute(Query<?> query) {
		return execute(query.build(getDialect()));
	}

	@Override
	public boolean execute(PreparedQuery query) {
		return execute(query, Collections.emptyMap());
	}

	@Override
	public boolean execute(Query<?> query, Map<String, Object> variables) {
		return execute(query.build(getDialect()), variables);
	}

	@Override
	public boolean execute(PreparedQuery query, Map<String, Object> variables) {
		return getConnectionWithResult(connection -> execute(connection, query, variables));
	}

	@Override
	public boolean execute(Connection connection, Query<?> query) throws SQLException {
		return execute(connection, query.build(getDialect()));
	}

	@Override
	public boolean execute(Connection connection, PreparedQuery query) throws SQLException {
		return execute(connection, query, Collections.emptyMap());
	}

	@Override
	public boolean execute(Connection connection, Query<?> query,
						   Map<String, Object> variables) throws SQLException {
		return execute(connection, query.build(getDialect()), variables);
	}

	@Override
	public boolean execute(Connection connection, PreparedQuery query,
						   Map<String, Object> variables) throws SQLException {
		if (query.hasResults()) {
			return query.execute(connection, variables).getString(1) != null;
		}
		return query.execute(connection, variables).getBoolean(null);
	}

	/* ------------------------- */
	/*         Internal          */
	/* ------------------------- */
	protected void checkDatasource() {
		Boolean success = getConnectionWithResult(connection -> {
			String sql = fallbackTestSQL;
			if (sql == null && dialect != null) {
				sql = dialect.buildQuery(testSQL).getSQL();
			}
			internalExecuteQuery(connection, sql, PreparedStatement::execute);
			return true;
		});
		if (success == null || !success) {
			throw new RuntimeException("Couldn't setup database");
		}
	}

	protected void setupSettings(Settings settings) {
		if (settings == null) {
			return;
		}
		handleSettingIfPresent(settings, "jdbc", databaseConfig::setJdbcUrl);
		handleSettingIfPresent(settings, "username", databaseConfig::setUsername);
		handleSettingIfPresent(settings, "password", databaseConfig::setPassword);
		handleSettingIfPresent(settings, "testSQL", setting -> this.fallbackTestSQL = setting);
		handleSettingIfPresent(settings, "driverClassName", databaseConfig::setDriverClassName);

		handleSettingIfPresentBoolean(settings, "setAutoCommit", databaseConfig::setAutoCommit);
		handleSettingIfPresentInteger(settings, "setIdleTimeout", databaseConfig::setIdleTimeout);
		handleSettingIfPresentInteger(settings, "setMaxLifetime", databaseConfig::setMaxLifetime);
		handleSettingIfPresentInteger(settings, "setMinimumIdle", databaseConfig::setMinimumIdle);
		handleSettingIfPresentInteger(settings, "setMaximumPoolSize", databaseConfig::setMaximumPoolSize);
		handleSettingIfPresentInteger(settings, "setConnectionTimeout", databaseConfig::setConnectionTimeout);
		handleSettingIfPresent(settings, "setConnectionInitSql", databaseConfig::setConnectionInitSql);
		handleSettingIfPresent(settings, "setPoolName", databaseConfig::setPoolName);

		handleSettingIfPresent(settings, "cachePrepStmts", databaseConfig::addDataSourceProperty);
		handleSettingIfPresent(settings, "prepStmtCacheSize", databaseConfig::addDataSourceProperty);
		handleSettingIfPresent(settings, "useServerPrepStmts", databaseConfig::addDataSourceProperty);
		handleSettingIfPresent(settings, "useLocalSessionState", databaseConfig::addDataSourceProperty);
		handleSettingIfPresent(settings, "rewriteBatchedStatements", databaseConfig::addDataSourceProperty);
		handleSettingIfPresent(settings, "cacheServerConfiguration", databaseConfig::addDataSourceProperty);
		handleSettingIfPresent(settings, "cacheResultSetMetadata", databaseConfig::addDataSourceProperty);
		handleSettingIfPresent(settings, "prepStmtCacheSqlLimit", databaseConfig::addDataSourceProperty);
		handleSettingIfPresent(settings, "elideSetAutoCommits", databaseConfig::addDataSourceProperty);
		handleSettingIfPresent(settings, "maintainTimeStats", databaseConfig::addDataSourceProperty);
		handleSettingIfPresent(settings, "serverTimezone", databaseConfig::addDataSourceProperty);
	}

	protected void handleSettingIfPresent(Settings settings, String setting, Consumer<String> onSettingPresent) {
		if (!settings.has(setting)) {
			return;
		}
		String foundSetting = settings.getString(setting);
		onSettingPresent.accept(foundSetting);
	}

	protected void handleSettingIfPresent(Settings settings, String setting, BiConsumer<String, String> onSettingPresent) {
		if (!settings.has(setting)) {
			return;
		}
		String foundSetting = settings.getString(setting);
		onSettingPresent.accept(setting, foundSetting);
	}

	protected void handleSettingIfPresentInteger(Settings settings, String setting, Consumer<Integer> onSettingPresent) {
		if (!settings.has(setting)) {
			return;
		}
		Integer foundSetting = settings.getInt(setting);
		onSettingPresent.accept(foundSetting);
	}

	protected void handleSettingIfPresentBoolean(Settings settings, String setting, Consumer<Boolean> onSettingPresent) {
		if (!settings.has(setting)) {
			return;
		}
		Boolean foundSetting = settings.getBoolean(setting);
		onSettingPresent.accept(foundSetting);
	}

	protected static void close(AutoCloseable closeable) {
		SafeTry.execute(() -> {
			if (closeable != null) {
				closeable.close();
			}
		});
	}
}
