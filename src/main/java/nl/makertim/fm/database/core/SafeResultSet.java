package nl.makertim.fm.database.core;

import nl.makertim.fm.safe.SafeTry;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;
import java.util.Map;

@SuppressWarnings({ "deprecation", "MagicConstant", "Convert2MethodRef" })
public class SafeResultSet implements ResultSet, AutoCloseable {

	private final ResultSet internal;

	public SafeResultSet(ResultSet internal) {
		this.internal = internal;
	}

	@Override
	public boolean next() {
		return SafeTry.execute(() -> internal.next(), error -> false);
	}

	@Override
	public void close() {
		SafeTry.execute(() -> internal.close());
	}

	@Override
	public boolean wasNull() {
		return SafeTry.execute(() -> internal.wasNull(), error -> false);
	}

	@Override
	public String getString(int i) {
		return SafeTry.execute(() -> internal.getString(i), error -> null);
	}

	@Override
	public boolean getBoolean(int i) {
		return SafeTry.execute(() -> internal.getBoolean(i), error -> false);
	}

	@Override
	public byte getByte(int i) {
		return SafeTry.execute(() -> internal.getByte(i), error -> (byte) -1);
	}

	@Override
	public short getShort(int i) {
		return SafeTry.execute(() -> internal.getShort(i), error -> (short) -1);
	}

	@Override
	public int getInt(int i) {
		return SafeTry.execute(() -> internal.getInt(i), error -> -1);
	}

	@Override
	public long getLong(int i) {
		return SafeTry.execute(() -> internal.getLong(i), error -> -1L);
	}

	@Override
	public float getFloat(int i) {
		return SafeTry.execute(() -> internal.getFloat(i), error -> -1F);
	}

	@Override
	public double getDouble(int i) {
		return SafeTry.execute(() -> internal.getDouble(i), error -> -1D);
	}

	@Override
	public BigDecimal getBigDecimal(int i, int i1) {
		return SafeTry.execute(() -> {
			return internal.getBigDecimal(i, i1);
		}, error -> new BigDecimal(-1));
	}

	@Override
	public byte[] getBytes(int i) {
		return SafeTry.execute(() -> internal.getBytes(i), error -> null);
	}

	@Override
	public Date getDate(int i) {
		return SafeTry.execute(() -> internal.getDate(i), error -> null);
	}

	@Override
	public Time getTime(int i) {
		return SafeTry.execute(() -> internal.getTime(i), error -> null);
	}

	@Override
	public Timestamp getTimestamp(int i) {
		return SafeTry.execute(() -> internal.getTimestamp(i), error -> null);
	}

	@Override
	public InputStream getAsciiStream(int i) {
		return SafeTry.execute(() -> internal.getAsciiStream(i), error -> null);
	}

	@Override
	public InputStream getUnicodeStream(int i) {
		return SafeTry.execute(() -> internal.getUnicodeStream(i), error -> null);
	}

	@Override
	public InputStream getBinaryStream(int i) {
		return SafeTry.execute(() -> internal.getBinaryStream(i), error -> null);
	}

	@Override
	public String getString(String s) {
		return SafeTry.execute(() -> internal.getString(s), error -> null);
	}

	@Override
	public boolean getBoolean(String s) {
		return SafeTry.execute(() -> internal.getBoolean(s), error -> false);
	}

	@Override
	public byte getByte(String s) {
		return SafeTry.execute(() -> internal.getByte(s), error -> (byte) -1);
	}

	@Override
	public short getShort(String s) {
		return SafeTry.execute(() -> internal.getShort(s), error -> (short) -1);
	}

	@Override
	public int getInt(String s) {
		return SafeTry.execute(() -> internal.getInt(s), error -> -1);
	}

	@Override
	public long getLong(String s) {
		return SafeTry.execute(() -> internal.getLong(s), error -> -1L);
	}

	@Override
	public float getFloat(String s) {
		return SafeTry.execute(() -> internal.getFloat(s), error -> -1F);
	}

	@Override
	public double getDouble(String s) {
		return SafeTry.execute(() -> internal.getDouble(s), error -> -1D);
	}

	@Override
	public BigDecimal getBigDecimal(String s, int i) {
		return SafeTry.execute(() -> {
			return internal.getBigDecimal(s, i);
		}, error -> new BigDecimal(-1));
	}

	@Override
	public byte[] getBytes(String s) {
		return SafeTry.execute(() -> internal.getBytes(s), error -> null);
	}

	@Override
	public Date getDate(String s) {
		return SafeTry.execute(() -> internal.getDate(s), error -> null);
	}

	@Override
	public Time getTime(String s) {
		return SafeTry.execute(() -> internal.getTime(s), error -> null);
	}

	@Override
	public Timestamp getTimestamp(String s) {
		return SafeTry.execute(() -> internal.getTimestamp(s), error -> null);
	}

	@Override
	public InputStream getAsciiStream(String s) {
		return SafeTry.execute(() -> internal.getAsciiStream(s), error -> null);
	}

	@Override
	public InputStream getUnicodeStream(String s) {
		return SafeTry.execute(() -> internal.getUnicodeStream(s), error -> null);
	}

	@Override
	public InputStream getBinaryStream(String s) {
		return SafeTry.execute(() -> internal.getBinaryStream(s), error -> null);
	}

	@Override
	public SQLWarning getWarnings() {
		return SafeTry.execute(() -> internal.getWarnings(), error -> null);
	}

	@Override
	public void clearWarnings() {
		SafeTry.execute(() -> internal.clearWarnings());
	}

	@Override
	public String getCursorName() {
		return SafeTry.execute(() -> internal.getCursorName(), error -> null);
	}

	@Override
	public ResultSetMetaData getMetaData() {
		return SafeTry.execute(() -> internal.getMetaData(), error -> null);
	}

	@Override
	public Object getObject(int i) {
		return SafeTry.execute(() -> internal.getObject(i), error -> null);
	}

	@Override
	public Object getObject(String s) {
		return SafeTry.execute(() -> internal.getObject(s), error -> null);
	}

	@Override
	public int findColumn(String s) {
		return SafeTry.execute(() -> internal.findColumn(s), error -> -1);
	}

	@Override
	public Reader getCharacterStream(int i) {
		return SafeTry.execute(() -> internal.getCharacterStream(i), error -> null);
	}

	@Override
	public Reader getCharacterStream(String s) {
		return SafeTry.execute(() -> internal.getCharacterStream(s), error -> null);
	}

	@Override
	public BigDecimal getBigDecimal(int i) {
		return SafeTry.execute(() -> {
			return internal.getBigDecimal(i);
		}, error -> new BigDecimal(-1));
	}

	@Override
	public BigDecimal getBigDecimal(String s) {
		return SafeTry.execute(() -> {
			return internal.getBigDecimal(s);
		}, error -> new BigDecimal(-1));
	}

	@Override
	public boolean isBeforeFirst() {
		return SafeTry.execute(() -> internal.isBeforeFirst(), error -> false);
	}

	@Override
	public boolean isAfterLast() {
		return SafeTry.execute(() -> internal.isAfterLast(), error -> false);
	}

	@Override
	public boolean isFirst() {
		return SafeTry.execute(() -> internal.isFirst(), error -> false);
	}

	@Override
	public boolean isLast() {
		return SafeTry.execute(() -> internal.isLast(), error -> false);
	}

	@Override
	public void beforeFirst() {
		SafeTry.execute(() -> internal.beforeFirst());
	}

	@Override
	public void afterLast() {
		SafeTry.execute(() -> internal.afterLast());
	}

	@Override
	public boolean first() {
		return SafeTry.execute(() -> internal.first(), error -> false);
	}

	@Override
	public boolean last() {
		return SafeTry.execute(() -> internal.last(), error -> false);
	}

	@Override
	public int getRow() {
		return SafeTry.execute(() -> internal.getRow(), error -> -1);
	}

	@Override
	public boolean absolute(int i) {
		return SafeTry.execute(() -> internal.absolute(i), error -> false);
	}

	@Override
	public boolean relative(int i) {
		return SafeTry.execute(() -> internal.relative(i), error -> false);
	}

	@Override
	public boolean previous() {
		return SafeTry.execute(() -> internal.previous(), error -> false);
	}

	@Override
	public void setFetchDirection(int i) {
		SafeTry.execute(() -> internal.setFetchDirection(i));
	}

	@Override
	public int getFetchDirection() {
		return SafeTry.execute(() -> internal.getFetchDirection(), error -> -1);
	}

	@Override
	public void setFetchSize(int i) {
		SafeTry.execute(() -> internal.setFetchSize(i));
	}

	@Override
	public int getFetchSize() {
		return SafeTry.execute(() -> internal.getFetchSize(), error -> -1);
	}

	@Override
	public int getType() {
		return SafeTry.execute(() -> internal.getType(), error -> -1);
	}

	@Override
	public int getConcurrency() {
		return SafeTry.execute(() -> internal.getConcurrency(), error -> -1);
	}

	@Override
	public boolean rowUpdated() {
		return SafeTry.execute(() -> internal.rowUpdated(), error -> false);
	}

	@Override
	public boolean rowInserted() {
		return SafeTry.execute(() -> internal.rowInserted(), error -> false);
	}

	@Override
	public boolean rowDeleted() {
		return SafeTry.execute(() -> internal.rowDeleted(), error -> false);
	}

	@Override
	public void updateNull(int i) {
		SafeTry.execute(() -> internal.updateNull(i));
	}

	@Override
	public void updateBoolean(int i, boolean b) {
		SafeTry.execute(() -> internal.updateBoolean(i, b));
	}

	@Override
	public void updateByte(int i, byte b) {
		SafeTry.execute(() -> internal.updateByte(i, b));
	}

	@Override
	public void updateShort(int i, short i1) {
		SafeTry.execute(() -> internal.updateShort(i, i1));
	}

	@Override
	public void updateInt(int i, int i1) {
		SafeTry.execute(() -> internal.updateInt(i, i1));
	}

	@Override
	public void updateLong(int i, long l) {
		SafeTry.execute(() -> internal.updateLong(i, l));
	}

	@Override
	public void updateFloat(int i, float v) {
		SafeTry.execute(() -> internal.updateFloat(i, v));
	}

	@Override
	public void updateDouble(int i, double v) {
		SafeTry.execute(() -> internal.updateDouble(i, v));
	}

	@Override
	public void updateBigDecimal(int i, BigDecimal bigDecimal) {
		SafeTry.execute(() -> internal.updateBigDecimal(i, bigDecimal));
	}

	@Override
	public void updateString(int i, String s) {
		SafeTry.execute(() -> internal.updateString(i, s));
	}

	@Override
	public void updateBytes(int i, byte[] bytes) {
		SafeTry.execute(() -> internal.updateBytes(i, bytes));
	}

	@Override
	public void updateDate(int i, Date date) {
		SafeTry.execute(() -> internal.updateDate(i, date));
	}

	@Override
	public void updateTime(int i, Time time) {
		SafeTry.execute(() -> internal.updateTime(i, time));
	}

	@Override
	public void updateTimestamp(int i, Timestamp timestamp) {
		SafeTry.execute(() -> internal.updateTimestamp(i, timestamp));
	}

	@Override
	public void updateAsciiStream(int i, InputStream inputStream, int i1) {
		SafeTry.execute(() -> internal.updateAsciiStream(i, inputStream, i1));
	}

	@Override
	public void updateBinaryStream(int i, InputStream inputStream, int i1) {
		SafeTry.execute(() -> internal.updateBinaryStream(i, inputStream, i1));
	}

	@Override
	public void updateCharacterStream(int i, Reader reader, int i1) {
		SafeTry.execute(() -> internal.updateCharacterStream(i, reader, i1));
	}

	@Override
	public void updateObject(int i, Object o, int i1) {
		SafeTry.execute(() -> internal.updateObject(i, o, i1));
	}

	@Override
	public void updateObject(int i, Object o) {
		SafeTry.execute(() -> internal.updateObject(i, o));
	}

	@Override
	public void updateNull(String s) {
		SafeTry.execute(() -> internal.updateNull(s));
	}

	@Override
	public void updateBoolean(String s, boolean b) {
		SafeTry.execute(() -> internal.updateBoolean(s, b));
	}

	@Override
	public void updateByte(String s, byte b) {
		SafeTry.execute(() -> internal.updateByte(s, b));
	}

	@Override
	public void updateShort(String s, short i1) {
		SafeTry.execute(() -> internal.updateShort(s, i1));
	}

	@Override
	public void updateInt(String s, int i1) {
		SafeTry.execute(() -> internal.updateInt(s, i1));
	}

	@Override
	public void updateLong(String s, long l) {
		SafeTry.execute(() -> internal.updateLong(s, l));
	}

	@Override
	public void updateFloat(String s, float v) {
		SafeTry.execute(() -> internal.updateFloat(s, v));
	}

	@Override
	public void updateDouble(String s, double v) {
		SafeTry.execute(() -> internal.updateDouble(s, v));
	}

	@Override
	public void updateBigDecimal(String s, BigDecimal bigDecimal) {
		SafeTry.execute(() -> internal.updateBigDecimal(s, bigDecimal));
	}

	@Override
	public void updateString(String s, String s1) {
		SafeTry.execute(() -> internal.updateString(s, s1));
	}

	@Override
	public void updateBytes(String s, byte[] bytes) {
		SafeTry.execute(() -> internal.updateBytes(s, bytes));
	}

	@Override
	public void updateDate(String s, Date date) {
		SafeTry.execute(() -> internal.updateDate(s, date));
	}

	@Override
	public void updateTime(String s, Time time) {
		SafeTry.execute(() -> internal.updateTime(s, time));
	}

	@Override
	public void updateTimestamp(String s, Timestamp timestamp) {
		SafeTry.execute(() -> internal.updateTimestamp(s, timestamp));
	}

	@Override
	public void updateAsciiStream(String s, InputStream inputStream, int i1) {
		SafeTry.execute(() -> internal.updateAsciiStream(s, inputStream, i1));
	}

	@Override
	public void updateBinaryStream(String s, InputStream inputStream, int i1) {
		SafeTry.execute(() -> internal.updateBinaryStream(s, inputStream, i1));
	}

	@Override
	public void updateCharacterStream(String s, Reader reader, int i1) {
		SafeTry.execute(() -> internal.updateCharacterStream(s, reader, i1));
	}

	@Override
	public void updateObject(String s, Object o, int i1) {
		SafeTry.execute(() -> internal.updateObject(s, o, i1));
	}

	@Override
	public void updateObject(String s, Object o) {
		SafeTry.execute(() -> internal.updateObject(s, o));
	}

	@Override
	public void insertRow() {
		SafeTry.execute(() -> internal.insertRow());
	}

	@Override
	public void updateRow() {
		SafeTry.execute(() -> internal.updateRow());
	}

	@Override
	public void deleteRow() {
		SafeTry.execute(() -> internal.deleteRow());
	}

	@Override
	public void refreshRow() {
		SafeTry.execute(() -> internal.refreshRow());
	}

	@Override
	public void cancelRowUpdates() {
		SafeTry.execute(() -> internal.cancelRowUpdates());
	}

	@Override
	public void moveToInsertRow() {
		SafeTry.execute(() -> internal.moveToInsertRow());
	}

	@Override
	public void moveToCurrentRow() {
		SafeTry.execute(() -> internal.moveToCurrentRow());
	}

	@Override
	public Statement getStatement() {
		return SafeTry.execute(() -> internal.getStatement(), error -> null);
	}

	@Override
	public Object getObject(int i, Map<String, Class<?>> map) {
		return SafeTry.execute(() -> internal.getObject(i, map), error -> null);
	}

	@Override
	public Ref getRef(int i) {
		return SafeTry.execute(() -> internal.getRef(i), error -> null);
	}

	@Override
	public Blob getBlob(int i) {
		return SafeTry.execute(() -> internal.getBlob(i), error -> null);
	}

	@Override
	public Clob getClob(int i) {
		return SafeTry.execute(() -> internal.getClob(i), error -> null);
	}

	@Override
	public Array getArray(int i) {
		return SafeTry.execute(() -> internal.getArray(i), error -> null);
	}

	@Override
	public Object getObject(String s, Map<String, Class<?>> map) {
		return SafeTry.execute(() -> internal.getObject(s, map), error -> null);
	}

	@Override
	public Ref getRef(String s) {
		return SafeTry.execute(() -> internal.getRef(s), error -> null);
	}

	@Override
	public Blob getBlob(String s) {
		return SafeTry.execute(() -> internal.getBlob(s), error -> null);
	}

	@Override
	public Clob getClob(String s) {
		return SafeTry.execute(() -> internal.getClob(s), error -> null);
	}

	@Override
	public Array getArray(String s) {
		return SafeTry.execute(() -> internal.getArray(s), error -> null);
	}

	@Override
	public Date getDate(int i, Calendar calendar) {
		return SafeTry.execute(() -> internal.getDate(i, calendar), error -> null);
	}

	@Override
	public Date getDate(String s, Calendar calendar) {
		return SafeTry.execute(() -> internal.getDate(s, calendar), error -> null);
	}

	@Override
	public Time getTime(int i, Calendar calendar) {
		return SafeTry.execute(() -> internal.getTime(i, calendar), error -> null);
	}

	@Override
	public Time getTime(String s, Calendar calendar) {
		return SafeTry.execute(() -> internal.getTime(s, calendar), error -> null);
	}

	@Override
	public Timestamp getTimestamp(int i, Calendar calendar) {
		return SafeTry.execute(() -> internal.getTimestamp(i, calendar), error -> null);
	}

	@Override
	public Timestamp getTimestamp(String s, Calendar calendar) {
		return SafeTry.execute(() -> internal.getTimestamp(s, calendar), error -> null);
	}

	@Override
	public URL getURL(int i) {
		return SafeTry.execute(() -> internal.getURL(i), error -> null);
	}

	@Override
	public URL getURL(String s) {
		return SafeTry.execute(() -> internal.getURL(s), error -> null);
	}

	@Override
	public void updateRef(int i, Ref ref) {
		SafeTry.execute(() -> internal.updateRef(i, ref));
	}

	@Override
	public void updateRef(String s, Ref ref) {
		SafeTry.execute(() -> internal.updateRef(s, ref));
	}

	@Override
	public void updateBlob(int i, Blob blob) {
		SafeTry.execute(() -> internal.updateBlob(i, blob));
	}

	@Override
	public void updateBlob(String s, Blob blob) {
		SafeTry.execute(() -> internal.updateBlob(s, blob));
	}

	@Override
	public void updateClob(int i, Clob clob) {
		SafeTry.execute(() -> internal.updateClob(i, clob));
	}

	@Override
	public void updateClob(String s, Clob clob) {
		SafeTry.execute(() -> internal.updateClob(s, clob));
	}

	@Override
	public void updateArray(int i, Array array) {
		SafeTry.execute(() -> internal.updateArray(i, array));
	}

	@Override
	public void updateArray(String s, Array array) {
		SafeTry.execute(() -> internal.updateArray(s, array));
	}

	@Override
	public RowId getRowId(int i) {
		return SafeTry.execute(() -> internal.getRowId(i), error -> null);
	}

	@Override
	public RowId getRowId(String s) {
		return SafeTry.execute(() -> internal.getRowId(s));
	}

	@Override
	public void updateRowId(int i, RowId rowId) {
		SafeTry.execute(() -> internal.updateRowId(i, rowId));
	}

	@Override
	public void updateRowId(String s, RowId rowId) {
		SafeTry.execute(() -> internal.updateRowId(s, rowId));
	}

	@Override
	public int getHoldability() {
		return SafeTry.execute(() -> internal.getHoldability(), error -> -1);
	}

	@Override
	public boolean isClosed() {
		return SafeTry.execute(() -> internal.isClosed(), error -> false);
	}

	@Override
	public void updateNString(int i, String s) {
		SafeTry.execute(() -> internal.updateNString(i, s));
	}

	@Override
	public void updateNString(String s, String s1) {
		SafeTry.execute(() -> internal.updateNString(s, s1));
	}

	@Override
	public void updateNClob(int i, NClob nClob) {
		SafeTry.execute(() -> internal.updateNClob(i, nClob));
	}

	@Override
	public void updateNClob(String s, NClob nClob) {
		SafeTry.execute(() -> internal.updateNClob(s, nClob));
	}

	@Override
	public NClob getNClob(int i) {
		return SafeTry.execute(() -> internal.getNClob(i), error -> null);
	}

	@Override
	public NClob getNClob(String s) {
		return SafeTry.execute(() -> internal.getNClob(s), error -> null);
	}

	@Override
	public SQLXML getSQLXML(int i) {
		return SafeTry.execute(() -> internal.getSQLXML(i), error -> null);
	}

	@Override
	public SQLXML getSQLXML(String s) {
		return SafeTry.execute(() -> internal.getSQLXML(s), error -> null);
	}

	@Override
	public void updateSQLXML(int i, SQLXML sqlxml) {
		SafeTry.execute(() -> internal.updateSQLXML(i, sqlxml));
	}

	@Override
	public void updateSQLXML(String s, SQLXML sqlxml) {
		SafeTry.execute(() -> internal.updateSQLXML(s, sqlxml));
	}

	@Override
	public String getNString(int i) {
		return SafeTry.execute(() -> internal.getNString(i), error -> null);
	}

	@Override
	public String getNString(String s) {
		return SafeTry.execute(() -> internal.getNString(s), error -> null);
	}

	@Override
	public Reader getNCharacterStream(int i) {
		return SafeTry.execute(() -> internal.getNCharacterStream(i), error -> null);
	}

	@Override
	public Reader getNCharacterStream(String s) {
		return SafeTry.execute(() -> internal.getNCharacterStream(s), error -> null);
	}

	@Override
	public void updateNCharacterStream(int i, Reader reader, long l) {
		SafeTry.execute(() -> internal.updateNCharacterStream(i, reader, l));
	}

	@Override
	public void updateNCharacterStream(String s, Reader reader, long l) {
		SafeTry.execute(() -> internal.updateNCharacterStream(s, reader, l));
	}

	@Override
	public void updateAsciiStream(int i, InputStream inputStream, long l) {
		SafeTry.execute(() -> internal.updateAsciiStream(i, inputStream, l));
	}

	@Override
	public void updateBinaryStream(int i, InputStream inputStream, long l) {
		SafeTry.execute(() -> internal.updateBinaryStream(i, inputStream, l));
	}

	@Override
	public void updateCharacterStream(int i, Reader reader, long l) {
		SafeTry.execute(() -> internal.updateCharacterStream(i, reader, l));
	}

	@Override
	public void updateAsciiStream(String s, InputStream inputStream, long l) {
		SafeTry.execute(() -> internal.updateAsciiStream(s, inputStream, l));
	}

	@Override
	public void updateBinaryStream(String s, InputStream inputStream, long l) {
		SafeTry.execute(() -> internal.updateBinaryStream(s, inputStream, l));
	}

	@Override
	public void updateCharacterStream(String s, Reader reader, long l) {
		SafeTry.execute(() -> internal.updateCharacterStream(s, reader, l));
	}

	@Override
	public void updateBlob(int i, InputStream inputStream, long l) {
		SafeTry.execute(() -> internal.updateBlob(i, inputStream, l));
	}

	@Override
	public void updateBlob(String s, InputStream inputStream, long l) {
		SafeTry.execute(() -> internal.updateBlob(s, inputStream, l));
	}

	@Override
	public void updateClob(int i, Reader reader, long l) {
		SafeTry.execute(() -> internal.updateClob(i, reader, l));
	}

	@Override
	public void updateClob(String s, Reader reader, long l) {
		SafeTry.execute(() -> internal.updateClob(s, reader, l));
	}

	@Override
	public void updateNClob(int i, Reader reader, long l) {
		SafeTry.execute(() -> internal.updateNClob(i, reader, l));
	}

	@Override
	public void updateNClob(String s, Reader reader, long l) {
		SafeTry.execute(() -> internal.updateNClob(s, reader, l));
	}

	@Override
	public void updateNCharacterStream(int i, Reader reader) {
		SafeTry.execute(() -> internal.updateNCharacterStream(i, reader));
	}

	@Override
	public void updateNCharacterStream(String s, Reader reader) {
		SafeTry.execute(() -> internal.updateNCharacterStream(s, reader));
	}

	@Override
	public void updateAsciiStream(int i, InputStream inputStream) {
		SafeTry.execute(() -> internal.updateAsciiStream(i, inputStream));
	}

	@Override
	public void updateBinaryStream(int i, InputStream inputStream) {
		SafeTry.execute(() -> internal.updateBinaryStream(i, inputStream));
	}

	@Override
	public void updateCharacterStream(int i, Reader reader) {
		SafeTry.execute(() -> internal.updateCharacterStream(i, reader));
	}

	@Override
	public void updateAsciiStream(String s, InputStream inputStream) {
		SafeTry.execute(() -> internal.updateAsciiStream(s, inputStream));
	}

	@Override
	public void updateBinaryStream(String s, InputStream inputStream) {
		SafeTry.execute(() -> internal.updateBinaryStream(s, inputStream));
	}

	@Override
	public void updateCharacterStream(String s, Reader reader) {
		SafeTry.execute(() -> internal.updateCharacterStream(s, reader));
	}

	@Override
	public void updateBlob(int i, InputStream inputStream) {
		SafeTry.execute(() -> internal.updateBlob(i, inputStream));
	}

	@Override
	public void updateBlob(String s, InputStream inputStream) {
		SafeTry.execute(() -> internal.updateBlob(s, inputStream));
	}

	@Override
	public void updateClob(int i, Reader reader) {
		SafeTry.execute(() -> internal.updateClob(i, reader));
	}

	@Override
	public void updateClob(String s, Reader reader) {
		SafeTry.execute(() -> internal.updateClob(s, reader));
	}

	@Override
	public void updateNClob(int i, Reader reader) {
		SafeTry.execute(() -> internal.updateNClob(i, reader));
	}

	@Override
	public void updateNClob(String s, Reader reader) {
		SafeTry.execute(() -> internal.updateNClob(s, reader));
	}

	@Override
	public <T> T getObject(int i, Class<T> aClass) {
		return SafeTry.execute(() -> internal.getObject(i, aClass), error -> null);
	}

	@Override
	public <T> T getObject(String s, Class<T> aClass) {
		return SafeTry.execute(() -> internal.getObject(s, aClass), error -> null);
	}

	@Override
	public <T> T unwrap(Class<T> aClass) {
		return SafeTry.execute(() -> internal.unwrap(aClass), error -> null);
	}

	@Override
	public boolean isWrapperFor(Class<?> aClass) {
		return SafeTry.execute(() -> internal.isWrapperFor(aClass), error -> false);
	}

	@Override
	public void updateObject(int columnIndex, Object x, SQLType targetSqlType, int scaleOrLength) {
		SafeTry.execute(() -> internal.updateObject(columnIndex, x, targetSqlType, scaleOrLength));
	}

	@Override
	public void updateObject(String columnLabel, Object x, SQLType targetSqlType, int scaleOrLength) {
		SafeTry.execute(() -> internal.updateObject(columnLabel, x, targetSqlType, scaleOrLength));
	}

	@Override
	public void updateObject(int columnIndex, Object x, SQLType targetSqlType) {
		SafeTry.execute(() -> internal.updateObject(columnIndex, x, targetSqlType));
	}

	@Override
	public void updateObject(String columnLabel, Object x, SQLType targetSqlType) {
		SafeTry.execute(() -> internal.updateObject(columnLabel, x, targetSqlType));
	}
}
