package nl.makertim.fm.database.core;

import org.apache.commons.lang3.NotImplementedException;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;
import java.util.Map;

public class SafeResultSetExecute extends SafeResultSet {

	private final boolean result;

	public SafeResultSetExecute(boolean result) {
		super(null);
		this.result = result;
	}

	public boolean getResult() {
		return result;
	}

	public boolean getBoolean() {
		return getResult();
	}

	@Override
	public boolean getBoolean(int i) {
		if (i == 1) return getBoolean();
		return false;
	}

	@Override
	public boolean getBoolean(String s) {
		return getBoolean();
	}

	@Override
	public boolean next() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void close() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean wasNull() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public String getString(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public byte getByte(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public short getShort(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public int getInt(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public long getLong(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public float getFloat(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public double getDouble(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public BigDecimal getBigDecimal(int i, int i1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public byte[] getBytes(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Date getDate(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Time getTime(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Timestamp getTimestamp(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public InputStream getAsciiStream(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public InputStream getUnicodeStream(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public InputStream getBinaryStream(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public String getString(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public byte getByte(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public short getShort(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public int getInt(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public long getLong(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public float getFloat(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public double getDouble(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public BigDecimal getBigDecimal(String s, int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public byte[] getBytes(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Date getDate(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Time getTime(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Timestamp getTimestamp(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public InputStream getAsciiStream(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public InputStream getUnicodeStream(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public InputStream getBinaryStream(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public SQLWarning getWarnings() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void clearWarnings() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public String getCursorName() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public ResultSetMetaData getMetaData() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Object getObject(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Object getObject(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public int findColumn(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Reader getCharacterStream(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Reader getCharacterStream(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public BigDecimal getBigDecimal(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public BigDecimal getBigDecimal(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean isBeforeFirst() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean isAfterLast() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean isFirst() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean isLast() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void beforeFirst() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void afterLast() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean first() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean last() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public int getRow() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean absolute(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean relative(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean previous() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void setFetchDirection(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public int getFetchDirection() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void setFetchSize(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public int getFetchSize() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public int getType() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public int getConcurrency() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean rowUpdated() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean rowInserted() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean rowDeleted() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateNull(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBoolean(int i, boolean b) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateByte(int i, byte b) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateShort(int i, short i1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateInt(int i, int i1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateLong(int i, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateFloat(int i, float v) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateDouble(int i, double v) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBigDecimal(int i, BigDecimal bigDecimal) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateString(int i, String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBytes(int i, byte[] bytes) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateDate(int i, Date date) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateTime(int i, Time time) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateTimestamp(int i, Timestamp timestamp) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateAsciiStream(int i, InputStream inputStream, int i1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBinaryStream(int i, InputStream inputStream, int i1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateCharacterStream(int i, Reader reader, int i1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateObject(int i, Object o, int i1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateObject(int i, Object o) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateNull(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBoolean(String s, boolean b) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateByte(String s, byte b) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateShort(String s, short i1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateInt(String s, int i1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateLong(String s, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateFloat(String s, float v) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateDouble(String s, double v) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBigDecimal(String s, BigDecimal bigDecimal) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateString(String s, String s1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBytes(String s, byte[] bytes) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateDate(String s, Date date) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateTime(String s, Time time) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateTimestamp(String s, Timestamp timestamp) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateAsciiStream(String s, InputStream inputStream, int i1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBinaryStream(String s, InputStream inputStream, int i1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateCharacterStream(String s, Reader reader, int i1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateObject(String s, Object o, int i1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateObject(String s, Object o) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void insertRow() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateRow() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void deleteRow() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void refreshRow() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void cancelRowUpdates() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void moveToInsertRow() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void moveToCurrentRow() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Statement getStatement() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Object getObject(int i, Map<String, Class<?>> map) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Ref getRef(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Blob getBlob(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Clob getClob(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Array getArray(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Object getObject(String s, Map<String, Class<?>> map) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Ref getRef(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Blob getBlob(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Clob getClob(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Array getArray(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Date getDate(int i, Calendar calendar) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Date getDate(String s, Calendar calendar) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Time getTime(int i, Calendar calendar) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Time getTime(String s, Calendar calendar) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Timestamp getTimestamp(int i, Calendar calendar) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Timestamp getTimestamp(String s, Calendar calendar) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public URL getURL(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public URL getURL(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateRef(int i, Ref ref) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateRef(String s, Ref ref) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBlob(int i, Blob blob) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBlob(String s, Blob blob) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateClob(int i, Clob clob) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateClob(String s, Clob clob) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateArray(int i, Array array) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateArray(String s, Array array) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public RowId getRowId(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public RowId getRowId(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateRowId(int i, RowId rowId) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateRowId(String s, RowId rowId) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public int getHoldability() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean isClosed() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateNString(int i, String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateNString(String s, String s1) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateNClob(int i, NClob nClob) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateNClob(String s, NClob nClob) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public NClob getNClob(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public NClob getNClob(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public SQLXML getSQLXML(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public SQLXML getSQLXML(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateSQLXML(int i, SQLXML sqlxml) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateSQLXML(String s, SQLXML sqlxml) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public String getNString(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public String getNString(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Reader getNCharacterStream(int i) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public Reader getNCharacterStream(String s) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateNCharacterStream(int i, Reader reader, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateNCharacterStream(String s, Reader reader, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateAsciiStream(int i, InputStream inputStream, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBinaryStream(int i, InputStream inputStream, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateCharacterStream(int i, Reader reader, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateAsciiStream(String s, InputStream inputStream, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBinaryStream(String s, InputStream inputStream, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateCharacterStream(String s, Reader reader, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBlob(int i, InputStream inputStream, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBlob(String s, InputStream inputStream, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateClob(int i, Reader reader, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateClob(String s, Reader reader, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateNClob(int i, Reader reader, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateNClob(String s, Reader reader, long l) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateNCharacterStream(int i, Reader reader) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateNCharacterStream(String s, Reader reader) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateAsciiStream(int i, InputStream inputStream) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBinaryStream(int i, InputStream inputStream) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateCharacterStream(int i, Reader reader) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateAsciiStream(String s, InputStream inputStream) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBinaryStream(String s, InputStream inputStream) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateCharacterStream(String s, Reader reader) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBlob(int i, InputStream inputStream) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateBlob(String s, InputStream inputStream) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateClob(int i, Reader reader) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateClob(String s, Reader reader) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateNClob(int i, Reader reader) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateNClob(String s, Reader reader) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public <T> T getObject(int i, Class<T> aClass) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public <T> T getObject(String s, Class<T> aClass) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public <T> T unwrap(Class<T> aClass) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean isWrapperFor(Class<?> aClass) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}


	@Override
	public void updateObject(int columnIndex, Object x, SQLType targetSqlType, int scaleOrLength) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateObject(String columnLabel, Object x, SQLType targetSqlType, int scaleOrLength) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateObject(int columnIndex, Object x, SQLType targetSqlType) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public void updateObject(String columnLabel, Object x, SQLType targetSqlType) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public boolean equals(Object o) {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public int hashCode() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}

	@Override
	public String toString() {
		throw new NotImplementedException("Only getBoolean(1) is supported");
	}
}
