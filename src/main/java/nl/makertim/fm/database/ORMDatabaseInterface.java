package nl.makertim.fm.database;

import nl.makertim.fm.database.core.SafeResultSet;
import nl.makertim.fm.database.oop.PreparedQuery;
import nl.makertim.fm.database.oop.query.Query;
import nl.makertim.fm.database.oop.query.ResultQuery;
import nl.makertim.fm.safe.ThrowingFunction;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface ORMDatabaseInterface {
	/* ------------------------- */
	/*            ORM            */
	/* ------------------------- */
	<T> List<T> getMultiple(ResultQuery<?> query,
							ThrowingFunction<SafeResultSet, T, SQLException> function);

	<T> List<T> getMultiple(PreparedQuery query,
							ThrowingFunction<SafeResultSet, T, SQLException> function);

	<T> List<T> getMultiple(ResultQuery<?> query,
							Map<String, Object> variables,
							ThrowingFunction<SafeResultSet, T, SQLException> function);

	<T> List<T> getMultiple(PreparedQuery query,
							Map<String, Object> variables,
							ThrowingFunction<SafeResultSet, T, SQLException> function);

	<T> List<T> getMultiple(Connection connection, ResultQuery<?> query,
							ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException;

	<T> List<T> getMultiple(Connection connection, PreparedQuery query,
							ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException;

	<T> List<T> getMultiple(Connection connection, ResultQuery<?> query,
							Map<String, Object> variables,
							ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException;

	<T> List<T> getMultiple(Connection connection, PreparedQuery query,
							Map<String, Object> variables,
							ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException;

	<T> T getSingle(ResultQuery<?> query,
					ThrowingFunction<SafeResultSet, T, SQLException> function);

	<T> T getSingle(PreparedQuery query,
					ThrowingFunction<SafeResultSet, T, SQLException> function);

	<T> T getSingle(ResultQuery<?> query,
					Map<String, Object> variables,
					ThrowingFunction<SafeResultSet, T, SQLException> function);

	<T> T getSingle(PreparedQuery query,
					Map<String, Object> variables,
					ThrowingFunction<SafeResultSet, T, SQLException> function);

	<T> T getSingle(Connection connection, ResultQuery<?> query,
					ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException;

	<T> T getSingle(Connection connection, PreparedQuery query,
					ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException;

	<T> T getSingle(Connection connection, ResultQuery<?> query,
					Map<String, Object> variables,
					ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException;

	<T> T getSingle(Connection connection, PreparedQuery query,
					Map<String, Object> variables,
					ThrowingFunction<SafeResultSet, T, SQLException> function) throws SQLException;

	boolean execute(Query<?> query);

	boolean execute(PreparedQuery query);

	boolean execute(Query<?> query, Map<String, Object> variables);

	boolean execute(PreparedQuery query, Map<String, Object> variables);

	boolean execute(Connection connection, Query<?> query) throws SQLException;

	boolean execute(Connection connection, PreparedQuery query) throws SQLException;

	boolean execute(Connection connection, Query<?> query,
					Map<String, Object> variables) throws SQLException;

	boolean execute(Connection connection, PreparedQuery query,
					Map<String, Object> variables) throws SQLException;
}
