package nl.makertim.fm.database;

import nl.makertim.fm.database.config.DatabaseConfig;
import nl.makertim.fm.database.oop.Dialect;
import nl.makertim.fm.safe.ThrowingConsumer;
import nl.makertim.fm.safe.ThrowingFunction;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.function.Consumer;

public interface FunctionalDatabaseInterface {
	/* ------------------------- */
	/*         Instance          */
	/* ------------------------- */
	DatabaseInterface open();

	Dialect getDialect();

	void getConnectionExecute(ThrowingConsumer<Connection, SQLException> connectionFunction);

	<T> T getConnectionWithResult(ThrowingFunction<Connection, T, SQLException> connectionFunction);

	DatabaseConfig<?, ?> internal();

	DatabaseInterface setupSettingsManually(Consumer<DatabaseConfig<?, ?>> settings);

	boolean close();

}
