package nl.makertim.fm.database.model.primarykey;

public class TriPrimaryKey<T1, T2, T3> implements PrimaryKey {

	protected final String column0;
	protected final String column1;
	protected final String column2;
	protected final T1 value0;
	protected final T2 value1;
	protected final T3 value2;

	public TriPrimaryKey(String column0, String column1, String column2, T1 value0, T2 value1, T3 value2) {
		this.column0 = column0;
		this.column1 = column1;
		this.column2 = column2;
		this.value0 = value0;
		this.value1 = value1;
		this.value2 = value2;
	}

	@Override
	public String[] getColumns() {
		return new String[] { column0, column1, column2 };
	}

	public String getColumn0() {
		return column0;
	}

	public String getColumn1() {
		return column1;
	}

	public String getColumn2() {
		return column2;
	}

	@Override
	public String getColumn(int index) {
		return index == 0 ? column0 :
			index == 1 ? column1 :
				index == 2 ? column2 : null;
	}

	@Override
	public int getColumnIndex(String column) {
		return this.column0.equalsIgnoreCase(column) ? 0 :
			this.column1.equalsIgnoreCase(column) ? 1 :
				this.column2.equalsIgnoreCase(column) ? 2 : -1;
	}

	@Override
	public Object[] getValues() {
		return new Object[] { value0, value1, value2 };
	}

	public T1 getValue0() {
		return value0;
	}

	public T2 getValue1() {
		return value1;
	}

	public T3 getValue2() {
		return value2;
	}

	@Override
	public Object getValue(String column) {
		return this.column0.equalsIgnoreCase(column) ? value0 :
			this.column1.equalsIgnoreCase(column) ? value1 :
				this.column2.equalsIgnoreCase(column) ? value2 : null;
	}

	@Override
	public Object getValue(int index) {
		return index == 0 ? value0 :
			index == 1 ? value1 :
				index == 2 ? value2 : null;
	}
}
