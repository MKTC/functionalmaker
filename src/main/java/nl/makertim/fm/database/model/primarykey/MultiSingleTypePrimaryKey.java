package nl.makertim.fm.database.model.primarykey;

public class MultiSingleTypePrimaryKey<T> implements PrimaryKey {

	protected final String[] columns;
	protected final T[] values;

	@SafeVarargs
	public MultiSingleTypePrimaryKey(String[] columns, T... values) {
		this.columns = columns;
		this.values = values;
	}

	@Override
	public String[] getColumns() {
		return columns;
	}

	@Override
	public String getColumn(int index) {
		if (index >= columns.length || index < 0) {
			return null;
		}
		return columns[index];
	}

	@Override
	public int getColumnIndex(String column) {
		for (int i = 0; i < columns.length; i++) {
			if (columns[i].equalsIgnoreCase(column)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public T[] getValues() {
		return values;
	}

	@Override
	public T getValue(String column) {
		for (int i = 0; i < columns.length; i++) {
			if (columns[i].equalsIgnoreCase(column)) {
				return values[i];
			}
		}
		return null;
	}

	@Override
	public T getValue(int index) {
		if (index >= values.length || index < 0) {
			return null;
		}
		return values[index];
	}
}
