package nl.makertim.fm.database.model.primarykey;

public class BiPrimaryKey<T1, T2> implements PrimaryKey {

	protected final String column0;
	protected final String column1;
	protected final T1 value0;
	protected final T2 value1;

	public BiPrimaryKey(String column0, String column1, T1 value0, T2 value1) {
		this.column0 = column0;
		this.column1 = column1;
		this.value0 = value0;
		this.value1 = value1;
	}

	@Override
	public String[] getColumns() {
		return new String[] { column0, column1 };
	}

	public String getColumn0() {
		return column0;
	}

	public String getColumn1() {
		return column1;
	}

	@Override
	public String getColumn(int index) {
		return index == 0 ? column0 :
			index == 1 ? column1 : null;
	}

	@Override
	public int getColumnIndex(String column) {
		return this.column0.equalsIgnoreCase(column) ? 0 :
			this.column1.equalsIgnoreCase(column) ? 1 : -1;
	}

	@Override
	public Object[] getValues() {
		return new Object[] { value0, value1 };
	}

	public T1 getValue0() {
		return value0;
	}

	public T2 getValue1() {
		return value1;
	}

	@Override
	public Object getValue(String column) {
		return this.column0.equalsIgnoreCase(column) ? value0 :
			this.column1.equalsIgnoreCase(column) ? value1 : null;
	}

	@Override
	public Object getValue(int index) {
		return index == 0 ? value0 :
			index == 1 ? value1 : null;
	}
}
