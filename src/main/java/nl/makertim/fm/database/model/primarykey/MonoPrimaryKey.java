package nl.makertim.fm.database.model.primarykey;

public class MonoPrimaryKey<T> implements PrimaryKey {

	protected final String column;
	protected final T value;

	public MonoPrimaryKey(String column, T value) {
		this.column = column;
		this.value = value;
	}

	@Override
	public String[] getColumns() {
		return new String[] { column };
	}

	public String getColumn() {
		return column;
	}

	@Override
	public String getColumn(int index) {
		return index == 0 ? column : null;
	}

	@Override
	public int getColumnIndex(String column) {
		return this.column.equalsIgnoreCase(column) ? 0 : -1;
	}

	@Override
	public Object[] getValues() {
		return new Object[] { value };
	}

	public T getValue() {
		return value;
	}

	@Override
	public T getValue(String column) {
		return this.column.equalsIgnoreCase(column) ? value : null;
	}

	@Override
	public T getValue(int index) {
		return index == 0 ? value : null;
	}
}
