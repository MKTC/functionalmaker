package nl.makertim.fm.database.model.primarykey;

public class MultiPrimaryKey extends MultiSingleTypePrimaryKey<Object> {

	public MultiPrimaryKey(String[] columns, Object... values) {
		super(columns, values);
	}
}
