package nl.makertim.fm.database.model.primarykey;

public interface PrimaryKey {

	String[] getColumns();

	String getColumn(int index);

	int getColumnIndex(String column);

	Object[] getValues();

	Object getValue(String column);

	Object getValue(int index);

	default <T> T getValueAs(String column, Class<T> clazz) {
		return getValueAs(getColumnIndex(column), clazz);
	}

	@SuppressWarnings("unchecked")
	default <T> T getValueAs(int index, Class<T> clazz) {
		Object value = getValue(index);
		if (value == null) {
			return null;
		}
		if (clazz.isAssignableFrom(value.getClass())) {
			return (T) value;
		}
		throw new ClassCastException(String.format("%s is not assignable for %s(%s)", clazz, value.toString(), value.getClass()));
	}
}
