package nl.makertim.fm.core;

import nl.makertim.fm.safe.SafeTry;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Settings {

	private final String name;
	private final Map<String, Object> settingsMap;

	public Settings(String name, Map<String, Object> settingsMap) {
		this.name = name;
		this.settingsMap = new HashMap<>(settingsMap);
	}

	public Integer getInt(String fullPath) {
		Object settingObject = get(fullPath);
		if (settingObject instanceof Number) {
			return ((Number) settingObject).intValue();
		}
		warn("getInt", "integer", fullPath, settingObject);
		return null;
	}

	public Double getDouble(String fullPath) {
		Object settingObject = get(fullPath);
		if (settingObject instanceof Number) {
			return ((Number) settingObject).doubleValue();
		}
		warn("getDouble", "double", fullPath, settingObject);
		return null;
	}

	public Boolean getBoolean(String fullPath) {
		Object settingObject = get(fullPath);
		if (settingObject instanceof Boolean) {
			return (Boolean) settingObject;
		} else if (settingObject instanceof Number) {
			return ((Number) settingObject).intValue() != 0;
		}
		warn("getBoolean", "boolean", fullPath, settingObject);
		return null;
	}

	public String getString(String fullPath) {
		Object settingObject = get(fullPath);
		if (settingObject instanceof String) {
			String settingString = (String) settingObject;
			if (!settingString.startsWith("$(")
				|| !settingString.endsWith(")")) {
				return settingString;
			}
			if (settingString.startsWith("$(read:")) {
				String path = settingString.substring(2, settingString.length() - 1);
				return SafeTry.execute(() -> Files.readString(Paths.get(path)));
			}
			return System.getenv(settingString.substring(6, settingString.length() - 1));
		} else if (settingObject instanceof Number || settingObject instanceof Boolean) {
			return settingObject.toString();
		}
		warn("getString", "string", fullPath, settingObject);
		return settingObject == null ? null : settingObject.toString();
	}


	public boolean has(String fullPath) {
		return get(fullPath) != null;
	}

	public Object get(String fullPath) {
		Map<?, ?> map = settingsMap;
		if (fullPath != null && !fullPath.isEmpty()) {
			for (String path : fullPath.split("\\.")) {
				Object settingObject = map.get(path);
				if (!(settingObject instanceof Map)) {
					return settingObject;
				}
				map = (Map<?, ?>) settingObject;
			}
		}
		return map;
	}

	public String getName() {
		return name;
	}

	private void warn(String methodName, String expectedType, String path, Object settingObject) {
		Logger.warn("Settings %s tried %s but value was not an %s. It was an %s type. Path was `%s`",
			name,
			methodName,
			expectedType,
			settingObject == null ? "null" : settingObject.getClass(),
			path
		);
	}
}
