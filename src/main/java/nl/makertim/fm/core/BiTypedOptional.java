package nl.makertim.fm.core;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class BiTypedOptional<T1, T2> {

	private static final BiTypedOptional<?, ?> EMPTY = new BiTypedOptional<>(null);

	public static <T1, T2> BiTypedOptional<T1, T2> empty() {
		//noinspection unchecked
		return (BiTypedOptional<T1, T2>) EMPTY;
	}

	private final Object value;

	private BiTypedOptional(Object value) {
		this.value = value;
	}

	public static <T1, T2> BiTypedOptional<T1, T2> ofT1(T1 value) {
		return new BiTypedOptional<>(Objects.requireNonNull(value));
	}

	public static <T1, T2> BiTypedOptional<T1, T2> ofT2(T2 value) {
		return new BiTypedOptional<>(Objects.requireNonNull(value));
	}

	public static <T1, T2> BiTypedOptional<T1, T2> ofNullableT1(T1 value) {
		//noinspection unchecked
		return value == null ?
			(BiTypedOptional<T1, T2>) EMPTY
			: new BiTypedOptional<>(value);
	}

	public static <T1, T2> BiTypedOptional<T1, T2> ofNullableT2(T2 value) {
		//noinspection unchecked
		return value == null ?
			(BiTypedOptional<T1, T2>) EMPTY
			: new BiTypedOptional<>(value);
	}

	public T1 getType1(Class<? super T1> t1c) {
		if (value == null) {
			throw new NoSuchElementException("No value present");
		}
		if (!hasType1(t1c)) {
			throw new NoSuchElementException("Value is not type of first type");
		}
		//noinspection unchecked
		return (T1) value;
	}

	public boolean hasType1(Class<? super T1> t1c) {
		if (this.value == null) {
			return false;
		}
		return t1c.isInstance(this.value);
	}

	public T2 getType2(Class<? super T2> t2c) {
		if (value == null) {
			throw new NoSuchElementException("No value present");
		}
		if (!hasType2(t2c)) {
			throw new NoSuchElementException("Value is not type of first type");
		}
		//noinspection unchecked
		return (T2) value;
	}

	public boolean hasType2(Class<? super T2> t2c) {
		if (value == null) {
			return false;
		}
		return t2c.isInstance(this.value);
	}

	public boolean isPresent() {
		return value != null;
	}

	public boolean isEmpty() {
		return value == null;
	}

	public BiTypedOptional<T1, T2> ifT1Present(Class<? super T1> t1c, Consumer<? super T1> action) {
		if (hasType1(t1c)) {
			//noinspection unchecked
			action.accept((T1) value);
		}
		return this;
	}

	public BiTypedOptional<T1, T2> ifT2Present(Class<? super T2> t2c, Consumer<? super T2> action) {
		if (hasType2(t2c)) {
			//noinspection unchecked
			action.accept((T2) value);
		}
		return this;
	}

	public BiTypedOptional<T1, T2> ifT1PresentOrElse(Class<? super T1> t1c, Consumer<? super T1> action, Runnable emptyAction) {
		if (hasType1(t1c)) {
			ifT1Present(t1c, action);
		} else {
			emptyAction.run();
		}
		return this;
	}

	public BiTypedOptional<T1, T2> ifT2PresentOrElse(Class<? super T2> t2c, Consumer<? super T2> action, Runnable emptyAction) {
		if (hasType2(t2c)) {
			ifT2Present(t2c, action);
		} else {
			emptyAction.run();
		}
		return this;
	}

	public Optional<T1> filterIfT1(Class<? super T1> t1c) {
		if (isEmpty()) {
			return Optional.empty();
		}
		if (!hasType1(t1c)) {
			return Optional.empty();
		}
		//noinspection unchecked
		return (Optional<T1>) Optional.of(value);
	}

	public Optional<T2> filterIfT2(Class<? super T2> t2c) {
		if (isEmpty()) {
			return Optional.empty();
		}
		if (!hasType2(t2c)) {
			return Optional.empty();
		}
		//noinspection unchecked
		return (Optional<T2>) Optional.of(value);
	}

	public Optional<T1> orT1(Class<? super T1> t1c, Supplier<? extends T1> supplier) {
		if (hasType1(t1c)) {
			return filterIfT1(t1c);
		} else {
			return Optional.ofNullable(supplier.get());
		}
	}

	public Optional<T2> orT2(Class<? super T2> t2c, Supplier<? extends T2> supplier) {
		if (hasType2(t2c)) {
			return filterIfT2(t2c);
		} else {
			return Optional.ofNullable(supplier.get());
		}
	}

	public Stream<T1> streamT1(Class<? super T1> t1c) {
		if (!hasType1(t1c)) {
			return Stream.empty();
		} else {
			//noinspection unchecked
			return (Stream<T1>) Stream.of(value);
		}
	}

	public Stream<T2> streamT2(Class<? super T2> t2c) {
		if (!hasType2(t2c)) {
			return Stream.empty();
		} else {
			//noinspection unchecked
			return (Stream<T2>) Stream.of(value);
		}
	}

	public T1 orElseT1(T1 other) {
		//noinspection unchecked
		if (hasType1((Class<? super T1>) other.getClass())) {
			//noinspection unchecked
			return (T1) value;
		}
		return other;
	}

	public T2 orElseT2(T2 other) {
		//noinspection unchecked
		if (hasType2((Class<? super T2>) other.getClass())) {
			//noinspection unchecked
			return (T2) value;
		}
		return other;
	}

	public T1 orElseThrowT1(Class<? super T1> t1c) {
		if (!isPresent()) {
			throw new NoSuchElementException("No value present");
		}
		if (!hasType1(t1c)) {
			throw new NoSuchElementException("No type 1 value present");
		}
		//noinspection unchecked
		return (T1) value;
	}

	public T2 orElseThrowT2(Class<? super T2> t2c) {
		if (!isPresent()) {
			throw new NoSuchElementException("No value present");
		}
		if (!hasType2(t2c)) {
			throw new NoSuchElementException("No type 2 value present");
		}
		//noinspection unchecked
		return (T2) value;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BiTypedOptional)) {
			return false;
		}

		BiTypedOptional<?, ?> other = (BiTypedOptional<?, ?>) obj;
		return Objects.equals(value, other.value);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(value);
	}

	@Override
	public String toString() {
		return value != null
			? String.format("BiOptional[%s]", value)
			: "BiOptional.empty";
	}
}
