package nl.makertim.fm.core;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.function.Function;

public class TriSet<T1, T2, T3> {

	private T1 t1;
	private T2 t2;
	private T3 t3;

	public TriSet() {
	}

	public TriSet(T1 t1, T2 t2, T3 t3) {
		this.t1 = t1;
		this.t2 = t2;
		this.t3 = t3;
	}

	public T1 getT1() {
		return t1;
	}

	public T2 getT2() {
		return t2;
	}

	public T3 getT3() {
		return t3;
	}

	public TriSet<T1, T2, T3> setT1(T1 t1) {
		this.t1 = t1;
		return this;
	}

	public TriSet<T1, T2, T3> setT2(T2 t2) {
		this.t2 = t2;
		return this;
	}

	public TriSet<T1, T2, T3> setT3(T3 t3) {
		this.t3 = t3;
		return this;
	}

	public <T4, T5, T6> TriSet<T4, T5, T6> map(Function<T1, T4> mapT1Function, Function<T2, T5> mapT2Function, Function<T3, T6> mapT3Function) {
		return new TriSet<>(mapT1Function.apply(t1), mapT2Function.apply(t2), mapT3Function.apply(t3));
	}

	public <T4> TriSet<T4, T2, T3> mapT1(Function<T1, T4> mapFunction) {
		return new TriSet<>(mapFunction.apply(t1), t2, t3);
	}

	public <T4> TriSet<T1, T4, T3> mapT2(Function<T2, T4> mapFunction) {
		return new TriSet<>(t1, mapFunction.apply(t2), t3);
	}

	public <T4> TriSet<T1, T2, T4> mapT3(Function<T3, T4> mapFunction) {
		return new TriSet<>(t1, t2, mapFunction.apply(t3));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TriSet<?, ?, ?> triSet = (TriSet<?, ?, ?>) o;
		return new EqualsBuilder()
			.append(t1, triSet.t1)
			.append(t2, triSet.t2)
			.append(t3, triSet.t3)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
			.append(t1)
			.append(t2)
			.append(t3)
			.toHashCode();
	}
}
