package nl.makertim.fm.core;

import org.apache.commons.lang3.builder.EqualsBuilder;

import java.util.function.Function;

public class Property<P> {

	private P property;

	public static <P> Property<P> empty() {
		return of(null);
	}

	public static <P> Property<P> of(P property) {
		return new Property<>(property);
	}

	public Property() {
	}

	public Property(P property) {
		this.property = property;
	}

	public P get() {
		return property;
	}

	public Property<P> set(P p) {
		this.property = p;
		return this;
	}

	public boolean isPresent() {
		return property != null;
	}

	public boolean isNull() {
		return property == null;
	}

	public <PNew> Property<PNew> map(Function<P, PNew> mapFunction) {
		return new Property<>(mapFunction.apply(property));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Property<?> biSet = (Property<?>) o;
		return new EqualsBuilder()
			.append(property, biSet.property)
			.isEquals();
	}
}
