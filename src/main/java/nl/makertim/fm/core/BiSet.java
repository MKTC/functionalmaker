package nl.makertim.fm.core;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Map;
import java.util.function.Function;

public class BiSet<T1, T2> implements Map.Entry<T1, T2> {

	private T1 t1;
	private T2 t2;

	public BiSet() {
	}

	public BiSet(T1 t1, T2 t2) {
		this.t1 = t1;
		this.t2 = t2;
	}

	public T1 getT1() {
		return t1;
	}

	public T2 getT2() {
		return t2;
	}

	public BiSet<T1, T2> setT1(T1 t1) {
		this.t1 = t1;
		return this;
	}

	public BiSet<T1, T2> setT2(T2 t2) {
		this.t2 = t2;
		return this;
	}

	public <T3, T4> BiSet<T3, T4> map(Function<T1, T3> mapT1Function, Function<T2, T4> mapT2Function) {
		return new BiSet<>(mapT1Function.apply(t1), mapT2Function.apply(t2));
	}

	public <T3> BiSet<T3, T2> mapT1(Function<T1, T3> mapFunction) {
		return new BiSet<>(mapFunction.apply(t1), t2);
	}

	public <T3> BiSet<T1, T3> mapT2(Function<T2, T3> mapFunction) {
		return new BiSet<>(t1, mapFunction.apply(t2));
	}

	@Override
	public T1 getKey() {
		return t1;
	}

	@Override
	public T2 getValue() {
		return t2;
	}

	@Override
	public T2 setValue(T2 value) {
		T2 old = t2;
		t2 = value;
		return old;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		BiSet<?, ?> biSet = (BiSet<?, ?>) o;
		return new EqualsBuilder()
			.append(t1, biSet.t1)
			.append(t2, biSet.t2)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
			.append(t1)
			.append(t2)
			.toHashCode();
	}
}
