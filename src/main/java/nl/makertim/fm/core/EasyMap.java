package nl.makertim.fm.core;

import java.util.HashMap;
import java.util.function.Consumer;

public class EasyMap extends HashMap<String, Object> {

	public static EasyMap map() {
		return new EasyMap();
	}

	public static EasyMap map(String key, Object value) {
		return map()
			.insert(key, value);
	}

	public static EasyMap map(String keyA, Object valueA, String keyB, Object valueB) {
		return map()
			.insert(keyA, valueA)
			.insert(keyB, valueB);
	}

	public static EasyMap map(String keyA, Object valueA, String keyB, Object valueB, String keyC, Object valueC) {
		return map()
			.insert(keyA, valueA)
			.insert(keyB, valueB)
			.insert(keyC, valueC);
	}

	public static EasyMap map(Object... keyValues) {
		EasyMap map = map();

		if (keyValues.length % 2 != 0) {
			throw new IllegalArgumentException("Every key needs a value: (String, Object)... now they are mis-matched");
		}

		for (int i = 0; i < keyValues.length; i += 2) {
			Object key = keyValues[i];
			Object value = keyValues[i + 1];

			if (!(key instanceof String)) {
				throw new IllegalArgumentException("All keys should be a string: (String, Object)... "
					+ key.getClass().getSimpleName());
			}
			map.insert((String) key, value);
		}

		return map;
	}

	public static EasyMap map(Consumer<EasyMap> map) {
		EasyMap newMap = map();
		map.accept(newMap);
		return newMap;
	}

	public EasyMap insert(String key, Object value) {
		return put(key, value);
	}

	public EasyMap add(String key, Object value) {
		return put(key, value);
	}

	public EasyMap set(String key, Object value) {
		return put(key, value);
	}

	@Override
	public EasyMap put(String key, Object value) {
		super.put(key, value);
		return this;
	}
}
