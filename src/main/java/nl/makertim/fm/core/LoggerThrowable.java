package nl.makertim.fm.core;

import java.util.Arrays;

public class LoggerThrowable extends Throwable {

	private static final int layers = 2;

	public LoggerThrowable(String message) {
		super(message);
	}

	public LoggerThrowable(Throwable cause) {
		super(cause);
	}

	@Override
	public String getMessage() {
		Throwable cause = getCause();
		return cause != null ? cause.getMessage() : super.getMessage();
	}

	@Override
	public StackTraceElement[] getStackTrace() {
		var stackTrace = super.getStackTrace();
		return Arrays.copyOfRange(stackTrace, layers, stackTrace.length);
	}
}
