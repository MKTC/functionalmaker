package nl.makertim.fm.core;

import com.bugsnag.Bugsnag;
import com.bugsnag.Severity;
import nl.makertim.fm.FunctionalMaker;
import nl.makertim.fm.safe.SafeTry;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static nl.makertim.fm.core.Logger.Level.*;

public class Logger {

	public static boolean canLog;
	private static final String fileLocation = "./logs/";
	private static final DateFormat fileFormat = new SimpleDateFormat("yyyy-MM-dd' %l.log'");
	private static final DateFormat prefixFormat = new SimpleDateFormat("yyyy-MM-dd | HH:mm:ss.SS");

	static final Bugsnag bugsnag;

	static {
		canLog = new File(fileLocation).mkdir();
		bugsnag = new Bugsnag(FunctionalMaker.getProperty("bugsnagKey"), false);
	}

	public enum Level {
		VERBOSE, INFO, WARN, ERROR, DANGER;

		static Level[] levelsInOrder = new Level[] { VERBOSE, INFO, WARN, ERROR, DANGER };

		Severity toBugsnagSeverity() {
			switch (this) {
				default:
				case VERBOSE:
				case INFO:
					return Severity.INFO;
				case WARN:
					return Severity.WARNING;
				case ERROR:
				case DANGER:
					return Severity.ERROR;
			}
		}
	}

	public static boolean verbose(String message) {
		return log(VERBOSE, message);
	}

	public static boolean info(String message) {
		return log(INFO, message);
	}

	public static boolean warn(String message) {
		return log(WARN, message);
	}

	public static boolean error(String message) {
		return log(ERROR, message);
	}

	public static boolean danger(String message) {
		return log(DANGER, message);
	}

	public static boolean verbose(String message, Object... args) {
		return log(VERBOSE, String.format(message, args));
	}

	public static boolean info(String message, Object... args) {
		return log(INFO, String.format(message, args));
	}

	public static boolean warn(String message, Object... args) {
		return log(WARN, String.format(message, args));
	}

	public static boolean error(String message, Object... args) {
		return log(ERROR, String.format(message, args));
	}

	public static boolean danger(String message, Object... args) {
		return log(DANGER, String.format(message, args));
	}

	public static boolean verbose(Throwable throwable) {
		return log(VERBOSE, throwable);
	}

	public static boolean info(Throwable throwable) {
		return log(INFO, throwable);
	}

	public static boolean warn(Throwable throwable) {
		return log(WARN, throwable);
	}

	public static boolean error(Throwable throwable) {
		return log(ERROR, throwable);
	}

	public static boolean danger(Throwable throwable) {
		return log(DANGER, throwable);
	}

	private static String prefixMessage(Level level, String message) {
		return
			makeCorrectLength(prefixFormat.format(new Date()), 25) + " | " +
				makeCorrectLength(Thread.currentThread().getName(), 12) + " | " +
				makeCorrectLength(level.name(), 7) + " | " +
				message + "\r\n";
	}

	private static String makeCorrectLength(String input, int length) {
		StringBuilder inputBuilder = new StringBuilder(input);
		while (inputBuilder.length() < length) {
			inputBuilder.append(' ');
		}
		return inputBuilder.subSequence(0, length).toString();
	}

	private static boolean log(Level level, Throwable throwable) {
		StringBuilder message = new StringBuilder();
		message.append(throwable.getClass().getName());
		if (throwable.getMessage() != null) {
			message.append(": ");
			message.append('"');
			message.append(throwable.getMessage().replaceAll("\"", "'"));
			message.append('"');
		}
		for (int i = 0; i < Math.min(15, throwable.getStackTrace().length); i++) {
			message.append(" @ ");
			message.append(throwable.getStackTrace()[i]);
		}

		bugsnag.notify(throwable, level.toBugsnagSeverity());
		return log(level, message.toString(), false);
	}

	public static boolean log(Level level, String message) {
		return log(level, message, true);
	}

	private static synchronized boolean log(Level level, String message, boolean isTextMessage) {
		String prefixedMessage = prefixMessage(level, message);
		if (level == DANGER || level == ERROR) {
			System.err.print(prefixedMessage);
		} else if (level != VERBOSE) {
			System.out.print(prefixedMessage);
		}
		if (!canLog) {
			return false;
		}

		if (isTextMessage) {
			bugsnag.notify(new LoggerThrowable(message), level.toBugsnagSeverity());
		}
		String logFilePath = fileLocation + fileFormat.format(new Date());
		for (Level levelInOrder : Level.levelsInOrder) {
			File logFile = new File(logFilePath.replace("%l", levelInOrder.name().toLowerCase()));
			SafeTry.execute(() -> {
				if (!logFile.exists()) {
					logFile.createNewFile();
				}
				FileOutputStream fos = new FileOutputStream(logFile, true);
				fos.write(prefixedMessage.getBytes(StandardCharsets.UTF_16));
				fos.flush();
				fos.close();
			});
			if (levelInOrder == level) {
				break;
			}
		}
		return true;
	}
}
