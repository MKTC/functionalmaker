package nl.makertim.fm.annotation;

import org.reflections.Reflections;

import java.lang.annotation.Annotation;
import java.util.Set;

public class AnnotationHelper {

	private static final Reflections reflections = new Reflections();

	public static Set<Class<?>> getClassesThatUsesAnnotation(Class<? extends Annotation> annotation) {
		return reflections.getTypesAnnotatedWith(annotation);
	}

}
