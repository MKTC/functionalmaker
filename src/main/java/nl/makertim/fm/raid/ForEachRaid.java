package nl.makertim.fm.raid;

import nl.makertim.fm.safe.ThrowingFunction;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class ForEachRaid<T> implements RaidStrategy<T> {
	@Override
	public void forEach(T[] toRaid, Consumer<T> consumer) {
		Arrays.stream(toRaid).forEach(consumer);
	}

	@Override
	public T forEachReturnFastest(T[] toRaid, Consumer<T> consumer) {
		return Arrays.stream(toRaid)
			.peek(consumer)
			.reduce((a, b) -> a)
			.orElse(null);
	}

	@Override
	public <T1> T1 forEachReturnResult(T[] toRaid, Function<T, T1> function) {
		return Arrays.stream(toRaid)
			.map(function)
			.reduce((a, b) -> b)
			.orElse(null);
	}

	@Override
	public <T1, Ex extends Throwable> T1 throwingForEachReturnResult(T[] toRaid, ThrowingFunction<T, T1, Ex> function) throws Ex {
		T1 lastResult = null;
		for (T raid : toRaid) {
			lastResult = function.apply(raid);
		}
		return lastResult;
	}

	@Override
	public boolean forEachReturnBoolean(T[] toRaid, Predicate<T> predicate) {
		return Arrays.stream(toRaid)
			.allMatch(predicate);
	}
}
