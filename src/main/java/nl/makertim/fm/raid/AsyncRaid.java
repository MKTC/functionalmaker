package nl.makertim.fm.raid;

import nl.makertim.fm.safe.SafeTry;
import nl.makertim.fm.safe.ThrowingFunction;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public abstract class AsyncRaid<T> implements RaidStrategy<T> {

	protected <T1, Ex extends Throwable> CompletableFuture<T1>[] buildFutures(T[] toRaid, ThrowingFunction<T, T1, Ex> function) {
		return Arrays.stream(toRaid)
			.map(item ->
				CompletableFuture.supplyAsync(() -> SafeTry.execute(() -> function.apply(item)))
			).toArray(CompletableFuture[]::new);
	}

	protected abstract <T1> T1 futureStrategy(CompletableFuture<T1>[] futures);

	@Override
	public void forEach(T[] toRaid, Consumer<T> consumer) {
		futureStrategy(buildFutures(toRaid, item -> {
			consumer.accept(item);
			return null;
		}));
	}

	@Override
	public T forEachReturnFastest(T[] toRaid, Consumer<T> consumer) {
		return futureStrategy(buildFutures(toRaid, item -> {
			consumer.accept(item);
			return item;
		}));
	}

	@Override
	public <T1> T1 forEachReturnResult(T[] toRaid, Function<T, T1> function) {
		return futureStrategy(buildFutures(toRaid, function::apply));
	}

	@Override
	public <T1, Ex extends Throwable> T1 throwingForEachReturnResult(T[] toRaid, ThrowingFunction<T, T1, Ex> function) throws Ex {
		return futureStrategy(buildFutures(toRaid, function));
	}

	@Override
	public boolean forEachReturnBoolean(T[] toRaid, Predicate<T> predicate) {
		return futureStrategy(buildFutures(toRaid, predicate::test));
	}
}
