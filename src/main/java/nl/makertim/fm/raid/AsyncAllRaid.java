package nl.makertim.fm.raid;

import nl.makertim.fm.safe.SafeTry;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;

public class AsyncAllRaid<T> extends AsyncRaid<T> {

	protected ArrayStrategy strategy;

	public AsyncAllRaid() {
		this(ArrayStrategy.FIRST);
	}

	public AsyncAllRaid(ArrayStrategy strategy) {
		this.strategy = strategy;
	}

	protected <T1> T1 futureStrategy(CompletableFuture<T1>[] futures) {
		if (futures.length == 0) {
			return null;
		}
		// By design allOf returns null, to get future
		// So it's safe to get the first future *if given*
		CompletableFuture.allOf(futures).join();
		Object[] futureResults = Arrays.stream(futures)
			.map(future -> SafeTry.execute(() -> future.get()))
			.toArray();
		return (T1) SafeTry.execute(() -> strategy.apply(futureResults));
	}

	public AsyncAllRaid<T> setResultStrategy(ArrayStrategy strategy) {
		this.strategy = strategy;
		return this;
	}
}
