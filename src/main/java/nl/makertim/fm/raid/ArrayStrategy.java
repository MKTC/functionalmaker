package nl.makertim.fm.raid;

import java.util.Random;

public enum ArrayStrategy {
	FIRST,
	FIRST_NON_NULL,
	MIDDLE,
	RANDOM,
	LAST;

	private static final Random random = new Random();

	public static <T> T apply(ArrayStrategy strategy, T[] input) {
		if (input == null || input.length == 0) {
			return null;
		}

		switch (strategy) {
			default:
			case FIRST:
				return input[0];
			case FIRST_NON_NULL:
				for (T object : input) {
					if (object != null) {
						return object;
					}
				}
				return null;
			case MIDDLE:
				return input[input.length / 2];
			case RANDOM:
				return input[random.nextInt(input.length)];
			case LAST:
				return input[input.length - 1];
		}
	}

	public <T> T apply(T[] input) {
		return ArrayStrategy.apply(this, input);
	}
}
