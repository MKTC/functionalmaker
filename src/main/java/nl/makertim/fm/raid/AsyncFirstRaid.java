package nl.makertim.fm.raid;

import java.util.concurrent.CompletableFuture;

public class AsyncFirstRaid<T> extends AsyncRaid<T> {

	protected <T1> T1 futureStrategy(CompletableFuture<T1>[] futures) {
		// Fix freeze when given no futures
		if (futures.length == 0) {
			return null;
		}
		return (T1) (CompletableFuture.anyOf(futures)).join();
	}
}
