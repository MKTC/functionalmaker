package nl.makertim.fm.raid;

import nl.makertim.fm.safe.ThrowingFunction;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public interface RaidStrategy<T> {

	void forEach(T[] toRaid, Consumer<T> consumer);

	T forEachReturnFastest(T[] toRaid, Consumer<T> consumer);

	<T1> T1 forEachReturnResult(T[] toRaid, Function<T, T1> function);

	<T1, Ex extends Throwable> T1 throwingForEachReturnResult(T[] toRaid, ThrowingFunction<T, T1, Ex> function) throws Ex;

	boolean forEachReturnBoolean(T[] toRaid, Predicate<T> predicate);

}
