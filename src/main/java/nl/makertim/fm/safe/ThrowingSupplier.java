package nl.makertim.fm.safe;

import java.util.function.Supplier;

public interface ThrowingSupplier<R, Ex extends Throwable> {

	R get() throws Ex;

	default <T1> ThrowingFunction<T1, R, Ex> toFunction() {
		return (r) -> get();
	}

	default Supplier<R> asSupplier() {
		return () -> {
			try {
				return ThrowingSupplier.this.get();
			} catch (Throwable thr) {
				throw new RuntimeException(thr);
			}
		};
	}
}
