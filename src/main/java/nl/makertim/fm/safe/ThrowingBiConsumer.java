package nl.makertim.fm.safe;

import java.util.function.BiConsumer;

public interface ThrowingBiConsumer<T1, T2, Ex extends Throwable> {

	void accept(T1 t1, T2 t2) throws Ex;

	default <R> ThrowingBiFunction<T1, T2, R, Ex> toFunction(R functionResponse) {
		return (t1, t2) -> {
			accept(t1, t2);
			return functionResponse;
		};
	}

	default ThrowingBiConsumer<T1, T2, Ex> andThen(ThrowingBiConsumer<T1, T2, Ex> after) {
		return (T1 t1, T2 t2) -> {
			accept(t1, t2);
			after.accept(t1, t2);
		};
	}

	default BiConsumer<T1, T2> asConsumer() {
		return (t1, t2) -> {
			try {
				ThrowingBiConsumer.this.accept(t1, t2);
			} catch (Throwable thr) {
				throw new RuntimeException(thr);
			}
		};
	}
}
