package nl.makertim.fm.safe;

import java.util.function.Consumer;
import java.util.function.Function;

public class SafeTry {

	public static <Ex extends Throwable> void execute(ThrowingRunnable<Ex> runnable) {
		execute(runnable, ex -> {
		});
	}

	public static <Ex extends Throwable> void execute(ThrowingRunnable<Ex> runnable, Consumer<Throwable> onError) {
		try {
			runnable.run();
		} catch (Throwable throwable) {
			onError.accept(throwable);
		}
	}

	public static <T, Ex extends Throwable> T execute(ThrowingSupplier<T, Ex> supplier) {
		return execute(supplier, ex -> null);
	}

	public static <T, Ex extends Throwable> T execute(ThrowingSupplier<T, Ex> supplier, Function<Throwable, T> onError) {
		try {
			return supplier.get();
		} catch (Throwable throwable) {
			return onError.apply(throwable);
		}
	}

	public static <T1, R, Ex extends Throwable> R executeNonFinal(T1 t1, ThrowingFunction<T1, R, Ex> function) {
		return executeNonFinal(t1, function, ex -> null);
	}

	public static <T1, R, Ex extends Throwable> R executeNonFinal(T1 t1, ThrowingFunction<T1, R, Ex> function, Function<Throwable, R> onError) {
		try {
			return function.apply(t1);
		} catch (Throwable throwable) {
			return onError.apply(throwable);
		}
	}

	public static <T1, T2, R, Ex extends Throwable> R executeNonFinal(T1 t1, T2 t2, ThrowingBiFunction<T1, T2, R, Ex> function) {
		return executeNonFinal(t1, t2, function, ex -> null);
	}

	public static <T1, T2, R, Ex extends Throwable> R executeNonFinal(T1 t1, T2 t2, ThrowingBiFunction<T1, T2, R, Ex> function, Function<Throwable, R> onError) {
		try {
			return function.accept(t1, t2);
		} catch (Throwable throwable) {
			return onError.apply(throwable);
		}
	}

	public static <T1, T2, T3, R, Ex extends Throwable> R executeNonFinal(T1 t1, T2 t2, T3 t3, ThrowingTriFunction<T1, T2, T3, R, Ex> function) {
		return executeNonFinal(t1, t2, t3, function, ex -> null);
	}

	public static <T1, T2, T3, R, Ex extends Throwable> R executeNonFinal(T1 t1, T2 t2, T3 t3, ThrowingTriFunction<T1, T2, T3, R, Ex> function, Function<Throwable, R> onError) {
		try {
			return function.accept(t1, t2, t3);
		} catch (Throwable throwable) {
			return onError.apply(throwable);
		}
	}

	public static <Ex extends Throwable> void throwAsRuntimeException(ThrowingRunnable<Ex> runnable) {
		execute(runnable, ex -> {
			throw new RuntimeException(ex);
		});
	}

	public static <T, Ex extends Throwable> T throwAsRuntimeException(ThrowingSupplier<T, Ex> supplier) {
		return execute(supplier, ex -> {
			throw new RuntimeException(ex);
		});
	}
}
