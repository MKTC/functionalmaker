package nl.makertim.fm.safe;

import nl.makertim.fm.core.TriSet;

import java.util.function.Function;

public interface ThrowingFunctionTri<T1, R1, R2, R3, Ex extends Throwable> {

	TriSet<R1, R2, R3> accept(T1 t1) throws Ex;

	default ThrowingConsumer<T1, Ex> toConsumer() {
		return this::accept;
	}

	default Function<T1, TriSet<R1, R2, R3>> asFunction() {
		return t1 -> {
			try {
				return ThrowingFunctionTri.this.accept(t1);
			} catch (Throwable thr) {
				throw new RuntimeException(thr);
			}
		};
	}
}
