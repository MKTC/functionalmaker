package nl.makertim.fm.safe;

import nl.makertim.fm.core.BiSet;

import java.util.function.Function;

public interface ThrowingFunctionBi<T1, R1, R2, Ex extends Throwable> {

	BiSet<R1, R2> accept(T1 t1) throws Ex;

	default ThrowingConsumer<T1, Ex> toConsumer() {
		return this::accept;
	}

	default Function<T1, BiSet<R1, R2>> asFunction() {
		return t1 -> {
			try {
				return ThrowingFunctionBi.this.accept(t1);
			} catch (Throwable thr) {
				throw new RuntimeException(thr);
			}
		};
	}
}
