package nl.makertim.fm.safe;

public interface ThrowingRunnable<Ex extends Throwable> {

	void run() throws Ex;

	default ThrowingRunnable<Ex> andThen(ThrowingRunnable<Ex> after) {
		return () -> {
			run();
			after.run();
		};
	}

	default Runnable asRunnable() {
		return () -> {
			try {
				ThrowingRunnable.this.run();
			} catch (Throwable thr) {
				throw new RuntimeException(thr);
			}
		};
	}
}
