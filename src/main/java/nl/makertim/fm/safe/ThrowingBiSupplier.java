package nl.makertim.fm.safe;

import nl.makertim.fm.core.BiSet;

import java.util.function.Supplier;

public interface ThrowingBiSupplier<R1, R2, Ex extends Throwable> {

	BiSet<R1, R2> get() throws Ex;

	default <T1> ThrowingFunction<T1, BiSet<R1, R2>, Ex> toFunction() {
		return (r) -> get();
	}

	default Supplier<BiSet<R1, R2>> asSupplier() {
		return () -> {
			try {
				return ThrowingBiSupplier.this.get();
			} catch (Throwable thr) {
				throw new RuntimeException(thr);
			}
		};
	}
}
