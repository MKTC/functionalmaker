package nl.makertim.fm.safe;

import nl.makertim.fm.core.TriSet;

import java.util.function.Function;

public interface ThrowingTriFunction<T1, T2, T3, R, Ex extends Throwable> {

	R accept(T1 t1, T2 t2, T3 t3) throws Ex;

	default ThrowingTriConsumer<T1, T2, T3, Ex> toConsumer() {
		return this::accept;
	}

	default Function<TriSet<T1, T2, T3>, R> asFunction() {
		return t -> {
			try {
				return ThrowingTriFunction.this.accept(t.getT1(), t.getT2(), t.getT3());
			} catch (Throwable thr) {
				throw new RuntimeException(thr);
			}
		};
	}
}
