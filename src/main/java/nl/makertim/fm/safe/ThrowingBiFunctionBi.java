package nl.makertim.fm.safe;

import nl.makertim.fm.core.BiSet;

import java.util.function.BiFunction;

public interface ThrowingBiFunctionBi<T1, T2, R1, R2, Ex extends Throwable> {

	BiSet<R1, R2> accept(T1 t1, T2 t2) throws Ex;

	default ThrowingBiConsumer<T1, T2, Ex> toConsumer() {
		return this::accept;
	}

	default BiFunction<T1, T2, BiSet<R1, R2>> asFunction() {
		return (t1, t2) -> {
			try {
				return ThrowingBiFunctionBi.this.accept(t1, t2);
			} catch (Throwable thr) {
				throw new RuntimeException(thr);
			}
		};
	}
}
