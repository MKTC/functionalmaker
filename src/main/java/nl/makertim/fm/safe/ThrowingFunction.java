package nl.makertim.fm.safe;

import java.util.function.Function;

public interface ThrowingFunction<T1, R, Ex extends Throwable> {

	R apply(T1 t1) throws Ex;

	default ThrowingConsumer<T1, Ex> toConsumer() {
		return this::apply;
	}

	default Function<T1, R> asFunction() {
		return t1 -> {
			try {
				return ThrowingFunction.this.apply(t1);
			} catch (Throwable thr) {
				throw new RuntimeException(thr);
			}
		};
	}
}
