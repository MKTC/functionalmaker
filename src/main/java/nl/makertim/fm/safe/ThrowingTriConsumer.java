package nl.makertim.fm.safe;

import nl.makertim.fm.core.TriSet;

import java.util.function.Consumer;

public interface ThrowingTriConsumer<T1, T2, T3, Ex extends Throwable> {

	void accept(T1 t1, T2 t2, T3 t3) throws Ex;

	default <R> ThrowingTriFunction<T1, T2, T3, R, Ex> toFunction(R functionResponse) {
		return (t1, t2, t3) -> {
			accept(t1, t2, t3);
			return functionResponse;
		};
	}

	default ThrowingTriConsumer<T1, T2, T3, Ex> andThen(ThrowingTriConsumer<T1, T2, T3, Ex> after) {
		return (T1 t1, T2 t2, T3 t3) -> {
			accept(t1, t2, t3);
			after.accept(t1, t2, t3);
		};
	}

	default Consumer<TriSet<T1, T2, T3>> asConsumer() {
		return t1 -> {
			try {
				ThrowingTriConsumer.this.accept(t1.getT1(), t1.getT2(), t1.getT3());
			} catch (Throwable thr) {
				throw new RuntimeException(thr);
			}
		};
	}
}
