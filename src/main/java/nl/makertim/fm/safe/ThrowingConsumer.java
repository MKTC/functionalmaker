package nl.makertim.fm.safe;

import java.util.function.Consumer;

public interface ThrowingConsumer<T1, Ex extends Throwable> {

	void accept(T1 t1) throws Ex;

	default <R> ThrowingFunction<T1, R, Ex> toFunction(R functionResponse) {
		return (t1) -> {
			accept(t1);
			return functionResponse;
		};
	}

	default ThrowingConsumer<T1, Ex> andThen(ThrowingConsumer<T1, Ex> after) {
		return (T1 t1) -> {
			accept(t1);
			after.accept(t1);
		};
	}

	default Consumer<T1> asConsumer() {
		return t1 -> {
			try {
				ThrowingConsumer.this.accept(t1);
			} catch (Throwable thr) {
				throw new RuntimeException(thr);
			}
		};
	}
}
