package nl.makertim.fm.safe;

import nl.makertim.fm.core.TriSet;

import java.util.function.Supplier;

public interface ThrowingTriSupplier<R1, R2, R3, Ex extends Throwable> {

	TriSet<R1, R2, R3> get() throws Ex;

	default <T1> ThrowingFunction<T1, TriSet<R1, R2, R3>, Ex> toFunction() {
		return (r) -> get();
	}

	default Supplier<TriSet<R1, R2, R3>> asSupplier() {
		return () -> {
			try {
				return ThrowingTriSupplier.this.get();
			} catch (Throwable thr) {
				throw new RuntimeException(thr);
			}
		};
	}
}
