package nl.makertim.fm.safe;

import nl.makertim.fm.core.TriSet;

import java.util.function.BiFunction;

public interface ThrowingBiFunctionTri<T1, T2, R1, R2, R3, Ex extends Throwable> {

	TriSet<R1, R2, R3> accept(T1 t1, T2 t2) throws Ex;

	default ThrowingBiConsumer<T1, T2, Ex> toConsumer() {
		return this::accept;
	}

	default BiFunction<T1, T2, TriSet<R1, R2, R3>> asFunction() {
		return (t1, t2) -> {
			try {
				return ThrowingBiFunctionTri.this.accept(t1, t2);
			} catch (Throwable thr) {
				throw new RuntimeException(thr);
			}
		};
	}
}
