package nl.makertim.fm.safe;

import java.util.function.BiFunction;

public interface ThrowingBiFunction<T1, T2, R, Ex extends Throwable> {

	R accept(T1 t1, T2 t2) throws Ex;

	default ThrowingBiConsumer<T1, T2, Ex> toConsumer() {
		return this::accept;
	}

	default BiFunction<T1, T2, R> asBiFunction() {
		return (t1, t2) -> {
			try {
				return ThrowingBiFunction.this.accept(t1, t2);
			} catch (Throwable thr) {
				throw new RuntimeException(thr);
			}
		};
	}
}
