package nl.makertim.fm.raid;

import nl.makertim.fm.core.Property;
import nl.makertim.fm.safe.SafeTry;
import nl.makertim.fm.safe.ThrowingFunction;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.jupiter.api.Assertions.assertAll;

public class RaidStrategyTest {

	private static final List<RaidStrategy<String>> strategies = Arrays.asList(
		new ForEachRaid<>(),
		new AsyncFirstRaid<>(),
		new AsyncAllRaid<>()
	);

	@Test
	public void testWithoutInput() {
		// Arrange
		var inputStringAsArray = new String[] { };
		var sleep = 100L;
		Function<String, String> function = (x) -> {
			SafeTry.execute(() -> Thread.sleep(sleep));
			return x;
		};

		for (RaidStrategy<String> strategy : strategies) {
			// Arrange
			var nowMs = System.currentTimeMillis();

			// Act
			String output = strategy.forEachReturnResult(inputStringAsArray, function);

			// Assert
			assertThat(strategy.getClass().getName(), output, is(nullValue()));
			assertThat(strategy.getClass().getName(), System.currentTimeMillis() - nowMs, lessThanOrEqualTo(sleep - 1));
		}
	}

	@Test
	public void testSingleFuture() {
		// Arrange
		var sleep = 100L;
		var inputString = "123";
		var inputStringAsArray = new String[] { inputString };
		Function<String, String> function = (x) -> {
			SafeTry.execute(() -> Thread.sleep(sleep));
			return x;
		};

		for (RaidStrategy<String> strategy : strategies) {
			// Arrange
			var nowMs = System.currentTimeMillis();

			// Act
			String output = strategy.forEachReturnResult(inputStringAsArray, function);

			// Assert
			assertThat(strategy.getClass().getName(), output, is(inputString));
			assertThat(strategy.getClass().getName(), System.currentTimeMillis() - nowMs, greaterThanOrEqualTo(sleep));
		}
	}

	@Test
	public void test3Futures() {
		// Arrange
		var sleep = 100L;
		var inputString = "this";
		var inputStringAsArray = new String[] { inputString, inputString, inputString };
		Function<String, String> function = (x) -> {
			SafeTry.execute(() -> Thread.sleep(sleep));
			return x;
		};

		for (RaidStrategy<String> strategy : strategies) {
			// Arrange
			var nowMs = System.currentTimeMillis();

			// Act
			String output = strategy.forEachReturnResult(inputStringAsArray, function);

			// Assert
			assertThat(strategy.getClass().getName(), output, is(inputString));
			assertThat(strategy.getClass().getName(), System.currentTimeMillis() - nowMs, greaterThanOrEqualTo(sleep));
		}
	}

	@Test
	public void test10Futures() {
		// Arrange
		var sleep = 10L;
		var inputString = "this";
		var inputStringAsArray = new String[] {
			inputString, inputString, inputString, inputString, inputString,
			inputString, inputString, inputString, inputString, inputString
		};
		Function<String, String> function = (x) -> {
			SafeTry.execute(() -> Thread.sleep(sleep));
			return x;
		};

		for (RaidStrategy<String> strategy : strategies) {
			// Arrange
			var nowMs = System.currentTimeMillis();

			// Act
			String output = strategy.forEachReturnResult(inputStringAsArray, function);

			// Assert
			assertThat(strategy.getClass().getName(), output, is(inputString));
			assertThat(strategy.getClass().getName(), System.currentTimeMillis() - nowMs, greaterThanOrEqualTo(sleep));
		}
	}

	@Test
	public void testRaidForEach() {
		// Arrange
		var inputString = "123";
		var inputStringAsArray = new String[] { inputString };
		var countProperty = Property.of(0);
		Consumer<String> function = x -> addToCountProp(countProperty);

		// Act
		for (RaidStrategy<String> strategy : strategies) {
			strategy.forEach(inputStringAsArray, function);
		}

		// Assert
		assertThat(countProperty.get(), is(strategies.size()));
	}

	@Test
	public void testRaidForEachMultiple() {
		// Arrange
		var inputString = "123";
		var inputStringAsArray = new String[] { inputString, inputString, inputString };
		var countProperty = Property.of(0);
		Consumer<String> function = x -> {
			addToCountProp(countProperty);
		};

		// Act
		for (RaidStrategy<String> strategy : strategies) {
			strategy.forEach(inputStringAsArray, function);
		}

		// Assert
		assertThat(countProperty.get(), is(strategies.size() * inputStringAsArray.length));
	}

	@Test
	public void testRaidFastestForEach() {
		// Arrange
		var inputString = "123";
		var inputStringAsArray = new String[] { inputString };
		var countProperty = Property.of(0);
		Consumer<String> function = x -> addToCountProp(countProperty);

		// Act
		for (RaidStrategy<String> strategy : strategies) {
			strategy.forEachReturnFastest(inputStringAsArray, function);
		}

		// Assert
		assertThat(countProperty.get(), is(strategies.size()));
	}

	@Test
	public void testRaidFastestForEachMultiple() {
		// Arrange
		var inputString = "123";
		var inputStringAsArray = new String[] { inputString, inputString, inputString };
		var countProperty = Property.of(0);
		Consumer<String> function = x -> addToCountProp(countProperty);

		// Act
		for (RaidStrategy<String> strategy : strategies) {
			strategy.forEachReturnFastest(inputStringAsArray, function);
		}

		// Assert
		assertThat(countProperty.get(), is(strategies.size() * inputStringAsArray.length));
	}

	@Test
	public void testThrowingRaidForEach() {
		// Arrange
		var inputString = "123";
		var inputStringAsArray = new String[] { inputString };
		var countProperty = Property.of(0);
		ThrowingFunction<String, String, Throwable> function = x -> {
			addToCountProp(countProperty);
			return x;
		};

		// Act
		for (RaidStrategy<String> strategy : strategies) {
			assertAll(() -> strategy.throwingForEachReturnResult(inputStringAsArray, function));
		}

		// Assert
		assertThat(countProperty.get(), is(strategies.size()));
	}

	@Test
	public void testRaidThrowingForEachMultiple() {
		// Arrange
		var inputString = "123";
		var inputStringAsArray = new String[] { inputString, inputString, inputString };
		var countProperty = Property.of(0);
		ThrowingFunction<String, String, Throwable> function = x -> {
			addToCountProp(countProperty);
			return x;
		};

		// Act
		for (RaidStrategy<String> strategy : strategies) {
			assertAll(() -> strategy.throwingForEachReturnResult(inputStringAsArray, function));
		}

		// Assert
		assertThat(countProperty.get(), is(strategies.size() * inputStringAsArray.length));
	}

	@Test
	public void testRaidBooleanForEach() {
		// Arrange
		var inputString = "123";
		var inputStringAsArray = new String[] { inputString };
		var countProperty = Property.of(0);
		Predicate<String> function = x -> {
			addToCountProp(countProperty);
			return true;
		};

		// Act
		for (RaidStrategy<String> strategy : strategies) {
			strategy.forEachReturnBoolean(inputStringAsArray, function);
		}

		// Assert
		assertThat(countProperty.get(), is(strategies.size()));
	}

	@Test
	public void testRaidBooleanForEachMultiple() {
		// Arrange
		var inputString = "123";
		var inputStringAsArray = new String[] { inputString, inputString, inputString };
		var countProperty = Property.of(0);
		Predicate<String> function = x -> {
			addToCountProp(countProperty);
			return true;
		};

		// Act
		for (RaidStrategy<String> strategy : strategies) {
			strategy.forEachReturnBoolean(inputStringAsArray, function);
		}

		// Assert
		assertThat(countProperty.get(), is(strategies.size() * inputStringAsArray.length));
	}

	private synchronized void addToCountProp(Property<Integer> property) {
		property.set(property.get() + 1);
	}
}
