package nl.makertim.fm.raid;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ArrayStrategyTest {

	@Test
	public void testArrayStrategyFirst() {
		// Arrange
		var firstInput = 'a';
		var secondInput = 'b';
		var thirdInput = 'c';
		var fourthInput = 'd';
		var fifthInput = 'e';
		var arrayInput = new Character[] { firstInput, secondInput, thirdInput, fourthInput, fifthInput };

		// Act
		var output = ArrayStrategy.FIRST.apply(arrayInput);

		// Assert
		assertThat(output, is(firstInput));
	}

	@Test
	public void testArrayStrategyFirstNotNull() {
		// Arrange
		var firstInput = 'a';
		var secondInput = 'b';
		var thirdInput = 'c';
		var fourthInput = 'd';
		var fifthInput = 'e';
		var arrayInput = new Character[] { firstInput, secondInput, thirdInput, fourthInput, fifthInput };

		// Act
		var output = ArrayStrategy.FIRST_NON_NULL.apply(arrayInput);

		// Assert
		assertThat(output, is(firstInput));
	}

	@Test
	public void testArrayStrategyFirstNotNullWithNullInputs() {
		// Arrange
		Character firstInput = null;
		var secondInput = 'b';
		var thirdInput = 'c';
		var fourthInput = 'd';
		var fifthInput = 'e';
		var arrayInput = new Character[] { firstInput, secondInput, thirdInput, fourthInput, fifthInput };

		// Act
		var output = ArrayStrategy.FIRST_NON_NULL.apply(arrayInput);

		// Assert
		assertThat(output, is(secondInput));
	}

	@Test
	public void testArrayStrategyFirstNotNullWithOnlyNullInputs() {
		// Arrange
		Character firstInput = null;
		Character secondInput = null;
		Character thirdInput = null;
		Character fourthInput = null;
		Character fifthInput = null;
		var arrayInput = new Character[] { firstInput, secondInput, thirdInput, fourthInput, fifthInput };

		// Act
		var output = ArrayStrategy.FIRST_NON_NULL.apply(arrayInput);

		// Assert
		assertThat(output, is(secondInput));
	}

	@Test
	public void testArrayStrategyMiddle() {
		// Arrange
		var firstInput = 'a';
		var secondInput = 'b';
		var thirdInput = 'c';
		var fourthInput = 'd';
		var fifthInput = 'e';
		var arrayInput = new Character[] { firstInput, secondInput, thirdInput, fourthInput, fifthInput };

		// Act
		var output = ArrayStrategy.MIDDLE.apply(arrayInput);

		// Assert
		assertThat(output, is(thirdInput));
	}

	@Test
	public void testArrayStrategyRandom() {
		// Arrange
		var firstInput = 'a';
		var secondInput = 'b';
		var thirdInput = 'c';
		var fourthInput = 'd';
		var fifthInput = 'e';
		var arrayInput = new Character[] { firstInput, secondInput, thirdInput, fourthInput, fifthInput };

		// Act
		var foundOptions = new Character[1000];
		for (int i = 0; i < foundOptions.length; i++) {
			foundOptions[i] = ArrayStrategy.RANDOM.apply(arrayInput);
		}

		// Assert
		assertThat(Arrays.stream(foundOptions).distinct().count(), greaterThanOrEqualTo(1L));
	}

	@Test
	public void testArrayStrategyLast() {
		// Arrange
		var firstInput = 'a';
		var secondInput = 'b';
		var thirdInput = 'c';
		var fourthInput = 'd';
		var fifthInput = 'e';
		var arrayInput = new Character[] { firstInput, secondInput, thirdInput, fourthInput, fifthInput };

		// Act
		var output = ArrayStrategy.LAST.apply(arrayInput);

		// Assert
		assertThat(output, is(fifthInput));
	}

	@Test
	public void testArrayStrategyLastWithoutInput() {
		// Arrange
		var arrayInput = new Character[0];

		// Act
		var output = ArrayStrategy.LAST.apply(arrayInput);

		// Assert
		assertThat(output, is(nullValue()));
	}

}
