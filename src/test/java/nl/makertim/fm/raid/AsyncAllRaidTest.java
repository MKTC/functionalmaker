package nl.makertim.fm.raid;

import org.junit.jupiter.api.Test;

import java.util.function.Function;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class AsyncAllRaidTest {

	@Test
	public void testAsyncAllRaidWithFirstStrategy() {
		// Arrange
		var firstInputString = "this";
		var lastInputString = "that";
		var inputStringAsArray = new String[] { firstInputString, lastInputString };
		Function<String, String> function = x -> x;
		var strategy = new AsyncAllRaid<String>();

		// Act
		String output = strategy.forEachReturnResult(inputStringAsArray, function);

		// Assert
		assertThat(output, is(firstInputString));
	}

	@Test
	public void testAsyncAllRaidWithLastStrategy() {
		// Arrange
		var firstInputString = "this";
		var lastInputString = "that";
		var inputStringAsArray = new String[] { firstInputString, lastInputString };
		Function<String, String> function = x -> x;
		var strategy = new AsyncAllRaid<String>()
			.setResultStrategy(ArrayStrategy.LAST);

		// Act
		String output = strategy.forEachReturnResult(inputStringAsArray, function);

		// Assert
		assertThat(output, is(lastInputString));
	}

	@Test
	public void testAsyncAllRaidWithoutInput() {
		// Arrange
		var inputStringAsArray = new String[0];
		Function<String, String> function = x -> x;
		var strategy = new AsyncAllRaid<String>()
			.setResultStrategy(ArrayStrategy.LAST);

		// Act
		String output = strategy.forEachReturnResult(inputStringAsArray, function);

		// Assert
		assertThat(output, is(nullValue()));
	}

}
