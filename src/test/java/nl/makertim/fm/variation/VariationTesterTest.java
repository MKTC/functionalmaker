package nl.makertim.fm.variation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import static nl.makertim.fm.variation.DateVariation.A_DAY;
import static nl.makertim.fm.variation.DateVariation.A_WEEK;
import static nl.makertim.fm.variation.MethodResult.FAILS;
import static nl.makertim.fm.variation.MethodResult.SUCCEEDS;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class VariationTesterTest {

	private Object placeholder;

	@Test
	public void testVariationsIntRange() throws Throwable {
		// Arrange
		var iVariation = new IntVariation(0, 10);

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodA", iVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsIntRangeDefaults() throws Throwable {
		// Arrange
		var iVariation = new IntVariation();

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodA", iVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsIntegerRange() throws Throwable {
		// Arrange
		var iVariation = new IntegerVariation(0, 10);

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodA", iVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsBoolean() throws Throwable {
		// Arrange
		var bVariation = new BooleanVariation();

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodB", bVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsBool() throws Throwable {
		// Arrange
		var bVariation = new BoolVariation();

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodB", bVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsBooleanFails() {
		// Arrange
		var bVariation = new BooleanVariation();

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodBFails", bVariation);

		// Assert
		assertThrows(AssertionError.class, executable);
	}

	@Test
	public void testVariationsBooleanConditional() throws Throwable {
		// Arrange
		var bVariation = new BooleanVariation(b -> {
			if (b) return FAILS;
			return SUCCEEDS;
		});

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodBConditional", bVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsDouble() throws Throwable {
		// Arrange
		var dVariation = new DoubleVariation();

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodD", dVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsDoubleRange() throws Throwable {
		// Arrange
		var dVariation = new DoubleVariation(0, 10);

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodD", dVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsDoubleObject() throws Throwable {
		// Arrange
		var dVariation = new DoubleObjectVariation();

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodD", dVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsString() throws Throwable {
		// Arrange
		var sVariation = new ObjectVariation<>(String.class, "test", "what", "how", "$ref");

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodS", sVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsStringVariant() throws Throwable {
		// Arrange
		var sVariation = new ObjectVariation<>(String.class,
			new String[] { "test", "what", "how", "$ref" },
			new String[] { "__internal__" },
			s -> s.equals("__internal__") ? FAILS : SUCCEEDS
		);

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodS", sVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsStringVariantWithNull() throws Throwable {
		// Arrange
		var sVariation = new WithNullVariation<>(SUCCEEDS,
			new ObjectVariation<>(String.class,
				new String[] { "test", "what", "how", "$ref" },
				new String[] { "__internal__" },
				s -> s.equals("__internal__") ? FAILS : SUCCEEDS
			)
		);

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodS", sVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsArray() throws Throwable {
		// Arrange
		getClass().getDeclaredMethods();

		var sVariation = new ArrayVariation<>(String[].class, new String[0]);

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodL", sVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsArrayVariation() throws Throwable {
		// Arrange
		getClass().getDeclaredMethods();

		var sVariation = new ArrayVariation<>(String[].class,
			new String[][] {
				{ "test" },
				{ "test", "what" },
				{ "test", "what", "how", },
				{ "test", "what", "how", "$ref" }
			},
			new String[][] { { "__internal__" } },
			L -> L.length >= 1 && Objects.equals(L[0], "TEST") ? FAILS : SUCCEEDS
		);

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodL", sVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsBooleanBoolean() throws Throwable {
		// Arrange
		var bVariation = new BooleanVariation();

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodBB", bVariation, bVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsDate() throws Throwable {
		// Arrange
		placeholder = new Date();
		var dVariation = new DateVariation(new Date(0), (Date) placeholder);

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodD", dVariation);

		// Assert
		executable.execute();
	}

	@Test
	public void testVariationsDate2() throws Throwable {
		// Arrange
		var dVariation = DateVariation.untilNow();
		placeholder = new Date();

		// Act
		Executable executable = () -> VariationTester.run(this, "testMethodD2", dVariation);

		// Assert
		executable.execute();
	}

	public void testMethodA(int i) {
		if (i < 0 || i > 10) {
			throw new RuntimeException("Oi only between 0 and 10");
		}
	}

	public void testMethodA(Integer i) {
		testMethodA(i.intValue());
	}

	public void testMethodB(boolean b) {
	}

	public void testMethodB(Boolean b) {
	}

	public void testMethodBFails(boolean b) {
		throw new RuntimeException();
	}

	public void testMethodBConditional(boolean b) {
		if (b) {
			throw new RuntimeException();
		}
	}

	public void testMethodD(double d) {
		if (d < 0 || d > 10) {
			throw new RuntimeException("Doi only between 0 and 10");
		}
	}

	public void testMethodD(Double d) {
		testMethodD(d.doubleValue());
	}

	public void testMethodS(String s) {
		if (Objects.equals(s, "__internal__")) {
			throw new RuntimeException("wow this has never happens before");
		}
	}

	public void testMethodL(String... L) {
		if (L.length >= 1 && Objects.equals(L[0], "TEST")) {
			throw new RuntimeException("listing");
		}
	}

	public void testMethodBB(boolean b1, boolean b2) {
	}

	public void testMethodD(Date date1) {
		if (date1.getTime() > ((Date) placeholder).getTime() ||
			date1.getTime() < 0) {
			throw new RuntimeException("time is beyond now!");
		}
	}

	public void testMethodD2(Date date1) {
		if (date1.getTime() > ((Date) placeholder).getTime() ||
			date1.getTime() < ((Date) placeholder).getTime() - (A_WEEK)) {
			throw new RuntimeException("time is beyond now!");
		}
	}

	public void testMethodD3(Date date1) {
		Date lastDayOfYear = Timestamp.valueOf(LocalDate.now()
			.with(TemporalAdjusters.lastDayOfYear()).atTime(LocalTime.MAX));

		if (date1.getTime() > lastDayOfYear.getTime()) {
			throw new RuntimeException("time is beyond last day of this year!");
		} else if(date1.getTime() < ((Date) placeholder).getTime()){
			throw new RuntimeException("time is in the past!");
		}
	}
}
