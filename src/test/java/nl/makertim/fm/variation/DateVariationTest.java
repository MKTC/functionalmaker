package nl.makertim.fm.variation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DateVariationTest {

	@Test
	public void testTillThisYear() {
		// Arrange
		var dVariation = DateVariation.tillThisYear();

		// Act

		// Assert
		assertTrue(dVariation instanceof DateVariation);
	}

	@Test
	public void testSinceMonday() {
		// Arrange
		var dVariation = DateVariation.sinceMonday();

		// Act

		// Assert
		assertTrue(dVariation instanceof DateVariation);
	}

	@Test
	public void testSinceSunday() {
		// Arrange
		var dVariation = DateVariation.sinceSunday();

		// Act

		// Assert
		assertTrue(dVariation instanceof DateVariation);
	}

	@Test
	public void testSinceStartOfMonth() {
		// Arrange
		var dVariation = DateVariation.sinceStartMonth();

		// Act

		// Assert
		assertTrue(dVariation instanceof DateVariation);
	}
}
