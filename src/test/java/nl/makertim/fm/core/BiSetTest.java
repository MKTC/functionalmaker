package nl.makertim.fm.core;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class BiSetTest {

	@Test
	public void testBiSetEmpty() {
		// Act
		var biSet = new BiSet<>();

		// Assert
		assertThat(biSet.getT1(), nullValue());
		assertThat(biSet.getT2(), nullValue());
	}

	@Test
	public void testBiSetHalfEmpty() {
		// Arrange
		String testStringIn = "TEST";

		// Act
		var biSet = new BiSet<>(testStringIn, null);

		// Assert
		assertThat(biSet.getT1(), is(testStringIn));
		assertThat(biSet.getT2(), nullValue());
	}

	@Test
	public void testBiSetHalfEmpty2() {
		// Arrange
		String testStringIn = "TEST";

		// Act
		var biSet = new BiSet<>(null, testStringIn);

		// Assert
		assertThat(biSet.getT1(), nullValue());
		assertThat(biSet.getT2(), is(testStringIn));
	}

	@Test
	public void testBiSetFullyFilled() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";

		// Act
		var biSet = new BiSet<>(testIntegerIn, testStringIn);

		// Assert
		assertThat(biSet.getT1(), is(testIntegerIn));
		assertThat(biSet.getT2(), is(testStringIn));
	}

	@Test
	public void testBiSetT1() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";
		Integer testIntegerSet = 2304;

		// Act
		var biSet = new BiSet<>(testIntegerIn, testStringIn);
		biSet.setT1(testIntegerSet);

		// Assert
		assertThat(biSet.getT1(), is(testIntegerSet));
		assertThat(biSet.getT2(), is(testStringIn));
	}

	@Test
	public void testBiSetT2() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";
		String testStringSet = "tim";

		// Act
		var biSet = new BiSet<>(testIntegerIn, testStringIn);
		biSet.setT2(testStringSet);

		// Assert
		assertThat(biSet.getT1(), is(testIntegerIn));
		assertThat(biSet.getT2(), is(testStringSet));
	}

	@Test
	public void testBiSetMap() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";
		Integer testIntegerMap = 2304;
		String testStringMap = "tim";

		// Act
		var biSet = new BiSet<>(testIntegerIn, testStringIn);
		biSet = biSet.map(i -> testIntegerMap, s -> testStringMap);

		// Assert
		assertThat(biSet.getT1(), is(testIntegerMap));
		assertThat(biSet.getT2(), is(testStringMap));
	}

	@Test
	public void testBiSetMapT1() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";
		Integer testIntegerMap = 2304;

		// Act
		var biSet = new BiSet<>(testIntegerIn, testStringIn);
		biSet = biSet.mapT1(i -> testIntegerMap);

		// Assert
		assertThat(biSet.getT1(), is(testIntegerMap));
		assertThat(biSet.getT2(), is(testStringIn));
	}

	@Test
	public void testBiSetMapT2() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";
		String testStringMap = "tim";

		// Act
		var biSet = new BiSet<>(testIntegerIn, testStringIn);
		biSet = biSet.mapT2(s -> testStringMap);

		// Assert
		assertThat(biSet.getT1(), is(testIntegerIn));
		assertThat(biSet.getT2(), is(testStringMap));
	}

	@Test
	public void testBiSetAsEntry() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";

		// Act
		Map.Entry<Integer, String> biSet = new BiSet<>(testIntegerIn, testStringIn);

		// Assert
		assertThat(biSet.getValue(), is(testStringIn));
		assertThat(biSet.getKey(), is(testIntegerIn));
	}

	@Test
	public void testBiSetAsEntrySetValue() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";
		String testStringSet = "new";

		// Act
		Map.Entry<Integer, String> biSet = new BiSet<>(testIntegerIn, testStringIn);
		String old = biSet.setValue(testStringSet);

		// Assert
		assertThat(biSet.getValue(), is(testStringSet));
		assertThat(old, is(testStringIn));
		assertThat(biSet.getKey(), is(testIntegerIn));
	}

	@Test
	public void testBiSetEquals() {
		// Act
		var biSet = new BiSet<>();
		var biSet2 = new BiSet<>();

		// Assert
		assertThat(biSet, is(biSet2));
		assertThat(biSet.hashCode(), is(biSet2.hashCode()));
	}
}
