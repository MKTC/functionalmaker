package nl.makertim.fm.core;

import nl.makertim.fm.safe.SafeTry;
import org.apache.commons.io.IOUtils;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Date;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItemInArray;
import static org.hamcrest.Matchers.is;

public class LoggerTest {

	private static final File logsFileDirectory = new File("./logs");

	@BeforeEach
	public void setBugsnagLevel() {
		Logger.bugsnag.setReleaseStage("junit");
	}

	@BeforeAll
	public static void removeLogs() {
		if (!logsFileDirectory.exists() || !logsFileDirectory.isDirectory()) {
			return;
		}
		File[] logFiles = logsFileDirectory.listFiles();
		if (logFiles == null) {
			return;
		}
		for (File logFile : logFiles) {
			logFile.delete();
		}
		logsFileDirectory.delete();
	}

	@BeforeEach
	public void cleanupLogs() {
		cleanupAllLogs();
	}

	@AfterAll
	public static void cleanupAllLogs() {
		if (logsFileDirectory.exists()) {
			if (!logsFileDirectory.isDirectory()) {
				throw new RuntimeException("logs should be a directory not a file");
			}
			File[] logFiles = logsFileDirectory.listFiles();
			if (logFiles == null) {
				throw new RuntimeException("cannot see log files");
			}
			for (File logFile : logFiles) {
				if (!logFile.delete()) {
					logFile.deleteOnExit();
				}
			}
		}
	}

	private void assertThatFile(String filePath, Matcher<String> matcher) {
		File file = new File(logsFileDirectory, filePath);
		String string = SafeTry.execute(() -> IOUtils.toString(new FileInputStream(file), StandardCharsets.UTF_16));
		assertThat(string, matcher);
	}

	@Test
	public void testDisableLogger() {
		// Arrange
		String logMessage = "yay";

		// Act
		Logger.canLog = false;
		boolean didLog = Logger.verbose(logMessage);

		// Assert
		assertThat(didLog, is(false));

		// After
		Logger.canLog = true;
	}

	@Test
	public void testVerboseLogger() {
		// Arrange
		String logMessage = "this is a verbose text";
		long currentTime = System.currentTimeMillis();
		Date date = new Date(currentTime);
		String file = date + " verbose.log";

		// Act
		Logger.verbose(logMessage);

		// Assert
		String[] logFiles = logsFileDirectory.list();
		if (logFiles == null) return;
		assertThat(logFiles, notNullValue());
		assertThat(logFiles, hasItemInArray(file));
		assertThatFile(file, containsString(logMessage));
	}

	@Test
	public void testVerboseParamLogger() {
		// Arrange
		String logMessage = "this is a verbose param";
		long currentTime = System.currentTimeMillis();
		Date date = new Date(currentTime);
		String file = date + " verbose.log";

		// Act
		Logger.verbose("%s", logMessage);

		// Assert
		String[] logFiles = logsFileDirectory.list();
		if (logFiles == null) return;
		assertThat(logFiles, notNullValue());
		assertThat(logFiles, hasItemInArray(file));
		assertThatFile(file, containsString(logMessage));
	}

	@Test
	public void testInfoLogger() {
		// Arrange
		String logMessage = "info logger here";
		long currentTime = System.currentTimeMillis();
		Date date = new Date(currentTime);
		String file = date + " info.log";

		// Act
		Logger.info(logMessage);

		// Assert
		String[] logFiles = logsFileDirectory.list();
		if (logFiles == null) return;
		assertThat(logFiles, notNullValue());
		assertThat(logFiles, hasItemInArray(file));
		assertThatFile(file, containsString(logMessage));
	}

	@Test
	public void testInfoParamLogger() {
		// Arrange
		String logMessage = "info param logger here";
		long currentTime = System.currentTimeMillis();
		Date date = new Date(currentTime);
		String file = date + " verbose.log";

		// Act
		Logger.info("%s", logMessage);

		// Assert
		String[] logFiles = logsFileDirectory.list();
		if (logFiles == null) return;
		assertThat(logFiles, notNullValue());
		assertThat(logFiles, hasItemInArray(file));
		assertThatFile(file, containsString(logMessage));
	}

	@Test
	public void testWarnLogger() {
		// Arrange
		String logMessage = "warning the house is on fire";
		long currentTime = System.currentTimeMillis();
		Date date = new Date(currentTime);
		String file = date + " warn.log";

		// Act
		Logger.warn(logMessage);

		// Assert
		String[] logFiles = logsFileDirectory.list();
		if (logFiles == null) return;
		assertThat(logFiles, notNullValue());
		assertThat(logFiles, hasItemInArray(file));
		assertThatFile(file, containsString(logMessage));
	}

	@Test
	public void testErrorLogger() {
		// Arrange
		String logMessage = "haha that goes BzZzZz";
		long currentTime = System.currentTimeMillis();
		Date date = new Date(currentTime);
		String file = date + " error.log";

		// Act
		Logger.error(logMessage);

		// Assert
		String[] logFiles = logsFileDirectory.list();
		if (logFiles == null) return;
		assertThat(logFiles, notNullValue());
		assertThat(logFiles, hasItemInArray(file));
		assertThatFile(file, containsString(logMessage));
	}


	@Test
	public void testErrorParamLogger() {
		// Arrange
		String logMessage = "haha that goes Padam-BzZzZz";
		long currentTime = System.currentTimeMillis();
		Date date = new Date(currentTime);
		String file = date + " error.log";

		// Act
		Logger.error("%s", logMessage);

		// Assert
		String[] logFiles = logsFileDirectory.list();
		if (logFiles == null) return;
		assertThat(logFiles, notNullValue());
		assertThat(logFiles, hasItemInArray(file));
		assertThatFile(file, containsString(logMessage));
	}

	@Test
	public void testDangerLogger() {
		// Arrange
		String logMessage = "did someone trigger the alarms?";
		long currentTime = System.currentTimeMillis();
		Date date = new Date(currentTime);
		String file = date + " danger.log";

		// Act
		Logger.danger(logMessage);

		// Assert
		String[] logFiles = logsFileDirectory.list();
		if (logFiles == null) return;
		assertThat(logFiles, notNullValue());
		assertThat(logFiles, hasItemInArray(file));
		assertThatFile(file, containsString(logMessage));
	}

	@Test
	public void testDangerParamLogger() {
		// Arrange
		String logMessage = "did someone trigger the param alarms?";
		long currentTime = System.currentTimeMillis();
		Date date = new Date(currentTime);
		String file = date + " danger.log";

		// Act
		Logger.danger("%s", logMessage);

		// Assert
		String[] logFiles = logsFileDirectory.list();
		if (logFiles == null) return;
		assertThat(logFiles, notNullValue());
		assertThat(logFiles, hasItemInArray(file));
		assertThatFile(file, containsString(logMessage));
	}

	@Test
	public void testVerboseThrowableLogger() {
		// Arrange
		Throwable thr = new Throwable("verBoomBot");
		long currentTime = System.currentTimeMillis();
		Date date = new Date(currentTime);
		String file = date + " verbose.log";

		// Act
		Logger.verbose(thr);

		// Assert
		String[] logFiles = logsFileDirectory.list();
		if (logFiles == null) return;
		assertThat(logFiles, notNullValue());
		assertThat(logFiles, hasItemInArray(file));
		assertThatFile(file, containsString(thr.getMessage()));
	}

	@Test
	public void testInfoThrowableLogger() {
		// Arrange
		Throwable thr = new Throwable("I for Info");
		long currentTime = System.currentTimeMillis();
		Date date = new Date(currentTime);
		String file = date + " info.log";

		// Act
		Logger.info(thr);

		// Assert
		String[] logFiles = logsFileDirectory.list();
		if (logFiles == null) return;
		assertThat(logFiles, notNullValue());
		assertThat(logFiles, hasItemInArray(file));
		assertThatFile(file, containsString(thr.getMessage()));
	}

	@Test
	public void testWarnThrowableLogger() {
		// Arrange
		Throwable thr = new Throwable("Welcome to my server");
		long currentTime = System.currentTimeMillis();
		Date date = new Date(currentTime);
		String file = date + " warn.log";

		// Act
		Logger.warn(thr);

		// Assert
		String[] logFiles = logsFileDirectory.list();
		if (logFiles == null) return;
		assertThat(logFiles, notNullValue());
		assertThat(logFiles, hasItemInArray(file));
		assertThatFile(file, containsString(thr.getMessage()));
	}

	@Test
	public void testErrorThrowableLogger() {
		// Arrange
		Throwable thr = new Throwable("This is not a test");
		long currentTime = System.currentTimeMillis();
		Date date = new Date(currentTime);
		String file = date + " error.log";

		// Act
		Logger.error(thr);

		// Assert
		String[] logFiles = logsFileDirectory.list();
		if (logFiles == null) return;
		assertThat(logFiles, notNullValue());
		assertThat(logFiles, hasItemInArray(file));
		assertThatFile(file, containsString(thr.getMessage()));
	}

	@Test
	public void testDangerThrowableLogger() {
		// Arrange
		Throwable thr = new Throwable("what are those");
		long currentTime = System.currentTimeMillis();
		Date date = new Date(currentTime);
		String file = date + " danger.log";

		// Act
		Logger.danger(thr);

		// Assert
		String[] logFiles = logsFileDirectory.list();
		if (logFiles == null) return;
		assertThat(logFiles, notNullValue());
		assertThat(logFiles, hasItemInArray(file));
		assertThatFile(file, containsString(thr.getMessage()));
	}
}
