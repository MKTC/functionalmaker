package nl.makertim.fm.core;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class LoggerThrowableTest {

	@Test
	public void testLoggerThrowable() {
		// Arrange
		var thrMsg = new LoggerThrowable("throw");
		var thrRethrow = new LoggerThrowable(new Throwable("throw"));

		// Act
		var stackTraceMsg = thrMsg.getStackTrace();
		var stackTraceRethrow = thrRethrow.getStackTrace();

		// Assert
		assertThat(stackTraceMsg[0], is(stackTraceRethrow[0]));
		assertThat(thrMsg.getMessage(), is(thrRethrow.getMessage()));
	}
}
