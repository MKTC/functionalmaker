package nl.makertim.fm.core;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class TriSetTest {

	@Test
	public void testTriSetEmpty() {
		// Act
		var triSet = new TriSet<>();

		// Assert
		assertThat(triSet.getT1(), nullValue());
		assertThat(triSet.getT2(), nullValue());
		assertThat(triSet.getT3(), nullValue());
	}

	@Test
	public void testTriSetThirdEmpty() {
		// Arrange
		String testStringIn = "TEST";

		// Act
		var triSet = new TriSet<>(testStringIn, null, null);

		// Assert
		assertThat(triSet.getT1(), is(testStringIn));
		assertThat(triSet.getT2(), nullValue());
		assertThat(triSet.getT3(), nullValue());
	}

	@Test
	public void testTriSetThirdEmpty2() {
		// Arrange
		String testStringIn = "TEST";

		// Act
		var triSet = new TriSet<>(null, testStringIn, null);

		// Assert
		assertThat(triSet.getT1(), nullValue());
		assertThat(triSet.getT2(), is(testStringIn));
		assertThat(triSet.getT3(), nullValue());
	}

	@Test
	public void testTriSetThirdEmpty3() {
		// Arrange
		Double testDoubleIn = 6.9;

		// Act
		var triSet = new TriSet<>(null, null, testDoubleIn);

		// Assert
		assertThat(triSet.getT1(), nullValue());
		assertThat(triSet.getT2(), nullValue());
		assertThat(triSet.getT3(), is(testDoubleIn));
	}

	@Test
	public void testTriSetFullyFilled() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";
		Double testDoubleIn = 6.9;

		// Act
		var triSet = new TriSet<>(testIntegerIn, testStringIn, testDoubleIn);

		// Assert
		assertThat(triSet.getT1(), is(testIntegerIn));
		assertThat(triSet.getT2(), is(testStringIn));
		assertThat(triSet.getT3(), is(testDoubleIn));
	}

	@Test
	public void testTriSetT1() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";
		Double testDoubleIn = 6.9;
		Integer testIntegerSet = 2304;

		// Act
		var triSet = new TriSet<>(testIntegerIn, testStringIn, testDoubleIn);
		triSet.setT1(testIntegerSet);

		// Assert
		assertThat(triSet.getT1(), is(testIntegerSet));
		assertThat(triSet.getT2(), is(testStringIn));
		assertThat(triSet.getT3(), is(testDoubleIn));
	}

	@Test
	public void testTriSetT2() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";
		Double testDoubleIn = 6.9;
		String testStringSet = "tim";

		// Act
		var triSet = new TriSet<>(testIntegerIn, testStringIn, testDoubleIn);
		triSet.setT2(testStringSet);

		// Assert
		assertThat(triSet.getT1(), is(testIntegerIn));
		assertThat(triSet.getT2(), is(testStringSet));
		assertThat(triSet.getT3(), is(testDoubleIn));
	}

	@Test
	public void testTriSetT3() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";
		Double testDoubleIn = 6.9;
		Double testDoubleSet = 4.20;

		// Act
		var triSet = new TriSet<>(testIntegerIn, testStringIn, testDoubleIn);
		triSet.setT3(testDoubleSet);

		// Assert
		assertThat(triSet.getT1(), is(testIntegerIn));
		assertThat(triSet.getT2(), is(testStringIn));
		assertThat(triSet.getT3(), is(testDoubleSet));
	}

	@Test
	public void testTriSetMap() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";
		Double testDoubleIn = 6.9;
		Integer testIntegerMap = 2304;
		String testStringMap = "tim";
		Double testDoubleMap = 4.20;

		// Act
		var triSet = new TriSet<>(testIntegerIn, testStringIn, testDoubleIn);
		triSet = triSet.map(i -> testIntegerMap, s -> testStringMap, d -> testDoubleMap);

		// Assert
		assertThat(triSet.getT1(), is(testIntegerMap));
		assertThat(triSet.getT2(), is(testStringMap));
		assertThat(triSet.getT3(), is(testDoubleMap));
	}

	@Test
	public void testTriSetMapT1() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";
		Double testDoubleIn = 6.9;
		Integer testIntegerMap = 2304;

		// Act
		var triSet = new TriSet<>(testIntegerIn, testStringIn, testDoubleIn);
		triSet = triSet.mapT1(i -> testIntegerMap);

		// Assert
		assertThat(triSet.getT1(), is(testIntegerMap));
		assertThat(triSet.getT2(), is(testStringIn));
		assertThat(triSet.getT3(), is(testDoubleIn));
	}

	@Test
	public void testTriSetMapT2() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";
		Double testDoubleIn = 6.9;
		String testStringMap = "tim";

		// Act
		var triSet = new TriSet<>(testIntegerIn, testStringIn, testDoubleIn);
		triSet = triSet.mapT2(s -> testStringMap);

		// Assert
		assertThat(triSet.getT1(), is(testIntegerIn));
		assertThat(triSet.getT2(), is(testStringMap));
		assertThat(triSet.getT3(), is(testDoubleIn));
	}


	@Test
	public void testTriSetMapT3() {
		// Arrange
		Integer testIntegerIn = 1337;
		String testStringIn = "TEST";
		Double testDoubleIn = 6.9;
		Double testDoubleMap = 4.20;

		// Act
		var triSet = new TriSet<>(testIntegerIn, testStringIn, testDoubleIn);
		triSet = triSet.mapT3(s -> testDoubleMap);

		// Assert
		assertThat(triSet.getT1(), is(testIntegerIn));
		assertThat(triSet.getT2(), is(testStringIn));
		assertThat(triSet.getT3(), is(testDoubleMap));
	}


	@Test
	public void testBiSetEquals() {
		// Act
		var triSet = new TriSet<>();
		var triSet2 = new TriSet<>();

		// Assert
		assertThat(triSet, is(triSet2));
		assertThat(triSet.hashCode(), is(triSet2.hashCode()));
	}
}
