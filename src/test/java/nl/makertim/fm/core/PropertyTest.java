package nl.makertim.fm.core;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PropertyTest {

	@Test
	public void createProperty() {
		// Arrange
		var input = "lala string";

		// Act
		var property = new Property<>(input);

		// Assert
		assertThat(property.get(), is(input));
		assertTrue(property.isPresent());
	}

	@Test
	public void createEmptyProperty() {
		// Arrange

		// Act
		var property = new Property<>();

		// Assert
		assertThat(property.get(), nullValue());
		assertTrue(property.isNull());
	}


	@Test
	public void createEmptyPropertyFillLater() {
		// Arrange
		var input = "testing";

		// Act
		var property = new Property<>();
		property.set(input);

		// Assert
		assertThat(property.get(), is(input));
	}

	@Test
	public void createPropertyFillLater() {
		// Arrange
		var input = "testing";
		var newInput = "now test";

		// Act
		var property = new Property<>(input);
		property.set(newInput);

		// Assert
		assertThat(property.get(), is(newInput));
	}

	@Test
	public void createPropertyChangeType() {
		// Arrange
		var input = 1;
		var newInput = "now test";

		// Act
		var property = new Property<Object>(input);
		property.set(newInput);

		// Assert
		assertThat(property.get(), is(newInput));
	}

	@Test
	public void createPropertyChangeTypeMap() {
		// Arrange
		var input = 1;

		// Act
		var property = new Property<>(input);
		var newProperty = property.map(i -> Integer.toString(i));

		// Assert
		assertThat(newProperty.get(), instanceOf(String.class));
	}

	@Test
	public void propertyEquals() {
		// Arrange
		var input = 1;

		// Act
		var propertyA = new Property<>(input);
		var propertyB = new Property<>(input);

		// Assert
		assertThat(propertyA, equalTo(propertyB));
		assertThat(propertyB, equalTo(propertyA));
	}
}
