package nl.makertim.fm.core;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EasyMapTest {

	@Test
	public void map() {
		// Act
		var result = EasyMap.map();

		// Assert
		assertThat(result, equalTo(Collections.emptyMap()));
	}

	@Test
	public void mapKV() {
		// Arrange
		var inputKey = "key";
		var inputValue = 1398;

		// Act
		var result = EasyMap.map(inputKey, inputValue);

		// Assert
		assertThat(result.keySet(), hasItem(inputKey));
		assertThat(result.get(inputKey), equalTo(inputValue));
	}

	@Test
	public void mapKVx2() {
		// Arrange
		var inputKeyA = "key";
		var inputValueA = 1398;
		var inputKeyB = "jan";
		var inputValueB = -1;

		// Act
		var result = EasyMap.map(inputKeyA, inputValueA, inputKeyB, inputValueB);

		// Assert
		assertThat(result.keySet(), hasItem(inputKeyA));
		assertThat(result.get(inputKeyA), equalTo(inputValueA));
		assertThat(result.keySet(), hasItem(inputKeyB));
		assertThat(result.get(inputKeyB), equalTo(inputValueB));
	}

	@Test
	public void mapKVx3() {
		// Arrange
		var inputKeyA = "key";
		var inputValueA = 1398;
		var inputKeyB = "jan";
		var inputValueB = -1;
		var inputKeyC = "a";
		var inputValueC = 'a';

		// Act
		var result = EasyMap.map(
			inputKeyA, inputValueA,
			inputKeyB, inputValueB,
			inputKeyC, inputValueC
		);

		// Assert
		assertThat(result.keySet(), hasItem(inputKeyA));
		assertThat(result.get(inputKeyA), equalTo(inputValueA));
		assertThat(result.keySet(), hasItem(inputKeyB));
		assertThat(result.get(inputKeyB), equalTo(inputValueB));
		assertThat(result.keySet(), hasItem(inputKeyC));
		assertThat(result.get(inputKeyC), equalTo(inputValueC));
	}

	@Test
	public void mapKVxX() {
		// Arrange
		var inputKeyA = "key";
		var inputValueA = 1398;
		var inputKeyB = "jan";
		var inputValueB = -1;
		var inputKeyC = "a";
		var inputValueC = 'a';
		var inputKeyD = "shewantsthe";
		var inputValueD = 'D';

		// Act
		var result = EasyMap.map(
			inputKeyA, inputValueA,
			inputKeyB, inputValueB,
			inputKeyC, inputValueC,
			inputKeyD, inputValueD
		);

		// Assert
		assertThat(result.keySet(), hasItem(inputKeyA));
		assertThat(result.get(inputKeyA), equalTo(inputValueA));
		assertThat(result.keySet(), hasItem(inputKeyB));
		assertThat(result.get(inputKeyB), equalTo(inputValueB));
		assertThat(result.keySet(), hasItem(inputKeyC));
		assertThat(result.get(inputKeyC), equalTo(inputValueC));
		assertThat(result.keySet(), hasItem(inputKeyD));
		assertThat(result.get(inputKeyD), equalTo(inputValueD));
	}

	@Test
	public void mapKVxXFaultyAmountArgs() {
		// Arrange
		var inputKeyA = "key";
		var inputValueA = 1398;
		var inputKeyB = "jan";
		var inputValueB = -1;
		var inputKeyC = "a";
		var inputValueC = 'a';
		var inputKeyD = "shewantsthe";

		// Act
		Executable executable = () -> EasyMap.map(
			inputKeyA, inputValueA,
			inputKeyB, inputValueB,
			inputKeyC, inputValueC,
			inputKeyD
		);

		// Assert
		assertThrows(RuntimeException.class, executable);
	}


	@Test
	public void mapKVxXFaultyKeyNotString() {
		// Arrange
		var inputKeyA = "key";
		var inputValueA = 1398;
		var inputKeyB = "jan";
		var inputValueB = -1;
		var inputKeyC = "a";
		var inputValueC = 'a';
		var inputKeyD = 1;
		var inputValueD = 'D';

		// Act
		Executable executable = () -> EasyMap.map(
			inputKeyA, inputValueA,
			inputKeyB, inputValueB,
			inputKeyC, inputValueC,
			inputKeyD, inputValueD
		);

		// Assert
		assertThrows(RuntimeException.class, executable);
	}

	@Test
	public void mapConsumer() {
		// Arrange
		var inputKey = "key";
		var inputValue = 1398;

		// Act
		var result = EasyMap.map(map -> map.insert(inputKey, inputValue));

		// Assert
		assertThat(result.keySet(), hasItem(inputKey));
		assertThat(result.get(inputKey), equalTo(inputValue));
	}

	@Test
	public void insert() {
		// Arrange
		var inputKey = "key";
		var inputValue = 1398;

		// Act
		var result = new EasyMap().insert(inputKey, inputValue);

		// Assert
		assertThat(result.keySet(), hasItem(inputKey));
		assertThat(result.get(inputKey), equalTo(inputValue));
	}

	@Test
	public void add() {
		// Arrange
		var inputKey = "key";
		var inputValue = 1234567890;

		// Act
		var result = new EasyMap().add(inputKey, inputValue);

		// Assert
		assertThat(result.keySet(), hasItem(inputKey));
		assertThat(result.get(inputKey), equalTo(inputValue));
	}

	@Test
	public void set() {
		// Arrange
		var inputKey = "key";
		var inputValue = 145;

		// Act
		var result = new EasyMap().set(inputKey, inputValue);

		// Assert
		assertThat(result.keySet(), hasItem(inputKey));
		assertThat(result.get(inputKey), equalTo(inputValue));
	}

	@Test
	public void put() {
		// Arrange
		var inputKey = "key";
		var inputValue = 11;

		// Act
		var result = new EasyMap().put(inputKey, inputValue);

		// Assert
		assertThat(result.keySet(), hasItem(inputKey));
		assertThat(result.get(inputKey), equalTo(inputValue));
	}
}
