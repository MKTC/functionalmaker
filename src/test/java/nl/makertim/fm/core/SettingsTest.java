package nl.makertim.fm.core;

import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SettingsTest {

	@SuppressWarnings("unchecked")
	public static Map<String, Object> readTestFile(String filename) {
		InputStream stream = SettingsTest.class.getResourceAsStream(filename);
		if (stream == null) throw new NullPointerException(filename + " not found");
		Reader reader = new InputStreamReader(stream);
		return (Map<String, Object>) new Gson().fromJson(reader, Map.class);
	}

	@Test
	public void testSettingsCreation() {
		// Arrange
		var name = "json";
		var fileName = "/settings.json";
		var settingsFile = readTestFile(fileName);

		// Act
		var settings = new Settings(name, settingsFile);

		// Assert
		assertThat(settings, not(nullValue()));
		assertThat(settings.getName(), is(name));
		assertThat(settings.get(""), not(nullValue()));
	}

	@Test
	public void testSettingsCreationNull() {
		// Act
		Executable creator = () -> new Settings(null, null);

		// Assert
		assertThrows(NullPointerException.class, creator);
	}

	@Test
	public void testSettingsInteger() {
		// Arrange
		var name = "json";
		var fileName = "/settings.json";
		var expectedInteger = 8181;
		var path = "integer";
		var settingsFile = readTestFile(fileName);
		var settings = new Settings(name, settingsFile);

		// Act
		var integerValue = settings.getInt(path);
		var integerNestedValue = settings.getInt("nested.properties." + path);
		var integerNestedNestedValue = settings.getInt("nested.nested.properties." + path);
		var integerNestedNestedNestedValue = settings.getInt("nested.nested.nested.properties." + path);
		var fail = settings.getInt("nested.nested.nested.properties.fail." + path);

		// Assert
		assertThat(integerValue, is(expectedInteger));
		assertThat(integerNestedValue, is(expectedInteger));
		assertThat(integerNestedNestedValue, is(expectedInteger));
		assertThat(integerNestedNestedNestedValue, is(expectedInteger));
		assertThat(fail, nullValue());
	}

	@Test
	public void testSettingsDouble() {
		// Arrange
		var name = "json";
		var fileName = "/settings.json";
		var expected = 5.1D;
		var path = "double";
		var settingsFile = readTestFile(fileName);
		var settings = new Settings(name, settingsFile);

		// Act
		var doubleValue = settings.getDouble(path);
		var doubleNestedValue = settings.getDouble("nested.properties." + path);
		var doubleNestedNestedValue = settings.getDouble("nested.nested.properties." + path);
		var doubleNestedNestedNestedValue = settings.getDouble("nested.nested.nested.properties." + path);
		var fail = settings.getDouble("nested.nested.nested.properties.fail." + path);

		// Assert
		assertThat(doubleValue, is(expected));
		assertThat(doubleNestedValue, is(expected));
		assertThat(doubleNestedNestedValue, is(expected));
		assertThat(doubleNestedNestedNestedValue, is(expected));
		assertThat(fail, nullValue());
	}

	@Test
	public void testSettingsBooleanFalse() {
		// Arrange
		var name = "json";
		var fileName = "/settings.json";
		var notTrue = false;
		var path = "booleanFalse";
		var settingsFile = readTestFile(fileName);
		var settings = new Settings(name, settingsFile);

		// Act
		var booleanValue = settings.getBoolean(path);
		var booleanNestedValue = settings.getBoolean("nested.properties." + path);
		var booleanNestedNestedValue = settings.getBoolean("nested.nested.properties." + path);
		var booleanNestedNestedNestedValue = settings.getBoolean("nested.nested.nested.properties." + path);
		var fail = settings.getBoolean("nested.nested.nested.properties.fail." + path);

		// Assert
		assertThat(booleanValue, is(notTrue));
		assertThat(booleanNestedValue, is(notTrue));
		assertThat(booleanNestedNestedValue, is(notTrue));
		assertThat(booleanNestedNestedNestedValue, is(notTrue));
		assertThat(fail, nullValue());
	}

	@Test
	public void testSettingsBooleanTrue() {
		// Arrange
		var name = "json";
		var fileName = "/settings.json";
		var isTrue = true;
		var path = "booleanTrue";
		var settingsFile = readTestFile(fileName);
		var settings = new Settings(name, settingsFile);

		// Act
		var booleanValue = settings.getBoolean(path);
		var booleanNestedValue = settings.getBoolean("nested.properties." + path);
		var booleanNestedNestedValue = settings.getBoolean("nested.nested.properties." + path);
		var booleanNestedNestedNestedValue = settings.getBoolean("nested.nested.nested.properties." + path);
		var fail = settings.getBoolean("nested.nested.nested.properties.fail." + path);

		// Assert
		assertThat(booleanValue, is(isTrue));
		assertThat(booleanNestedValue, is(isTrue));
		assertThat(booleanNestedNestedValue, is(isTrue));
		assertThat(booleanNestedNestedNestedValue, is(isTrue));
		assertThat(fail, nullValue());
	}

	@Test
	public void testSettingsString() {
		// Arrange
		var name = "json";
		var fileName = "/settings.json";
		var expectedString = "0.0.0.0";
		var path = "string";
		var settingsFile = readTestFile(fileName);
		var settings = new Settings(name, settingsFile);

		// Act
		var stringValue = settings.getString(path);
		var stringNestedValue = settings.getString("nested.properties." + path);
		var stringNestedNestedValue = settings.getString("nested.nested.properties." + path);
		var stringNestedNestedNestedValue = settings.getString("nested.nested.nested.properties." + path);
		var fail = settings.getString("nested.nested.nested.properties.fail." + path);

		// Assert
		assertThat(stringValue, is(expectedString));
		assertThat(stringNestedValue, is(expectedString));
		assertThat(stringNestedNestedValue, is("0.0"));
		assertThat(stringNestedNestedNestedValue, is(expectedString));
		assertThat(fail, nullValue());
	}

	@Test
	public void testSettingsPath() {
		// Arrange
		var name = "json";
		var fileName = "/settings.json";
		var path = "path";
		var settingsFile = readTestFile(fileName);
		var settings = new Settings(name, settingsFile);

		// Act
		var pathValue = settings.getString(path);
		var pathNestedValue = settings.getString("nested." + path);
		var pathNestedNestedValue = settings.getString("nested.nested." + path);
		var pathNestedNestedNestedValue = settings.getString("nested.nested.nested." + path);
		var fail = settings.getString("nested.nested.nested.properties.fail." + path);

		// Assert
		assertThat(pathValue, not(isEmptyString()));
		assertThat(pathNestedValue, not(isEmptyString()));
		assertThat(pathNestedNestedValue, not(isEmptyString()));
		assertThat(pathNestedNestedNestedValue, not(isEmptyString()));
		assertThat(fail, nullValue());
	}

	@Test
	public void testSettingsFile() {
		// Arrange
		var name = "json";
		var fileName = "/settings.json";
		var path = "propertiesFile";
		var settingsFile = readTestFile(fileName);
		var settings = new Settings(name, settingsFile);

		// Act
		var fileValue = settings.getString(path);
		var fileNestedValue = settings.getString("nested." + path);
		var fileNestedNestedValue = settings.getString("nested.nested." + path);
		var fileNestedNestedNestedValue = settings.getString("nested.nested.nested." + path);
		var fail = settings.getString("nested.nested.nested.properties.fail." + path);

		// Assert
		assertThat(fileValue, not(isEmptyString()));
		assertThat(fileNestedValue, not(isEmptyString()));
		assertThat(fileNestedNestedValue, not(isEmptyString()));
		assertThat(fileNestedNestedNestedValue, not(isEmptyString()));
		assertThat(fail, nullValue());
	}
}
