package nl.makertim.fm.core;

import nl.makertim.fm.safe.SafeTry;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

public class BiTypedOptionalTest {

	@Test
	public void testEmpty() {
		// Arrange
		var empty = BiTypedOptional.empty();

		// Act

		// Assert
		Assertions.assertTrue(empty.isEmpty());
		Assertions.assertFalse(empty.isPresent());
		Assertions.assertFalse(empty.hasType1(Object.class));
		Assertions.assertFalse(empty.hasType2(Object.class));
		Assertions.assertEquals(empty.toString(), "BiOptional.empty");
		Assertions.assertEquals(empty.hashCode(), 0);
		Assertions.assertEquals(empty, BiTypedOptional.empty());
		Assertions.assertSame(empty, BiTypedOptional.empty());
		Assertions.assertEquals(empty, BiTypedOptional.ofNullableT1(null));
		Assertions.assertNotEquals(empty, null);
		Assertions.assertThrows(NoSuchElementException.class, () -> empty.orElseThrowT1(Object.class));
		Assertions.assertThrows(NoSuchElementException.class, () -> empty.orElseThrowT2(Object.class));
		Assertions.assertArrayEquals(empty.streamT1(Object.class).toArray(), Stream.empty().toArray());
		Assertions.assertArrayEquals(empty.streamT2(Object.class).toArray(), Stream.empty().toArray());
		Assertions.assertEquals(empty.orT1(Object.class, () -> 2304), Optional.of(2304));
		Assertions.assertEquals(empty.orT2(Object.class, () -> 2304), Optional.of(2304));
	}

	@Test
	public void testT1() {
		// Arrange
		var biTypedOptional = BiTypedOptional.<Integer, String> ofT1(1);

		// Act

		// Assert
		Assertions.assertFalse(biTypedOptional.isEmpty());
		Assertions.assertTrue(biTypedOptional.isPresent());
		Assertions.assertEquals(biTypedOptional.toString(), "BiOptional[1]");
		Assertions.assertEquals(biTypedOptional.hashCode(), 1);
		Assertions.assertEquals(biTypedOptional, BiTypedOptional.ofT1(1));
		Assertions.assertNotSame(biTypedOptional, BiTypedOptional.ofT1(1));
		Assertions.assertNotEquals(biTypedOptional, 1);
		Assertions.assertThrows(NoSuchElementException.class, () -> biTypedOptional.orElseThrowT2(String.class));
		Assertions.assertEquals(biTypedOptional.orElseThrowT1(Number.class), 1);
		Assertions.assertEquals(biTypedOptional.orElseT1(2), 1);
		Assertions.assertEquals(biTypedOptional.orElseT2("a"), "a");
		Assertions.assertArrayEquals(biTypedOptional.streamT1(Number.class).toArray(), Stream.of(1).toArray());
		Assertions.assertEquals(biTypedOptional.orT1(Object.class, () -> 2304), Optional.of(1));
	}

	@Test
	public void testT2() {
		// Arrange
		var biTypedOptional = BiTypedOptional.<String, Integer> ofT2(1);

		// Act

		// Assert
		Assertions.assertFalse(biTypedOptional.isEmpty());
		Assertions.assertTrue(biTypedOptional.isPresent());
		Assertions.assertEquals(biTypedOptional.toString(), "BiOptional[1]");
		Assertions.assertEquals(biTypedOptional.hashCode(), 1);
		Assertions.assertEquals(biTypedOptional, BiTypedOptional.ofT2(1));
		Assertions.assertNotSame(biTypedOptional, BiTypedOptional.ofT2(1));
		Assertions.assertNotEquals(biTypedOptional, 1);
		Assertions.assertThrows(NoSuchElementException.class, () -> biTypedOptional.orElseThrowT1(String.class));
		Assertions.assertEquals(biTypedOptional.orElseThrowT2(Number.class), 1);
		Assertions.assertEquals(biTypedOptional.orElseT2(2), 1);
		Assertions.assertEquals(biTypedOptional.orElseT1("a"), "a");
		Assertions.assertArrayEquals(biTypedOptional.streamT2(Number.class).toArray(), Stream.of(1).toArray());
		Assertions.assertEquals(biTypedOptional.orT2(Object.class, () -> 2304), Optional.of(1));
	}

	@Test
	public void testOfNullableT1() {
		// Arrange
		var biTypedOptional = BiTypedOptional.<String, String> ofNullableT1("it just works");
		var exception = Property.empty();
		var valueProperty = Property.empty();
		var valueOrElseProperty = Property.empty();

		// Act
		biTypedOptional.ifT1Present(String.class, valueProperty::set);
		biTypedOptional.ifT1PresentOrElse(String.class, valueOrElseProperty::set, System.out::println);
		var filtered =  ((BiTypedOptional) biTypedOptional).filterIfT1(Integer.class);

		// Assert
		assertThat(biTypedOptional.hasType1(String.class), is(true));
		assertThat(biTypedOptional.getType1(String.class), is("it just works"));
		assertThat(valueProperty.get(), is("it just works"));
		assertThat(filtered.isEmpty(), is(true));
		SafeTry.execute(() -> ((BiTypedOptional) biTypedOptional).getType1(Integer.class), exception::set);
		assertThat(exception.get(), instanceOf(NoSuchElementException.class));
		assertThat(((Throwable) exception.get()).getMessage(), is("Value is not type of first type"));
	}

	@Test
	public void testOfNullableT2() {
		// Arrange
		var biTypedOptional = BiTypedOptional.<String, String> ofNullableT2("it just works");
		var exception = Property.empty();
		var valueProperty = Property.empty();
		var valueOrElseProperty = Property.empty();

		// Act
		biTypedOptional.ifT2Present(String.class, valueProperty::set);
		biTypedOptional.ifT2PresentOrElse(String.class, valueOrElseProperty::set, System.out::println);
		var filtered =  ((BiTypedOptional) biTypedOptional).filterIfT2(Integer.class);

		// Assert
		assertThat(biTypedOptional.hasType2(String.class), is(true));
		assertThat(biTypedOptional.getType2(String.class), is("it just works"));
		assertThat(valueProperty.get(), is("it just works"));
		assertThat(filtered.isEmpty(), is(true));
		SafeTry.execute(() -> ((BiTypedOptional) biTypedOptional).getType2(Integer.class), exception::set);
		assertThat(exception.get(), instanceOf(NoSuchElementException.class));
		assertThat(((Throwable) exception.get()).getMessage(), is("Value is not type of first type"));
	}

	@Test
	public void testOfNullableNullT1() {
		// Arrange
		var biTypedOptional = BiTypedOptional.<String, String> ofNullableT1(null);
		var exception = Property.empty();
		var valueOrElseProperty = Property.empty();

		// Act
		biTypedOptional.ifT1PresentOrElse(String.class, valueOrElseProperty::set, () -> {});
		var filtered = biTypedOptional.filterIfT1(String.class);

		// Assert
		assertThat(biTypedOptional.hasType1(String.class), is(false));
		assertThat(valueOrElseProperty.isNull(), is(true));
		assertThat(filtered.isEmpty(), is(true));
		SafeTry.execute(() -> biTypedOptional.getType1(String.class), exception::set);
		assertThat(exception.get(), instanceOf(NoSuchElementException.class));
		assertThat(((Throwable) exception.get()).getMessage(), is("No value present"));
	}

	@Test()
	public void testOfNullableNullT2() {
		// Arrange
		var biTypedOptional = BiTypedOptional.<String, String> ofNullableT2(null);
		var exception = Property.empty();
		var valueOrElseProperty = Property.empty();

		// Act
		biTypedOptional.ifT2PresentOrElse(String.class, valueOrElseProperty::set, () -> {});
		var filtered = biTypedOptional.filterIfT2(String.class);

		// Assert
		assertThat(biTypedOptional.hasType2(String.class), is(false));
		assertThat(valueOrElseProperty.isNull(), is(true));
		assertThat(filtered.isEmpty(), is(true));
		SafeTry.execute(() -> biTypedOptional.getType2(String.class), exception::set);
		assertThat(exception.get(), instanceOf(NoSuchElementException.class));
		assertThat(((Throwable) exception.get()).getMessage(), is("No value present"));
	}
}
