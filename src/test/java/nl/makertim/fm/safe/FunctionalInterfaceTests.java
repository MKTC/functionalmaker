package nl.makertim.fm.safe;

import nl.makertim.fm.core.BiSet;
import nl.makertim.fm.core.Property;
import nl.makertim.fm.core.TriSet;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.Is.is;

public class FunctionalInterfaceTests {

	@Test
	public void testThrowingBiConsumer() throws Exception {
		// Arrange
		String ret = "teststring";
		Property<Boolean> calledFirst = new Property<>(false);
		Property<Boolean> calledSecond = new Property<>(false);
		ThrowingBiConsumer<String, String, Exception> consumer =
			(a, b) -> calledFirst.set(true);

		// Act
		consumer = consumer.andThen((a, b) -> calledSecond.set(true));
		ThrowingBiFunction<String, String, String, Exception> function = consumer.toFunction(ret);

		// Assert
		assertThat(function.accept(null, null), is(ret));
		assertThat(calledFirst.get(), is(true));
		assertThat(calledSecond.get(), is(true));
	}

	@Test
	public void testThrowingBiFunction() throws Exception {
		// Arrange
		Property<Boolean> calledFirst = new Property<>(false);
		ThrowingBiFunction<String, String, String, Exception> function =
			(a, b) -> {
				calledFirst.set(true);
				return a + b;
			};

		// Act
		ThrowingBiConsumer<String, String, Exception> consumer = function.toConsumer();
		consumer.accept("", "");

		// Assert
		assertThat(calledFirst.get(), is(true));
	}

	@Test
	public void testThrowingBiFunctionBi() throws Exception {
		// Arrange
		Property<Boolean> calledFirst = new Property<>(false);
		ThrowingBiFunctionBi<String, String, String, String, Exception> function =
			(a, b) -> {
				calledFirst.set(true);
				return new BiSet<>(a, b);
			};

		// Act
		ThrowingBiConsumer<String, String, Exception> consumer = function.toConsumer();
		consumer.accept("", "");

		// Assert
		assertThat(calledFirst.get(), is(true));
	}

	@Test
	public void testThrowingBiFunctionTri() throws Exception {
		// Arrange
		Property<Boolean> calledFirst = new Property<>(false);
		ThrowingBiFunctionTri<String, String, String, String, String, Exception> function =
			(a, b) -> {
				calledFirst.set(true);
				return new TriSet<>(a, b, a + b);
			};

		// Act
		ThrowingBiConsumer<String, String, Exception> consumer = function.toConsumer();
		consumer.accept("", "");

		// Assert
		assertThat(calledFirst.get(), is(true));
	}

	@Test
	public void testThrowingBiSupplier() throws Exception {
		// Arrange
		Property<Boolean> calledFirst = new Property<>(false);
		ThrowingBiSupplier<String, String, Exception> supplier =
			() -> {
				calledFirst.set(true);
				return new BiSet<>("", "");
			};

		// Act
		ThrowingFunction<String, BiSet<String, String>, Exception> function = supplier.toFunction();

		// Assert
		assertThat(function.apply(""), instanceOf(BiSet.class));
		assertThat(calledFirst.get(), is(true));
	}

	@Test
	public void testThrowingConsumer() throws Exception {
		// Arrange
		String ret = "teststring";
		Property<Boolean> calledFirst = new Property<>(false);
		Property<Boolean> calledSecond = new Property<>(false);
		ThrowingConsumer<String, Exception> consumer =
			a -> calledFirst.set(true);

		// Act
		consumer = consumer.andThen(a -> calledSecond.set(true));
		ThrowingFunction<String, String, Exception> function = consumer.toFunction(ret);

		// Assert
		assertThat(function.apply(null), is(ret));
		assertThat(calledFirst.get(), is(true));
		assertThat(calledSecond.get(), is(true));
	}

	@Test
	public void testThrowingFunction() throws Exception {
		// Arrange
		Property<Boolean> calledFirst = new Property<>(false);
		ThrowingFunction<String, String, Exception> function =
			a -> {
				calledFirst.set(true);
				return a;
			};

		// Act
		ThrowingConsumer<String, Exception> consumer = function.toConsumer();
		consumer.accept("");

		// Assert
		assertThat(calledFirst.get(), is(true));
	}

	@Test
	public void testThrowingFunctionBi() throws Exception {
		// Arrange
		Property<Boolean> calledFirst = new Property<>(false);
		ThrowingFunctionBi<String, String, String, Exception> function =
			a -> {
				calledFirst.set(true);
				return new BiSet<>(a, a);
			};

		// Act
		ThrowingConsumer<String, Exception> consumer = function.toConsumer();
		consumer.accept("");

		// Assert
		assertThat(calledFirst.get(), is(true));
	}

	@Test
	public void testThrowingFunctionTri() throws Exception {
		// Arrange
		Property<Boolean> calledFirst = new Property<>(false);
		ThrowingFunctionTri<String, String, String, String, Exception> function =
			a -> {
				calledFirst.set(true);
				return new TriSet<>(a, a, a);
			};

		// Act
		ThrowingConsumer<String, Exception> consumer = function.toConsumer();
		consumer.accept("");

		// Assert
		assertThat(calledFirst.get(), is(true));
	}

	@Test
	public void testThrowingRunnable() throws Exception {
		// Arrange
		String ret = "teststring";
		Property<Boolean> calledFirst = new Property<>(false);
		Property<Boolean> calledSecond = new Property<>(false);
		ThrowingRunnable<Exception> runnable = () -> calledFirst.set(true);

		// Act
		runnable = runnable.andThen(() -> calledSecond.set(true));

		// Assert
		runnable.run();
		assertThat(calledFirst.get(), is(true));
		assertThat(calledSecond.get(), is(true));
	}

	@Test
	public void testThrowingSupplier() throws Exception {
		// Arrange
		String ret = "teststring";
		Property<Boolean> calledFirst = new Property<>(false);
		ThrowingSupplier<String, Exception> supplier =
			() -> {
				calledFirst.set(true);
				return ret;
			};

		// Act
		ThrowingFunction<Object, String, Exception> function = supplier.toFunction();

		// Assert
		assertThat(function.apply(""), is(ret));
		assertThat(calledFirst.get(), is(true));
	}

	@Test
	public void testThrowingTriConsumer() throws Exception {
		// Arrange
		String ret = "teststring";
		Property<Boolean> calledFirst = new Property<>(false);
		Property<Boolean> calledSecond = new Property<>(false);
		ThrowingTriConsumer<String, String, String, Exception> consumer =
			(a, b, c) -> calledFirst.set(true);

		// Act
		consumer = consumer.andThen((a, b, c) -> calledSecond.set(true));
		ThrowingTriFunction<String, String, String, String, Exception> function = consumer.toFunction(ret);

		// Assert
		assertThat(function.accept(null, null, null), is(ret));
		assertThat(calledFirst.get(), is(true));
		assertThat(calledSecond.get(), is(true));
	}

	@Test
	public void testThrowingTriFunction() throws Exception {
		// Arrange
		Property<Boolean> calledFirst = new Property<>(false);
		ThrowingTriFunction<String, String, String, String, Exception> function =
			(a, b, c) -> {
				calledFirst.set(true);
				return a + b + c;
			};

		// Act
		ThrowingTriConsumer<String, String, String, Exception> consumer = function.toConsumer();
		consumer.accept("", "", "");

		// Assert
		assertThat(calledFirst.get(), is(true));
	}

	@Test
	public void testThrowingTriFunctionBi() throws Exception {
		// Arrange
		Property<Boolean> calledFirst = new Property<>(false);
		ThrowingTriFunctionBi<String, String, String, String, String, Exception> function =
			(a, b, c) -> {
				calledFirst.set(true);
				return new BiSet<>(a, b);
			};

		// Act
		ThrowingTriConsumer<String, String, String, Exception> consumer = function.toConsumer();
		consumer.accept("", "", "");

		// Assert
		assertThat(calledFirst.get(), is(true));
	}

	@Test
	public void testThrowingTriFunctionTri() throws Exception {
		// Arrange
		Property<Boolean> calledFirst = new Property<>(false);
		ThrowingTriFunctionTri<String, String, String, String, String, String, Exception> function =
			(a, b, c) -> {
				calledFirst.set(true);
				return new TriSet<>(a, b, c);
			};

		// Act
		ThrowingTriConsumer<String, String, String, Exception> consumer = function.toConsumer();
		consumer.accept("", "", "");

		// Assert
		assertThat(calledFirst.get(), is(true));
	}

	@Test
	public void testThrowingTriSupplier() throws Exception {
		// Arrange
		Property<Boolean> calledFirst = new Property<>(false);
		ThrowingTriSupplier<String,String, String, Exception> supplier =
			() -> {
				calledFirst.set(true);
				return new TriSet<>("", "", "");
			};

		// Act
		ThrowingFunction<Object, TriSet<String, String, String>, Exception> function = supplier.toFunction();

		// Assert
		assertThat(function.apply(""), instanceOf(TriSet.class));
		assertThat(calledFirst.get(), is(true));
	}
}
