package nl.makertim.fm.safe;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SafeTryTest {

	@Test
	public void testSafeTryExecuteRunnable() {
		// Arrange
		int[] calls = { 0, 0 };

		// Act
		SafeTry.execute((ThrowingRunnable<?>) () -> {
			calls[0]++;
			throw new RuntimeException();
		});

		// Assert
		assertThat(calls[0], is(1));
		assertThat(calls[1], is(0));
	}

	@Test
	public void testSafeTryExecuteWithCatchRunnable() {
		// Arrange
		int[] calls = { 0, 0 };

		// Act
		SafeTry.execute((ThrowingRunnable<?>) () -> {
			calls[0]++;
			throw new RuntimeException();
		}, ex -> calls[1]++);

		// Assert
		assertThat(calls[0], is(1));
		assertThat(calls[1], is(1));
	}

	@Test
	public void testSafeTryExecuteSupplier() {
		// Arrange
		int[] calls = { 0, 0 };

		// Act
		SafeTry.execute((ThrowingSupplier<?, ?>) () -> {
			calls[0]++;
			throw new RuntimeException();
		});

		// Assert
		assertThat(calls[0], is(1));
		assertThat(calls[1], is(0));
	}

	@Test
	public void testSafeTryExecuteWithCatchSupplier() {
		// Arrange
		int[] calls = { 0, 0, 0 };

		// Act
		calls[2] = SafeTry.execute((ThrowingSupplier<Integer, ?>) () -> {
			calls[0]++;
			throw new RuntimeException();
		}, ex -> calls[1]++);

		// Assert
		assertThat(calls[0], is(1));
		assertThat(calls[1], is(1));
		assertThat(calls[2], is(0));
	}


	@Test
	public void testSafeTryExecuteFunction() {
		// Arrange
		String nonFinalString = "";
		int[] calls = { 0, 0 };

		// Act
		SafeTry.executeNonFinal(nonFinalString, str -> {
			calls[0]++;
			throw new RuntimeException();
		});

		// Assert
		assertThat(calls[0], is(1));
		assertThat(calls[1], is(0));
	}

	@Test
	public void testSafeTryExecuteFunctionCatchSupplier() {
		// Arrange
		String nonFinalString = "";
		int[] calls = { 0, 0, 0 };

		// Act
		calls[2] = SafeTry.executeNonFinal(nonFinalString, str -> {
			calls[0]++;
			throw new RuntimeException();
		}, ex -> calls[1]++);

		// Assert
		assertThat(calls[0], is(1));
		assertThat(calls[1], is(1));
		assertThat(calls[2], is(0));
	}

	@Test
	public void testSafeTryExecuteBiFunction() {
		// Arrange
		String nonFinalStringA = "";
		String nonFinalStringB = "";
		int[] calls = { 0, 0 };

		// Act
		SafeTry.executeNonFinal(nonFinalStringA, nonFinalStringB, (a, b) -> {
			calls[0]++;
			throw new RuntimeException();
		});

		// Assert
		assertThat(calls[0], is(1));
		assertThat(calls[1], is(0));
	}

	@Test
	public void testSafeTryExecuteBiFunctionCatchSupplier() {
		// Arrange
		String nonFinalStringA = "";
		String nonFinalStringB = "";
		int[] calls = { 0, 0, 0 };

		// Act
		calls[2] = SafeTry.executeNonFinal(nonFinalStringA, nonFinalStringB, (a, b) -> {
			calls[0]++;
			throw new RuntimeException();
		}, ex -> calls[1]++);

		// Assert
		assertThat(calls[0], is(1));
		assertThat(calls[1], is(1));
		assertThat(calls[2], is(0));
	}


	@Test
	public void testSafeTryExecuteTriFunction() {
		// Arrange
		String nonFinalStringA = "";
		String nonFinalStringB = "";
		String nonFinalStringC = "";
		int[] calls = { 0, 0 };

		// Act
		SafeTry.executeNonFinal(nonFinalStringA, nonFinalStringB, nonFinalStringC, (a, b, c) -> {
			calls[0]++;
			throw new RuntimeException();
		});

		// Assert
		assertThat(calls[0], is(1));
		assertThat(calls[1], is(0));
	}

	@Test
	public void testSafeTryExecuteTriFunctionCatchSupplier() {
		// Arrange
		String nonFinalStringA = "";
		String nonFinalStringB = "";
		String nonFinalStringC = "";
		int[] calls = { 0, 0, 0 };

		// Act
		calls[2] = SafeTry.executeNonFinal(nonFinalStringA, nonFinalStringB, nonFinalStringC, (a, b, c) -> {
			calls[0]++;
			throw new RuntimeException();
		}, ex -> calls[1]++);

		// Assert
		assertThat(calls[0], is(1));
		assertThat(calls[1], is(1));
		assertThat(calls[2], is(0));
	}

	@Test
	public void testSafeTryOrThrowRuntimeException() {
		// Arrange
		int[] calls = { 0 };

		// Act
		Runnable executable = () ->
			SafeTry.throwAsRuntimeException((ThrowingRunnable<?>) () -> {
				calls[0]++;
				throw new SQLException();
			});

		// Assert
		assertThrows(RuntimeException.class, executable::run);
		assertThat(calls[0], is(1));
	}

	@Test
	public void testSafeTryOrThrowRuntimeExceptionNoError() {
		// Arrange
		int[] calls = { 0 };

		// Act
		SafeTry.throwAsRuntimeException((ThrowingRunnable<?>) () -> calls[0]++);

		// Assert
		assertThat(calls[0], is(1));
	}

	@Test
	public void testSafeTryOrThrowRuntimeSupplierException() {
		// Arrange
		int[] calls = { 0 };

		// Act
		Runnable executable = () ->
			SafeTry.throwAsRuntimeException((ThrowingSupplier<?, ?>) () -> {
				calls[0]++;
				throw new SQLException();
			});

		// Assert
		assertThrows(RuntimeException.class, executable::run);
		assertThat(calls[0], is(1));
	}

	@Test
	public void testSafeTryOrThrowRuntimeSupplierExceptionNoError() {
		// Arrange
		int[] calls = { 0 };

		// Act
		SafeTry.throwAsRuntimeException((ThrowingSupplier<?, ?>) () -> calls[0]++);

		// Assert
		assertThat(calls[0], is(1));
	}
}
