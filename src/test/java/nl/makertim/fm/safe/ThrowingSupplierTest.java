package nl.makertim.fm.safe;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ThrowingSupplierTest {

	@Test
	public void testThrowingSupplierToFunction() throws Throwable {
		// Arrange
		ThrowingSupplier<Character, Throwable> supplier = () -> ' ';
		ThrowingFunction<String, Character, Throwable> function = supplier.toFunction();

		// Act
		char space = function.apply("TEST");

		// Assert
		assertThat(space, is(' '));
	}
}
