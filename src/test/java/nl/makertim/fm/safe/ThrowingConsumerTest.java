package nl.makertim.fm.safe;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ThrowingConsumerTest {

	@Test
	public void testAndThen() throws Throwable {
		// Arrange
		int[] calls = { 1, 0, 0 };
		int magicValue = 1337;
		int magicValueAndThen = 420;

		// Act
		ThrowingConsumer<Integer, ?> consumer = x -> calls[calls[0]++] = magicValue;
		consumer = consumer.andThen(x -> calls[calls[0]++] = magicValueAndThen);
		consumer.accept(0);

		// Assert
		assertThat(calls[1], is(magicValue));
		assertThat(calls[2], is(magicValueAndThen));
	}
}
