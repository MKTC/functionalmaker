package nl.makertim.fm.safe;

import nl.makertim.fm.core.BiSet;
import nl.makertim.fm.core.TriSet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ToNormalTest {

	@Test
	public void testThrowingBiConsumerSafe() {
		// Arrange
		ThrowingBiConsumer<Integer, Integer, ?> lambda = (x, y) -> {};

		// Act
		var javaLambda = lambda.asConsumer();

		// Assert
		javaLambda.accept(1, 2);
	}

	@Test
	public void testThrowingBiFunctionSafe() {
		// Arrange
		ThrowingBiFunction<Integer, Integer, Integer, ?> lambda = (x, y) -> x * y;

		// Act
		var javaLambda = lambda.asBiFunction();

		// Assert
		Assertions.assertEquals(javaLambda.apply(2, 3), 6);
	}

	@Test
	public void testThrowingBiFunctionBiSafe() {
		// Arrange
		ThrowingBiFunctionBi<Integer, Integer, Integer, Integer, ?> lambda = BiSet::new;

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertEquals(javaLambda.apply(1, 2), new BiSet<>(1, 2));
	}

	@Test
	public void testThrowingBiFunctionTriSafe() {
		// Arrange
		ThrowingBiFunctionTri<Integer, Integer, Integer, Integer, Integer, ?> lambda = (x, y) -> new TriSet<>(x, y, x * y);

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertEquals(javaLambda.apply(2, 3), new TriSet<>(2, 3, 6));
	}

	@Test
	public void testThrowingBiSupplierSafe() {
		// Arrange
		ThrowingBiSupplier<Integer, Integer, ?> lambda = () -> new BiSet<>(1, 2);

		// Act
		var javaLambda = lambda.asSupplier();

		// Assert
		Assertions.assertEquals(javaLambda.get(), new BiSet<>(1, 2));
	}

	@Test
	public void testThrowingConsumerSafe() {
		// Arrange
		ThrowingConsumer<Integer, ?> lambda = (x) -> {};

		// Act
		var javaLambda = lambda.asConsumer();

		// Assert
		javaLambda.accept(1);
	}

	@Test
	public void testThrowingFunctionSafe() {
		// Arrange
		ThrowingFunction<Integer, Integer, ?> lambda = (x) -> x;

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertEquals(javaLambda.apply(1), 1);
	}

	@Test
	public void testThrowingFunctionBiSafe() {
		// Arrange
		ThrowingFunctionBi<Integer, Integer, Integer, ?> lambda = (x) -> new BiSet<>(x, x);

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertEquals(javaLambda.apply(1), new BiSet<>(1, 1));
	}

	@Test
	public void testThrowingFunctionTriSafe() {
		// Arrange
		ThrowingFunctionTri<Integer, Integer, Integer, Integer, ?> lambda = (x) -> new TriSet<>(x, x, x);

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertEquals(javaLambda.apply(1), new TriSet<>(1, 1, 1));
	}

	@Test
	public void testThrowingRunnableSafe() {
		// Arrange
		ThrowingRunnable<?> lambda = () -> {};

		// Act
		var javaLambda = lambda.asRunnable();

		// Assert
		javaLambda.run();
	}

	@Test
	public void testThrowingSupplierSafe() {
		// Arrange
		ThrowingSupplier<Integer, ?> lambda = () -> 1;

		// Act
		var javaLambda = lambda.asSupplier();

		// Assert
		Assertions.assertEquals(javaLambda.get(), 1);
	}

	@Test
	public void testThrowingTriConsumerSafe() {
		// Arrange
		ThrowingTriConsumer<Integer, Integer, Integer, ?> lambda = (x, y, z) -> {};

		// Act
		var javaLambda = lambda.asConsumer();

		// Assert
		javaLambda.accept(new TriSet<>(1, 2, 3));
	}

	@Test
	public void testThrowingTriFunctionSafe() {
		// Arrange
		ThrowingTriFunction<Integer, Integer, Integer, Integer, ?> lambda = (x, y, z) -> x * y * z;

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertEquals(javaLambda.apply(new TriSet<>(2, 3, 4)), 24);
	}

	@Test
	public void testThrowingTriFunctionBiSafe() {
		// Arrange
		ThrowingTriFunctionBi<Integer, Integer, Integer, Integer, Integer, ?> lambda = (x, y, z) -> new BiSet<>(x, y * z);

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertEquals(javaLambda.apply(new TriSet<>(1, 2, 3)), new BiSet<>(1, 6));
	}

	@Test
	public void testThrowingTriFunctionTriSafe() {
		// Arrange
		ThrowingTriFunctionTri<Integer, Integer, Integer, Integer, Integer, Integer, ?> lambda = TriSet::new;

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertEquals(javaLambda.apply(new TriSet<>(2, 3, 4)), new TriSet<>(2, 3, 4));
	}

	@Test
	public void testThrowingTriSupplierSafe() {
		// Arrange
		ThrowingTriSupplier<Integer, Integer, Integer, ?> lambda = () -> new TriSet<>(1, 2, 3);

		// Act
		var javaLambda = lambda.asSupplier();

		// Assert
		Assertions.assertEquals(javaLambda.get(), new TriSet<>(1, 2, 3));
	}

	@Test
	public void testThrowingBiConsumerFail() {
		// Arrange
		ThrowingBiConsumer<Integer, Integer, ?> lambda = (x, y) -> fail();

		// Act
		var javaLambda = lambda.asConsumer();

		// Assert
		Assertions.assertThrows(RuntimeException.class, () -> javaLambda.accept(1, 2));
	}

	@Test
	public void testThrowingBiFunctionFail() {
		// Arrange
		ThrowingBiFunction<Integer, Integer, Integer, ?> lambda = (x, y) -> x * y * fail1();

		// Act
		var javaLambda = lambda.asBiFunction();

		// Assert
		Assertions.assertThrows(RuntimeException.class, () -> javaLambda.apply(2, 3));
	}

	@Test
	public void testThrowingBiFunctionBiFail() {
		// Arrange
		ThrowingBiFunctionBi<Integer, Integer, Integer, Integer, ?> lambda = (x, y) -> new BiSet<>(x, y * fail1());

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertThrows(RuntimeException.class, () -> javaLambda.apply(1, 2));
	}

	@Test
	public void testThrowingBiFunctionTriFail() {
		// Arrange
		ThrowingBiFunctionTri<Integer, Integer, Integer, Integer, Integer, ?> lambda = (x, y) -> new TriSet<>(x, y, x * y * fail1());

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertThrows(RuntimeException.class, () -> javaLambda.apply(2, 3));
	}

	@Test
	public void testThrowingBiSupplierFail() {
		// Arrange
		ThrowingBiSupplier<Integer, Integer, ?> lambda = () -> new BiSet<>(1, 2 * fail1());

		// Act
		var javaLambda = lambda.asSupplier();

		// Assert
		Assertions.assertThrows(RuntimeException.class, javaLambda::get);
	}

	@Test
	public void testThrowingConsumerFail() {
		// Arrange
		ThrowingConsumer<Integer, ?> lambda = (x) -> fail1();

		// Act
		var javaLambda = lambda.asConsumer();

		// Assert
		Assertions.assertThrows(RuntimeException.class, () -> javaLambda.accept(1));
	}

	@Test
	public void testThrowingFunctionFail() {
		// Arrange
		ThrowingFunction<Integer, Integer, ?> lambda = (x) -> fail1();

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertThrows(RuntimeException.class, () -> javaLambda.apply(1));
	}

	@Test
	public void testThrowingFunctionBiFail() {
		// Arrange
		ThrowingFunctionBi<Integer, Integer, Integer, ?> lambda = (x) -> new BiSet<>(x, x * fail1());

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertThrows(RuntimeException.class, () -> javaLambda.apply(1));
	}

	@Test
	public void testThrowingFunctionTriFail() {
		// Arrange
		ThrowingFunctionTri<Integer, Integer, Integer, Integer, ?> lambda = (x) -> new TriSet<>(x, x, x * fail1());

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertThrows(RuntimeException.class, () -> javaLambda.apply(1));
	}

	@Test
	public void testThrowingRunnableFail() {
		// Arrange
		ThrowingRunnable<?> lambda = this::fail;

		// Act
		var javaLambda = lambda.asRunnable();

		// Assert
		Assertions.assertThrows(RuntimeException.class, javaLambda::run);
	}

	@Test
	public void testThrowingSupplierFail() {
		// Arrange
		ThrowingSupplier<Integer, ?> lambda = this::fail1;

		// Act
		var javaLambda = lambda.asSupplier();

		// Assert
		Assertions.assertThrows(RuntimeException.class, javaLambda::get);
	}

	@Test
	public void testThrowingTriConsumerFail() {
		// Arrange
		ThrowingTriConsumer<Integer, Integer, Integer, ?> lambda = (x, y, z) -> fail();

		// Act
		var javaLambda = lambda.asConsumer();

		// Assert
		Assertions.assertThrows(RuntimeException.class, () -> javaLambda.accept(new TriSet<>(1, 2, 3)));
	}

	@Test
	public void testThrowingTriFunctionFail() {
		// Arrange
		ThrowingTriFunction<Integer, Integer, Integer, Integer, ?> lambda = (x, y, z) -> x * y * z * fail1();

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertThrows(RuntimeException.class, () -> javaLambda.apply(new TriSet<>(2, 3, 4)));
	}

	@Test
	public void testThrowingTriFunctionBiFail() {
		// Arrange
		ThrowingTriFunctionBi<Integer, Integer, Integer, Integer, Integer, ?> lambda = (x, y, z) -> new BiSet<>(x, y * z * fail1());

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertThrows(RuntimeException.class, () -> javaLambda.apply(new TriSet<>(1, 2, 3)));
	}

	@Test
	public void testThrowingTriFunctionTriFail() {
		// Arrange
		ThrowingTriFunctionTri<Integer, Integer, Integer, Integer, Integer, Integer, ?> lambda =
			(x, y, z) -> new TriSet<>(x, y, z * fail1());

		// Act
		var javaLambda = lambda.asFunction();

		// Assert
		Assertions.assertThrows(RuntimeException.class, () -> javaLambda.apply(new TriSet<>(2, 3, 4)));
	}

	@Test
	public void testThrowingTriSupplierFail() {
		// Arrange
		ThrowingTriSupplier<Integer, Integer, Integer, ?> lambda = () -> new TriSet<>(1, 2, 3 * fail1());

		// Act
		var javaLambda = lambda.asSupplier();

		// Assert
		Assertions.assertThrows(RuntimeException.class, javaLambda::get);
	}

	private void fail() {
		throw new RuntimeException("This should fail");
	}

	private int fail1() {
		throw new RuntimeException("This should fail");
	}
}
