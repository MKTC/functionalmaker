package nl.makertim.fm.database.core;

import nl.makertim.fm.core.BiSet;
import nl.makertim.fm.safe.ThrowingRunnable;
import nl.makertim.fm.safe.ThrowingSupplier;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.*;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SafeResultSetExecuteTest {

	@Test
	public void callAllMethodsTest() {
		// Arrange
		var resultSet = new SafeResultSetExecute(true);

		var lookup = new HashMap<>();
		lookup.put(String.class, "a");
		lookup.put(byte[].class, new byte[0]);
		lookup.put(int.class, 1);
		lookup.put(long.class, 1L);
		lookup.put(short.class, (short) 1);
		lookup.put(boolean.class, true);
		lookup.put(double.class, 1D);
		lookup.put(float.class, 1F);
		lookup.put(byte.class, (byte) 0);
		lookup.put(Map.class, new HashMap<>());
		lookup.put(Class.class, Class.class);
		lookup.put(Calendar.class, new GregorianCalendar());
		lookup.put(Time.class, new Time(0));
		lookup.put(Reader.class, new StringReader(""));
		lookup.put(SQLXML.class, null);
		lookup.put(BigDecimal.class, new BigDecimal(0));
		lookup.put(Ref.class, null);
		lookup.put(Timestamp.class, new Timestamp(0));
		lookup.put(InputStream.class, null);
		lookup.put(Date.class, new Date(0));
		lookup.put(Array.class, null);
		lookup.put(Object.class, new Object());
		lookup.put(RowId.class, null);
		lookup.put(Clob.class, null);
		lookup.put(NClob.class, null);
		lookup.put(Blob.class, null);
		lookup.put(SQLType.class, JDBCType.BINARY);

		// Act
		var methods = Arrays.stream((resultSet.getClass().getMethods()))
			.filter(method -> !method.getName().equals("wait"))
			.filter(method -> !method.getName().contains("notify"))
			.filter(method -> !method.getName().contains("getClass"))
			.map(method -> {
				var args = Arrays.stream(method.getParameterTypes())
					.peek(x -> {
						if (!lookup.containsKey(x))
							throw new RuntimeException("MISSING lookup ~ " + x.toString() + "\n" + method);
					})
					.map(lookup::get)
					.toArray();

				return new BiSet<>(method, (ThrowingSupplier<?, ?>) (() -> method.invoke(resultSet, args)));
			})
			.toArray(BiSet[]::new);
		ArrayUtils.add(methods, new BiSet<>("close", (ThrowingRunnable<?>) (resultSet::close)));
		ArrayUtils.add(methods, new BiSet<>("clearWarnings", (ThrowingRunnable<?>) (resultSet::clearWarnings)));
		ArrayUtils.add(methods, new BiSet<>("beforeFirst", (ThrowingRunnable<?>) (resultSet::beforeFirst)));
		ArrayUtils.add(methods, new BiSet<>("afterLast", (ThrowingRunnable<?>) (resultSet::afterLast)));
		ArrayUtils.add(methods, new BiSet<>("insertRow", (ThrowingRunnable<?>) (resultSet::insertRow)));
		ArrayUtils.add(methods, new BiSet<>("updateRow", (ThrowingRunnable<?>) (resultSet::updateRow)));
		ArrayUtils.add(methods, new BiSet<>("deleteRow", (ThrowingRunnable<?>) (resultSet::deleteRow)));
		ArrayUtils.add(methods, new BiSet<>("refreshRow", (ThrowingRunnable<?>) (resultSet::refreshRow)));
		ArrayUtils.add(methods, new BiSet<>("cancelRowUpdates", (ThrowingRunnable<?>) (resultSet::cancelRowUpdates)));
		ArrayUtils.add(methods, new BiSet<>("moveToInsertRow", (ThrowingRunnable<?>) (resultSet::moveToInsertRow)));
		ArrayUtils.add(methods, new BiSet<>("moveToCurrentRow", (ThrowingRunnable<?>) (resultSet::moveToCurrentRow)));

		// Assert
		for (BiSet<?, ?> methodContext : methods) {
			assertThrows(
				NotImplementedException.class,
				() -> {
					if (methodContext.getT2() instanceof ThrowingSupplier) {
						try {
							Object obj = ((ThrowingSupplier<?, ?>) methodContext.getT2()).get();
							if (Boolean.TRUE.equals(obj)) {
								throw new NotImplementedException();
							} else {
								throw new InvocationTargetException(null);
							}
						} catch (InvocationTargetException ex) {
							throw ex.getTargetException();
						}
					} else {
						((ThrowingRunnable<?>) methodContext.getT2()).run();
					}
				},
				methodContext.getT1().toString()
			);
		}
	}

	@Test
	public void getWrongBoolean() {
		// Arrange
		var resultSet = new SafeResultSetExecute(true);

		// Act
		var result = resultSet.getBoolean(0);

		// Assert
		assertThat(result, is(false));
	}
}
