package nl.makertim.fm.database;

import nl.makertim.fm.core.Property;
import nl.makertim.fm.database.config.DatabaseConfig;
import nl.makertim.fm.database.core.SafeResultSet;
import nl.makertim.fm.database.oop.Dialect;
import nl.makertim.fm.database.oop.dialects.SQLiteDialect;
import nl.makertim.fm.database.oop.query.SelectQuery;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RaidDatabaseTest {
	public static final File file1 = new File("./raid1.sqlite3");
	public static final String SQLITE_JDBC1 = "jdbc:sqlite:" + file1.getPath();
	public static final File file2 = new File("./raid2.sqlite3");
	public static final String SQLITE_JDBC2 = "jdbc:sqlite:" + file2.getPath();
	private Database database1;
	private Database database2;

	@BeforeAll
	@AfterAll
	public static void cleanup() {
		file1.delete();
		file1.deleteOnExit();
		file2.delete();
		file2.deleteOnExit();
	}

	@AfterEach
	public void cleanupAfter() {
		// After
		if (database1 != null) {
			assertThat(database1.close(), is(true));
			database1 = null;
			file1.delete();
		}
		if (database2 != null) {
			assertThat(database2.close(), is(true));
			database2 = null;
			file2.delete();
		}
	}

	@Test
	public void testRaidConstruct() {
		// Arrange
		database1 = Database.createConnection()
			.setupSettingsManually(settings -> settings.setJdbcUrl(SQLITE_JDBC1))
			.open();
		database2 = Database.createConnection()
			.setupSettingsManually(settings -> settings.setJdbcUrl(SQLITE_JDBC2))
			.open();
		var raidDatabase = new RaidDatabase(database1, database2);

		// Act
		raidDatabase.open();

		// Assert
		assertThat(
			raidDatabase.getSingle(new SelectQuery(1), rs -> rs.getInt(1)),
			is(1)
		);
	}

	@Test
	public void testRaidAsManyThings() {
		// Arrange
		final var magicSecretNumber = 1337;
		database1 = Database.createConnection()
			.setupSettingsManually(settings -> settings.setJdbcUrl(SQLITE_JDBC1))
			.open();
		database2 = Database.createConnection()
			.setupSettingsManually(settings -> settings.setJdbcUrl(SQLITE_JDBC2))
			.open();
		var raidDatabase = new RaidDatabase(database1, database2);
		var callsConnectionWithResultProperty = Property.of(0);
		var callsGetConnectionWithResult = Property.of(0);
		var query = new SelectQuery(1);
		var preparedQuery = query.build(raidDatabase.getDialect());

		// Act
		raidDatabase.open();
		raidDatabase.getConnectionExecute(connection -> addToProperty(callsConnectionWithResultProperty));
		var resultGetConnectionWithResult = raidDatabase.getConnectionWithResult(connection -> {
			addToProperty(callsGetConnectionWithResult);
			return magicSecretNumber;
		});
		var internal = raidDatabase.internal();
		raidDatabase.setupSettingsManually(x -> {});
		raidDatabase.close();
		raidDatabase.open();
		var multiple = raidDatabase.getMultiple(query, rs -> rs.getInt(1));
		var multiplePrepared = raidDatabase.getMultiple(preparedQuery, rs -> rs.getInt(1));
		var multipleMap = raidDatabase.getMultiple(query, new HashMap<>(), rs -> rs.getInt(1));
		var multiplePreparedMap = raidDatabase.getMultiple(preparedQuery, new HashMap<>(), rs -> rs.getInt(1));
		var single = raidDatabase.getSingle(query, rs -> rs.getInt(1));
		var singlePrepared = raidDatabase.getSingle(preparedQuery, rs -> rs.getInt(1));
		var singleMap = raidDatabase.getSingle(query, new HashMap<>(), rs -> rs.getInt(1));
		var singlePreparedMap = raidDatabase.getSingle(preparedQuery, new HashMap<>(), rs -> rs.getInt(1));
		var newTable = raidDatabase.execute(query);
		var newTableMap = raidDatabase.execute(query, new HashMap<>());
		var newTablePrepared = raidDatabase.execute(preparedQuery);
		var newTablePreparedMap = raidDatabase.execute(preparedQuery, new HashMap<>());
		var list = raidDatabase.resultToList(preparedQuery.getSQL(), rs -> rs.getInt(1));
		var listPrepare = raidDatabase.resultToList(preparedQuery.getSQL(), ps -> {}, rs -> rs.getInt(1));
		var listPrepareSupply = raidDatabase.resultToList(preparedQuery.getSQL(), ps -> {}, rs -> rs.getInt(1), LinkedList::new);
		var singleRaw = raidDatabase.executeSingleResultQuery(preparedQuery.getSQL(), rs -> rs.getInt(1));
		var singleRawPrepared = raidDatabase.executeSingleResultQuery(preparedQuery.getSQL(), ps -> {}, rs -> rs.getInt(1));
		var resultRaw = raidDatabase.executeResultQuery(preparedQuery.getSQL(), SafeResultSet::next);
		var resultRawPrepared = raidDatabase.executeResultQuery(preparedQuery.getSQL(), ps -> {}, SafeResultSet::next);
		var execute = raidDatabase.executeRawQuery(preparedQuery.getSQL());
		var executePrepared = raidDatabase.executeRawQuery(preparedQuery.getSQL(), ps -> {});

		raidDatabase.getConnectionExecute(connection -> {
			var _multiple = raidDatabase.getMultiple(connection, query, rs -> rs.getInt(1));
			var _multiplePrepared = raidDatabase.getMultiple(connection, preparedQuery, rs -> rs.getInt(1));
			var _multipleMap = raidDatabase.getMultiple(connection, query, new HashMap<>(), rs -> rs.getInt(1));
			var _multiplePreparedMap = raidDatabase.getMultiple(connection, preparedQuery, new HashMap<>(), rs -> rs.getInt(1));
			var _single = raidDatabase.getSingle(connection, query, rs -> rs.getInt(1));
			var _singlePrepared = raidDatabase.getSingle(connection, preparedQuery, rs -> rs.getInt(1));
			var _singleMap = raidDatabase.getSingle(connection, query, new HashMap<>(), rs -> rs.getInt(1));
			var _singlePreparedMap = raidDatabase.getSingle(connection, preparedQuery, new HashMap<>(), rs -> rs.getInt(1));
			var _newTable = raidDatabase.execute(connection, query);
			var _newTableMap = raidDatabase.execute(connection, query, new HashMap<>());
			var _newTablePrepared = raidDatabase.execute(connection, preparedQuery);
			var _newTablePreparedMap = raidDatabase.execute(connection, preparedQuery, new HashMap<>());
			var _list = raidDatabase.resultToList(connection, preparedQuery.getSQL(), rs -> rs.getInt(1));
			var _listPrepare = raidDatabase.resultToList(connection, preparedQuery.getSQL(), ps -> {}, rs -> rs.getInt(1));
			var _listPrepareSupply = raidDatabase.resultToList(connection, preparedQuery.getSQL(), ps -> {}, rs -> rs.getInt(1), LinkedList::new);
			var _singleRaw = raidDatabase.executeSingleResultQuery(connection, preparedQuery.getSQL(), rs -> rs.getInt(1));
			var _singleRawPrepared = raidDatabase.executeSingleResultQuery(connection, preparedQuery.getSQL(), ps -> {}, rs -> rs.getInt(1));
			var _resultRaw = raidDatabase.executeResultQuery(connection, preparedQuery.getSQL(), SafeResultSet::next);
			var _resultRawPrepared = raidDatabase.executeResultQuery(connection, preparedQuery.getSQL(), ps -> {}, SafeResultSet::next);
			var _execute = raidDatabase.executeRawQuery(connection, preparedQuery.getSQL());
			var _executePrepared = raidDatabase.executeRawQuery(connection, preparedQuery.getSQL(), ps -> {});

			assertThat(_multiple.size(), is(1));
			assertThat(_multiplePrepared.size(), is(1));
			assertThat(_multipleMap.size(), is(1));
			assertThat(_multiplePreparedMap.size(), is(1));
			assertThat(_single, is(1));
			assertThat(_singlePrepared, is(1));
			assertThat(_singleMap, is(1));
			assertThat(_singlePreparedMap, is(1));
			assertTrue(_newTable);
			assertTrue(_newTableMap);
			assertTrue(_newTablePrepared);
			assertTrue(_newTablePreparedMap);
			assertThat(_list.size(), is(1));
			assertThat(_listPrepare.size(), is(1));
			assertThat(_listPrepareSupply.size(), is(1));
			assertThat(_singleRaw, is(1));
			assertThat(_singleRawPrepared, is(1));
			assertThat(_resultRaw, is(true));
			assertThat(_resultRawPrepared, is(true));
			assertThat(_execute, is(true));
			assertThat(_executePrepared, is(true));
		});


		// Assert
		assertThat(raidDatabase.getDialect(), equalTo(Dialect.getDialect(SQLiteDialect.class)));
		assertThat(callsConnectionWithResultProperty.get(), is(raidDatabase.accessDatabasesIndividual().length));
		assertThat(callsGetConnectionWithResult.get(), is(raidDatabase.accessDatabasesIndividual().length));
		assertThat(resultGetConnectionWithResult, is(magicSecretNumber));
		assertThat(internal, instanceOf(DatabaseConfig.class));
		assertThat(multiple.size(), is(1));
		assertThat(multiplePrepared.size(), is(1));
		assertThat(multipleMap.size(), is(1));
		assertThat(multiplePreparedMap.size(), is(1));
		assertThat(single, is(1));
		assertThat(singlePrepared, is(1));
		assertThat(singleMap, is(1));
		assertThat(singlePreparedMap, is(1));
		assertTrue(newTable);
		assertTrue(newTableMap);
		assertTrue(newTablePrepared);
		assertTrue(newTablePreparedMap);
		assertThat(list.size(), is(1));
		assertThat(listPrepare.size(), is(1));
		assertThat(listPrepareSupply.size(), is(1));
		assertThat(singleRaw, is(1));
		assertThat(singleRawPrepared, is(1));
		assertThat(resultRaw, is(true));
		assertThat(resultRawPrepared, is(true));
		assertThat(execute, is(true));
		assertThat(executePrepared, is(true));
	}

	private void addToProperty(Property<Integer> property) {
		property.set(property.get() + 1);
	}

}
