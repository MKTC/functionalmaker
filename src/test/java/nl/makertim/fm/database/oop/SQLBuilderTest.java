package nl.makertim.fm.database.oop;

import nl.makertim.fm.database.oop.constructs.Column;
import nl.makertim.fm.database.oop.constructs.Comparison;
import nl.makertim.fm.database.oop.constructs.ConstantColumn;
import nl.makertim.fm.database.oop.dialects.MySQLBuilder;
import nl.makertim.fm.database.oop.query.SelectQuery;
import nl.makertim.fm.safe.ThrowingRunnable;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SQLBuilderTest {

	@Test
	public void testBuildComparison() throws Exception {
		// Arrange
		SQLBuilder builder = new MySQLBuilder(new SelectQuery("1"));
		var method = SQLBuilder.class.getDeclaredMethod("buildComparison", Comparison.class);

		// Act
		var sql = (String) method.invoke(builder, (Object) null);

		// Assert
		assertThat(sql, isEmptyString());
	}

	@Test
	public void testEscapeColumn() throws Exception {
		// Arrange
		SQLBuilder builder = new MySQLBuilder(new SelectQuery("1"));
		var method = SQLBuilder.class.getDeclaredMethod("escapeColumn", String.class, String.class);
		var inBetween = "WE ARE NUMBER ONE";

		// Act
		var sql = (String) method.invoke(builder, "", inBetween);

		// Assert
		assertThat(sql, isEmptyString());
	}

	@Test
	public void testUnknownColumn() throws Exception {
		// Arrange
		SQLBuilder builder = new MySQLBuilder(new SelectQuery("1"));
		var method = SQLBuilder.class.getDeclaredMethod("buildColumn", Column.class);

		var abstractColumn = new ConstantColumn(1) {
		};

		// Act
		ThrowingRunnable<?> action = () -> method.invoke(builder, abstractColumn);

		// Assert
		assertThrows(RuntimeException.class, () -> {
			try {
				action.run();
				throw new InvocationTargetException(null);
			} catch (InvocationTargetException ex) {
				throw ex.getTargetException();
			}
		});
	}

	@Test
	public void testCachingOfQuery() {
		// Arrange
		SelectQuery query = new SelectQuery("1");
		SQLBuilder builder = new MySQLBuilder(query);

		// Act
		var preparedQuery = builder.build();
		var preparedQueryCached = builder.build();
		query.removeCache(builder.getDialect());
		var preparedQueryRebuild = builder.build();

		// Assert
		assertThat(preparedQuery, is(preparedQueryCached));
		assertThat(preparedQueryCached, is(preparedQuery));
		assertThat(preparedQuery, not(is(preparedQueryRebuild)));
		assertThat(preparedQueryRebuild, not(is(preparedQuery)));
		assertThat(preparedQueryCached, not(is(preparedQueryRebuild)));
		assertThat(preparedQueryRebuild, not(is(preparedQueryCached)));
	}
}
