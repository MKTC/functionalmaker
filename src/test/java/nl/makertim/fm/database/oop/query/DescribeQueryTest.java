package nl.makertim.fm.database.oop.query;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class DescribeQueryTest {

	@Test
	public void constructorDescribeTest() {
		// Arrange
		var tableName = "message";

		// Act
		var describeQuery = new DescribeQuery(tableName);

		// Assert
		assertThat(describeQuery.table, is(tableName));
	}

	@Test
	public void staticDescribeTest() {
		// Arrange
		var tableName = "message";

		// Act
		var describeQuery = DescribeQuery.describe(tableName);

		// Assert
		assertThat(describeQuery.table, is(tableName));
	}

	@Test
	public void describeCloneTest() {
		// Arrange
		var tableName = "message";

		// Act
		var describeQuery = DescribeQuery.describe(tableName);
		var describeQueryClone = describeQuery.clone();

		// Assert
		assertThat(describeQuery, is(describeQueryClone));
		assertThat(describeQueryClone, is(describeQuery));
	}

	@Test
	public void describeInternalTest() {
		// Arrange
		var tableName = "message";

		// Act
		var describeQuery = DescribeQuery.describe(tableName);
		var internal = describeQuery.internal();

		// Assert
		assertThat(internal.getTable(), is(tableName));
	}
}
