package nl.makertim.fm.database.oop.dialects;

import nl.makertim.fm.database.oop.Dialect;
import nl.makertim.fm.database.oop.constructs.*;
import nl.makertim.fm.database.oop.query.SelectQuery;
import org.junit.jupiter.api.Test;

import static nl.makertim.fm.database.oop.constructs.Condition.*;
import static nl.makertim.fm.database.oop.constructs.Join.full;
import static nl.makertim.fm.database.oop.constructs.Join.inner;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.IsNot.not;

public class MySQLBuildSelectTest {

	@Test
	public void selectOne() {
		// Arrange
		var select = new SelectQuery(1);

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString("SELECT"));
		assertThat(sql, containsString(" 1"));
	}

	@Test
	public void selectAllFromNode() {
		// Arrange
		var select = SelectQuery.selectFrom("node");

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString("SELECT"));
		assertThat(sql, containsString(".*"));
		assertThat(sql, containsString("FROM `node`"));
	}

	@Test
	public void selectAllAndVarFromNode() {
		// Arrange
		var varColumnName = ":var";
		var select = SelectQuery.selectFrom("node")
			.addColumn(new VarColumn(varColumnName));

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString("SELECT"));
		assertThat(sql, containsString(".*"));
		assertThat(sql, containsString(varColumnName));
		assertThat(sql, containsString("FROM `node`"));
	}

	@Test
	public void selectFunction() {
		// Arrange
		var select = SelectQuery.selectFrom("node")
			.addColumn(Column.function("node", "pid", Function.COUNT, "jan"));

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString("SELECT"));
		assertThat(sql, containsString("COUNT"));
		assertThat(sql, containsString("pid"));
		assertThat(sql, containsString("jan"));
		assertThat(sql, containsString("FROM `node`"));
	}

	@Test
	public void selectAllFromNodeMinified() {
		// Arrange
		var select = SelectQuery.selectFrom("node");

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.minifiedSQL();

		// Assert
		assertThat(sql, not(containsString("  ")));
	}

	@Test
	public void selectAllFromNodeExplain() {
		// Arrange
		var select = SelectQuery.selectFrom("node");

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildExplainQuery(select);
		var sql = preparedQuery.minifiedSQL();

		// Assert
		assertThat(sql, containsString("EXPLAIN"));
	}

	@Test
	public void selectAllWithCustomCondition() {
		// Arrange
		var customWhere = "1=1";
		var select = SelectQuery.selectFrom("node")
			.addCondition(new ConditionCustom(sqlBuilder -> customWhere));

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString(customWhere));
	}

	@Test
	public void selectAllEqualsCondition() {
		// Arrange
		var column = "pid";
		var var = ":pid";
		var select = SelectQuery.selectFrom("node")
			.addCondition(Where.equalTo("node", column, var));

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString(column));
		assertThat(sql, containsString(var));
	}

	@Test
	public void selectAllEqualsMultipleCondition() {
		// Arrange
		var table = "node";
		var column = "pid";
		var var = ":pid";
		var select = SelectQuery.selectFrom(table)
			.addCondition(
				and(
					or(
						in(table, column, var),
						notEqualTo(table, column, var),
						between(table, column, var, var),
						biggerThan(table, column, var),
						smallerThan(table, column, var),
						biggerThanOrEqual(table, column, var),
						smallerThanOrEqual(table, column, var),
						like(table, column, var),
						between(table, column, var, var),
						isNull(table, column),
						isNotNull(table, column),
						custom(b -> "1=1"),
						inSubQuery(table, column, new SelectQuery(1))
					)
				)
			);

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString("OR"));
		assertThat(sql, containsString(column));
		assertThat(sql, containsString(var));
	}

	@Test
	public void selectNullTable() {
		// Arrange
		var table = "node";
		var columnNode = "nid";
		var select = SelectQuery.selectFrom(table)
			.addColumn(Column.field(null, columnNode));

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString(columnNode));
	}

	@Test
	public void selectAllInnerJoin() {
		// Arrange
		var table = "node";
		var columnNode = "nid";
		var tableFlood = "flood";
		var columnFlood = "fid";
		var select = SelectQuery.selectFrom(table)
			.join(inner(tableFlood, new FieldColumn(tableFlood, columnFlood), new FieldColumn(table, columnNode)));

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString(table));
		assertThat(sql, containsString(tableFlood));
		assertThat(sql, containsString(columnNode));
		assertThat(sql, containsString(columnFlood));
	}

	@Test
	public void selectAllFullJoin() {
		// Arrange
		var table = "node";
		var tableFlood = "flood";
		var select = SelectQuery.selectFrom(table)
			.join(full(tableFlood));

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString(table));
		assertThat(sql, containsString(tableFlood));
	}

	@Test
	public void selectWithRecursiveFunction() {
		// Arrange
		var table = "node";
		var columnNode = "nid";
		var function = Column.function(table, columnNode, Function.MAX, "test");
		var recursiveFunction = Column.recursiveFunction(Function.ROUND, function);
		var select = SelectQuery.selectFrom(table)
			.addColumn(recursiveFunction);

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString("ROUND"));
		assertThat(sql, containsString("MAX"));
		assertThat(sql, containsString(columnNode));
		assertThat(sql, containsString(table));
	}

	@Test
	public void selectAllGroupBy() {
		// Arrange
		var table = "node";
		var column = "nid";
		var select = SelectQuery.selectFrom(table)
			.groupBy(column);

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString(table));
		assertThat(sql, containsString("GROUP BY"));
		assertThat(sql, containsString(column));
	}

	@Test
	public void selectAllOrderBy() {
		// Arrange
		var table = "node";
		var column = "nid";
		var select = SelectQuery.selectFrom(table)
			.orderByAscending(column);

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString(table));
		assertThat(sql, containsString("ORDER BY"));
		assertThat(sql, containsString("ASC"));
		assertThat(sql, containsString(column));
	}

	@Test
	public void selectAllOrderByDesc() {
		// Arrange
		var table = "node";
		var column = "nid";
		var select = SelectQuery.selectFrom(table)
			.orderByDescending(column);

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString(table));
		assertThat(sql, containsString("ORDER BY"));
		assertThat(sql, containsString("DESC"));
		assertThat(sql, containsString(column));
	}

	@Test
	public void selectAllLimitAndOffset() {
		// Arrange
		var table = "node";
		var limit = 10;
		var offset = 3;
		var select = SelectQuery.selectFrom(table)
			.setLimit(limit)
			.setOffset(offset);

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class).buildQuery(select);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString("LIMIT"));
		assertThat(sql, containsString(table));
		assertThat(sql, containsString(String.valueOf(limit)));
		assertThat(sql, containsString(String.valueOf(offset)));
	}
}
