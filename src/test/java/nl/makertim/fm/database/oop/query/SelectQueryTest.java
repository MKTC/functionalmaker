package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.database.oop.constructs.*;
import org.junit.jupiter.api.Test;

import static nl.makertim.fm.database.oop.constructs.And.and;
import static nl.makertim.fm.database.oop.constructs.Function.*;
import static nl.makertim.fm.database.oop.constructs.JoinType.*;
import static nl.makertim.fm.database.oop.constructs.Or.or;
import static nl.makertim.fm.database.oop.constructs.WhereIn.whereIn;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItemInArray;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SelectQueryTest {

	@Test
	public void testSelectQuery() {
		// Arrange
		var userTable = "users";

		// Act
		var getUsersQuery = SelectQuery.selectFrom(userTable);

		// Assert
		assertThat(getUsersQuery.columns.size(), is(1));
		assertThat(getUsersQuery.columns.get(0), instanceOf(TableColumn.class));
		assertThat(getUsersQuery.from.size(), is(1));
		assertThat(getUsersQuery.from.stream().findFirst().orElse(null), is(userTable));
		assertThat(getUsersQuery.currentTableShorts.values().size(), is(1));
		assertThat(getUsersQuery.columns.get(0).getTable(), is(userTable));
	}

	@Test
	public void testSelectQueryWithColumns() {
		// Arrange
		var userTable = "users";
		var columns = new String[] { "id", "age", "username" };

		// Act
		var getUsersQuery = SelectQuery.selectFrom(userTable, columns);

		// Assert
		assertThat(getUsersQuery.columns.size(), is(3));
		assertThat(getUsersQuery.columns.get(0), instanceOf(FieldColumn.class));
		assertThat(getUsersQuery.columns.get(1), instanceOf(FieldColumn.class));
		assertThat(getUsersQuery.columns.get(2), instanceOf(FieldColumn.class));
		assertThat(((FieldColumn) getUsersQuery.columns.get(0)).getColumn(), is(columns[0]));
		assertThat(((FieldColumn) getUsersQuery.columns.get(1)).getColumn(), is(columns[1]));
		assertThat(((FieldColumn) getUsersQuery.columns.get(2)).getColumn(), is(columns[2]));
		assertThat(getUsersQuery.columns.get(0).getTable(), is(userTable));
		assertThat(getUsersQuery.columns.get(1).getTable(), is(userTable));
		assertThat(getUsersQuery.columns.get(2).getTable(), is(userTable));
		assertThat(getUsersQuery.from.size(), is(1));
		assertThat(getUsersQuery.from.stream().findFirst().orElse(null), is(userTable));
		assertThat(getUsersQuery.currentTableShorts.values().size(), is(1));
	}

	@Test
	public void testSelectQueryJoinOtherTable() {
		// Arrange
		var userTable = "users";
		var scoreTable = "scores";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.join(Join.full(scoreTable));

		// Assert
		assertThat(getUsersQuery.columns.size(), is(1));
		assertThat(getUsersQuery.columns.get(0), instanceOf(TableColumn.class));
		assertThat(getUsersQuery.from.size(), is(1));
		assertThat(getUsersQuery.from.stream().findFirst().orElse(null), is(userTable));
		assertThat(getUsersQuery.currentTableShorts.values().size(), is(2));
		assertThat(getUsersQuery.columns.get(0).getTable(), is(userTable));
	}

	@Test
	public void testSelectQueryJoinLeftOtherTable() {
		// Arrange
		var userTable = "users";
		var scoreTable = "scores";
		var scoreField = new FieldColumn("userId");
		var userField = new FieldColumn("id");

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.joinLeft(scoreTable, scoreField, userField);

		// Assert
		assertThat(getUsersQuery.join.size(), is(1));
		assertThat(getUsersQuery.join.get(0), instanceOf(Join.class));
		assertThat(getUsersQuery.join.get(0).getTable(), is(scoreTable));
		assertThat(getUsersQuery.join.get(0).getOnLeft(), is(scoreField));
		assertThat(getUsersQuery.join.get(0).getOnRight(), is(userField));
		assertThat(getUsersQuery.join.get(0).getType(), is(LEFT));
	}

	@Test
	public void testSelectQueryJoinRightOtherTable() {
		// Arrange
		var userTable = "users";
		var scoreTable = "scores";
		var scoreField = new FieldColumn("userId");
		var userField = new FieldColumn("id");

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.joinRight(scoreTable, scoreField, userField);

		// Assert
		assertThat(getUsersQuery.join.size(), is(1));
		assertThat(getUsersQuery.join.get(0), instanceOf(Join.class));
		assertThat(getUsersQuery.join.get(0).getTable(), is(scoreTable));
		assertThat(getUsersQuery.join.get(0).getOnLeft(), is(scoreField));
		assertThat(getUsersQuery.join.get(0).getOnRight(), is(userField));
		assertThat(getUsersQuery.join.get(0).getType(), is(RIGHT));
	}

	@Test
	public void testSelectQueryFullJoinOtherTable() {
		// Arrange
		var userTable = "users";
		var scoreTable = "scores";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.joinFull(scoreTable);

		// Assert
		assertThat(getUsersQuery.join.size(), is(1));
		assertThat(getUsersQuery.join.get(0), instanceOf(Join.class));
		assertThat(getUsersQuery.join.get(0).getTable(), is(scoreTable));
		assertThat(getUsersQuery.join.get(0).getType(), is(INNER));
	}

	@Test
	public void testSelectQueryInnerJoinOtherTable() {
		// Arrange
		var userTable = "users";
		var scoreTable = "scores";
		var scoreField = new FieldColumn("userId");
		var userField = new FieldColumn("id");

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.joinInner(scoreTable, scoreField, userField);

		// Assert
		assertThat(getUsersQuery.join.size(), is(1));
		assertThat(getUsersQuery.join.get(0), instanceOf(Join.class));
		assertThat(getUsersQuery.join.get(0).getTable(), is(scoreTable));
		assertThat(getUsersQuery.join.get(0).getType(), is(INNER));
	}

	@Test
	public void testSelectQueryJoinOtherTableSameFirstLetter() {
		// Arrange
		var userTable = "users";
		var upgradesTable = "upgrades";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.join(Join.full(upgradesTable));

		// Assert
		assertThat(getUsersQuery.columns.size(), is(1));
		assertThat(getUsersQuery.columns.get(0), instanceOf(TableColumn.class));
		assertThat(getUsersQuery.from.size(), is(1));
		assertThat(getUsersQuery.from.stream().findFirst().orElse(null), is(userTable));
		assertThat(getUsersQuery.currentTableShorts.values().size(), is(2));
		assertThat((getUsersQuery.columns.get(0)).getTable(), is(userTable));
	}

	@Test
	public void testSelectQueryWithConditionIn() {
		// Arrange
		var userTable = "users";
		var conditionColumn = "age";
		var conditionSet = new String[] { "monkey", "nut", "who" };

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.addCondition(whereIn(userTable, conditionColumn, conditionSet));

		// Assert
		assertThat(getUsersQuery.conditions.size(), is(1));
		assertThat(getUsersQuery.conditions.get(0), instanceOf(WhereIn.class));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getTable(), is(userTable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getColumn(), is(conditionColumn));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getVar(), is(conditionSet));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getComparison(), is(Comparison.IN));
	}

	@Test
	public void testSelectQueryWithConditionNotNull() {
		// Arrange
		var userTable = "users";
		var conditionColumn = "age";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.addCondition(Where.isNotNull(userTable, conditionColumn));

		// Assert
		assertThat(getUsersQuery.conditions.size(), is(1));
		assertThat(getUsersQuery.conditions.get(0), instanceOf(Where.class));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getTable(), is(userTable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getColumn(), is(conditionColumn));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getVar(), nullValue());
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getComparison(), is(Comparison.NOT));
	}

	@Test
	public void testSelectQueryWithConditionBiggerThanWitoutVariablePrefix() {
		// Arrange
		var userTable = "users";
		var conditionColumn = "age";
		var conditionVariable = ":age";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.addCondition(Where.biggerThan(userTable, conditionColumn, conditionVariable));

		// Assert
		assertThat(getUsersQuery.conditions.size(), is(1));
		assertThat(getUsersQuery.conditions.get(0), instanceOf(Where.class));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getTable(), is(userTable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getColumn(), is(conditionColumn));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getVar(), is(conditionVariable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getComparison(), is(Comparison.BIGGER_THEN));
	}

	@Test
	public void testSelectQueryWithConditionBiggerThanOrEqual() {
		// Arrange
		var userTable = "users";
		var conditionColumn = "age";
		var conditionVariable = ":age";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.addCondition(Where.biggerThanOrEqual(userTable, conditionColumn, conditionVariable));

		// Assert
		assertThat(getUsersQuery.conditions.size(), is(1));
		assertThat(getUsersQuery.conditions.get(0), instanceOf(Where.class));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getTable(), is(userTable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getColumn(), is(conditionColumn));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getVar(), is(conditionVariable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getComparison(), is(Comparison.BIGGER_OR_EQUAL_THEN));
	}

	@Test
	public void testSelectQueryWithConditionEqualTo() {
		// Arrange
		var userTable = "users";
		var conditionColumn = "age";
		var conditionVariable = ":age";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.addCondition(Where.equalTo(userTable, conditionColumn, conditionVariable));

		// Assert
		assertThat(getUsersQuery.conditions.size(), is(1));
		assertThat(getUsersQuery.conditions.get(0), instanceOf(Where.class));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getTable(), is(userTable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getColumn(), is(conditionColumn));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getVar(), is(conditionVariable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getComparison(), is(Comparison.EQUAL));
	}

	@Test
	public void testSelectQueryWithConditionNotEqualTo() {
		// Arrange
		var userTable = "users";
		var conditionColumn = "age";
		var conditionVariable = ":age";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.addCondition(Where.notEqualTo(userTable, conditionColumn, conditionVariable));

		// Assert
		assertThat(getUsersQuery.conditions.size(), is(1));
		assertThat(getUsersQuery.conditions.get(0), instanceOf(Where.class));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getTable(), is(userTable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getColumn(), is(conditionColumn));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getVar(), is(conditionVariable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getComparison(), is(Comparison.NOT));
	}

	@Test
	public void testSelectQueryWithConditionSmallerThanOrEqual() {
		// Arrange
		var userTable = "users";
		var conditionColumn = "age";
		var conditionVariable = ":age";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.addCondition(Where.smallerThanOrEqual(userTable, conditionColumn, conditionVariable));

		// Assert
		assertThat(getUsersQuery.conditions.size(), is(1));
		assertThat(getUsersQuery.conditions.get(0), instanceOf(Where.class));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getTable(), is(userTable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getColumn(), is(conditionColumn));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getVar(), is(conditionVariable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getComparison(), is(Comparison.SMALLER_OR_EQUAL_THEN));
	}

	@Test
	public void testSelectQueryWithConditionLike() {
		// Arrange
		var userTable = "users";
		var conditionColumn = "age";
		var conditionVariable = ":age";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.addCondition(Where.like(userTable, conditionColumn, conditionVariable));

		// Assert
		assertThat(getUsersQuery.conditions.size(), is(1));
		assertThat(getUsersQuery.conditions.get(0), instanceOf(Where.class));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getTable(), is(userTable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getColumn(), is(conditionColumn));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getVar(), is(conditionVariable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getComparison(), is(Comparison.LIKE));
	}

	@Test
	public void testSelectQueryWithConditionBetween() {
		// Arrange
		var userTable = "users";
		var conditionColumn = "age";
		var conditionVariableLow = ":minage";
		var conditionVariableHigh = ":maxage";
		var betweenWhere = Where.between(userTable, conditionColumn, conditionVariableLow, conditionVariableHigh);

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.addCondition(or(betweenWhere));

		// Assert
		assertThat(getUsersQuery.conditions.size(), is(1));
		assertThat(getUsersQuery.conditions.get(0), instanceOf(CombineCondition.class));
		assertThat(getUsersQuery.conditions.get(0), instanceOf(Or.class));
		assertThat(((Or)getUsersQuery.conditions.get(0)).getJoiner(), is(ConditionCombiner.OR));
		assertThat(((Or)getUsersQuery.conditions.get(0)).getConditions(), hasItemInArray(betweenWhere));
		assertThat(((Or)getUsersQuery.conditions.get(0)).getConditions()[0], is(betweenWhere));
		assertThat(((WhereBetween)((Or)getUsersQuery.conditions.get(0)).getConditions()[0]).getVarLow(), is(conditionVariableLow));
		assertThat(((WhereBetween)((Or)getUsersQuery.conditions.get(0)).getConditions()[0]).getVarHigh(), is(conditionVariableHigh));
	}

	@Test
	public void testSelectQueryWithConditionInArray() {
		// Arrange
		var userTable = "users";
		var conditionColumn = "age";
		var conditionVariableA = ":a";
		var conditionVariableB = ":b";
		var conditionVariableC = ":c";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.addCondition(and(Where.in(userTable, conditionColumn, conditionVariableA, conditionVariableB, conditionVariableC)));

		// Assert
		assertThat(getUsersQuery.conditions.size(), is(1));
		assertThat(getUsersQuery.conditions.get(0), instanceOf(And.class));
	}

	@Test
	public void testSelectQueryWithConditionInQuery() {
		// Arrange
		var userTable = "users";
		var conditionColumn = "id";

		// Act
		var getUsersSubQuery = SelectQuery.selectFromWithNoColumns(userTable)
			.addColumn(new FieldColumn(userTable, conditionColumn))
			.setLimit(1);
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.addCondition(WhereInSubQuery.whereInSubQuery(userTable, conditionColumn, getUsersSubQuery));

		// Assert
		assertThat(getUsersQuery.conditions.size(), is(1));
		assertThat(getUsersQuery.conditions.get(0), instanceOf(WhereInSubQuery.class));
		assertThat(((WhereInSubQuery) getUsersQuery.conditions.get(0)).getTable(), is(userTable));
		assertThat(((WhereInSubQuery) getUsersQuery.conditions.get(0)).getColumn(), is(conditionColumn));
		assertThat(((WhereInSubQuery) getUsersQuery.conditions.get(0)).getQuery(), is(getUsersSubQuery));
		assertThat(((WhereInSubQuery) getUsersQuery.conditions.get(0)).getComparison(), is(Comparison.IN));
	}

	@Test
	public void testSelectQueryWithConditionSmallerThan() {
		// Arrange
		var userTable = "users";
		var conditionColumn = "age";
		var conditionVariable = ":age";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.addCondition(Where.smallerThan(userTable, conditionColumn, conditionVariable));

		// Assert
		assertThat(getUsersQuery.conditions.size(), is(1));
		assertThat(getUsersQuery.conditions.get(0), instanceOf(Where.class));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getTable(), is(userTable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getColumn(), is(conditionColumn));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getVar(), is(conditionVariable));
		assertThat(((Where<?>) getUsersQuery.conditions.get(0)).getComparison(), is(Comparison.SMALLER_THEN));
	}

	@Test
	public void testSelectQueryWithRecursiveFunction() {
		// Arrange
		var userTable = "users";
		var conditionColumn = "age";
		var conditionColumnAlias = "newAge";
		var function = SUM;

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.addColumn(RecursiveFunctionColumn.overFunction(function, new FunctionColumn(userTable, conditionColumn, MIN, conditionColumnAlias)));

		// Assert
		assertThat(getUsersQuery.columns.get(1), instanceOf(RecursiveFunctionColumn.class));
		assertThat(getUsersQuery.columns.get(1).getTable(), is(userTable));
		assertThat(((RecursiveFunctionColumn) getUsersQuery.columns.get(1)).getAlias(), is(conditionColumnAlias));
		assertThat(((RecursiveFunctionColumn) getUsersQuery.columns.get(1)).getFunction(), is(function));
		assertThat(((RecursiveFunctionColumn) getUsersQuery.columns.get(1)).getColumn(), instanceOf(FunctionColumn.class));
	}

	@Test
	public void testSelectQueryGroupBy() {
		// Arrange
		var userTable = "users";
		var groupByColumn = "mail";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.groupBy(userTable, groupByColumn);

		// Assert
		assertThat(getUsersQuery.groupBys.size(), is(1));
		assertThat(getUsersQuery.groupBys.get(0), instanceOf(Grouping.class));
		assertThat(getUsersQuery.groupBys.get(0).getColumn(), instanceOf(Column.class));
	}

	@Test
	public void testSelectQueryWithFunctionAverage() {
		// Arrange
		var userTable = "users";
		var functionColumn = "age";
		var functionColumnAlias = "averageAge";

		// Act
		var getUsersQuery = SelectQuery
			.selectFromWithNoColumns(userTable)
			.addColumnAverage(userTable, functionColumn, functionColumnAlias);

		// Assert
		assertThat(getUsersQuery.columns.size(), is(1));
		assertThat(getUsersQuery.columns.get(0), instanceOf(FunctionColumn.class));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getColumn(), is(functionColumn));
		assertThat(getUsersQuery.columns.get(0).getTable(), is(userTable));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getAlias(), is(functionColumnAlias));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getFunction(), is(AVERAGE));
	}

	@Test
	public void testSelectQueryWithFunctionSum() {
		// Arrange
		var userTable = "users";
		var functionColumn = "age";
		var functionColumnAlias = "sumAge";

		// Act
		var getUsersQuery = SelectQuery
			.selectFromWithNoColumns(userTable)
			.addColumnSum(userTable, functionColumn, functionColumnAlias);

		// Assert
		assertThat(getUsersQuery.columns.size(), is(1));
		assertThat(getUsersQuery.columns.get(0), instanceOf(FunctionColumn.class));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getColumn(), is(functionColumn));
		assertThat(getUsersQuery.columns.get(0).getTable(), is(userTable));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getAlias(), is(functionColumnAlias));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getFunction(), is(SUM));
	}


	@Test
	public void testSelectQueryWithFunctionMax() {
		// Arrange
		var userTable = "users";
		var functionColumn = "age";
		var functionColumnAlias = "maxAge";

		// Act
		var getUsersQuery = SelectQuery
			.selectFromWithNoColumns(userTable)
			.addColumnMax(userTable, functionColumn, functionColumnAlias);

		// Assert
		assertThat(getUsersQuery.columns.size(), is(1));
		assertThat(getUsersQuery.columns.get(0), instanceOf(FunctionColumn.class));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getColumn(), is(functionColumn));
		assertThat(getUsersQuery.columns.get(0).getTable(), is(userTable));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getAlias(), is(functionColumnAlias));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getFunction(), is(MAX));
	}

	@Test
	public void testSelectQueryWithFunctionMin() {
		// Arrange
		var userTable = "users";
		var functionColumn = "age";
		var functionColumnAlias = "minAge";

		// Act
		var getUsersQuery = SelectQuery
			.selectFromWithNoColumns(userTable)
			.addColumnMin(userTable, functionColumn, functionColumnAlias);

		// Assert
		assertThat(getUsersQuery.columns.size(), is(1));
		assertThat(getUsersQuery.columns.get(0), instanceOf(FunctionColumn.class));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getColumn(), is(functionColumn));
		assertThat(getUsersQuery.columns.get(0).getTable(), is(userTable));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getAlias(), is(functionColumnAlias));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getFunction(), is(MIN));
	}

	@Test
	public void testSelectQueryWithFunctionCount() {
		// Arrange
		var userTable = "users";
		var functionColumn = "age";
		var functionColumnAlias = "typeOfAges";

		// Act
		var getUsersQuery = SelectQuery
			.selectFromWithNoColumns(userTable)
			.setDistinct(true)
			.addColumnCount(userTable, functionColumn, functionColumnAlias);

		// Assert
		assertThat(getUsersQuery.columns.size(), is(1));
		assertThat(getUsersQuery.columns.get(0), instanceOf(FunctionColumn.class));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getColumn(), is(functionColumn));
		assertThat(getUsersQuery.columns.get(0).getTable(), is(userTable));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getAlias(), is(functionColumnAlias));
		assertThat(((FunctionColumn) getUsersQuery.columns.get(0)).getFunction(), is(COUNT));
	}

	@Test
	public void testSelectQueryOrderByAsc() {
		// Arrange
		var userTable = "users";
		var groupByColumn = "mail";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.orderByAscending(userTable, groupByColumn);

		// Assert
		assertThat(getUsersQuery.orderBys.size(), is(1));
		assertThat(getUsersQuery.orderBys.get(0), instanceOf(Ordering.class));
		assertThat(getUsersQuery.orderBys.get(0).getColumn(), instanceOf(Column.class));
		assertThat(getUsersQuery.orderBys.get(0).getDirection(), is(OrderingDirection.ASCENDING));
	}

	@Test
	public void testSelectQueryOrderByDesc() {
		// Arrange
		var userTable = "users";
		var groupByColumn = "mail";

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.orderByDescending(userTable, groupByColumn);

		// Assert
		assertThat(getUsersQuery.orderBys.size(), is(1));
		assertThat(getUsersQuery.orderBys.get(0), instanceOf(Ordering.class));
		assertThat(getUsersQuery.orderBys.get(0).getColumn(), instanceOf(Column.class));
		assertThat(getUsersQuery.orderBys.get(0).getDirection(), is(OrderingDirection.DESCENDING));
	}


	@Test
	public void testSelectQueryOnlyOne() {
		// Arrange
		var userTable = "users";
		var limit = 1337;

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.setLimit(limit)
			.onlyOne();

		// Assert
		assertThat(getUsersQuery.getLimit(), is(1));
	}

	@Test
	public void testSelectQueryLimit() {
		// Arrange
		var userTable = "users";
		var limit = 1337;

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.setLimit(limit);

		// Assert
		assertThat(getUsersQuery.getLimit(), is(limit));
	}

	@Test
	public void testSelectQueryOffset() {
		// Arrange
		var userTable = "users";
		var offset = 1337;

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.setOffset(offset);

		// Assert
		assertThat(getUsersQuery.getOffset(), is(offset));
	}

	@Test
	public void testSelectQueryLimitToNone() {
		// Arrange
		var userTable = "users";
		var limit = 0;

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.setLimit(limit);

		// Assert
		assertThat(getUsersQuery.getLimit(), is(Integer.MIN_VALUE));
	}

	@Test
	public void testSelectQueryOffsetToNone() {
		// Arrange
		var userTable = "users";
		var offset = 0;

		// Act
		var getUsersQuery = SelectQuery
			.selectFrom(userTable)
			.groupBy("test")
			.setOffset(offset);

		// Assert
		assertThat(getUsersQuery.getOffset(), is(Integer.MIN_VALUE));
	}

	@Test
	public void testSelectDistinctTrueQuery() {
		// Arrange
		var distinct = true;
		var userTable = "users";

		// Act
		var getUsersQuery = SelectQuery.selectFrom(userTable);
		getUsersQuery.setDistinct(distinct);

		// Assert
		assertThat(getUsersQuery.isDistinct(), is(distinct));
	}

	@Test
	public void testSelectDistinctFalseQuery() {
		// Arrange
		var distinct = false;
		var userTable = "users";

		// Act
		var getUsersQuery = SelectQuery.selectFrom(userTable);
		getUsersQuery.setDistinct(distinct);

		// Assert
		assertThat(getUsersQuery.isDistinct(), is(distinct));
	}

	@Test
	public void testSelectQueryCloneable() {
		// Arrange
		var userTable = "users";
		var groupByColumn = "age";
		var getUsersQuery = SelectQuery.selectFrom(userTable);

		// Act
		var getUsersQueryClone = getUsersQuery.clone();
		getUsersQueryClone
			.setDistinct(true)
			.orderByDescending(userTable, groupByColumn);

		// Assert
		assertThat(getUsersQuery.isDistinct(), is(false));
		assertThat(getUsersQuery.orderBys.size(), is(0));
		assertThat(getUsersQueryClone.isDistinct(), is(true));
		assertThat(getUsersQueryClone.orderBys.size(), is(1));
	}

	@Test
	public void testSelectQueryGetters() {
		// Arrange
		var userTable = "users";
		var getUsersQuery = SelectQuery.selectFrom(userTable);
		var getUsersQueryInternal = getUsersQuery.internal();

		// Act
		var currentTableShorts = getUsersQueryInternal.getCurrentTableShorts();
		var columns = getUsersQueryInternal.getColumns();
		var from = getUsersQueryInternal.getFrom();
		var join = getUsersQueryInternal.getJoin();
		var conditions = getUsersQueryInternal.getConditions();
		var groupBys = getUsersQueryInternal.getGroupBys();
		var orderBys = getUsersQueryInternal.getOrderBys();
		var limit = getUsersQueryInternal.getLimit();
		var offset = getUsersQueryInternal.getOffset();
		var distinct = getUsersQueryInternal.isDistinct();
		var primaryTable = getUsersQueryInternal.getTable();

		// Assert
		assertThrows(UnsupportedOperationException.class,
			() -> currentTableShorts.put("", ""));
		assertThrows(UnsupportedOperationException.class,
			() -> columns.add(null));
		assertThrows(UnsupportedOperationException.class,
			() -> from.add(""));
		assertThrows(UnsupportedOperationException.class,
			() -> join.add(null));
		assertThrows(UnsupportedOperationException.class,
			() -> conditions.add(null));
		assertThrows(UnsupportedOperationException.class,
			() -> groupBys.add(null));
		assertThrows(UnsupportedOperationException.class,
			() -> orderBys.add(null));
		assertThat(limit, is(Integer.MIN_VALUE));
		assertThat(offset, is(Integer.MIN_VALUE));
		assertThat(distinct, is(false));
		assertThat(primaryTable, is(userTable));
	}
}
