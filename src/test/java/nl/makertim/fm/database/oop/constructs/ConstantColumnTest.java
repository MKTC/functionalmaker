package nl.makertim.fm.database.oop.constructs;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;

public class ConstantColumnTest {

	@Test
	public void constructNumber(){
		// Arrange
		var constant = 1;

		// Act
		var column = Column.constant(constant);

		// Assert
		assertThat(column.getColumn(), is(constant));
		assertThat(column.getTable(), nullValue());
	}


	@Test
	public void constructString(){
		// Arrange
		var constant = "Amsterdam";

		// Act
		var column = new ConstantColumn(constant);

		// Assert
		assertThat(column.getColumn(), is(constant));
		assertThat(column.getTable(), nullValue());
	}

	@Test
	public void equalTo(){
		// Arrange
		var constant = 1;

		// Act
		var columnA = new ConstantColumn(constant);
		var columnB = new ConstantColumn(constant);

		// Assert
		assertThat(columnA, is(columnB));
		assertThat(columnB, is(columnA));
	}
}
