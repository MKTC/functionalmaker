package nl.makertim.fm.database.oop.constructs;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.Is.is;

public class ConditionCustomTest {

	@Test
	public void testAbstraction() {
		// Arrange
		var customString = "1=1";

		// Act
		var custom = new ConditionCustom(sqlBuilder -> customString);

		// Assert
		assertThat(custom, instanceOf(Condition.class));
		assertThat(custom.parseQuery(null), is(customString));
	}

}
