package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.core.BiSet;
import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;
import java.util.LinkedList;

import static nl.makertim.fm.database.oop.constructs.ColumnDefinition.columnTypeless;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class AlterQueryTest {

	@Test
	public void constructorChangeNameTest() {
		// Arrange
		var tableName = "user";
		var newTableName = "users";

		// Act
		var alterQuery = new AlterQuery(tableName, newTableName);

		// Assert
		assertThat(alterQuery.table, is(tableName));
		assertThat(alterQuery.newTableName, is(newTableName));
	}

	@Test
	public void constructorChangeColumnName() {
		// Arrange
		var tableName = "user";
		var oldColumnName = "uuid";
		var newColumnName = "id";

		// Act
		var alterQuery = new AlterQuery(tableName,
			new BiSet<>(oldColumnName, newColumnName));

		// Assert
		assertThat(alterQuery.renames.size(), is(1));
		assertThat(alterQuery.renames.get(oldColumnName), is(newColumnName));
	}

	@Test
	public void constructorAddColumn() {
		// Arrange
		var tableName = "user";
		var columnName = "age";
		var newColumn = columnTypeless(columnName);

		// Act
		var alterQuery = new AlterQuery(tableName, newColumn);

		// Assert
		assertThat(alterQuery.newColumns.size(), is(1));
		assertThat(alterQuery.newColumns.get(0), is(newColumn));
	}

	@Test
	public void constructorRemoveColumn() {
		// Arrange
		var tableName = "user";
		var columnName = "age";

		// Act
		var alterQuery = new AlterQuery(tableName, new String[] { columnName });

		// Assert
		assertThat(alterQuery.droppedColumns.size(), is(1));
		assertThat(alterQuery.droppedColumns.get(0), is(columnName));
	}

	@Test
	public void staticChangeNameTest() {
		// Arrange
		var tableName = "user";
		var newTableName = "users";

		// Act
		var alterQuery = AlterQuery.alterName(tableName, newTableName);

		// Assert
		assertThat(alterQuery.table, is(tableName));
		assertThat(alterQuery.newTableName, is(newTableName));
	}

	@Test
	public void staticChangeColumnName() {
		// Arrange
		var tableName = "user";
		var oldColumnName = "uuid";
		var newColumnName = "id";

		// Act
		var alterQuery = AlterQuery.renameColumns(tableName,
			new BiSet<>(oldColumnName, newColumnName));

		// Assert
		assertThat(alterQuery.renames.size(), is(1));
		assertThat(alterQuery.renames.get(oldColumnName), is(newColumnName));
	}

	@Test
	public void staticAddColumn() {
		// Arrange
		var tableName = "user";
		var columnName = "age";
		var newColumn = columnTypeless(columnName);

		// Act
		var alterQuery = AlterQuery.addColumn(tableName, newColumn);

		// Assert
		assertThat(alterQuery.newColumns.size(), is(1));
		assertThat(alterQuery.newColumns.get(0), is(newColumn));
	}

	@Test
	public void staticAddColumns() {
		// Arrange
		var tableName = "user";
		var columnName = "age";
		var newColumn = columnTypeless(columnName);

		// Act
		var alterQuery = AlterQuery.addColumns(tableName, newColumn);

		// Assert
		assertThat(alterQuery.newColumns.size(), is(1));
		assertThat(alterQuery.newColumns.get(0), is(newColumn));
	}

	@Test
	public void staticRemoveColumn() {
		// Arrange
		var tableName = "user";
		var columnName = "age";

		// Act
		var alterQuery = AlterQuery.dropColumn(tableName, columnName);

		// Assert
		assertThat(alterQuery.droppedColumns.size(), is(1));
		assertThat(alterQuery.droppedColumns.get(0), is(columnName));
	}

	@Test
	public void staticRemoveColumns() {
		// Arrange
		var tableName = "user";
		var columnName = "age";

		// Act
		var alterQuery = AlterQuery.dropColumns(tableName, columnName);

		// Assert
		assertThat(alterQuery.droppedColumns.size(), is(1));
		assertThat(alterQuery.droppedColumns.get(0), is(columnName));
	}

	@Test
	public void cloneTest() {
		// Arrange
		var tableName = "user";
		var newTableName = "users";

		// Act
		var alterQuery = new AlterQuery(tableName, newTableName);
		var alterQueryClone = alterQuery.clone();

		// Assert
		assertThat(alterQuery, is(alterQueryClone));
		assertThat(alterQueryClone, is(alterQuery));
	}

	@Test
	public void internalTest() {
		// Arrange
		var tableName = "user";
		var newTableName = "users";

		// Act
		var alterQuery = new AlterQuery(tableName, newTableName);
		var internal = alterQuery.internal();

		// Assert
		assertThat(internal.getTable(), is(tableName));
		assertThat(internal.getNewTableName(), is(newTableName));
		assertThat(internal.getRenames(), is(new LinkedHashMap<>()));
		assertThat(internal.getNewColumns(), is(new LinkedList<>()));
		assertThat(internal.getDroppedColumns(), is(new LinkedList<>()));
	}
}
