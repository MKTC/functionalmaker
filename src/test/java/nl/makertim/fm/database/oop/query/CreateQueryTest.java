package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.database.oop.constructs.ColumnDefinition;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CreateQueryTest {

	@Test
	public void testConstructorCreateQuery() {
		// Arrange
		var tableName = "user-tab";
		var column = ColumnDefinition.column("id", "1", 3);

		// Act
		var query = new CreateQuery(tableName, column);

		// Assert
		assertThat(query, instanceOf(CreateQuery.class));
		assertThat(query.columns, hasItem(column));
		assertThat(query.columns.get(0).getName(), is(column.getName()));
		assertThat(query.columns.get(0).getDefaultValue(), is(column.getDefaultValue()));
		assertThat(query.columns.get(0).getLengthType(), is(column.getLengthType()));
		assertThat(query.columns.get(0).getType(), equalTo(column.getType()));
	}

	@Test
	public void testStaticCreateQuery() {
		// Arrange
		var tableName = "user-tab";
		var columnId = ColumnDefinition.column("id", "1", false, 3);
		var columnName = ColumnDefinition.column("name", "Tim", 255)
			.setAllowNull();
		var columnTab = ColumnDefinition.columnWithNull("tab", "0", 3);

		// Act
		var query = CreateQuery.create(tableName, columnId, columnName, columnTab);

		// Assert
		assertThat(query, instanceOf(CreateQuery.class));
	}

	@Test
	public void testConstructorCreateQuerySetIgnoreExistingTrue() {
		// Arrange
		var tableName = "user-tab";
		var ignore = true;
		var columnId = ColumnDefinition.columnWithNull("id", 0, 3);
		var columnName = ColumnDefinition.columnWithNull("name", String.class, 255);
		var columnTab = ColumnDefinition.columnWithNull("tab", int.class, 3);

		// Act
		var query = new CreateQuery(tableName, columnId, columnName, columnTab)
			.setIgnoreExisting(ignore);

		// Assert
		assertThat(query.ignoreExisting, is(ignore));
	}

	@Test
	public void testConstructorCreateQuerySetIgnoreExistingFalse() {
		// Arrange
		var tableName = "user-tab";
		var ignore = false;

		// Act
		var query = new CreateQuery(tableName)
			.setIgnoreExisting(ignore);

		// Assert
		assertThat(query.ignoreExisting, is(ignore));
	}

	@Test
	public void testConstructorCreateQuerySetIgnoreExisting() {
		// Arrange
		var tableName = "user-tab";

		// Act
		var query = new CreateQuery(tableName)
			.setIgnoreExisting();

		// Assert
		assertThat(query.ignoreExisting, is(true));
	}

	@Test
	public void testConstructorCreateQueryAddPK() {
		// Arrange
		var tableName = "user-tab";
		var columnName = "test";

		// Act
		var column = ColumnDefinition.columnTypeless(columnName);
		var query = new CreateQuery(tableName, column)
			.addPrimaryColumns(columnName);

		// Assert
		assertThat(query.primaryColumns, hasItem(columnName));
	}

	@Test
	public void testConstructorCreateQueryAddPKFail() {
		// Arrange
		var tableName = "user-tab";

		// Act
		Runnable addPK = () -> new CreateQuery(tableName)
			.addPrimaryColumns("test");

		// Assert
		assertThrows(IllegalArgumentException.class, addPK::run);
	}

	@Test
	public void testConstructorCreateQueryAddIndex() {
		// Arrange
		var tableName = "user-tab";
		var columnName = "test";

		// Act
		var column = ColumnDefinition.columnTypeless(columnName);
		var query = new CreateQuery(tableName, column)
			.addIndexedColumns(columnName);

		// Assert
		assertThat(query.keyColumns, hasItem(columnName));
	}

	@Test
	public void testConstructorCreateQueryAddIndexFail() {
		// Arrange
		var tableName = "user-tab";

		// Act
		Runnable addIndex = () -> new CreateQuery(tableName)
			.addIndexedColumns("test");

		// Assert
		assertThrows(IllegalArgumentException.class, addIndex::run);
	}

	@Test
	public void testConstructorInternal() {
		// Arrange
		var tableName = "user-tab";

		// Act
		var query = new CreateQuery(tableName);
		var internal = query.internal();

		// Assert
		assertThat(internal.isIgnoreExisting(), is(false));
		assertThat(internal.getTable(), is(tableName));
		assertThat(internal.getColumns(), is(new LinkedList<>()));
		assertThat(internal.getPrimaryColumns(), is(new LinkedList<>()));
		assertThat(internal.getKeyColumns(), is(new LinkedList<>()));
	}

	@Test
	public void testConstructorClone() {
		// Arrange
		var tableName = "user-tab";

		// Act
		var query = new CreateQuery(tableName);
		var queryClone = query.clone();

		// Assert
		assertThat(query, is(queryClone));
		assertThat(queryClone, is(query));
	}

	@Test
	public void testAddColumn() {
		// Arrange
		var basicColumn = ColumnDefinition.columnTypeless("basic_column");
		var extendedColumn = ColumnDefinition.columnTypeless("extended_column");

		// Act
		var query = new CreateQuery("table", basicColumn);
		query.addColumn(extendedColumn);
		var internal = query.internal();

		// Assert
		assertThat(internal.getColumns().get(0), is(basicColumn));
		assertThat(internal.getColumns().get(1), is(extendedColumn));
	}
}
