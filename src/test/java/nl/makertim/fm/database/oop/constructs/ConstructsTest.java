package nl.makertim.fm.database.oop.constructs;

import nl.makertim.fm.database.oop.query.ResultQuery;
import nl.makertim.fm.database.oop.query.SelectQuery;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;

public class ConstructsTest {

	private static final Set<Class<?>> constructClasses = Set.of(
		And.class,
		ColumnDefinition.class,
		FunctionColumn.class,
		Grouping.class,
		Join.class,
		Ordering.class,
		Or.class,
		RecursiveFunctionColumn.class,
		FieldColumn.class,
		TableColumn.class,
		Where.class,
		WhereBetween.class,
		WhereIn.class,
		WhereInSubQuery.class,
		WhereNull.class,
		WhereNotNull.class
	);

	// @Test
	@SuppressWarnings("ConstantConditions")
	private void testEqualsOfX(String nameTest, Object instanceA, Object instanceB) {
		// Assert
		assertThat(nameTest, instanceA, instanceOf(Object.class));
		assertThat(nameTest, instanceB, instanceOf(Object.class));
		assertThat(nameTest, instanceA, equalTo(instanceB));
		assertThat(nameTest, instanceB, equalTo(instanceA));
		assertThat(nameTest, !instanceA.equals(null));
		assertThat(nameTest, !instanceA.equals("TEST"));
	}

	@Test
	public void testEqualsOfXSetup() throws Exception {
		Map<Class<?>, Object> defaultValues = new HashMap<>();
		defaultValues.put(String.class, "ThisIsAValue");
		defaultValues.put(String[].class, new String[0]);
		defaultValues.put(Integer.class, 123);
		defaultValues.put(Double.class, 1.1D);
		defaultValues.put(Float.class, 1F);
		defaultValues.put(JoinType.class, JoinType.LEFT);
		defaultValues.put(Function.class, Function.ROUND);
		defaultValues.put(OrderingDirection.class, OrderingDirection.DESCENDING);
		defaultValues.put(FunctionColumnInterface.class, new FunctionColumn(null, null, null, null));
		defaultValues.put(Comparison.class, Comparison.EQUAL);
		defaultValues.put(SelectQuery.class, new SelectQuery(1));
		defaultValues.put(ResultQuery.class, new SelectQuery(1));
		defaultValues.put(Column.class, new FieldColumn("DifferentlyAColumn"));
		defaultValues.put(Condition.class, Where.isNull("abc", "def"));
		defaultValues.put(Condition[].class, new Condition[] { Where.isNull("arr", "yarr") });
		defaultValues.put(Object.class, 1);
		defaultValues.put(Class.class, Object.class);
		defaultValues.put(int[].class, new int[] { 1 });

		for (Class<?> constructClass : constructClasses) {
			var nameTest = "testEqualsOf" + constructClass.getSimpleName();

			// Arrange
			var constructors = constructClass.getDeclaredConstructors();
			Object instanceA;
			Object instanceB;
			if (constructors.length < 1) {
				throw new RuntimeException("Constructor not found of " + constructClass);
			}

			var constructor = constructors[0];
			constructor.setAccessible(true);
			var constructorArgs = Arrays.stream(constructor.getParameterTypes()).map(clazz -> {
				var ret = defaultValues.getOrDefault(clazz, null);
				if (ret == null) throw new RuntimeException(clazz.toString());
				return ret;
			}).toArray();
			instanceA = constructor.newInstance(constructorArgs);
			instanceB = constructor.newInstance(constructorArgs);

			testEqualsOfX(nameTest, instanceA, instanceB);
		}
	}
}
