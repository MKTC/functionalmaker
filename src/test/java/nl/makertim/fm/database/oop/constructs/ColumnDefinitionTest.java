package nl.makertim.fm.database.oop.constructs;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ColumnDefinitionTest {

	@Test
	public void testAutoIncrement(){
		// Arrange
		var column = ColumnDefinition.columnTypeless("test");

		// Act
		column.setAutoIncrementing();

		// Assert
		assertThat(column.isAutoIncrement(), is(true));
	}

}
