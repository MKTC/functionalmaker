package nl.makertim.fm.database.oop.dialects;

import nl.makertim.fm.database.oop.Dialect;
import nl.makertim.fm.database.oop.DialectManager;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class DialectManagerTest {

	@Test
	public void testMySQLDialectJDBC() {
		// Arrange
		var jdbc = "jdbc:mysql:localhost/test";

		// Act
		Optional<Dialect> dialect = DialectManager.findDialectOnJDBC(jdbc);

		// Assert
		assertThat(dialect.isPresent(), is(true));
		assertThat(dialect.get(), instanceOf(MySQLDialect.class));
	}

	@Test
	public void testInvalidDialectJDBC() {
		// Arrange
		var jdbc = "jdbc:makertimdatabasetype:localhost/test";

		// Act
		Optional<Dialect> dialect = DialectManager.findDialectOnJDBC(jdbc);

		// Assert
		assertThat(dialect.isPresent(), is(false));
	}

	@Test
	public void testRegistryDialects() {
		// Arrange
		List<Dialect> dialects = DialectManager.getRegisteredDialects();
		Dialect mySQLDialect = DialectManager.findDialectOnJDBC("jdbc:mysql:localhost/test").orElse(null);

		// Assert
		assertThat(dialects, hasItem(mySQLDialect));
	}
}
