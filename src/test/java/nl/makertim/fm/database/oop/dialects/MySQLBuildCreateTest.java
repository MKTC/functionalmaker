package nl.makertim.fm.database.oop.dialects;

import nl.makertim.fm.database.oop.Dialect;
import nl.makertim.fm.database.oop.constructs.ColumnDefinition;
import nl.makertim.fm.database.oop.query.CreateQuery;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class MySQLBuildCreateTest {

	@Test
	public void testCreateQuery() {
		// Arrange
		var table = "this_is_a_new_table";
		var column = "here_is_the_data";
		var query = new CreateQuery(
			table,
			ColumnDefinition.columnTypeless(column),
			ColumnDefinition.column(column, Integer.class, 3)
		);

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class)
			.buildQuery(query);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString("CREATE"));
		assertThat(sql, containsString("TABLE"));
		assertThat(sql, containsString(table));
		assertThat(sql, containsString(column));
	}
}
