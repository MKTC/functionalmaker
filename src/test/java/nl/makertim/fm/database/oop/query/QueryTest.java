package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.database.oop.constructs.Column;
import nl.makertim.fm.database.oop.constructs.Condition;
import nl.makertim.fm.database.oop.constructs.Where;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;

import static nl.makertim.fm.database.oop.constructs.WhereNull.whereNull;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class QueryTest {

	@Test
	public void testStaticVsSelf() {
		var table = "users";
		var sqlBuilderSelect = Query.selectFrom(table);
		var sqlClassSelect = SelectQuery.selectFrom(table);

		assertThat(sqlBuilderSelect, equalTo(sqlClassSelect));
	}

	@Test
	public void testSelectFromWithColumns() {
		// Arrange
		var userTable = "users";
		var userColumn = "id";
		var select = Query.selectFrom(userTable, userColumn);

		// Assert
		assertThat(select.from.size(), is(1));
		assertThat(select.from, hasItem(userTable));
		assertThat(select.columns.size(), is(1));
		assertThat(select.columns.stream().map(Column::getTable).collect(Collectors.toList()), hasItem(userTable));
	}

	@Test
	public void testSelectFromWithColumnsAndCondition() {
		// Arrange
		var userTable = "users";
		var userColumn = new String[] { "id" };
		var select = Query
			.selectFromWhere(userTable, userColumn, Condition.equalTo(userTable, userColumn[0], "var"));

		// Assert
		assertThat(select.from.size(), is(1));
		assertThat(select.from, hasItem(userTable));
		assertThat(select.columns.size(), is(1));
		assertThat(select.columns.stream().map(Column::getTable).collect(Collectors.toList()), hasItem(userTable));
		assertThat(select.conditions.size(), is(1));
		assertThat(select.conditions.stream().map(condition -> (Where<?>)condition).map(Where::getTable).collect(Collectors.toList()), hasItem(userTable));
	}

	@Test
	public void testSelectFromWithColumnsAndConditionAndLimit() {
		// Arrange
		var userTable = "users";
		var userColumn = new String[] { "id" };
		var select = Query
			.selectFromWhereLimit(userTable, userColumn, Where.equalTo(userTable, userColumn[0], "var"), 12);

		// Assert
		assertThat(select.from.size(), is(1));
		assertThat(select.from, hasItem(userTable));
		assertThat(select.columns.size(), is(1));
		assertThat(select.columns.stream().map(Column::getTable).collect(Collectors.toList()), hasItem(userTable));
		assertThat(select.conditions.size(), is(1));
		assertThat(select.conditions.stream().map(condition -> (Where<?>)condition).map(Where::getTable).collect(Collectors.toList()), hasItem(userTable));
		assertThat(select.getLimit(), is(12));
	}

	@Test
	public void testSelectFrom() {
		// Arrange
		var userTable = "users";
		var select = Query.selectFrom(userTable);

		// Assert
		assertThat(select.from.size(), is(1));
		assertThat(select.from, hasItem(userTable));
	}


	@Test
	public void testSelectFromWithCondition() {
		// Arrange
		var userTable = "users";
		var userColumn = "idz";
		var select = Query.selectFromWhere(userTable, Where.equalTo(userTable, userColumn, "var"));

		// Assert
		assertThat(select.from.size(), is(1));
		assertThat(select.from, hasItem(userTable));
		assertThat(select.conditions.size(), is(1));
		assertThat(select.conditions.stream().map(condition -> (Where<?>)condition).map(Where::getTable).collect(Collectors.toList()), hasItem(userTable));
	}

	@Test
	public void testSelectFromCondition() {
		// Arrange
		var userTable = "users";
		var userColumn = "id";
		var select = Query
			.selectFromWhere(userTable, Where.equalTo(userTable, userColumn, "var"));

		// Assert
		assertThat(select.from.size(), is(1));
		assertThat(select.from, hasItem(userTable));
		assertThat(select.columns.size(), is(1));
		assertThat(select.columns.stream().map(Column::getTable).collect(Collectors.toList()), hasItem(userTable));
		assertThat(select.conditions.size(), is(1));
		assertThat(select.conditions.stream().map(condition -> (Where<?>)condition).map(Where::getTable).collect(Collectors.toList()), hasItem(userTable));
	}

	@Test
	public void testSelectFromConditionAndLimit() {
		// Arrange
		var userTable = "users";
		var userColumn = "id";
		var limit = 19;
		var select = Query
			.selectFromWhereLimit(userTable, Where.equalTo(userTable, userColumn, "var"), limit);

		// Assert
		assertThat(select.from.size(), is(1));
		assertThat(select.from, hasItem(userTable));
		assertThat(select.columns.size(), is(1));
		assertThat(select.columns.stream().map(Column::getTable).collect(Collectors.toList()), hasItem(userTable));
		assertThat(select.conditions.size(), is(1));
		assertThat(select.conditions.stream().map(condition -> (Where<?>)condition).map(Where::getTable).collect(Collectors.toList()), hasItem(userTable));
		assertThat(select.getLimit(), is(limit));
	}

	@Test
	public void testSelectFromConditionIn() {
		// Arrange
		var userTable = "users";
		var userColumn = "id";
		var vars = new String[] { "a", "b", "c" };
		var select = Query
			.selectFromWhere(userTable, userColumn, vars);

		// Assert
		assertThat(select.from.size(), is(1));
		assertThat(select.from, hasItem(userTable));
		assertThat(select.columns.size(), is(1));
		assertThat(select.columns.stream().map(Column::getTable).collect(Collectors.toList()), hasItem(userTable));
		assertThat(select.conditions.size(), is(1));
		assertThat(select.conditions.stream().map(condition -> (Where<?>)condition).map(Where::getTable).collect(Collectors.toList()), hasItem(userTable));
	}

	@Test
	public void testSelectFromConditionNull() {
		// Arrange
		var userTable = "users";
		var userColumn = "id";
		var select = Query
			.selectFromWhere(userTable, whereNull(userTable, userColumn));

		// Assert
		assertThat(select.from.size(), is(1));
		assertThat(select.from, hasItem(userTable));
		assertThat(select.columns.size(), is(1));
		assertThat(select.columns.stream().map(Column::getTable).collect(Collectors.toList()), hasItem(userTable));
		assertThat(select.conditions.size(), is(1));
		assertThat(select.conditions.stream().map(condition -> (Where<?>)condition).map(Where::getTable).collect(Collectors.toList()), hasItem(userTable));
	}

	@Test
	public void testSelectFromConditionInAndLimit() {
		// Arrange
		var userTable = "users";
		var userColumn = "id";
		var vars = new String[] { "a", "b", "c" };
		var limit = 19;
		var select = Query
			.selectFromWhereLimit(userTable, userColumn, vars, limit);

		// Assert
		assertThat(select.from.size(), is(1));
		assertThat(select.from, hasItem(userTable));
		assertThat(select.columns.size(), is(1));
		assertThat(select.columns.stream().map(Column::getTable).collect(Collectors.toList()), hasItem(userTable));
		assertThat(select.conditions.size(), is(1));
		assertThat(select.conditions.stream().map(condition -> (Where<?>)condition).map(Where::getTable).collect(Collectors.toList()), hasItem(userTable));
		assertThat(select.getLimit(), is(limit));
	}
}
