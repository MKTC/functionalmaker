package nl.makertim.fm.database.oop.query;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class DropQueryTest {

	@Test
	public void constructorDropQueryTest() {
		// Arrange
		var tableName = "user-log";

		// Act
		var dropQuery = new DropQuery(tableName);

		// Assert
		assertThat(dropQuery.table, is(tableName));
	}

	@Test
	public void staticDropQueryTest() {
		// Arrange
		var tableName = "user-log";

		// Act
		var dropQuery = DropQuery.drop(tableName);

		// Assert
		assertThat(dropQuery.table, is(tableName));
	}

	@Test
	public void staticDropIgnoreTrueQueryTest() {
		// Arrange
		var tableName = "user-log";
		var ignore = true;

		// Act
		var dropQuery = DropQuery.drop(tableName, ignore);

		// Assert
		assertThat(dropQuery.ignoreIfNotExists, is(ignore));
	}

	@Test
	public void staticDropIgnoreFalseQueryTest() {
		// Arrange
		var tableName = "user-log";
		var ignore = false;

		// Act
		var dropQuery = DropQuery.drop(tableName, ignore);

		// Assert
		assertThat(dropQuery.ignoreIfNotExists, is(ignore));
	}

	@Test
	public void staticDropIgnoreQueryTest() {
		// Arrange
		var tableName = "user-log";

		// Act
		var dropQuery = DropQuery.dropIgnore(tableName);

		// Assert
		assertThat(dropQuery.ignoreIfNotExists, is(true));
	}

	@Test
	public void staticDropIgnoreQueryClone() {
		// Arrange
		var tableName = "user-log";

		// Act
		var dropQuery = DropQuery.drop(tableName);
		var dropQueryClone = dropQuery.clone();

		// Assert
		assertThat(dropQuery, is(dropQueryClone));
		assertThat(dropQueryClone, is(dropQuery));
	}

	@Test
	public void staticDropIgnoreQueryInternal() {
		// Arrange
		var tableName = "user-log";

		// Act
		var dropQuery = DropQuery.drop(tableName);
		var internal = dropQuery.internal();

		// Assert
		assertThat(internal.getTable(), is(tableName));
		assertThat(internal.ignoreIfNotExists(), is(false));
	}
}
