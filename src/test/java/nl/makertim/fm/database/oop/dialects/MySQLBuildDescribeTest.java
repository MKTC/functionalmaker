package nl.makertim.fm.database.oop.dialects;

import nl.makertim.fm.database.oop.Dialect;
import nl.makertim.fm.database.oop.query.DescribeQuery;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class MySQLBuildDescribeTest {

	// TODO: describe should be consistent with output

	@Test
	public void testDescribeQuery() {
		// Arrange
		var table = "this_is_a_table_name";
		var query = new DescribeQuery(table);

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class)
			.buildQuery(query);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString("SHOW"));
		assertThat(sql, containsString("CREATE"));
		assertThat(sql, containsString("TABLE"));
	}

}
