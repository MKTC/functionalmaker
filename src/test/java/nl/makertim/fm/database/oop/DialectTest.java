package nl.makertim.fm.database.oop;

import nl.makertim.fm.database.oop.dialects.MySQLBuilder;
import nl.makertim.fm.database.oop.dialects.MySQLDialect;
import nl.makertim.fm.database.oop.dialects.SQLiteBuilder;
import nl.makertim.fm.database.oop.dialects.SQLiteDialect;
import nl.makertim.fm.database.oop.query.SelectQuery;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class DialectTest {

	@Test
	public void getDialectByClass() {
		// Arrange
		var mysqlDialectClass = MySQLDialect.class;

		// Act
		var mysqlDialect = Dialect.getDialect(mysqlDialectClass);

		// Assert
		assertThat(mysqlDialect, instanceOf(mysqlDialectClass));
	}

	@Test
	public void dialectGetBuilder() {
		// Arrange
		var mysqlDialect = new MySQLDialect();
		var mysqlBuilderClass = MySQLBuilder.class;

		// Act
		var mysqlBuilder = mysqlDialect.getBuilder(new SelectQuery("Amsterdam"));

		// Assert
		assertThat(mysqlBuilder, instanceOf(mysqlBuilderClass));
	}

	@Test
	public void dialectDirectlyBuild() {
		// Arrange
		var mysqlDialect = new MySQLDialect();
		var query = new SelectQuery(1);
		var statementClass = PreparedQuery.class;

		// Act
		var statement = mysqlDialect.buildQuery(query);

		// Assert
		assertThat(statement, instanceOf(statementClass));
		assertThat(statement.getSQL(), not(isEmptyString()));
	}

	@Test
	public void testMySQLGetBuilder() {
		// Arrange
		var mysqlDialect = new MySQLDialect();

		// Act
		var builderClass = mysqlDialect.getBuilderClass();

		// Assert
		assertThat(builderClass, equalTo(MySQLBuilder.class));
	}

	@Test
	public void testSQLiteGetBuilder() {
		// Arrange
		var mysqlDialect = new SQLiteDialect();

		// Act
		var builderClass = mysqlDialect.getBuilderClass();

		// Assert
		assertThat(builderClass, equalTo(SQLiteBuilder.class));
	}
}
