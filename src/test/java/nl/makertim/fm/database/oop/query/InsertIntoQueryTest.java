package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.database.oop.constructs.Mode;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItemInArray;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class InsertIntoQueryTest {

	@Test
	public void testInsertQuery() {
		// Arrange
		var table = "entry";

		// Act
		var insertQuery = new InsertQuery(table);

		// Assert
		assertThat(insertQuery.table, is(table));
	}

	@Test
	public void testInsertQueryStatic() {
		// Arrange
		var table = "entry";

		// Act
		var insertQuery = InsertQuery.insertInto(table);

		// Assert
		assertThat(insertQuery.table, is(table));
	}

	@Test
	public void testInsertQueryWithColumnsStatic() {
		// Arrange
		var table = "entry";
		var columnA = "id";
		var columnB = "value";
		var columnC = "that";

		// Act
		var insertQuery = InsertQuery.insertIntoWithColumns(table, columnA, columnB, columnC);

		// Assert
		assertThat(insertQuery.table, is(table));
		assertThat(insertQuery.columns, hasItemInArray(columnA));
		assertThat(insertQuery.columns, hasItemInArray(columnB));
		assertThat(insertQuery.columns, hasItemInArray(columnC));
	}

	@Test
	public void testInsertQueryStaticOnlyOne() {
		// Arrange
		var table = "entry";
		var amount = 10;

		// Act
		var insertQuery = InsertQuery
			.insertMultipleInto(table, amount)
			.onlyOne();

		// Assert
		assertThat(insertQuery.table, is(table));
		assertThat(insertQuery.amount, is(1));
	}

	@Test
	public void testInsertQueryStaticInvalidAmount() {
		// Arrange
		var table = "entry";
		var amount = 0;

		// Act
		Runnable invalidAmountFunction = () -> InsertQuery
			.insertMultipleInto(table, amount)
			.onlyOne();

		// Assert
		assertThrows(IllegalArgumentException.class, invalidAmountFunction::run);
	}

	@Test
	public void testInsertQueryStaticWithAmount() {
		// Arrange
		var table = "entry";
		var amount = 10;

		// Act
		var insertQuery = InsertQuery.insertMultipleInto(table, amount);

		// Assert
		assertThat(insertQuery.table, is(table));
		assertThat(insertQuery.amount, is(amount));
	}

	@Test
	public void testInsertQueryWithColumnsStaticWithAmount() {
		// Arrange
		var table = "entry";
		var columnA = "id";
		var columnB = "value";
		var columnC = "that";
		var amount = 10;

		// Act
		var insertQuery = InsertQuery.insertMultipleIntoWithColumns(table, amount, columnA, columnB, columnC);

		// Assert
		assertThat(insertQuery.table, is(table));
		assertThat(insertQuery.amount, is(amount));
		assertThat(insertQuery.columns, hasItemInArray(columnA));
		assertThat(insertQuery.columns, hasItemInArray(columnB));
		assertThat(insertQuery.columns, hasItemInArray(columnC));
	}

	@Test
	public void testInsertQueryClone() {
		// Arrange
		var table = "entry";
		var amount = 10;

		// Act
		var insertQuery = InsertQuery.insertMultipleInto(table, amount);
		var insertQueryClone = insertQuery.clone();

		// Assert
		assertThat(insertQuery, is(insertQueryClone));
		assertThat(insertQuery.table, is(insertQueryClone.table));
		assertThat(insertQuery.amount, is(insertQueryClone.amount));
	}

	@Test
	public void testInsertQueryInternals() {
		// Arrange
		var table = "entry";
		var amount = 10;

		// Act
		var insertQuery = InsertQuery.insertMultipleInto(table, amount);
		var insertQueryInternal = insertQuery.internal();

		// Assert
		assertThat(insertQueryInternal.getAmount(), is(amount));
		assertThat(insertQueryInternal.getTable(), is(table));
		assertThat(insertQueryInternal.getMode(), is(Mode.PLAIN));
		assertThat(insertQueryInternal.getColumns(), nullValue());
	}

	@Test
	public void testInsertQueryIgnore() {
		// Arrange
		var table = "entry";

		// Act
		var insertQuery = InsertQuery.insertIntoIgnore(table);

		// Assert
		assertThat(insertQuery.mode, is(Mode.IGNORE));
	}


	@Test
	public void testInsertQueryIgnoreColumns() {
		// Arrange
		var table = "entry";

		// Act
		var insertQuery = InsertQuery.insertIntoIgnoreWithColumns(table);

		// Assert
		assertThat(insertQuery.mode, is(Mode.IGNORE));
	}

	@Test
	public void testInsertIgnoreMultipleInto() {
		// Arrange
		var table = "entry";
		var amount = 10;

		// Act
		var insertQuery = InsertQuery.insertIgnoreMultipleInto(table, amount);

		// Assert
		assertThat(insertQuery.mode, is(Mode.IGNORE));
	}

	@Test
	public void testInsertAbortMultipleInto() {
		// Arrange
		var table = "entry";
		var amount = 10;

		// Act
		var insertQuery = InsertQuery.insertAbortMultipleInto(table, amount);

		// Assert
		assertThat(insertQuery.mode, is(Mode.ABORT));
	}

	@Test
	public void testInsertReplaceMultipleInto() {
		// Arrange
		var table = "entry";
		var amount = 10;

		// Act
		var insertQuery = InsertQuery.insertReplaceMultipleInto(table, amount);

		// Assert
		assertThat(insertQuery.mode, is(Mode.REPLACE));
	}

	@Test
	public void testInsertRollbackMultipleInto() {
		// Arrange
		var table = "entry";
		var amount = 10;

		// Act
		var insertQuery = InsertQuery.insertRollbackMultipleInto(table, amount);

		// Assert
		assertThat(insertQuery.mode, is(Mode.ROLLBACK));
	}

	@Test
	public void testInsertIgnoreMultipleIntoWithColumns() {
		// Arrange
		var table = "entry";
		var amount = 10;

		// Act
		var insertQuery = InsertQuery.insertIgnoreMultipleIntoWithColumns(table, amount);

		// Assert
		assertThat(insertQuery.mode, is(Mode.IGNORE));
	}

	@Test
	public void testInsertAbortMultipleIntoWithColumns() {
		// Arrange
		var table = "entry";
		var amount = 10;

		// Act
		var insertQuery = InsertQuery.insertAbortMultipleIntoWithColumns(table, amount);

		// Assert
		assertThat(insertQuery.mode, is(Mode.ABORT));
	}

	@Test
	public void testInsertReplaceMultipleIntoWithColumns() {
		// Arrange
		var table = "entry";
		var amount = 10;

		// Act
		var insertQuery = InsertQuery.insertReplaceMultipleIntoWithColumns(table, amount);

		// Assert
		assertThat(insertQuery.mode, is(Mode.REPLACE));
	}

	@Test
	public void testInsertRollbackMultipleIntoWithColumns() {
		// Arrange
		var table = "entry";
		var amount = 10;

		// Act
		var insertQuery = InsertQuery.insertRollbackMultipleIntoWithColumns(table, amount);

		// Assert
		assertThat(insertQuery.mode, is(Mode.ROLLBACK));
	}
}
