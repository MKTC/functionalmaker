package nl.makertim.fm.database.oop.constructs;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;

public class VarColumnTest {

	@Test
	public void construct(){
		// Arrange
		var var = ":userid";

		// Act
		var column = Column.var(var);

		// Assert
		assertThat(column.getColumn(), is(var));
		assertThat(column.getTable(), nullValue());
	}

	@Test
	public void equalTo(){
		// Arrange
		var varA = ":userid";
		var varB = "userid";

		// Act
		var columnA = new VarColumn(varA);
		var columnB = new VarColumn(varB);

		// Assert
		assertThat(columnA, is(columnB));
		assertThat(columnB, is(columnA));
	}
}
