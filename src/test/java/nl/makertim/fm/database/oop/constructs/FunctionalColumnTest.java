package nl.makertim.fm.database.oop.constructs;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class FunctionalColumnTest {

	@Test
	public void setAliasTest() {
		// Arrange
		var table = "hurray";
		var columnName = "saying";
		var oldAlias = "nay";
		var newAlias = "yay";
		var function = Function.SUM;

		// Act
		var column = new FunctionColumn(table, columnName, function, oldAlias);
		column.setAlias(newAlias);

		// Assert
		assertThat(column.getAlias(), is(newAlias));
	}

	@Test
	public void setRecursiveAliasTest() {
		// Arrange
		var table = "hurray";
		var columnName = "saying";
		var oldAlias = "nay";
		var newAlias = "yay";
		var function = Function.SUM;

		// Act
		var column = new FunctionColumn(table, columnName, function, oldAlias);
		var recursiveColumn = new RecursiveFunctionColumn(function, column);
		recursiveColumn.setAlias(newAlias);

		// Assert
		assertThat(column.getAlias(), is(newAlias));
	}

}
