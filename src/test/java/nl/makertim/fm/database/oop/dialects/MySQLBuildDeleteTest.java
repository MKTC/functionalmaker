package nl.makertim.fm.database.oop.dialects;

import nl.makertim.fm.database.oop.Dialect;
import nl.makertim.fm.database.oop.query.DeleteQuery;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

public class MySQLBuildDeleteTest {

	@Test
	public void testDeleteQuery() {
		// Arrange
		var table = "this_used_to_be_a_table";
		var query = new DeleteQuery(table);

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class)
			.buildQuery(query);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString("DELETE"));
		assertThat(sql, containsString("FROM"));
		assertThat(sql, not(containsString("WHERE")));
	}
}
