package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.database.oop.constructs.Ordering;
import nl.makertim.fm.database.oop.constructs.OrderingDirection;
import nl.makertim.fm.database.oop.constructs.Where;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static nl.makertim.fm.database.oop.constructs.Mode.IGNORE;
import static nl.makertim.fm.database.oop.constructs.Mode.PLAIN;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;

public class UpdateQueryTest {

	@Test
	public void testUpdateQueryConstructor() {
		// Arrange
		var tableName = "users";

		// Act
		var updateQuery = new UpdateQuery(tableName);

		// Assert
		assertThat(updateQuery, instanceOf(UpdateQuery.class));
		assertThat(updateQuery.table, is(tableName));
	}

	@Test
	public void testUpdateQueryStaticly() {
		// Arrange
		var tableName = "users";

		// Act
		var updateQuery = UpdateQuery.update(tableName);

		// Assert
		assertThat(updateQuery, instanceOf(UpdateQuery.class));
		assertThat(updateQuery.table, is(tableName));
	}

	@Test
	public void testUpdateIgnoreQuery() {
		// Arrange
		var tableName = "users";

		// Act
		var updateQuery = UpdateQuery.updateIgnore(tableName);

		// Assert
		assertThat(updateQuery, instanceOf(UpdateQuery.class));
		assertThat(updateQuery.table, is(tableName));
		assertThat(updateQuery.mode, is(IGNORE));
	}

	@Test
	public void testUpdateSingleIgnoreQuery() {
		// Arrange
		var tableName = "users";

		// Act
		var updateQuery = UpdateQuery.updateSingleIgnore(tableName);

		// Assert
		assertThat(updateQuery, instanceOf(UpdateQuery.class));
		assertThat(updateQuery.table, is(tableName));
		assertThat(updateQuery.mode, is(IGNORE));
		assertThat(updateQuery.limit, is(1));
	}

	@Test
	public void testUpdateSingleSpecificColumns() {
		// Arrange
		var tableName = "users";
		var columnA = "id";
		var columnB = "name";

		// Act
		var updateQuery = UpdateQuery.updateSingleSpecificColumns(tableName, columnA, columnB);

		// Assert
		assertThat(updateQuery, instanceOf(UpdateQuery.class));
		assertThat(updateQuery.table, is(tableName));
		assertThat(updateQuery.columns, hasItemInArray(columnA));
		assertThat(updateQuery.columns, hasItemInArray(columnB));
		assertThat(updateQuery.limit, is(1));
	}

	@Test
	public void testUpdateIgnoreSpecificColumns() {
		// Arrange
		var tableName = "users";
		var columnA = "id";
		var columnB = "name";

		// Act
		var updateQuery = UpdateQuery.updateIgnoreSpecificColumns(tableName, columnA, columnB);

		// Assert
		assertThat(updateQuery, instanceOf(UpdateQuery.class));
		assertThat(updateQuery.table, is(tableName));
		assertThat(updateQuery.columns, hasItemInArray(columnA));
		assertThat(updateQuery.columns, hasItemInArray(columnB));
		assertThat(updateQuery.mode, is(IGNORE));
	}

	@Test
	public void testUpdateIgnoreSingeSpecificColumns() {
		// Arrange
		var tableName = "users";
		var columnA = "id";
		var columnB = "name";

		// Act
		var updateQuery = UpdateQuery.updateIgnoreSingeSpecificColumns(tableName, columnA, columnB);

		// Assert
		assertThat(updateQuery, instanceOf(UpdateQuery.class));
		assertThat(updateQuery.table, is(tableName));
		assertThat(updateQuery.columns, hasItemInArray(columnA));
		assertThat(updateQuery.columns, hasItemInArray(columnB));
		assertThat(updateQuery.mode, is(IGNORE));
		assertThat(updateQuery.limit, is(1));
	}

	@Test
	public void testUpdateWithCondition() {
		// Arrange
		var tableName = "users";
		var column = "id";
		var variable = ":id";

		// Act
		var where = Where.equalTo(tableName, column, variable);
		var updateQuery = UpdateQuery
			.update(tableName)
			.addCondition(where);

		// Assert
		assertThat(updateQuery, instanceOf(UpdateQuery.class));
		assertThat(updateQuery.table, is(tableName));
		assertThat(updateQuery.conditions, hasItem(where));
	}

	@Test
	public void testUpdateWithOrderAsc() {
		// Arrange
		var tableName = "users";
		var column = "id";

		// Act
		var updateQuery = UpdateQuery
			.update(tableName)
			.orderByAscending(tableName, column);

		// Assert
		assertThat(updateQuery, instanceOf(UpdateQuery.class));
		assertThat(updateQuery.table, is(tableName));
		assertThat(updateQuery.orderBys.size(), is(1));
		assertThat(updateQuery.orderBys.get(0), instanceOf(Ordering.class));
		assertThat(updateQuery.orderBys.get(0).getDirection(), is(OrderingDirection.ASCENDING));
	}

	@Test
	public void testUpdateWithOrderDesc() {
		// Arrange
		var tableName = "users";
		var column = "id";

		// Act
		var updateQuery = UpdateQuery
			.update(tableName)
			.orderByDescending(tableName, column);

		// Assert
		assertThat(updateQuery, instanceOf(UpdateQuery.class));
		assertThat(updateQuery.table, is(tableName));
		assertThat(updateQuery.orderBys.size(), is(1));
		assertThat(updateQuery.orderBys.get(0), instanceOf(Ordering.class));
		assertThat(updateQuery.orderBys.get(0).getDirection(), is(OrderingDirection.DESCENDING));
	}

	@Test
	public void testUpdateInvalidLimit() {
		// Arrange
		var tableName = "users";
		var limit = -1;

		// Act
		var updateQuery = UpdateQuery
			.update(tableName)
			.setLimit(limit);

		// Assert
		assertThat(updateQuery.limit, is(Integer.MIN_VALUE));
	}

	@Test
	public void testUpdateOffset() {
		// Arrange
		var tableName = "users";
		var offset = 12;

		// Act
		var updateQuery = UpdateQuery
			.update(tableName)
			.setOffset(offset);

		// Assert
		assertThat(updateQuery, instanceOf(UpdateQuery.class));
		assertThat(updateQuery.table, is(tableName));
		assertThat(updateQuery.offset, is(offset));
	}

	@Test
	public void testUpdateInvalidOffset() {
		// Arrange
		var tableName = "users";
		var offset = -1;

		// Act
		var updateQuery = UpdateQuery
			.update(tableName)
			.setOffset(offset);

		// Assert
		assertThat(updateQuery.offset, is(Integer.MIN_VALUE));
	}

	@Test
	public void testUpdateGetInternal() {
		// Arrange
		var tableName = "users";

		// Act
		var updateQuery = UpdateQuery
			.update(tableName);
		var internal = updateQuery.internal();

		// Assert
		assertThat(updateQuery, instanceOf(UpdateQuery.class));
		assertThat(updateQuery.internal(), is(internal));
	}

	@Test
	public void testUpdateEquals() {
		// Arrange
		var tableName = "users";

		// Act
		var updateQueryA = UpdateQuery
			.update(tableName);
		var updateQueryB = UpdateQuery
			.update(tableName);

		// Assert
		assertThat(updateQueryA, instanceOf(UpdateQuery.class));
		assertThat(updateQueryA, is(updateQueryB));
	}

	@Test
	public void testUpdateClone() {
		// Arrange
		var tableName = "users";

		// Act
		var updateQueryA = UpdateQuery
			.update(tableName);
		var updateQueryB = updateQueryA.clone();

		// Assert
		assertThat(updateQueryB, instanceOf(UpdateQuery.class));
		assertThat(updateQueryA, is(updateQueryB));
		assertThat(updateQueryA, equalTo(updateQueryB));
	}

	@Test
	public void testUpdateInternal() {
		// Arrange
		var tableName = "users";

		// Act
		var updateQuery = UpdateQuery
			.update(tableName);
		var internal = updateQuery.internal();

		// Assert
		assertThat(updateQuery, instanceOf(UpdateQuery.class));
		assertThat(internal.getTable(), is(tableName));
		assertThat(internal.getMode(), is(PLAIN));
		assertThat(internal.getColumns(), nullValue());
		assertThat(internal.getConditions(), is(new LinkedList<>()));
		assertThat(internal.getOrderBys(), is(new LinkedList<>()));
		assertThat(internal.getLimit(), is(Integer.MIN_VALUE));
		assertThat(internal.getOffset(), is(Integer.MIN_VALUE));
	}
}
