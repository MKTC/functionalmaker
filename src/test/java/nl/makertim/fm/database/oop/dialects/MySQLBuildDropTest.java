package nl.makertim.fm.database.oop.dialects;

import nl.makertim.fm.database.oop.Dialect;
import nl.makertim.fm.database.oop.query.DropQuery;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

public class MySQLBuildDropTest {

	@Test
	public void testDropQuery() {
		// Arrange
		var table = "this_used_to_be_a_table";
		var query = new DropQuery(table);

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class)
			.buildQuery(query);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString("DROP"));
		assertThat(sql, containsString("TABLE"));
		assertThat(sql, not(containsString("EXISTS")));
	}

	@Test
	public void testDropIfExistsQuery() {
		// Arrange
		var table = "this_used_to_be_a_table";
		var query = new DropQuery(table)
			.ignoreIfNotExists();

		// Act
		var preparedQuery = Dialect.getDialect(MySQLDialect.class)
			.buildQuery(query);
		var sql = preparedQuery.getSQL();

		// Assert
		assertThat(sql, containsString("DROP"));
		assertThat(sql, containsString("TABLE"));
		assertThat(sql, containsString("IF"));
		assertThat(sql, containsString("EXISTS"));
	}
}
