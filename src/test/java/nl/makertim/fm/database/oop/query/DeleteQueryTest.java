package nl.makertim.fm.database.oop.query;

import nl.makertim.fm.database.oop.constructs.Where;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class DeleteQueryTest {

	@Test
	public void testDeleteConstructor() {
		// Arrange
		var tableName = "transactions";

		// Act
		var deleteQuery = new DeleteQuery(tableName);

		// Assert
		assertThat(deleteQuery.table, is(tableName));
	}

	@Test
	public void testDeleteStatic() {
		// Arrange
		var tableName = "transactions";

		// Act
		var deleteQuery = DeleteQuery.truncate(tableName);

		// Assert
		assertThat(deleteQuery.table, is(tableName));
	}

	@Test
	public void testDeleteStaticWhere() {
		// Arrange
		var tableName = "transactions";
		var column = "userid";

		// Act
		var where = Where.inSubQuery(tableName, column, SelectQuery.selectFrom(tableName));
		var deleteQuery = DeleteQuery.deleteWhere(tableName, where);

		// Assert
		assertThat(deleteQuery.conditions, hasItem(where));
	}

	@Test
	public void testDeleteStaticFirst() {
		// Arrange
		var tableName = "transactions";

		// Act
		var deleteQuery = DeleteQuery.deleteFirst(tableName);

		// Assert
		assertThat(deleteQuery.limit, is(1));
	}

	@Test
	public void testDeleteStaticFirstOrderAscending() {
		// Arrange
		var tableName = "transactions";
		var column = "id";

		// Act
		var deleteQuery = DeleteQuery.deleteFirstFromOrderAscending(tableName, column);

		// Assert
		assertThat(deleteQuery.limit, is(1));
		assertThat(deleteQuery.orderBys.size(), is(1));
	}

	@Test
	public void testDeleteStaticFirstOrderDescending() {
		// Arrange
		var tableName = "transactions";
		var column = "id";

		// Act
		var deleteQuery = DeleteQuery.deleteFirstFromOrderDescending(tableName, column);

		// Assert
		assertThat(deleteQuery.limit, is(1));
		assertThat(deleteQuery.orderBys.size(), is(1));
	}

	@Test
	public void testDeleteStaticFirstWhere() {
		// Arrange
		var tableName = "transactions";
		var column = "id";

		// Act
		var where = Where.isNull(tableName, column);
		var deleteQuery = DeleteQuery.deleteFirstWhere(tableName, where);

		// Assert
		assertThat(deleteQuery.limit, is(1));
		assertThat(deleteQuery.conditions, hasItem(where));
	}

	@Test
	public void testDeleteStaticInvalidLimit() {
		// Arrange
		var tableName = "transactions";

		// Act
		var deleteQuery = DeleteQuery.truncate(tableName)
			.setLimit(-1);

		// Assert
		assertThat(deleteQuery.limit, is(Integer.MIN_VALUE));
	}

	@Test
	public void testDeleteStaticInvalidOffset() {
		// Arrange
		var tableName = "transactions";

		// Act
		var deleteQuery = DeleteQuery.truncate(tableName)
			.setOffset(-1);

		// Assert
		assertThat(deleteQuery.offset, is(Integer.MIN_VALUE));
	}

	@Test
	public void testDeleteStaticOffset() {
		// Arrange
		var tableName = "transactions";
		var offset = 11;

		// Act
		var deleteQuery = DeleteQuery.truncate(tableName)
			.setOffset(offset);

		// Assert
		assertThat(deleteQuery.offset, is(offset));
	}

	@Test
	public void testDeleteInternal() {
		// Arrange
		var tableName = "transactions";

		// Act
		var deleteQuery = DeleteQuery.truncate(tableName);
		var internal = deleteQuery.internal();

		// Assert
		assertThat(internal.getTable(), is(tableName));
		assertThat(internal.getConditions(), is(new LinkedList<>()));
		assertThat(internal.getOrderBys(), is(new LinkedList<>()));
		assertThat(internal.getLimit(), is(Integer.MIN_VALUE));
		assertThat(internal.getOffset(), is(Integer.MIN_VALUE));
	}

	@Test
	public void testDeleteClone() {
		// Arrange
		var tableName = "transactions";

		// Act
		var deleteQuery = DeleteQuery.truncate(tableName);
		var deleteQueryClone = deleteQuery.clone();

		// Assert
		assertThat(deleteQuery, is(deleteQueryClone));
		assertThat(deleteQueryClone, is(deleteQuery));
	}
}
