package nl.makertim.fm.database.oop.dialects;

import nl.makertim.fm.database.oop.BasePreparedQuery;
import nl.makertim.fm.database.oop.PreparedQuery;
import nl.makertim.fm.database.oop.query.SelectQuery;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

public class BasePreparedQueryTest {

	@Test
	public void dialectDirectlyBuild() {
		// Arrange
		var mysqlDialect = new MySQLDialect();
		var query = new SelectQuery(1);
		var statementClass = PreparedQuery.class;

		// Act
		var statement = mysqlDialect.buildQuery(query);
		var statementClone = new BasePreparedQuery((BasePreparedQuery) statement) {
		};

		// Assert
		assertThat(statement, instanceOf(statementClass));
		assertThat(statementClone, instanceOf(statementClass));
		assertThat(statement.getSQL(), is(statementClone.getSQL()));
		assertThat(statementClone.getSQL(), is(statement.getSQL()));
	}


	@Test
	public void emptyBase() {
		// Arrange
		var buildTime = 1337;
		var statement = new BasePreparedQuery("SELECT 1;", true)
			.setBuildTime(buildTime);
		var expectedPlaceholders = new String[0];


		// Act
		var actualPlaceholders = statement.getPlaceholderNames();
		var actualPlaceholderAmount = statement.getPlaceholderAmount();
		var actualPlaceholdersDistinct = statement.getPlaceholderNamesDistinct();
		var actualPlaceholderDistinctAmount = statement.getPlaceholderDistinctAmount();
		var actualBuildTime = statement.getBuildTimeMs();
		var actualPrepareTime = statement.getLastPrepareMs();
		var actualExecTime = statement.getLastExecuteMs();

		// Assert
		assertThat(actualPlaceholders, is(expectedPlaceholders));
		assertThat(actualPlaceholderAmount, is(0));
		assertThat(actualPlaceholdersDistinct, is(expectedPlaceholders));
		assertThat(actualPlaceholderDistinctAmount, is(0));
		assertThat(actualBuildTime, is(0.0D));
		assertThat(actualPrepareTime, is(0.0D));
		assertThat(actualExecTime, is(0.0D));
	}
}
