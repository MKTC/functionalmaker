package nl.makertim.fm.database.config;

import com.zaxxer.hikari.HikariConfig;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.Is.is;

public class HikariTest {

	@Test
	public void getWrapped() {
		// Act
		var config = new HikariConfigWrapper();
		var result = config.getWrapped();

		// Assert
		assertThat(result, instanceOf(HikariConfig.class));
	}

	@Test
	public void getDriverClassName() {
		// Arrange
		var value = "com.mysql.jdbc.Driver";

		// Act
		var config = new HikariConfigWrapper();
		config.setDriverClassName(value);
		var result = config.getDriverClassName();

		// Assert
		assertThat(result, is(result));
	}

	@Test
	public void getJdbcUrl() {
		// Arrange
		var value = "jdbc:sqlite:tmp.sqlite3";

		// Act
		var config = new HikariConfigWrapper();
		config.setJdbcUrl(value);
		var result = config.getJdbcUrl();

		// Assert
		assertThat(result, is(result));
	}

	@Test
	public void getUsername() {
		// Arrange
		var value = "MakerTim";

		// Act
		var config = new HikariConfigWrapper();
		config.setUsername(value);
		var result = config.getUsername();

		// Assert
		assertThat(result, is(result));
	}

	@Test
	public void getPassword() {
		// Arrange
		var value = "SuperPassword";

		// Act
		var config = new HikariConfigWrapper();
		config.setPassword(value);
		var result = config.getPassword();

		// Assert
		assertThat(result, is(result));
	}

	@Test
	public void getConnectionTimeout() {
		// Arrange
		var value = 1337;

		// Act
		var config = new HikariConfigWrapper();
		config.setConnectionTimeout(value);
		var result = config.getConnectionTimeout();

		// Assert
		assertThat(result, is(result));
	}

	@Test
	public void getIdleTimeout() {
		// Arrange
		var value = 11;

		// Act
		var config = new HikariConfigWrapper();
		config.setIdleTimeout(value);
		var result = config.getIdleTimeout();

		// Assert
		assertThat(result, is(result));
	}

	@Test
	public void getMaxLifetime() {
		// Arrange
		var value = 100;

		// Act
		var config = new HikariConfigWrapper();
		config.setMaxLifetime(value);
		var result = config.getMaxLifetime();

		// Assert
		assertThat(result, is(result));
	}

	@Test
	public void getMinimumIdle() {
		// Arrange
		var value = 10;

		// Act
		var config = new HikariConfigWrapper();
		config.setMinimumIdle(value);
		var result = config.getMinimumIdle();

		// Assert
		assertThat(result, is(result));
	}

	@Test
	public void getMaximumPoolSize() {
		// Arrange
		var value = 6;

		// Act
		var config = new HikariConfigWrapper();
		config.setMaximumPoolSize(value);
		var result = config.getMaximumPoolSize();

		// Assert
		assertThat(result, is(result));
	}

	@Test
	public void getPoolName() {
		// Arrange
		var value = "SuperQueryLane";

		// Act
		var config = new HikariConfigWrapper();
		config.setPoolName(value);
		var result = config.getPoolName();

		// Assert
		assertThat(result, is(result));
	}

	@Test
	public void getConnectionInitSql() {
		// Arrange
		var value = "SELECT 1;";

		// Act
		var config = new HikariConfigWrapper();
		config.setConnectionInitSql(value);
		var result = config.getConnectionInitSql();

		// Assert
		assertThat(result, is(result));
	}
}
