package nl.makertim.fm.database;

import nl.makertim.fm.core.BiSet;
import nl.makertim.fm.core.EasyMap;
import nl.makertim.fm.database.oop.constructs.ColumnDefinition;
import nl.makertim.fm.database.oop.constructs.Condition;
import nl.makertim.fm.database.oop.constructs.Mode;
import nl.makertim.fm.database.oop.query.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.function.Executable;

import java.io.File;
import java.util.List;
import java.util.UUID;

import static nl.makertim.fm.core.EasyMap.map;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

public class DatabaseOOPTest {

	public static final File file = new File("./test.sqlite3");
	public static final String SQLITE_JDBC = "jdbc:sqlite:" + file.getPath();
	private Database database;

	@BeforeAll
	@AfterAll
	public static void cleanup() {
		file.delete();
		file.deleteOnExit();
	}

	@BeforeEach
	public void setupDatabase() {
		database = Database.createConnection(SQLITE_JDBC).open();

		database.execute(CreateQuery.create("users", ColumnDefinition.columnTypeless("name")));
		database.execute(InsertQuery.insertMultipleIntoWithColumns("users", 3, "name"),
			EasyMap.map(
				"name1", "tim",
				"name2", "faab",
				"name3", "rebecca"
			));
		database.execute(CreateQuery.create("table_old",
			ColumnDefinition.columnTypeless("id"),
			ColumnDefinition.columnTypeless("value")
		));
	}

	@AfterEach
	public void cleanupAfter() {
		// After
		if (database != null) {
			assertThat(database.close(), is(true));
			database = null;
			file.delete();
		}
	}

	@Test
	public void queryNoDialect() {
		// Arrange
		var select = new SelectQuery(null);

		// Act
		Executable executable = () -> select.build(null);

		// Assert
		assertThrows(RuntimeException.class, executable);
	}

	@Test
	public void basicSelectTest() {
		// Arrange
		var initialStaticValue = 42;
		var select = new SelectQuery(initialStaticValue);

		// Act
		Integer resultStaticValue = database.getSingle(select, resultSet -> resultSet.getInt(1));

		// Assert
		assertThat(resultStaticValue, is(initialStaticValue));
	}

	@Test
	public void inSelectTest() {
		// Arrange
		var initialStaticValue = "tim";
		var select = SelectQuery.selectFrom("users")
			.addCondition(Condition.in("users", "name", "nameA", "nameB"));

		// Act
		String resultStaticValue = database.getSingle(select,
			map("nameA", "tim", "nameB", "nan"),
			resultSet -> resultSet.getString(1));

		// Assert
		assertThat(resultStaticValue, is(initialStaticValue));
	}

	@Test
	public void getSingleTest() {
		// Arrange
		var initialStaticValue = "tim";
		var select = SelectQuery.selectFrom("users")
			.setLimit(1);

		// Act
		String resultStaticValue = database.getConnectionWithResult(connection ->
			database.getSingle(connection, select, resultSet -> resultSet.getString(1))
		);

		// Assert
		assertThat(resultStaticValue, is(initialStaticValue));
	}

	@Test
	public void getSingleWithVarsTest() {
		// Arrange
		var initialStaticValue = "tim";
		var select = SelectQuery.selectFrom("users")
			.addCondition(Condition.equalTo("users", "name", "name"));

		// Act
		String resultStaticValue = database.getConnectionWithResult(connection ->
			database.getSingle(connection, select, map("name", initialStaticValue), resultSet -> resultSet.getString(1))
		);

		// Assert
		assertThat(resultStaticValue, is(initialStaticValue));
	}

	@Test
	public void getMultipleTest() {
		// Arrange
		var select = SelectQuery.selectFrom("users");

		// Act
		List<String> resultStaticValue = database.getMultiple(select, resultSet -> resultSet.getString(1));

		// Assert
		assertThat(resultStaticValue, instanceOf(List.class));
		assertThat(resultStaticValue.size(), is(3));
		assertThat(resultStaticValue.get(0), is("tim"));
		assertThat(resultStaticValue.get(1), is("faab"));
		assertThat(resultStaticValue.get(2), is("rebecca"));
	}

	@Test
	public void getMultipleWhereTest() {
		// Arrange
		var select = SelectQuery
			.selectFrom("users")
			.addCondition(Condition.equalTo("users", "name", "name"));

		// Act
		List<String> resultStaticValue = database.getMultiple(select,
			map("name", "tim"),
			resultSet -> resultSet.getString(1));

		// Assert
		assertThat(resultStaticValue, instanceOf(List.class));
		assertThat(resultStaticValue.size(), is(1));
		assertThat(resultStaticValue.get(0), is("tim"));
	}

	@Test
	public void getMultipleOwnConnectionTest() {
		// Arrange
		var select = SelectQuery.selectFrom("users");

		// Act
		List<String> resultStaticValue = database.getConnectionWithResult(connection ->
			database.getMultiple(connection, select,
				resultSet -> resultSet.getString(1))
		);

		// Assert
		assertThat(resultStaticValue, instanceOf(List.class));
		assertThat(resultStaticValue.size(), is(3));
		assertThat(resultStaticValue.get(0), is("tim"));
		assertThat(resultStaticValue.get(1), is("faab"));
		assertThat(resultStaticValue.get(2), is("rebecca"));
	}

	@Test
	public void getMultipleOwnConnectionWithVariableTest() {
		// Arrange
		var select = SelectQuery.selectFrom("users")
			.addCondition(Condition.like("users", "name", "name"));

		// Act
		List<String> resultStaticValue = database.getConnectionWithResult(connection ->
			database.getMultiple(connection, select,
				map("name", "TIM"),
				resultSet -> resultSet.getString(1))
		);

		// Assert
		assertThat(resultStaticValue, instanceOf(List.class));
		assertThat(resultStaticValue.size(), is(1));
		assertThat(resultStaticValue.get(0), is("tim"));
	}

	@Test
	public void basicUpdateTest() {
		// Arrange
		var update = UpdateQuery.updateSpecificColumns("users", "name");

		// Act
		boolean success = database.execute(update, map().add("name", "Tim"));

		// Assert
		assertTrue(success);
	}

	@Test
	public void basicUpdateNoParamTest() {
		// Arrange
		var update = UpdateQuery.updateSpecificColumns("users", "name");

		// Act
		Executable executable = () -> database.execute(update);

		// Assert
		assertThrows(RuntimeException.class, executable);
	}

	@Test
	public void basicUpdateSelfConnectionTest() {
		// Arrange
		var update = UpdateQuery.updateSpecificColumns("users", "name");

		// Act
		boolean success = database.getConnectionWithResult(connection -> database.execute(connection, update, map().set("name", "Tim")));

		// Assert
		assertTrue(success);
	}

	@Test
	public void basicQuerySelfConnectionTest() {
		// Arrange
		var query = new CreateQuery(UUID.randomUUID().toString()).addColumn(ColumnDefinition.columnTypeless("test"));

		// Act
		boolean success = database.getConnectionWithResult(connection -> database.execute(connection, query));

		// Assert
		assertTrue(success);
	}

	@Test
	public void basicPreparedQuerySelfConnectionTest() {
		// Arrange
		var query = new CreateQuery(UUID.randomUUID().toString()).addColumn(ColumnDefinition.columnTypeless("test"));
		var preparedQuery = query.build(database.getDialect());

		// Act
		boolean success = database.getConnectionWithResult(connection -> database.execute(connection, preparedQuery));

		// Assert
		assertTrue(success);
	}

	@Test
	public void basicInsertTest() {
		// Arrange
		var update = InsertQuery.insertIntoWithColumns("users", "name");

		// Act
		boolean success = database.execute(update, map("name", "mick"));

		// Assert
		assertTrue(success);
	}

	@Test
	public void multipleInsertTest() {
		// Arrange
		var insertQuery = InsertQuery.insertIntoWithColumns("users", "name")
			.setAmount(2);

		// Act
		boolean success = database.execute(insertQuery, map(
			"name1", "mick",
			"name2", "nikki"
		));

		// Assert
		assertTrue(success);
	}

	@Test
	public void testInvalidQuery() {
		// Arrange
		var query = UpdateQuery.updateSpecificColumns("users")
			.setMode(Mode.ROLLBACK);

		// Act
		Executable executable = () -> database.execute(query);

		// Assert
		assertThrows(RuntimeException.class, executable);
	}

	@Test
	public void testDropQuery() {
		// Arrange
		var query = DropQuery.drop("users");

		// Act
		var success = database.execute(query);

		// Assert
		assertTrue(success);
	}

	@Test
	public void testDropIfExistsQuery() {
		// Arrange
		var query = DropQuery.drop("users")
			.ignoreIfNotExists();

		// Act
		var success = database.execute(query);

		// Assert
		assertTrue(success);
	}

	@Test
	public void testDescribeQuery() {
		// Arrange
		var query = DescribeQuery.describe("users");

		// Act
		var createString = database.getSingle(query, resultSet -> resultSet.getString("Create Table"));

		// Assert
		assertThat(createString, containsString("CREATE TABLE"));
	}

	@Test
	public void testDeleteQuery() {
		// Arrange
		var query = DeleteQuery.deleteFirst("users");

		// Act
		var deleted = database.execute(query);

		// Assert
		assertTrue(deleted);
	}

	@Test
	public void testCreateQuery() {
		// Arrange
		var tableName = "testCreateA";
		var query = CreateQuery.create(tableName,
			ColumnDefinition.column("uuid", String.class, 36));

		// Act
		var created = database.execute(query);

		// Assert
		assertTrue(created);

		// After
		assertTrue(database.execute(new DropQuery(tableName)));
	}

	@Test
	public void testCreateIgnoreQuery() {
		// Arrange
		var tableName = "testCreateA";
		var query = CreateQuery.create(tableName,
				ColumnDefinition.column("uuid", String.class, 36))
			.setIgnoreExisting();

		// Act
		var created = database.execute(query);

		// Assert
		assertTrue(created);

		// After
		assertTrue(database.execute(new DropQuery(tableName)));
	}

	@Test
	public void testAlterQuery() {
		// Arrange
		var tableNameOld = "table_old";
		var tableNameNew = "table_new";
		var query = AlterQuery.alterName(tableNameOld, tableNameNew);

		// Act
		var altered = database.execute(query);

		// Assert
		assertTrue(altered);

		// After
		assertTrue(database.execute(AlterQuery.alterName(tableNameNew, tableNameOld)));
	}

	@Test
	public void testAlterColumnQuery() {
		// Arrange
		var columnNameOld = "id";
		var columnNameNew = "external_id";

		// Act
		var query = AlterQuery.renameColumns("table_old", new BiSet<>(columnNameOld, columnNameNew));
		var altered = database.execute(query);

		// Assert
		assertTrue(altered);

		// After
		assertTrue(database.execute(AlterQuery.renameColumns("table_old", new BiSet<>(columnNameNew, columnNameOld))));
	}

	@Test
	public void testAlterColumnSizeQuery() {
		// Arrange
		var column = ColumnDefinition.columnTypeless("id");

		// Act
		var query = AlterQuery.dropColumn("table_old", column.getName());
		var altered = query.build(database.getDialect()).getSQL();

		// Assert
		assertEquals("ALTER TABLE `table_old` DROP COLUMN `id`;", altered);
	}

	@Test
	public void testAlterColumnSize2Query() {
		// Arrange
		var column = ColumnDefinition.columnTypeless("id");

		// Act
		var query = AlterQuery.addColumn("table_old", column);
		var altered = query.build(database.getDialect()).getSQL();

		// Assert
		assertThat(altered, containsString("ALTER TABLE"));
		assertThat(altered, containsString("`table_old`"));
		assertThat(altered, containsString("ADD COLUMN"));
		assertThat(altered, containsString("`id`"));
		assertThat(altered, containsString("NOT NULL"));
	}
}
