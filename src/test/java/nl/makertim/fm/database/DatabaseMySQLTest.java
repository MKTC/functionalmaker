package nl.makertim.fm.database;

import nl.makertim.fm.core.EasyMap;
import nl.makertim.fm.database.oop.constructs.ColumnDefinition;
import nl.makertim.fm.database.oop.constructs.Condition;
import nl.makertim.fm.database.oop.query.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static nl.makertim.fm.core.EasyMap.map;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DatabaseMySQLTest {

	public static final String MYSQL_JDBC = "jdbc:mysql://localhost/";
	private Database database;

	@BeforeEach
	public void setupDatabase() {
		try {
			database = Database.createConnection(MYSQL_JDBC,
				"root", System.getenv("MYSQL_ROOT_PASSWORD")).open();
		} catch (Exception ignore) {
		}
		Assumptions.assumeTrue(database != null, "MySQL database is offline, skipping");

		database.executeRawQuery("CREATE DATABASE IF NOT EXISTS `test`;");
		database.executeRawQuery("USE `test`;");

		cleanupAfter();

		database.execute(CreateQuery.create("users",
			ColumnDefinition.column("id", Integer.class, true, 6),
			ColumnDefinition.column("name", String.class, true, 255)
		));
		database.execute(InsertQuery.insertMultipleIntoWithColumns("users", 3, "name"),
			EasyMap.map(
				"name1", "tim",
				"name2", "faab",
				"name3", "rebecca"
			));
		database.execute(CreateQuery.create("table_old",
			ColumnDefinition.columnTypeless("id")
		));
	}

	@AfterEach
	public void cleanupAfter() {
		// After
		if (database != null) {
			database.execute(DropQuery.drop("users").ignoreIfNotExists());
			database.execute(DropQuery.drop("table_old").ignoreIfNotExists());
		}
	}

	@Test
	public void basicSelectTest() {
		// Arrange
		var initialStaticValue = 42;
		var select = new SelectQuery(initialStaticValue);

		// Act
		Integer resultStaticValue = database.getSingle(select, resultSet -> resultSet.getInt(1));

		// Assert
		assertThat(resultStaticValue, is(initialStaticValue));
	}

	@Test
	public void inSelectTest() {
		// Arrange
		var initialStaticValue = "tim";
		var select = SelectQuery.selectFrom("users", "name")
			.addCondition(Condition.in("users", "name", ":nameA", ":nameB"));

		// Act
		String resultStaticValue = database.getSingle(select,
			map(":nameA", "tim", ":nameB", "nan"),
			resultSet -> resultSet.getString(1));

		// Assert
		assertThat(resultStaticValue, is(initialStaticValue));
	}

	@Test
	public void testDescribeQuery() {
		// Arrange
		var query = DescribeQuery.describe("users");

		// Act
		var createString = database.getSingle(query, resultSet -> resultSet.getString("Create Table"));

		// Assert
		assertThat(createString, containsString("CREATE TABLE"));
	}

	@Test
	public void testDeleteQuery() {
		// Arrange
		var query = DeleteQuery.deleteFirst("users");

		// Act
		var deleted = database.execute(query);

		// Assert
		assertTrue(deleted);
	}

	@Test
	public void testCreateQuery() {
		// Arrange
		var tableName = "testCreateA";
		var query = CreateQuery.create(tableName,
			ColumnDefinition.column("uuid", String.class, 36));

		// Act
		var created = database.execute(query);

		// Assert
		assertTrue(created);

		// After
		assertTrue(database.execute(new DropQuery(tableName)));
	}

	@Test
	public void testAlterQuery() {
		// Arrange
		var tableNameOld = "table_old";
		var tableNameNew = "table_new";
		var query = AlterQuery.alterName(tableNameOld, tableNameNew);

		// Act
		var altered = database.execute(query);

		// Assert
		assertTrue(altered);

		// After
		assertTrue(database.execute(AlterQuery.alterName(tableNameNew, tableNameOld)));
	}
}
