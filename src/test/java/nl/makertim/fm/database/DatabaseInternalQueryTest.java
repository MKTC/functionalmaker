package nl.makertim.fm.database;

import nl.makertim.fm.core.Settings;
import nl.makertim.fm.core.SettingsTest;
import nl.makertim.fm.database.config.HikariConfigWrapper;
import nl.makertim.fm.database.oop.dialects.SQLiteDialect;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.File;
import java.sql.SQLException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

public class DatabaseInternalQueryTest {

	public static final File file = new File("./test.sqlite3");
	public static final String SQLITE_JDBC = "jdbc:sqlite:" + file.getPath();
	private Database database;

	@BeforeAll
	@AfterAll
	public static void cleanup() {
		file.delete();
		file.deleteOnExit();
	}

	@AfterEach
	public void cleanupAfter() {
		// After
		if (database != null) {
			assertThat(database.close(), is(true));
			database = null;
			file.delete();
		}
	}

	@Test
	public void createDatabaseNoConfiguration() {
		// Act
		Executable createConnection = () -> Database.createConnection()
			.setupSettingsManually(settings -> {})
			.open();

		// Assert
		assertThrows(IllegalArgumentException.class, createConnection);
	}

	@Test
	public void testEmptyPreparedStatement() throws SQLException {
		// Act
		Database.PREPARE_NOTHING.accept(null);
	}

	@Test
	public void createDatabase() {
		// Act
		database = Database.createConnection()
			.setupSettingsManually(settings -> settings.setJdbcUrl(SQLITE_JDBC))
			.open();

		// Assert
		assertThat(database, instanceOf(Database.class));
		assertThat(database.internal().getJdbcUrl(), is(SQLITE_JDBC));
	}

	@Test
	public void createDatabaseWithSpecificConfig() {
		// Arrange
		var configClass = HikariConfigWrapper.class;

		// Act
		database = Database.createConnection(configClass)
			.setupSettingsManually(settings -> settings.setJdbcUrl(SQLITE_JDBC))
			.open();

		// Assert
		assertThat(database, instanceOf(Database.class));
		assertThat(database.internal(), instanceOf(configClass));
		assertThat(database.internal().getJdbcUrl(), is(SQLITE_JDBC));
	}

	@Test
	public void createDatabaseJDBC() {
		// Act
		database = Database.createConnection(SQLITE_JDBC)
			.open();

		// Assert
		assertThat(database, instanceOf(Database.class));
		assertThat(database.internal().getJdbcUrl(), is(SQLITE_JDBC));
	}

	@Test
	public void createDatabasePDO() {
		// Act
		database = Database.createConnection(SQLITE_JDBC, null, null)
			.open();

		// Assert
		assertThat(database, instanceOf(Database.class));
		assertThat(database.internal().getJdbcUrl(), is(SQLITE_JDBC));
		assertThat(database.internal().getUsername(), nullValue());
		assertThat(database.internal().getPassword(), nullValue());
	}

	@Test
	public void createDatabasePDOWithSpecificConfig() {
		// Arrange
		var configClass = HikariConfigWrapper.class;

		// Act
		database = Database.createConnection(SQLITE_JDBC, null, null, configClass)
			.open();

		// Assert
		assertThat(database, instanceOf(Database.class));
		assertThat(database.internal(), instanceOf(configClass));
		assertThat(database.internal().getJdbcUrl(), is(SQLITE_JDBC));
		assertThat(database.internal().getUsername(), nullValue());
		assertThat(database.internal().getPassword(), nullValue());
	}

	@Test
	public void createDatabaseSettings() {
		// Arrange
		var settings = new Settings("SQL_test", SettingsTest.readTestFile("/sqllite.json"));

		// Act
		database = Database.createConnection(settings)
			.open();

		// Assert
		assertThat(database, instanceOf(Database.class));
		assertThat(database.internal().getJdbcUrl(), is("jdbc:sqlite:./test.sqlite3")); // from sqllite.json
		assertThat(database.internal().getUsername(), nullValue());
		assertThat(database.internal().getPassword(), nullValue());
	}

	@Test
	public void createDatabaseSettingsWithClass() {
		// Arrange
		var settings = new Settings("SQL_test", SettingsTest.readTestFile("/sqllite.json"));
		var configClass = HikariConfigWrapper.class;

		// Act
		database = Database.createConnection(settings, configClass)
			.open();

		// Assert
		assertThat(database, instanceOf(Database.class));
		assertThat(database.internal(), instanceOf(configClass));
		assertThat(database.internal().getJdbcUrl(), is("jdbc:sqlite:./test.sqlite3")); // from sqllite.json
		assertThat(database.internal().getUsername(), nullValue());
		assertThat(database.internal().getPassword(), nullValue());
	}

	@Test
	public void createDatabaseJDBCInvalidTest() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC);
		database.fallbackTestSQL = "SELECT SELECT";

		// Act
		Executable executable = () -> database.open();

		// Assert
		assertThrows(RuntimeException.class, executable);
	}

	@Test
	public void testDialect() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC)
			.open();

		// Act
		var dialect = database.getDialect();

		// Assert
		assertThat(dialect, instanceOf(SQLiteDialect.class));
	}

	@Test
	public void testExecuteMethod() {
		// Arrange
		var mysticNumber = 1;
		database = Database.createConnection(SQLITE_JDBC)
			.open();

		// Act
		int[] calls = { 0 };
		database.getConnectionExecute(connection -> calls[0] = mysticNumber);

		// Assert
		assertThat(calls[0], is(mysticNumber));
	}

	@Test
	public void testSQLiteX() {
		// Arrange
		var settings = new Settings("SQL_test", SettingsTest.readTestFile("/sqllite-x.json"));

		// Act
		database = Database.createConnection(settings);

		// Assert
		assertTrue(database.internal().isAutoCommit());

		// After
		database = null;
	}

	@Test
	public void testCloseTwice() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC)
			.open();

		// Act
		var closeFirst = database.close();
		var closeSecond = database.close();

		// Assert
		assertTrue(closeFirst);
		assertFalse(closeSecond);

		// After
		database = null;
	}
}
