package nl.makertim.fm.database.model.primarykey;

import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PrimaryKeyTest {

	@Test
	public void createPrimaryKey() {
		// Arrange
		int key = 1337;
		String column = "column";

		// Act
		var pk = new MonoPrimaryKey<>(column, key);

		// Assert
		assertThat(pk.getColumns().length, is(1));
		assertThat(pk.getColumn(0), is(column));
		assertThat(pk.getValue(0), is(key));
		assertThat(pk.getValueAs(0, Integer.class), is(key));
		assertThat(pk.getColumnIndex(column), is(0));
		assertThat(pk.getValue(column), is(key));
		assertThat(pk.getValueAs(column, Object.class), is(key));
		assertThat(pk.getValueAs(column, Integer.class), is(key));
		assertThrows(ClassCastException.class, () -> assertThat(pk.getValueAs(column, String.class), is(key)));
		assertThat(pk.getValueAs("An non existing column", Integer.class), nullValue());
	}

	@Test
	public void createMonoPrimaryKey() {
		// Arrange
		int key = 1337;
		String column = "column";

		// Act
		var pk = new MonoPrimaryKey<>(column, key);

		// Assert
		assertThat(pk.getColumns().length, is(1));
		assertThat(pk.getColumn(0), is(column));
		assertThat(pk.getColumn(), is(column));
		assertArrayEquals(pk.getValues(), new Integer[] { key });
		assertThat(pk.getValue(0), is(key));
		assertThat(pk.getValue(), is(key));
		assertThat(pk.getValueAs(0, Integer.class), is(key));
		assertThat(pk.getColumnIndex(column), is(0));
		assertThat(pk.getValue(column), is(key));
		assertThat(pk.getValueAs(column, Integer.class), is(key));
		assertThrows(ClassCastException.class, () -> pk.getValueAs(column, String.class));
		assertThat(pk.getValueAs("An non existing column", Integer.class), nullValue());
	}

	@Test
	public void createBiPrimaryKey() {
		// Arrange
		int keyA = 1337;
		Date keyB = new Date();
		String columnA = "id";
		String columnB = "timestamp";

		// Act
		var pk = new BiPrimaryKey<>(columnA, columnB, keyA, keyB);

		// Assert
		assertThat(pk.getColumns().length, is(2));
		assertThat(pk.getColumn(0), is(columnA));
		assertThat(pk.getColumn(1), is(columnB));
		assertThat(pk.getColumn0(), is(columnA));
		assertThat(pk.getColumn1(), is(columnB));
		assertArrayEquals(pk.getValues(), new Object[] { keyA, keyB });
		assertThat(pk.getValue(0), is(keyA));
		assertThat(pk.getValue(1), is(keyB));
		assertThat(pk.getValue0(), is(keyA));
		assertThat(pk.getValue1(), is(keyB));
		assertThat(pk.getValueAs(0, Integer.class), is(keyA));
		assertThat(pk.getValueAs(1, Date.class), is(keyB));
		assertThat(pk.getColumnIndex(columnA), is(0));
		assertThat(pk.getColumnIndex(columnB), is(1));
		assertThat(pk.getValue(columnA), is(keyA));
		assertThat(pk.getValue(columnB), is(keyB));
		assertThat(pk.getValueAs(columnA, Integer.class), is(keyA));
		assertThat(pk.getValueAs(columnB, Date.class), is(keyB));
		assertThrows(ClassCastException.class, () -> pk.getValueAs(columnA, String.class));
		assertThat(pk.getValueAs("An non existing column", Integer.class), nullValue());
	}

	@Test
	public void createTriPrimaryKey() {
		// Arrange
		UUID keyA = UUID.randomUUID();
		UUID keyB = UUID.randomUUID();
		UUID keyC = UUID.randomUUID();
		String columnA = "id";
		String columnB = "playerA";
		String columnC = "playerB";

		// Act
		var pk = new TriPrimaryKey<>(columnA, columnB, columnC, keyA, keyB, keyC);

		// Assert
		assertThat(pk.getColumns().length, is(3));
		assertThat(pk.getColumn(0), is(columnA));
		assertThat(pk.getColumn(1), is(columnB));
		assertThat(pk.getColumn(2), is(columnC));
		assertThat(pk.getColumn0(), is(columnA));
		assertThat(pk.getColumn1(), is(columnB));
		assertThat(pk.getColumn2(), is(columnC));
		assertArrayEquals(pk.getValues(), new Object[] { keyA, keyB, keyC });
		assertThat(pk.getValue(0), is(keyA));
		assertThat(pk.getValue(1), is(keyB));
		assertThat(pk.getValue(2), is(keyC));
		assertThat(pk.getValue0(), is(keyA));
		assertThat(pk.getValue1(), is(keyB));
		assertThat(pk.getValue2(), is(keyC));
		assertThat(pk.getValueAs(0, UUID.class), is(keyA));
		assertThat(pk.getValueAs(1, UUID.class), is(keyB));
		assertThat(pk.getValueAs(2, UUID.class), is(keyC));
		assertThat(pk.getColumnIndex(columnA), is(0));
		assertThat(pk.getColumnIndex(columnB), is(1));
		assertThat(pk.getColumnIndex(columnC), is(2));
		assertThat(pk.getValue(columnA), is(keyA));
		assertThat(pk.getValue(columnB), is(keyB));
		assertThat(pk.getValue(columnC), is(keyC));
		assertThat(pk.getValueAs(columnA, UUID.class), is(keyA));
		assertThat(pk.getValueAs(columnB, UUID.class), is(keyB));
		assertThat(pk.getValueAs(columnC, UUID.class), is(keyC));
		assertThrows(ClassCastException.class, () -> pk.getValueAs(columnA, String.class));
		assertThat(pk.getValueAs("An non existing column", Integer.class), nullValue());
	}

	@Test
	public void createQuadPrimaryKey() {
		// Arrange
		UUID keyA = UUID.randomUUID();
		int keyB = 13;
		Date keyC = new java.sql.Date(System.currentTimeMillis());
		String keyD = "NL";
		String columnA = "id";
		String columnB = "field";
		String columnC = "on";
		String columnD = "language";

		// Act
		var pk = new QuadPrimaryKey<>(columnA, columnB, columnC, columnD, keyA, keyB, keyC, keyD);

		// Assert
		assertThat(pk.getColumns().length, is(4));
		assertThat(pk.getColumn(0), is(columnA));
		assertThat(pk.getColumn(1), is(columnB));
		assertThat(pk.getColumn(2), is(columnC));
		assertThat(pk.getColumn(3), is(columnD));
		assertThat(pk.getColumn0(), is(columnA));
		assertThat(pk.getColumn1(), is(columnB));
		assertThat(pk.getColumn2(), is(columnC));
		assertThat(pk.getColumn3(), is(columnD));
		assertArrayEquals(pk.getValues(), new Object[] { keyA, keyB, keyC, keyD });
		assertThat(pk.getValue(0), is(keyA));
		assertThat(pk.getValue(1), is(keyB));
		assertThat(pk.getValue(2), is(keyC));
		assertThat(pk.getValue(3), is(keyD));
		assertThat(pk.getValue0(), is(keyA));
		assertThat(pk.getValue1(), is(keyB));
		assertThat(pk.getValue2(), is(keyC));
		assertThat(pk.getValue3(), is(keyD));
		assertThat(pk.getValueAs(0, UUID.class), is(keyA));
		assertThat(pk.getValueAs(1, Integer.class), is(keyB));
		assertThat(pk.getValueAs(2, Date.class), is(keyC));
		assertThat(pk.getValueAs(3, String.class), is(keyD));
		assertThat(pk.getColumnIndex(columnA), is(0));
		assertThat(pk.getColumnIndex(columnB), is(1));
		assertThat(pk.getColumnIndex(columnC), is(2));
		assertThat(pk.getColumnIndex(columnD), is(3));
		assertThat(pk.getValue(columnA), is(keyA));
		assertThat(pk.getValue(columnB), is(keyB));
		assertThat(pk.getValue(columnC), is(keyC));
		assertThat(pk.getValue(columnD), is(keyD));
		assertThat(pk.getValueAs(columnA, UUID.class), is(keyA));
		assertThat(pk.getValueAs(columnB, Integer.class), is(keyB));
		assertThat(pk.getValueAs(columnC, Date.class), is(keyC));
		assertThat(pk.getValueAs(columnD, String.class), is(keyD));
		assertThrows(ClassCastException.class, () -> pk.getValueAs(columnA, String.class));
		assertThat(pk.getValueAs("An non existing column", Integer.class), nullValue());
	}


	@Test
	public void createMultiSingleTypePrimaryKey() {
		// Arrange
		UUID keyA = UUID.randomUUID();
		UUID keyB = UUID.randomUUID();
		UUID keyC = UUID.randomUUID();
		UUID keyD = UUID.randomUUID();
		UUID keyE = UUID.randomUUID();
		String columnA = "eventId";
		String columnB = "player";
		String columnC = "playerFor";
		String columnD = "assignee";
		String columnE = "assistent";

		// Act
		var pk = new MultiSingleTypePrimaryKey<>(
			new String[] { columnA, columnB, columnC, columnD, columnE },
			new UUID[] { keyA, keyB, keyC, keyD, keyE });

		// Assert
		assertThat(pk.getColumns().length, is(5));
		assertThat(pk.getColumn(0), is(columnA));
		assertThat(pk.getColumn(1), is(columnB));
		assertThat(pk.getColumn(2), is(columnC));
		assertThat(pk.getColumn(3), is(columnD));
		assertThat(pk.getColumn(4), is(columnE));
		assertArrayEquals(pk.getValues(), new Object[] { keyA, keyB, keyC, keyD, keyE });
		assertThat(pk.getValue(0), is(keyA));
		assertThat(pk.getValue(1), is(keyB));
		assertThat(pk.getValue(2), is(keyC));
		assertThat(pk.getValue(3), is(keyD));
		assertThat(pk.getValue(4), is(keyE));
		assertThat(pk.getValueAs(0, UUID.class), is(keyA));
		assertThat(pk.getValueAs(1, UUID.class), is(keyB));
		assertThat(pk.getValueAs(2, UUID.class), is(keyC));
		assertThat(pk.getValueAs(3, UUID.class), is(keyD));
		assertThat(pk.getValueAs(4, UUID.class), is(keyE));
		assertThat(pk.getColumnIndex(columnA), is(0));
		assertThat(pk.getColumnIndex(columnB), is(1));
		assertThat(pk.getColumnIndex(columnC), is(2));
		assertThat(pk.getColumnIndex(columnD), is(3));
		assertThat(pk.getColumnIndex(columnE), is(4));
		assertThat(pk.getValue(columnA), is(keyA));
		assertThat(pk.getValue(columnB), is(keyB));
		assertThat(pk.getValue(columnC), is(keyC));
		assertThat(pk.getValue(columnD), is(keyD));
		assertThat(pk.getValue(columnE), is(keyE));
		assertThat(pk.getValueAs(columnA, UUID.class), is(keyA));
		assertThat(pk.getValueAs(columnB, UUID.class), is(keyB));
		assertThat(pk.getValueAs(columnC, UUID.class), is(keyC));
		assertThat(pk.getValueAs(columnD, UUID.class), is(keyD));
		assertThat(pk.getValueAs(columnE, UUID.class), is(keyE));
		assertThrows(ClassCastException.class, () -> pk.getValueAs(columnA, String.class));
		assertThat(pk.getValueAs("An non existing column", Integer.class), nullValue());
	}

	@Test
	public void createMultiPrimaryKey() {
		// Arrange
		UUID keyA = UUID.randomUUID();
		int keyB = 1;
		String keyC = "WOW";
		float keyD = 1f / 3f;
		double keyE = (1d / 9d) * 10d;
		String columnA = "element";
		String columnB = "thing";
		String columnC = "text";
		String columnD = "left";
		String columnE = "top";

		// Act
		var pk = new MultiPrimaryKey(
			new String[] { columnA, columnB, columnC, columnD, columnE },
			keyA, keyB, keyC, keyD, keyE);

		// Assert
		assertThat(pk.getColumns().length, is(5));
		assertThat(pk.getColumn(-1), nullValue());
		assertThat(pk.getColumn(0), is(columnA));
		assertThat(pk.getColumn(1), is(columnB));
		assertThat(pk.getColumn(2), is(columnC));
		assertThat(pk.getColumn(3), is(columnD));
		assertThat(pk.getColumn(4), is(columnE));
		assertArrayEquals(pk.getValues(), new Object[] { keyA, keyB, keyC, keyD, keyE });
		assertThat(pk.getValue(0), is(keyA));
		assertThat(pk.getValue(1), is(keyB));
		assertThat(pk.getValue(2), is(keyC));
		assertThat(pk.getValue(3), is(keyD));
		assertThat(pk.getValue(4), is(keyE));
		assertThat(pk.getValueAs(0, UUID.class), is(keyA));
		assertThat(pk.getValueAs(1, Integer.class), is(keyB));
		assertThat(pk.getValueAs(2, String.class), is(keyC));
		assertThat(pk.getValueAs(3, Float.class), is(keyD));
		assertThat(pk.getValueAs(4, Double.class), is(keyE));
		assertThat(pk.getColumnIndex(columnA), is(0));
		assertThat(pk.getColumnIndex(columnB), is(1));
		assertThat(pk.getColumnIndex(columnC), is(2));
		assertThat(pk.getColumnIndex(columnD), is(3));
		assertThat(pk.getColumnIndex(columnE), is(4));
		assertThat(pk.getValue("An non existing column"), nullValue());
		assertThat(pk.getValue(columnA), is(keyA));
		assertThat(pk.getValue(columnB), is(keyB));
		assertThat(pk.getValue(columnC), is(keyC));
		assertThat(pk.getValue(columnD), is(keyD));
		assertThat(pk.getValue(columnE), is(keyE));
		assertThat(pk.getValueAs(columnA, UUID.class), is(keyA));
		assertThat(pk.getValueAs(columnB, Number.class), is(keyB));
		assertThat(pk.getValueAs(columnC, String.class), is(keyC));
		assertThat(pk.getValueAs(columnD, Number.class), is(keyD));
		assertThat(pk.getValueAs(columnE, Number.class), is(keyE));
		assertThrows(ClassCastException.class, () -> pk.getValueAs(columnA, String.class));
		assertThat(pk.getValueAs("An non existing column", Integer.class), nullValue());
	}
}
