package nl.makertim.fm.database;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DatabasePlainTest {

	public static final File file = new File("./test.sqlite3");
	public static final String SQLITE_JDBC = "jdbc:sqlite:" + file.getPath();
	private Database database;

	@BeforeAll
	@AfterAll
	public static void cleanup() {
		file.delete();
		file.deleteOnExit();
	}

	@AfterEach
	public void cleanupAfter() {
		// After
		if (database != null) {
			assertThat(database.close(), is(true));
			database = null;
			file.delete();
		}
	}

	private void fillDatabaseWithData() {
		assertTrue(database.executeRawQuery("CREATE TABLE `users` (`name`);"));
		assertTrue(database.executeRawQuery("INSERT INTO `users` VALUES ('tim');"));
		assertTrue(database.executeRawQuery("INSERT INTO `users` VALUES ('faab');"));
		assertTrue(database.executeRawQuery("INSERT INTO `users` VALUES ('rebecca');"));
	}

	@Test
	public void resultToList() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC)
			.open();
		fillDatabaseWithData();

		// Act
		var list = database.resultToList("SELECT * FROM `users`",
			resultSet -> resultSet.getString(1));

		// Assert
		assertThat(list, instanceOf(List.class));
		assertThat(list.size(), is(3));
		assertThat(list.get(0), is("tim"));
		assertThat(list.get(1), is("faab"));
		assertThat(list.get(2), is("rebecca"));
	}

	@Test
	public void resultToListPrepare() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC, null, null)
			.open();
		fillDatabaseWithData();

		// Act
		var list = database.resultToList("SELECT * FROM `users` WHERE `name` = ?",
			preparedStatement -> preparedStatement.setString(1, "tim"),
			resultSet -> resultSet.getString(1));

		// Assert
		assertThat(list, instanceOf(List.class));
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("tim"));
	}

	@Test
	public void resultToListPrepareWithArrayList() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC, null, null)
			.open();
		fillDatabaseWithData();

		// Act
		var list = database.resultToList("SELECT * FROM `users` WHERE `name` = ?",
			preparedStatement -> preparedStatement.setString(1, "tim"),
			resultSet -> resultSet.getString(1),
			ArrayList::new);

		// Assert
		assertThat(list, instanceOf(ArrayList.class));
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("tim"));
	}

	@Test
	public void resultToSingle() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC)
			.open();
		fillDatabaseWithData();

		// Act
		var tim = database.executeSingleResultQuery("SELECT * FROM `users` LIMIT 1",
			resultSet -> resultSet.getString(1));

		// Assert
		assertThat(tim, instanceOf(String.class));
		assertThat(tim, is("tim"));
	}

	@Test
	public void resultToSingleInvalid() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC)
			.open();
		database.executeRawQuery("CREATE TABLE `users` (`name`);");

		// Act
		var nan = database.executeSingleResultQuery("SELECT * FROM `users`",
			resultSet -> resultSet.getString(1));

		// Assert
		assertThat(nan, nullValue());
	}

	@Test
	public void resultToSingleWithPrepare() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC, null, null)
			.open();
		fillDatabaseWithData();

		// Act
		var tim = database.executeSingleResultQuery("SELECT * FROM `users` WHERE `name` = ?",
			preparedStatement -> preparedStatement.setString(1, "tim"),
			resultSet -> resultSet.getString(1));

		// Assert
		assertThat(tim, instanceOf(String.class));
		assertThat(tim, is("tim"));
	}

	@Test
	public void executeResultQuery() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC, null, null)
			.open();
		fillDatabaseWithData();

		// Act
		var tim = database.executeResultQuery("SELECT * FROM `users`",
			resultSet -> {
				resultSet.next();
				return resultSet.getString(1);
			});

		// Assert
		assertThat(tim, instanceOf(String.class));
		assertThat(tim, is("tim"));
	}

	@Test
	public void executeResultQueryWithPrepare() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC, null, null)
			.open();
		fillDatabaseWithData();

		// Act
		var tim = database.executeResultQuery("SELECT * FROM `users` WHERE `name` = ?",
			preparedStatement -> preparedStatement.setString(1, "rebecca"),
			resultSet -> {
				resultSet.next();
				return resultSet.getString(1);
			});

		// Assert
		assertThat(tim, instanceOf(String.class));
		assertThat(tim, is("rebecca"));
	}

	@Test
	public void executeQueryInvalid() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC, null, null)
			.open();
		database.executeRawQuery("CREATE TABLE `users` (`name`);");

		// Act
		var succeeded = database.executeRawQuery("CREATE TABLE `users` (`name`);");

		// Assert
		assertFalse(succeeded);
	}

	@Test
	public void executeQueryWithPrepare() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC, null, null)
			.open();
		database.executeRawQuery("CREATE TABLE `users` (`name`);");

		// Act
		var succeeded = database.executeRawQuery("INSERT INTO `users` VALUES (?);",
			preparedStatement -> preparedStatement.setString(1, "sampie"));

		// Assert
		assertTrue(succeeded);
		var list = database.resultToList("SELECT * FROM `users`",
			resultSet -> resultSet.getString(1));
		assertThat(list, instanceOf(List.class));
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("sampie"));
	}

	@Test
	public void testFillObjectPreparedStatement() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC, null, null)
			.open();
		fillDatabaseWithData();

		// Act
		var list = database.resultToList("SELECT * FROM `users` WHERE `name` = ?",
			Database.fillObject(new Object[] { "tim" }),
			resultSet -> resultSet.getString(1));

		// Assert
		assertThat(list, instanceOf(List.class));
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("tim"));
	}

	@Test
	public void testFillObjectPreparedStatementWithoutData() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC, null, null)
			.open();
		fillDatabaseWithData();

		// Act
		var list = database.resultToList("SELECT * FROM `users` WHERE `name` = ?",
			Database.fillObject(),
			resultSet -> resultSet.getString(1));

		// Assert
		assertThat(list, instanceOf(List.class));
		assertThat(list.size(), is(0));
	}

	@Test
	public void testFillObjectPreparedStatementWithStrangeData() {
		// Arrange
		database = Database.createConnection(SQLITE_JDBC, null, null)
			.open();
		fillDatabaseWithData();

		// Act
		var list = database.resultToList("SELECT * FROM `users` WHERE `name` = ?",
			Database.fillObject(new Object[] {
				new Object() {
					public String toString() {
						throw new RuntimeException("break this toString");
					}
				}
			}),
			resultSet -> resultSet.getString(1));

		// Assert
		assertThat(list, instanceOf(List.class));
		assertThat(list.size(), is(0));
	}
}
