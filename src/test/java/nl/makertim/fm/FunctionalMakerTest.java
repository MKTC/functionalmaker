package nl.makertim.fm;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class FunctionalMakerTest {

	@Test
	public void testVersionIsNotNull() {
		// Arrange
		String expectedVersion = "$VERSION";

		// Act
		String actualVersion = FunctionalMaker.version;

		// Assert
		assertThat("FunctionalMaker Version", actualVersion, is(expectedVersion));
	}

	@Test
	public void testReleaseDateIsNotNull() {
		// Arrange
		String expectedDate = "$DATE";

		// Act
		String actualDate = FunctionalMaker.releaseDate;

		// Assert
		assertThat("FunctionalMaker Version", actualDate, is(expectedDate));
	}

	@Test
	public void testVersionIsCustomPropertyVersion() {
		// Arrange
		String expectedVersion = "$VERSION";

		// Act
		String actualVersion = FunctionalMaker.getProperty("version");

		// Assert
		assertThat("FunctionalMaker Custom Property Version", actualVersion, is(expectedVersion));
	}

	@Test
	public void testVersionIsCustomPropertyDate() {
		// Arrange
		String expectedDate = "$DATE";

		// Act
		String actualDate = FunctionalMaker.getProperty("release");

		// Assert
		assertThat("FunctionalMaker Custom Property Version", actualDate, is(expectedDate));
	}

	@Test
	public void testOverridePropertyTest(){
		// Arrange
		String newPropertyKey = "MAKERTIM~TEST~KEY";
		String newPropertyValue = "963.01478";

		// Act
		FunctionalMaker.overrideProperty(newPropertyKey, newPropertyValue);
		String actualValue = FunctionalMaker.getProperty(newPropertyKey);

		// Assert
		assertThat(actualValue, is(newPropertyValue));
	}
}
